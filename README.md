
### Dependencies

- Java 7+
- NodeJS LTS
- Mysql 8.0

### Building frontend assets

Browse to frontend folder

npm install 

### Build for development environment 

npm run build-dev 

### Build for qa environment 

npm run build-qa

### Build for production environment (https://itpde.davidjones.com.au:8080/dj/)

npm run build-prod

### Building the backend

Place contents of "frontend/dist/prod" to "backend/main/resources/static"

mvn clean install

Browse target folder 

Run command "java -jar ITPDE.jar"