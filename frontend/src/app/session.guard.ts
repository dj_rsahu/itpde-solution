import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
  ActivatedRoute
} from "@angular/router";
import { Observable } from "rxjs";
import { map, catchError } from "rxjs/operators";

import { of } from "rxjs";

import { MatSnackBar } from "@angular/material";
import { FeaturesService } from "./modules/features/features.service";

@Injectable({
  providedIn: "root"
})
export class SessionGuard implements CanActivate {
  mode: any;
  stepIndex: any;
  projectId;
  constructor(
    private _featuresService: FeaturesService  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let params = next.params;
    this.projectId = params["id"]
      ? params["id"]
      : localStorage.getItem("projectId");

    this.mode = params["mode"] ? params["mode"] : "new";
    this.stepIndex = params["step"];

    if (this.mode === "edit") {
      return this._featuresService
        .setEstimationEditMode(this.projectId, 0)
        .pipe(
          map(() => {
            return true;
          }),
          catchError((err) => {
            return of(false);
          })
        );
    } else {
      return true;
    }
  }
}
