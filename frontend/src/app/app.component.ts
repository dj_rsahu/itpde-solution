import {
  Component,
  OnInit,
  HostListener,
  HostBinding
} from "@angular/core";
import { OverlayContainer } from "@angular/cdk/overlay";
import { AuthenticationService } from "./shared/services/authentication/authentication.service";
import { PageService } from "./shared/services/pages/pages.service";
import { StorageService } from "./shared/services/storage/storage.service";
import { Router } from "@angular/router";
import { AppStoreService } from './app-store.service';

@Component({
  selector: "dj-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  isAuthenticated: boolean;
  title = "SDCUI";
  authenticated: boolean = false;
  route: any;
  sideNavMode = "side";
  sideNavOpen = false;
  sideNavPreviousState = true;
  sideNavText = false;
  chosenTheme = "default-theme";
  @HostBinding("class") componentCssClass;
  isMobile: boolean;
  hash: string;
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    if (event.target.innerWidth <= 769) {
      this.appStoreService.setMobile(true);
      this.appStoreService.setSideNavMode('over');
      this.appStoreService.setSideNavOpen(false);
    }
    else {
      this.appStoreService.setMobile(false);
      this.appStoreService.setSideNavMode('side');
      this.appStoreService.setSideNavOpen(true);
    }
  }

  constructor(
    private appStoreService: AppStoreService,
    private authenticationService: AuthenticationService,
    private pageService: PageService,
    public overlayContainer: OverlayContainer,
    private router: Router,
    private storageService: StorageService
  ) {
    // this.oktaAuth.handleAuthentication();
  }

  ngOnInit() {
    if (window.innerWidth < 769) {
      this.appStoreService.setMobile(true);
      this.appStoreService.setSideNavMode('over');
      this.appStoreService.setSideNavOpen(false);
    }
    else {
      this.appStoreService.setMobile(false);
      this.appStoreService.setSideNavMode('side');
      this.appStoreService.setSideNavOpen(true);
    }
  }

}
