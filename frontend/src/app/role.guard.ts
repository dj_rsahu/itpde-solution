import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { MatSnackBar } from "@angular/material";

@Injectable({
  providedIn: "root"
})
export class RoleGuard implements CanActivate {
  constructor(private router: Router, private snackBar: MatSnackBar) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (JSON.parse(localStorage.getItem("user"))["role"] === "Admin") {
      return true;
    }
    // navigate to not found page
    this.router.navigate(["/pagenotfound"]);
    this.snackBar.open("Not authorised to access this page", "DISMISS", {
      duration: 5000,
      verticalPosition: "top", // 'top' | 'bottom'
      horizontalPosition: "center",
      panelClass: ["error-snackbar"]
    });
    // you can save redirect url so after authing we can move them back to the page they requested
    return false;
  }
}
