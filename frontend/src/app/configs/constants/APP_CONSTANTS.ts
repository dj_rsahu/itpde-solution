// STORAGE CONSTANTS
export const STORAGE_CONSTANTS = {
  LOGGED_USER: "user"
};
// USER MANAGEMENT
export const USER_MANAGEMENT_CONSTANTS = {
  USER_STATUS: {
    active: "Active",
    inactive: "InActive"
  }
};

export const ESTIMATE_CONSTANTS = {
  internal: "Internal",
  external: "External",
  inprogress: "Pending TA",
  complete: "Approved",
  draft: "Draft",
  known: "Known",
  unKnown: "UnKnown",
  other: "Others",
  pending1: "Pending OA"
 
};

export const roles={
  OverallApprover:"Pending OA",
  TestApprover:"Pending TA"
}

export const PORTAL_ROLES = {
  admin: "Admin",
  testApprover: "Test Approver",
  overallApprover:"Overall Approver",
  user: "User",
  noAccess:"No Access"
};
export const CREATE_ESTIMATE = {
  PROJECT_STATUS: {
    development: "Development",
    testing: "Testing",
    uat: "UAT",
    production: "Production",
    other: "Others"
  },
  ESTIMATE_STATUS: {
    inProgress: "In Progress",
    draft: "Draft"
  }
};
