import {
  Component,
  OnInit,
  OnDestroy,
  Inject,
  Output,
  EventEmitter
} from "@angular/core";
import { AuthenticationService } from "src/app/shared/services/authentication/authentication.service";
import { DOCUMENT } from "@angular/common";
import { StorageService } from "src/app/shared/services/storage/storage.service";
import { PageService } from "src/app/shared/services/pages/pages.service";
import { AppStoreService } from "src/app/app-store.service";
import { Router } from "@angular/router";
import { MatDialogConfig, MatDialog, MatSnackBar } from "@angular/material";
import { ConfirmDialogComponent } from "../confirm-dialog/confirm-dialog.component";
import { Compiler } from "@angular/core";

@Component({
  selector: "dj-header-component",
  templateUrl: "header.component.html"
})
export class HeaderComponent implements OnInit, OnDestroy {
  currentUser: any;
  applicationLogo: any;
  elem;
  fullScreen = false;
  oktaUser: any;
  isShowTedUsers: boolean;
  isShowOktaUsers: boolean;
  sideNavOpen;
  isHamburgerMenu: boolean;
  constructor(
    @Inject(DOCUMENT) private document: any,
    private loginService: AuthenticationService,
    private storageService: StorageService,
    private pageService: PageService,
    private router: Router,
    private dialog: MatDialog,
    private appStoreService: AppStoreService,
    private _compiler: Compiler
  ) {}

  ngOnInit() {
    this.appStoreService.sideNavOpen.subscribe(res => {
      this.sideNavOpen = res;
    });
    this.appStoreService.user.subscribe(res => {
      this.currentUser = res;
    });
    this.appStoreService.isHamburgerMenu.subscribe(res => {
      this.isHamburgerMenu = res;
    });
  }

  fullscreen() {
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
    }
    this.fullScreen = true;
  }

  /* Close fullscreen */
  exitFullscreen() {
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
    this.fullScreen = false;
  }
  ngOnDestroy() {
    // this.currentUser = null;
  }
  sideNavEmit() {
    this.appStoreService.setSideNavOpen(!this.sideNavOpen);
  }
  logout() {
    if (this.currentUser) {
      this.pageService.navigateToPage("login");
    }
    this.storageService.clearAll();
    this._compiler.clearCache();
  }
  navigation() {
    this.router.navigate(["/projects"]);
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: ["Cancel", "Logout", "Logout Confirmation","Are you sure you want to logout?"]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.logout();
      } else {
      }
    });
  }
}
