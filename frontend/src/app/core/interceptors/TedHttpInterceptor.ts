import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
  HttpResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { SpinnerService } from "src/app/shared/services/spinner/spinner.service";
import { finalize, catchError, retry, tap } from "rxjs/operators";
import { MatSnackBar } from "@angular/material";

@Injectable()
export class TedHttpInterceptor implements HttpInterceptor {
  constructor(
    public spinnerService: SpinnerService,
    public snackBar: MatSnackBar
  ) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (localStorage.getItem("user")) {
      req = req.clone({
        setHeaders: {
          Authorization: JSON.parse(localStorage.getItem("user")).token,
          "Content-Type": "application/json"
        }
      });
    }

    this.spinnerService.showLoadingSpinner();
    return next.handle(req).pipe(
      // tap(evt => {
      //   if (evt instanceof HttpResponse) {
      //     if (evt.body && evt.body.success)
      //       this.snackBar.open(evt.body.success.message, "DISMISS", {
      //         duration: 10000,
      //         panelClass: ["snackbar-error"]
      //       });
      //   }
      // }),
      finalize(() =>
        setTimeout(() => {
          this.spinnerService.hideLoadingSpinner();
        }, 300)
      ),
      catchError((error: HttpErrorResponse) => {
        console.log("http error:" + JSON.stringify(error));
        this.snackBar.open(
          error.error ? error.error.message : error.message,
          "DISMISS",
          {
            duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
          }
        );
        return throwError(error);
      })
    );
  }
}
