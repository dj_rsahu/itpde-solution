import { IHttpOperations } from "./IHttpOperations";
import { Observable } from "rxjs";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { catchError, tap } from "rxjs/operators";
export abstract class ApiService<T, ID> implements IHttpOperations<T, ID> {
  baseUrl: string = environment.baseUrl;
  constructor(protected _http: HttpClient) {}
  save(uriPath: string, t: T): Observable<T> {
    return this._http.post<T>(this.baseUrl + uriPath, t);
  }
  saveAllWithPram(uriPath: string, param:HttpParams, t: T) {
    return this._http.post<T[]>(this.baseUrl + uriPath ,t, {
      params: param
    });
  }
  saveList(uriPath: string, t: T[]): Observable<T> {
    return this._http.post<T>(this.baseUrl + uriPath, t);
  }
  updateList(uriPath: string, t: T[]): Observable<T> {
    return this._http.put<T>(this.baseUrl + uriPath, t);
  }
  update(uriPath: string, t: T): Observable<T> {
    return this._http.put<T>(this.baseUrl + uriPath, t);
  }
  findOne(uriPath: string, id: ID): Observable<T> {
    return this._http.get<T>(this.baseUrl + uriPath + "/" + id);
  }
  find(uriPath: string, id: string): Observable<T> {

    return this._http.get<T>(this.baseUrl + uriPath + "/" + id);
  }
  
  // findAllWithPram(uriPath: string, id) {
  //   return this._http.get<T[]>(this.baseUrl + uriPath + "/" + id);
  // }
  findAllWithPram(uriPath: string, param:HttpParams) {
    return this._http.get<T[]>(this.baseUrl + uriPath , {
      params: param
    });
  }
  findList(uriPath: string, key: string) {
    let param = new HttpParams().append("search", key);
    return this._http.get<T[]>(this.baseUrl + uriPath, {
      params: param
    });
  }
  findNoParams(uriPath: string): Observable<T> {
    return this._http.get<T>(this.baseUrl + uriPath);
  }
  findWithParams(uriPath: string, limit, nextUrl): Observable<T> {
    let paramObj = {
      limit,
      nextUrl
    };
    let param = new HttpParams()
      .append("limit", limit)
      .append("nextUrl", nextUrl);
    return this._http.get<T>(this.baseUrl + uriPath, {
      params: param
    });
  }
  findWithParamsList(uriPath: string, limit, nextUrl): Observable<T[]> {
    let paramObj = {
      limit,
      nextUrl
    };
    let param = new HttpParams()
      .append("startDate", limit)
      .append("endDate", nextUrl);
    return this._http.get<T[]>(this.baseUrl + uriPath, {
      params: param
    });
  }
  findWithParamsListPage(
    uriPath: string,
    startDate,
    endDate,
    page
  ): Observable<T[]> {
    let param = new HttpParams()
      .append("startDate", startDate)
      .append("endDate", endDate)
      .append("pageName", page);
    return this._http.get<T[]>(this.baseUrl + uriPath, {
      params: param
    });
  }

  findAll(uriPath: string): Observable<T[]> {
    return this._http.get<T[]>(this.baseUrl + uriPath, {});
  }
  delete(uriPath: string, id: ID): Observable<any> {
    return this._http.delete<T>(this.baseUrl + uriPath + "/" + id, {});
  }
  deletemultiple(uriPath: string, item: any): Observable<any> {
    return this._http.post<T>(this.baseUrl + uriPath, item, {});
  }
  deleteByParam(uriPath: string, param): Observable<any> {
    return this._http.delete<T>(this.baseUrl + uriPath,  {
      params: param
    });
  }
}
