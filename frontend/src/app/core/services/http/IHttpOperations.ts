import { Observable } from "rxjs";

export interface IHttpOperations<T, ID> {
  save(uriPath: string, t: T): Observable<T>;
  update(uriPath: string, t: T): Observable<T>;
  findOne(uriPath: string, id: ID): Observable<T>;
  findAll(uriPath: string): Observable<T[]>;
  delete(uriPath: string, id: ID): Observable<any>;
}
