import { NgModule } from "@angular/core";
import { HeaderComponent } from "./layout/header/header.component";
import { FooterComponent } from "./layout/footer/footer.component";
import { MatMenuModule } from "@angular/material/menu";
import { RouterModule } from "@angular/router";
import { MatButtonModule } from "@angular/material/button";
import { CommonModule } from "@angular/common";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { TedHttpInterceptor } from "./interceptors/TedHttpInterceptor";
import { MatIconModule, MatSnackBarModule, MatDialogModule } from "@angular/material";
import { ConfirmDialogComponent } from './layout/confirm-dialog/confirm-dialog.component';
@NgModule({
  imports: [
    RouterModule,
    MatMenuModule,
    MatButtonModule,
    CommonModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule
  ],
  exports: [HeaderComponent, FooterComponent],
  declarations: [HeaderComponent, FooterComponent, ConfirmDialogComponent],
  entryComponents:[ConfirmDialogComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TedHttpInterceptor, multi: true }
  ]
})
export class CoreModule {}
