import { Injectable, isDevMode } from "@angular/core";
import { LocalStorage } from "./interfaces/local-storage";
import { SessionStorage } from "./interfaces/session-storage";

@Injectable({
  providedIn: "root"
})
export class UtilsService implements LocalStorage, SessionStorage {
  constructor() {}
  // Local Storage
  setLocal(key: string, item: any) {
    localStorage.setItem(key, JSON.stringify(item));
  }
  getLocal(key: string) {
    if (localStorage.getItem(key)) {
      return JSON.parse(localStorage.getItem(key));
    } else {
      return undefined;
    }
  }
  removeLocal(key: string) {
    if (localStorage.getItem(key)) {
      localStorage.removeItem(key);
    } else {
    }
  }
  clearLocal() {
    localStorage.clear();
  }
  // Session Storage
  setSession(key: string, item: any) {
    sessionStorage.setItem(key, JSON.stringify(item));
  }
  getSession(key: string) {
    if (sessionStorage.getItem(key)) {
      return JSON.parse(sessionStorage.getItem(key));
    } else {
      return undefined;
    }
  }
  removeSession(key: string) {
    if (sessionStorage.getItem(key)) {
      sessionStorage.removeItem(key);
    } else {
    }
  }
  clearSession() {
    sessionStorage.clear();
  }

  // Clone
  clone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  // log
  log(msg: any, item) {
    if (!isDevMode()) {
      console.log(new Date() + ": " + JSON.stringify(msg) + ">>>", item);
    }
  }
  currencyFormatter(number) {
    var formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD"
    });

    return formatter.format(number);
  }

  compareObjects(o1, o2) {
    for (var p in o1) {
      if (o1.hasOwnProperty(p)) {
        if (o1[p] !== o2[p]) {
          return false;
        }
      }
    }
    for (var p in o2) {
      if (o2.hasOwnProperty(p)) {
        if (o1[p] !== o2[p]) {
          return false;
        }
      }
    }
    return true;
  }
}
