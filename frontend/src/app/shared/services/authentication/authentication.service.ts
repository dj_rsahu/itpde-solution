import { Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';
import { STORAGE_CONSTANTS } from 'src/app/configs/constants/APP_CONSTANTS';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment";

@Injectable()
export class AuthenticationService {
  currentUser: any;
  private subject = new Subject<any>();
  constructor(private storageService: StorageService, private http: HttpClient) {}

  loggedUser(user: any) {
    this.currentUser = user;
  }
  getLoggedUser() {
    this.currentUser = this.currentUser
      ? this.currentUser
      : this.storageService.getItem(STORAGE_CONSTANTS.LOGGED_USER);

    return this.currentUser;
  }
  setLoginStatus(status: boolean) {
    this.subject.next(status);
  }
  getLoginStatus(): Observable<any> {
    return this.subject.asObservable();
  }
  getLoggedOktaUser(oktaUser) {
    return this.http.post(environment.baseUrl + 'auth/authOktaUser', oktaUser);
  }
}
