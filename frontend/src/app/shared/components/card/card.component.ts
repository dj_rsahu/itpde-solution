import {
  Component,
  OnInit,
  Input,
  TemplateRef,
  Output,
  EventEmitter
} from "@angular/core";

@Component({
  selector: "dj-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.scss"]
})
export class CardComponent implements OnInit {
  @Input() cardContent: any;
  @Input() templateRef: TemplateRef<any>;
  @Output() saveContent = new EventEmitter();
  @Output() clearContent = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  save() {
    this.saveContent.emit();
  }
  clear() {
    this.clearContent.emit();
  }
}
