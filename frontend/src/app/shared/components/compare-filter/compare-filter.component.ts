import { Component, OnInit } from "@angular/core";

@Component({
  selector: "dj-compare-filter",
  templateUrl: "./compare-filter.component.html",
  styleUrls: ["./compare-filter.component.scss"]
})
export class CompareFilterComponent implements OnInit {
  compareList = [];

  constructor() {}

  ngOnInit() {}

  compareAdd(event) {
    if (event.checked) {
      this.compareList.push({ isAdd: true });
    } else {
      this.compareList = [];
    }
  }

  addFilter(i) {
    console.log(i)
    this.compareList[i].isAdd = false;
    this.compareList.push({ isAdd: true });
  }
  removeFilter(i) {
    this.compareList.splice(i, 1);
  }
}
