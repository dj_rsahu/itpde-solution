import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompareFilterComponent } from './compare-filter.component';

describe('CompareFilterComponent', () => {
  let component: CompareFilterComponent;
  let fixture: ComponentFixture<CompareFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompareFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompareFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
