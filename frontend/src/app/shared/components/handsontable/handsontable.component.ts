import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from "@angular/core";
import Handsontable from "handsontable";

@Component({
  selector: "dj-handsontable",
  templateUrl: "./handsontable.component.html",
  styleUrls: ["./handsontable.component.scss"]
})
export class HandsontableComponent implements OnInit {
  @Input("data") data;
  @Input("columns") columns;
  @Input("colHeaders") colHeaders;
  @Input("colWidths") colWidths;
  @Input("settings") settings;
  @Output() afterChange = new EventEmitter();
  @Output() beforeChange = new EventEmitter();
  @Output() afterBeginEditing = new EventEmitter();
  @Output() beforeOnCellMouseDown = new EventEmitter();
  @Output() afterOnCellMouseDown = new EventEmitter();
  @Output() beforeCreateRow = new EventEmitter();
  @Output() afterCreateRow = new EventEmitter();
  @Output() beforeOnCellContextMenu = new EventEmitter();
  @Output() afterInit = new EventEmitter();
  @Output() afterRender = new EventEmitter();
  @Output() afterColumnSort = new EventEmitter();
  @Output() beforeColumnSort = new EventEmitter();
  @Output() afterFilter = new EventEmitter();
  @Output() beforeFilter = new EventEmitter();
  @Output() afterRemoveRow = new EventEmitter();
  @ViewChild("hotref") hotref;
  instance;
  defaultSettings: Handsontable.GridSettings = {
    licenseKey: "3a0f3-a0387-c8981-44d2d-82041",
    afterChange: (changes, src) => {
      this.handleAfterChange(changes, src);
    },
    afterRender: () => {
      this.handleAfterRender();
    },
    beforeChange: (changes, src) => {
      this.handleBeforeChange(changes, src);
    },
    afterBeginEditing: (row, column) => {
      this.handleAfterBeginEditing(row, column);
    },
    beforeOnCellMouseDown: (event, cellIndex, td) => {
      this.handleBeforeOnCellMouseDown(event, cellIndex, td);
    },
    afterOnCellMouseDown: (event, cellIndex) => {
      this.handleAfterOnCellMouseDown(event, cellIndex);
    },
    afterCreateRow: (index, amt, source) => {
      this.handleAfterCreateRow(index, amt, source);
    },
    beforeCreateRow: (index, amt, source) => {
      this.handleBeforeCreateRow(index, amt, source);
    },
    beforeOnCellContextMenu: (Event, CellCoords) => {
      this.handleBeforeOnCellContextMenu(Event, CellCoords);
    },
    afterInit: () => {
      this.handleAfterInit();
    },
    afterColumnSort: (currentSortConfig, destinationSortConfigs) => {
      this.handleAfterColumnSort(currentSortConfig, destinationSortConfigs);
    },
    beforeColumnSort: (currentSortConfig, destinationSortConfigs) => {
      this.handleBeforeColumnSort(currentSortConfig, destinationSortConfigs);
    },
    afterFilter: conditionsStack => {
      this.handleAfterFilter(conditionsStack);
    },
    beforeFilter: conditionsStack => {
      this.handleBeforeFilter(conditionsStack);
    },
    afterRemoveRow: () => {
      this.handleAfterRemoveRow();
    }
  };

  constructor() {}

  ngOnInit() {}
  handleAfterChange(changes, source) {
    this.afterChange.emit({ changes, source });
  }
  handleBeforeChange(changes, source) {
    this.beforeChange.emit({ changes, source });
  }
  handleAfterBeginEditing(row, column) {
    this.afterBeginEditing.emit({ row, column });
  }
  getSettings() {
    return Object.assign(this.defaultSettings, this.settings);
  }
  getInstance() {
    return this.hotref.hotInstance.getInstance();
  }
  handleBeforeOnCellMouseDown(event, cellIndex, td) {
    this.beforeOnCellMouseDown.emit(cellIndex);
  }
  handleAfterOnCellMouseDown(event, cellIndex) {
    this.afterOnCellMouseDown.emit(cellIndex);
  }
  handleAfterCreateRow(index, amt, source) {
    this.afterCreateRow.emit({ index, amt, source });
  }
  handleBeforeCreateRow(index, amt, source) {
    this.beforeCreateRow.emit({ index, amt, source });
  }
  handleBeforeOnCellContextMenu(event, CellCoords) {
    this.beforeOnCellContextMenu.emit({ event, CellCoords });
  }
  handleAfterInit() {
    this.afterInit.emit();
  }
  handleAfterRender() {
    this.afterRender.emit();
  }
  handleAfterColumnSort(currentSortConfig, destinationSortConfigs) {
    this.afterColumnSort.emit({ currentSortConfig, destinationSortConfigs });
  }
  handleBeforeColumnSort(currentSortConfig, destinationSortConfigs) {
    this.beforeColumnSort.emit({ currentSortConfig, destinationSortConfigs });
  }
  handleAfterFilter(conditionsStack) {
    this.afterFilter.emit({ conditionsStack });
  }
  handleBeforeFilter(conditionsStack) {
    this.beforeFilter.emit({ conditionsStack });
  }
  handleAfterRemoveRow(){
    this.afterRemoveRow.emit({});
  }
}
