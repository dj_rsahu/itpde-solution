import { Component, OnInit, ViewChild, AfterContentChecked } from "@angular/core";
import * as moment from "moment";

@Component({
  selector: "dj-date-filter",
  templateUrl: "./date-filter.component.html",
  styleUrls: ["./date-filter.component.scss"]
})
export class DateFilterComponent implements OnInit, AfterContentChecked {
  @ViewChild("pickerStart") pickerStart;
  @ViewChild("pickerEnd") pickerEnd;
  startdate;
  enddate;
  range = "";
  today= new Date;

  constructor() {}

  ngOnInit() {
    this.range = moment(this.startdate).format("DD-MMM-YYYY") +
    " -- " +
    moment(this.enddate).format("DD-MMM-YYYY");
  }

  openStartDatepicker() {
    this.pickerStart.open();
  }
  
  openEndDatepicker() {
    this.pickerEnd.open();
    
  }
  ngAfterContentChecked(){
    this.range = moment(this.startdate).format("DD-MMM-YYYY") +
    " -- " +
    moment(this.enddate).format("DD-MMM-YYYY");
  }

}
