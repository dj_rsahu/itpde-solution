import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthenticationService } from "./services/authentication/authentication.service";
import { PageService } from "./services/pages/pages.service";
import { StorageService } from "./services/storage/storage.service";
import { SpinnerService } from "./services/spinner/spinner.service";
import { HotTableModule } from "@handsontable/angular";

import {
  MatProgressSpinnerModule,
  MatCardModule,
  MatDividerModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatButtonToggleModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatTableModule,
  MatDialogModule,
  MatPaginatorModule,
  MatSortModule,
  MatToolbarModule,
  MatSlideToggleModule
} from "@angular/material";
import { SpinnerComponent } from "./components/spinner/spinner.component";
import { CardComponent } from "./components/card/card.component";
import { ExcelService } from "./services/excel/excel.service";
import { SplitStringPipe } from "./pipes/split-string.pipe";
import { FieldFormaterPipe } from "./pipes/field-formater.pipe";
import { OverlayModule } from "@angular/cdk/overlay";
import { CompareFilterComponent } from "./components/compare-filter/compare-filter.component";
import { DateFilterComponent } from "./components/date-filter/date-filter.component";
import { ConfirmDialogComponent } from "./components/confirm-dialog/confirm-dialog.component";
import { NumbersOnlyDirective } from "./directives/numbers-only.directive";
import { HandsontableComponent } from "./components/handsontable/handsontable.component";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatMenuModule,
    MatCheckboxModule,
    MatIconModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatToolbarModule,
    MatDialogModule,
    OverlayModule,
    MatPaginatorModule,
    MatSortModule,
    MatSlideToggleModule,
    HotTableModule.forRoot()
  ],
  exports: [
    SpinnerComponent,
    CardComponent,
    SplitStringPipe,
    FieldFormaterPipe,
    NumbersOnlyDirective,
    HandsontableComponent
  ],
  declarations: [
    SpinnerComponent,
    CardComponent,
    SplitStringPipe,
    FieldFormaterPipe,
    CompareFilterComponent,
    DateFilterComponent,
    ConfirmDialogComponent,
    NumbersOnlyDirective,
    HandsontableComponent
  ],
  providers: [
    AuthenticationService,
    PageService,
    StorageService,
    SpinnerService,
    ExcelService
  ],
  entryComponents: [ConfirmDialogComponent]
})
export class SharedModule {
  constructor() {}
}
