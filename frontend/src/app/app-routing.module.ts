import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {AuthguardGuard} from "./authguard.guard";


const routes: Routes = [
  {
    path: "",
    loadChildren: "./modules/authentication/authentication.module#AuthenticationModule"
  },
  {
    path: "",
    loadChildren: "./modules/features/features.module#FeaturesModule",
    canActivate: [AuthguardGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    onSameUrlNavigation: 'reload'
  })],
  
  exports: [RouterModule]
})
export class AppRoutingModule { }
