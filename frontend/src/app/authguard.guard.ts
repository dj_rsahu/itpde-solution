import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { MatSnackBar } from "@angular/material";
import { UtilsService } from './shared/services/utils/utils.service';

@Injectable({
  providedIn: "root"
})
export class AuthguardGuard implements CanActivate {
  constructor(private router: Router, private snackBar: MatSnackBar, 
    private utils: UtilsService,) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (JSON.parse(localStorage.getItem("user"))) {
      return true;
    }

    // navigate to login page
    console.log(next.url);
    this.utils.setLocal('url', state.url);
    this.router.navigate(["/login"]);

    
    this.snackBar.open("please login to enjoy ITPDE services", "DISMISS", {
      duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
    });
    return false;
  }
}
