import { Injectable, Injector } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, from, throwError } from "rxjs";
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { AuthenticationService } from './modules/authentication/authentication.service';

@Injectable({
  providedIn: "root"
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private router: Router, private snackBar: MatSnackBar,public injector: Injector) {}
  
  intercept(request: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
    
    let auth = this.injector.get(AuthenticationService)
    if (auth.getJwtToken()) {
      request = this.addToken(request, auth.getJwtToken());
    }
  
    return next.handle(request).pipe(catchError(error => {
      if (error instanceof HttpErrorResponse && (error.status === 403 || error.status === 401)) {
        localStorage.removeItem("user")
         this.router.navigate(['/login']);
         this.snackBar.open('session has been expired', 'DISMISS', {
          duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
        });
        return throwError(error);
       
      } else {
        return throwError(error);
      }
    }));
  }

  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        'Authorization': token
      }
    });
  }
}
