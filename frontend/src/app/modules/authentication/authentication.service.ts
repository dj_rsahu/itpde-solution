import { Injectable } from "@angular/core";
import { ApiService } from "src/app/core/services/http/api.service";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Authentication } from "./authentication";
import { environment } from "src/environments/environment";
import { Subject, Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AuthenticationService extends ApiService<Authentication, number> {
  baseUrl: string = environment.baseUrl;
  userName: string;
  result;
  user;
  currentUser: any;
  constructor(public _http: HttpClient) {
    super(_http);
  }
  setUserName(value: string) {
    this.userName = value;
  }
  getUserName() {
    return this.userName;
  }
  authenticate(payLoad) {
    // let param = new HttpParams()
    //   .append("email", payLoad.email)
    //   .append("password", payLoad.password);
    return this.save("login", payLoad);
    //return this._http.post(this.baseUrl + "user/login", payLoad);
  }
  forgotPassword(payLoad: any) {
    // payLoad["baseUrl"] = environment.originUrl;
    return this.save("forgotPassword", payLoad);
    //return this._http.post(this.baseUrl + "user/forgotPassword", payLoad);
  }
  resetPassword(payLoad: any) {
    return this.save("resetPassword", payLoad);// url changed form user/resetPassword
    //return this._http.post(this.baseUrl + "user/resetPassword", payLoad);
  }
  loggedUser(user: any) {
    this.currentUser = user;
  }
  oneTimeTokenVerification(payLoad: any) {
    return this.save("user/verifyToken", payLoad);
  }
  getJwtToken() {
    if (localStorage.getItem("user")) {
      this.user = localStorage.getItem("user");
      return this.user.token;
    }
    return false;
  }
}
