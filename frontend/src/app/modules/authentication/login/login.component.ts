import { Component, OnInit, Compiler } from "@angular/core";
import { FormControl, Validators, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AuthenticationService } from "../authentication.service";
import { AppStoreService } from "src/app/app-store.service";
import { UtilsService } from "src/app/shared/services/utils/utils.service";

@Component({
  selector: "dj-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  hide = true;
  userForm;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private snackBar: MatSnackBar,
    private appStoreService: AppStoreService,
    private _compiler: Compiler,
    private utils: UtilsService,
  ) {}
  user;

  ngOnInit() {
    this.utils.removeLocal('user');
    this.utils.removeLocal('project');
    this._compiler.clearCache();
    this.userForm = this.fb.group({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }
  signUpPage() {
    // this.router.navigate(['/', 'signup']);
  }
  signin(payload) {
    
    this.utils.removeLocal('user');
    this.utils.removeLocal('project');
    this.authService.authenticate(payload).subscribe(
      res => {
        this.user = res;
        localStorage.setItem("user", JSON.stringify(res));
        this.appStoreService.setUserStateAfterlogin(res);
        // this.appStoreService.setLoader(false);
        if(this.utils.getLocal('url')){
          this.router.navigateByUrl(this.utils.getLocal('url'));
        }else{
          this.router.navigate(["AllEstimates"]);
        }
      },
      err => {
        console.log("login err", err);
        this.snackBar.open("Invalid credentials", "DISMISS", {
          duration: 5000,
          verticalPosition: "top", // 'top' | 'bottom'
          horizontalPosition: "center",
          panelClass: ["error-snackbar"]
        });
        // this.appStoreService.setLoader(false);
        // this.router.navigateByUrl("AllEstimates");
      }
    );
    //
  }
}
