import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthenticationComponent } from "./authentication.component";
import { LoginComponent } from "./login/login.component";
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AlertPageComponent } from './alert-page/alert-page.component';

const routes: Routes = [
  {
    path: "",
    component: AuthenticationComponent,
    children: [
      {
        path: "login",
        component: LoginComponent
      },
      {
        path: "forgotpassword",
        component: ForgotPasswordComponent
      },
      {
        path: "alertpage",
        component: AlertPageComponent
      },
      {
        path: "resetpassword/:oneTimeToken/:email",
        component: ResetPasswordComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule {}
