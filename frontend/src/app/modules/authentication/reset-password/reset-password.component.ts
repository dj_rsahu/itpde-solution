import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators, ValidationErrors, ValidatorFn, AbstractControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'dj-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  userSignUpForm = this.fb.group({
    password: new FormControl('', [
      Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')
    ]),
    confirmPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(8)
    ])
  }, {
    validator: this.passwordValidator
  }
  );
  email;
  hide = true;
  payload = {};
  isAdmin = false;

  constructor(
    private router: Router,
    private fb: FormBuilder, private authService: AuthenticationService,
    private snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      (param) => {
        // console.log(param);
        this.payload['email'] = param.email;
        this.payload['oneTimeToken'] = param.oneTimeToken;
        if (param.from) {
          this.isAdmin = true;
        }
        // this.authService.getUserByEmail(param.email).subscribe(
        //   res => {
        //     this.payload = res;
        //   }
        // );
        this.oneTimeTokenVerification(this.payload);
      });


  }
  regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  }
  passwordValidator(form: FormGroup) {
    const condition = form.get('password').value !== form.get('confirmPassword').value;
    return condition ? { passwordsDoNotMatch: true } : null;
  }
  resetPassword() {
    
    this.payload['password'] = this.userSignUpForm.get('password').value;
    this.authService.resetPassword(this.payload).subscribe(
      (res) => {
        this.snackBar.open('Success! Please login again', 'DISMISS', {
          duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
        });
        this.router.navigateByUrl('login');
      }, (err) => {
        // console.log(err);
      }
    );
  }

  oneTimeTokenVerification(payload:any) {
    this.authService.oneTimeTokenVerification(this.payload).subscribe(
      (res) => {
        // console.log(res);
        this.snackBar.open('Success! Please login again', 'DISMISS', {
          duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['success-snackbar']
        });
        this.router.navigate(['resetpassword/'+payload.oneTimeToken+'/'+payload.email]);
      }, (err) => {
        // console.log("======"+err+"=======");
        this.snackBar.open("One Time Token Expired", "DISMISS", {
          duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
        });
        this.router.navigateByUrl("login");
        //this.appStoreService.setLoader(false);
      }
    );
  }

}

