import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'dj-alert-page',
  templateUrl: './alert-page.component.html',
  styleUrls: ['./alert-page.component.scss']
})
export class AlertPageComponent implements OnInit {

  constructor( private router: Router,) { }

  ngOnInit() {
  }
  submit(){
    this.router.navigateByUrl('login');
  }

}
