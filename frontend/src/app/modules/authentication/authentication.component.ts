import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppStoreService } from 'src/app/app-store.service';

@Component({
  selector: 'dj-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class AuthenticationComponent implements OnInit {
  isMobile=false;


  constructor(private appStoreService: AppStoreService) { }

  ngOnInit() {
    this.appStoreService.isMobile.subscribe(
      (res) => {
        this.isMobile = res;
      }
    )
  }

}
