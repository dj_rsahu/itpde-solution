import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppStoreService } from 'src/app/app-store.service';
import { MatSnackBar } from '@angular/material';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'dj-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  email = new FormControl('', [
    Validators.required,
    Validators.email
  ]);

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private appStoreService: AppStoreService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
  }
  forgotPassword() {
    this.appStoreService.setLoader(true);
    let payload = {
      email: this.email.value,
      
    };
    this.authService.forgotPassword(payload).subscribe(
      resP => {
        // console.log(resP);
        this.router.navigateByUrl('alertpage');
        // this.appStoreService.setLoader(false);
      },
      err => {
        // console.log(err);
        this.appStoreService.setLoader(false);
        this.snackBar.open('User does not exist', 'DISMISS', {
          duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
        });
      }
    );
  }
  returnToSignin() {
    this.router.navigateByUrl('login');
  }

}


