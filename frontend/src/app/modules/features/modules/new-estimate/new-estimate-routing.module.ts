import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewEstimateComponent } from './new-estimate.component';
import { SessionGuard } from 'src/app/session.guard';

const routes: Routes = [
  {
    path: "NewEstimates",
    component: NewEstimateComponent
  },
  {
    path: "editEstimates/:id/:mode/:step",
    component: NewEstimateComponent,
    canActivate:[SessionGuard]
  }
 

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewEstimateRoutingModule { }
