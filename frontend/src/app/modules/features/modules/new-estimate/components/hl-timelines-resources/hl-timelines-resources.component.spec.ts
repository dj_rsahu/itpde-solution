import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HlTimelinesResourcesComponent } from './hl-timelines-resources.component';

describe('HlTimelinesResourcesComponent', () => {
  let component: HlTimelinesResourcesComponent;
  let fixture: ComponentFixture<HlTimelinesResourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HlTimelinesResourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HlTimelinesResourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
