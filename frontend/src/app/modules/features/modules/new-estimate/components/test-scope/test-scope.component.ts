import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  Input
} from "@angular/core";
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { NewEstimateService } from "../../new-estimate.service";
import Handsontable from "handsontable";
import { UtilsService } from "src/app/shared/services/utils/utils.service";
import { element } from 'protractor';

@Component({
  selector: "dj-test-scope",
  templateUrl: "./test-scope.component.html",
  styleUrls: ["./test-scope.component.scss"]
})
export class TestScopeComponent implements OnInit {
  @Output() djStepNext = new EventEmitter();
  @ViewChild("tsTable") tsTable: Handsontable;
  @Input() mode;
  data = [];
  defaultValueRender = function(instance, td, row, col, prop, value) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (col === 8 || col === 9) {
      // console.log(value);
    }
    if (value && (value === "#VALUE!" || value === "#DIV/0!")) {
      td.innerHTML = 0;
    } else if (
      (col === 3 || col === 4 || col === 5 || col === 8 || col === 9) &&
      row !== 0
    ) {
      if (typeof value === "string") {
        if (value !== "") {
          td.innerHTML = parseFloat(value).toFixed(2);
        } else if (value === "") {
          td.innerHTML = 0;
        }
      } else if (value === null) {
      } else {
        td.innerHTML = value.toFixed(2);
      }
    }
    if (value && row === 0 && col === 4) {
      td.innerHTML = value + "%";
    }
  };
  percentageValueRender = function(instance, td, row, col, prop, value) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (value === "#VALUE!") {
      td.innerHTML = 0;
    }
  };
  percentageRendererForDataSetup = function(
    instance,
    td,
    row,
    col,
    prop,
    value
  ) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (value === "#VALUE!") {
      td.innerHTML = 0;
    } else if (value && row === 0 && col === 4) {
      td.innerHTML = value + "%";
    }
  };
  totalRenderer = function(instance: Handsontable, td, row, col, prop, value) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (value && (value === "#VALUE!" || value === "#DIV/0!")) {
      let cell3 =
        instance.getDataAtCell(row, 3) === "#VALUE!" ||
        instance.getDataAtCell(row, 3) === "#DIV/0!"
          ? 0
          : instance.getDataAtCell(row, 3);
      let cell4 =
        instance.getDataAtCell(row, 4) === "#VALUE!" ||
        instance.getDataAtCell(row, 4) === "#DIV/0!"
          ? 0
          : instance.getDataAtCell(row, 4);
      let cell5 =
        instance.getDataAtCell(row, 5) === "#VALUE!" ||
        instance.getDataAtCell(row, 5) === "#DIV/0!"
          ? 0
          : instance.getDataAtCell(row, 5);
      td.innerHTML = (cell3 + cell4 + cell5).toFixed(2);
    }
  };
  emptyRow = {
    tsname: "",
    tsexeplan: "",
    tceffort: "",
    tdsetup: "",
    testexe: "",
    total: "",
    tcnewreuse: "",
    nooftc: "",
    exeprod: "",
    comments: ""
  };
  hightLevelRow = {
    tsname: "High level Estimates",
    tsexeplan: "",
    tceffort: "",
    tdsetup: "",
    testexe: "",
    total: "",
    tcnewreuse: "",
    nooftc: "",
    exeprod: "",
    comments: ""
  };
  totalRow = {
    tsname: "Total",
    tceffort: "",
    tdsetup: "",
    testexe: "",
    total: "",
    nooftc: "",
    exeprod: ""
  };
  columns = [
    { data: "id" },
    { data: "tsname" },
    {
      data: "tsexeplan"
    },
    {
      data: "tceffort",
      renderer: this.defaultValueRender
    },
    {
      data: "tdsetup",
      renderer: this.defaultValueRender
    },

    { data: "testexe", renderer: this.defaultValueRender, editor: false },
    {
      data: "total",
      renderer: this.totalRenderer,
      editor: false
    },
    {
      data: "tcnewreuse",
      type: "dropdown",
      source: ["New", "Reuse"],
      trimDropdown: false
    },
    {
      data: "nooftc",
      renderer: this.defaultValueRender
    },
    {
      data: "exeprod",
      renderer: this.defaultValueRender
    },

    {
      data: "comments"
    }
  ];
  colHeaders = [
    "id",
    "Test Module",
    "Sub Module",
    "Planning Efforts",
    "Data Setup",
    "Test Execution",
    "Total",
    "New/Reuse",
    "TC Count",
    "TE Productivity",
    "Comments/Details"
  ];
  settings: Handsontable.GridSettings = {
    rowHeaders: false,
    colHeaders: this.colHeaders,
    contextMenu: [
      "row_above",
      "row_below",
      "remove_row",
      "clear_column",
      "undo",
      "redo",
      "cut",
      "copy"
      // "hidden_columns_hide",
      // "hidden_columns_show"
    ],
    cells: function(row, col, prop) {
      var cellProperties = {};
      let rowsCount = this.instance.countRows();
      if (row === rowsCount - 1) {
        cellProperties["readOnly"] = true;
        cellProperties["className"] = "bg-secondary htRight";
      }
      else{
        cellProperties["className"] = "";
      }
      if (col > 2) {
        cellProperties["className"] = "htRight";
      }
      if (col === 7 || col === 10) {
        cellProperties["className"] = "htLeft";
      }
      if (col > 2 && col < 7) {
        cellProperties["className"] = "text-body htRight";
      }

      if (col === 5 || col === 6) {
        cellProperties["readOnly"] = true;
      }
      if (col === 6) {
        cellProperties["className"] = "font-weight-bold text-body  htRight";
      }
      if (row !== 0 && (col === 8 || col === 9)) {
        cellProperties["className"] = "cell-mandatory htRight";
        // this.instance.setCellMeta(row, col, "className", "bg-grey");
      }
      if (row === 0 && col !== 3 && col !== 4) {
        cellProperties["readOnly"] = true;
        cellProperties["className"] = "bg-secondary htRight";
        // this.instance.setCellMeta(row, col, "className", "bg-grey");
      } else if (row > 0 && (col == 3 || col == 4)) {
        cellProperties["readOnly"] = true;
      } else if (row === 0 && (col === 3 || col === 4)) {
        cellProperties["className"] = "cell-mandatory htRight";
        if (col === 3) {
          cellProperties["placeholder"] = "< Productivity >";
        } else if (col === 4) {
          cellProperties["placeholder"] = "< Productivity (%) >";
        }
      }

      cellProperties["renderer"] = this.defaultRowRender;
      return cellProperties;
    },
    filters: true,
    dropdownMenu: false,
    hiddenColumns: {
      // set columns that are hidden by default
      columns: [0],
      // show where are hidden columns
      indicators: false
    },
    formulas: true,
    copyPaste: true,
    width: "100%"
  };
  projectDetails: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private newEstimationService: NewEstimateService,
    private snackBar: MatSnackBar,
    private utils: UtilsService
  ) {}

  ngOnInit() {
    if (this.mode === "view" || this.mode === "approve") {
      this.settings.readOnly = true;
      this.settings.contextMenu = false;
    }

    this.projectDetails = this.utils.getLocal("project");
    if (this.projectDetails) {
      this.getTableData();
    } else {
      this.data.push(this.hightLevelRow);
      for(let i=0; i<8;i++){
        this.data.push(this.emptyRow);
      }
      this.setTotal();
    }
  }
  getTableData() {
    this.newEstimationService
      .fetchTestScopeDetails(this.projectDetails.id)
      .subscribe(res => {
        this.data = res;
        this.setHighLevel();
      });
  }
  setTotal() {
    let totalLevel = this.totalRow;
    let lastRow = this.data.length;
    totalLevel["tceffort"] = `=ROUND(SUM(D2:D${lastRow}),2)`;
    totalLevel["tdsetup"] = `=ROUND(SUM(E2:E${lastRow}),2)`;
    totalLevel["testexe"] = `=ROUND(SUM(F2:F${lastRow}),2)`;
    totalLevel["total"] = `=ROUND(SUM(G2:G${lastRow}),2)`;
    totalLevel["nooftc"] = `=ROUND(SUM(I2:I${lastRow}),2)`;
    totalLevel["exeprod"] = `=ROUND(SUM(J2:J${lastRow}),2)`;
    this.data.push(totalLevel);
    this.setColumnTotal(JSON.parse(JSON.stringify(this.data)));
  }
  setHighLevel() {
    let hightLevel = this.emptyRow;
    hightLevel["tsname"] = "High level Estimates";
    hightLevel["tsexeplan"] = "";
    hightLevel["tceffort"] = "";
    hightLevel["tdsetup"] = "";
    hightLevel["testexe"] = "";
    hightLevel["total"] = "";
    hightLevel["tcnewreuse"] = "";
    hightLevel["nooftc"] = "";
    hightLevel["exeprod"] = "";
    
    if (this.data.length < 10) {
      for(let i=this.data.length; i<9;i++){
        this.data.push(hightLevel);
      }
      // this.data.unshift(hightLevel);
      this.setDefaultRow();
    }
    this.setTotal();
    this.setColumnTotal(JSON.parse(JSON.stringify(this.data)));
  }
  setColumnTotal(data) {
    let dataSoure = [];
    let size = this.data.length - 1;
    data.forEach((element, index) => {
      if (index > 0 && index < size) {
        let colNum = ++index;
        element["tceffort"] = `=ROUND(I${colNum}/D1,2)`;
        element["tdsetup"] = `=(F${colNum}*E1)/100`;
        element["testexe"] = `=ROUND(I${colNum}/J${colNum},2)`;
        element["total"] = `=ROUND(SUM(C${colNum}:F${colNum}),2)`;
        dataSoure.push(element);
      } else if (index === 0) {
        dataSoure.push(element);
      } else {
        dataSoure.push(JSON.parse(JSON.stringify(this.totalRow)));
      }
    });
    //dataSoure[dataSoure.length - 1] = this.totalRow;
    this.data = dataSoure;
  }
  setDefaultRow() {
    let defaultRow = this.emptyRow;
    defaultRow["tsname"] = "";
    defaultRow["tsexeplan"] = "";
    defaultRow["tceffort"] = "";
    defaultRow["tdsetup"] = "";
    defaultRow["testexe"] = "";
    defaultRow["total"] = "";
    defaultRow["tcnewreuse"] = "";
    defaultRow["nooftc"] = "";
    defaultRow["exeprod"] = "";
    this.data.push(JSON.parse(JSON.stringify(defaultRow)));
  }
  handleAfterChange(event) {
    //this.setColumnTotal(this.data);
  }
  saveTestScope() {
    this.data.pop();
    const firstRow = this.data.shift();
    
    let testPayload = this.data.filter((element, index) => {
      return (
        (element.tsname && element.tsname !== "") ||
        (element.tsexeplan && element.tsexeplan !== "") || 
        (element.tcnewreuse && element.tcnewreuse !== "") || 
        (element.comments && element.comments !== "")
      )
    })
    let payLoad = {
      id: this.projectDetails.id,
      payLoad: {
        testScopeListObj: [firstRow, ...testPayload]
      }
    };
    this.newEstimationService
      .saveOrUpdateTestScopeDetails(payLoad)
      .subscribe(res => {
        if(this.mode=='edit'){
          this.snackBar.open(
            "Test Scope sheet updates successsfully",
            "DISMISS",
            {
              duration: 5000,
              verticalPosition: "top", // 'top' | 'bottom'
              horizontalPosition: "center",
              panelClass: ["success-snackbar"]
            }
          );
        }
        else{
            this.snackBar.open(
            "Test Scope saved successsfully",
            "DISMISS",
            {
              duration: 5000,
              verticalPosition: "top", // 'top' | 'bottom'
              horizontalPosition: "center",
              panelClass: ["success-snackbar"]
            }
          );
        }
        this.djStepNext.emit();
      });
  }
  handleBeforeOnCellContextMenu(event) {
    const hotRef: Handsontable = this.tsTable.getInstance();
    if (event.CellCoords.row + 1 === hotRef.countRows()) {
      event.event.stopImmediatePropagation();
    }
    if (event.CellCoords.row === 0) {
      event.event.stopImmediatePropagation();
    }
  }
  handleAfterCreateRow(event) {
    const hotRef: Handsontable = this.tsTable.getInstance();
    let initialRow = JSON.parse(JSON.stringify(this.data[1]));
    // console.log(initialRow);
    let size = event.index + 1;
    initialRow["id"] = null;
    initialRow["tsname"] = "";
    initialRow["tsexeplan"] = "";
    initialRow["tcnewreuse"] = "";
    initialRow["nooftc"] = 0;
    initialRow["exeprod"] = 0;
    initialRow["tcrevised"] = null;
    initialRow["comments"] = "";
    initialRow["interfacerep"] = null;
    initialRow["tsname"] = "";
    initialRow["tsname"] = "";
    initialRow["tceffort"] = `=ROUND(I${size}/D1,2)`;
    initialRow["tdsetup"] = `=(F${size}*E1)/100`;
    initialRow["testexe"] = `=ROUND(I${size}/J${size},2)`;
    initialRow["total"] = `=ROUND(SUM(C${size}:F${size}),2)`;

    Object.keys(initialRow).forEach(ele => {
      hotRef.setDataAtRowProp(event.index, ele, initialRow[ele]);
    });

    let totalRow = this.totalRow;
    let lastRow = hotRef.countRows() - 1;
    totalRow["tceffort"] = `=ROUND(SUM(D2:D${lastRow}),2)`;
    totalRow["tdsetup"] = `=ROUND(SUM(E2:E${lastRow}),2)`;
    totalRow["testexe"] = `=ROUND(SUM(F2:F${lastRow}),2)`;
    totalRow["total"] = `=ROUND(SUM(G2:G${lastRow}),2)`;
    totalRow["nooftc"] = `=ROUND(SUM(I2:I${lastRow}),2)`;
    totalRow["exeprod"] = `=ROUND(SUM(J2:J${lastRow}),2)`;
    Object.keys(totalRow).forEach(ele => {
      hotRef.setDataAtRowProp(hotRef.countRows() - 1, ele, totalRow[ele]);
    });
  }
}
