import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  Input
} from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatSnackBar
} from "@angular/material";
import { NewEstimateService } from "../../new-estimate.service";
import { ActivatedRoute } from "@angular/router";
import Handsontable from "handsontable";
import { UtilsService } from "src/app/shared/services/utils/utils.service";

@Component({
  selector: "dj-detailed-estimates",
  templateUrl: "./detailed-estimates.component.html",
  styleUrls: ["./detailed-estimates.component.scss"]
})
export class DetailedEstimatesComponent implements OnInit {
  @Output() djStepNext = new EventEmitter();
  @ViewChild("detailedTable") detailedTable;
  @Input() mode;
  defaultValueRender = (instance: Handsontable, td, row, col, prop, value) => {
    //  Handsontable.renderers.TextRenderer.apply(this, [instance, td, row, col, prop, value]);
    let rowCount = instance.countRows();
    let colCount = instance.countCols();

    if (col === colCount - 3 && row < instance.countRows() - 1) {
      let analyze =
        instance.getDataAtCell(row, colCount - 6) &&
        typeof instance.getDataAtCell(row, colCount - 6) !== "string"
          ? instance.getDataAtCell(row, colCount - 6)
          : 0;
      let design =
        instance.getDataAtCell(row, colCount - 5) &&
        typeof instance.getDataAtCell(row, colCount - 5) !== "string"
          ? instance.getDataAtCell(row, colCount - 5)
          : 0;
      let build =
        instance.getDataAtCell(row, colCount - 4) &&
        typeof instance.getDataAtCell(row, colCount - 4) !== "string"
          ? instance.getDataAtCell(row, colCount - 4)
          : 0;
      td.innerHTML = analyze + design + build;
      // instance.setDataAtCell(row, col, analyze + design + build)
    } else if (col === colCount - 1 && row < instance.countRows() - 1) {
      let analyze =
        instance.getDataAtCell(row, colCount - 6) &&
        typeof instance.getDataAtCell(row, colCount - 6) !== "string"
          ? instance.getDataAtCell(row, colCount - 6)
          : 0;
      let design =
        instance.getDataAtCell(row, colCount - 5) &&
        typeof instance.getDataAtCell(row, colCount - 5) !== "string"
          ? instance.getDataAtCell(row, colCount - 5)
          : 0;
      let build =
        instance.getDataAtCell(row, colCount - 4) &&
        typeof instance.getDataAtCell(row, colCount - 4) !== "string"
          ? instance.getDataAtCell(row, colCount - 4)
          : 0;
      let implementation =
        instance.getDataAtCell(row, colCount - 2) &&
        typeof instance.getDataAtCell(row, colCount - 2) !== "string"
          ? instance.getDataAtCell(row, colCount - 2)
          : 0;
      td.innerHTML = analyze + design + build + implementation;
      // instance.setDataAtCell(row, col, analyze + design + build + implementation)
    } else if (typeof value === "string" || value === null) {
      td.innerHTML = 0;
    } else {
      td.innerHTML = value;
    }
    if (row === instance.countRows() - 1 && col === colCount - 1) {
      let percentTotal = 0;

      for (let index = 0; index < rowCount - 1; index++) {
        let analyze =
          instance.getDataAtCell(index, colCount - 6) &&
          typeof instance.getDataAtCell(index, colCount - 6) !== "string"
            ? instance.getDataAtCell(index, colCount - 6)
            : 0;
        let design =
          instance.getDataAtCell(index, colCount - 5) &&
          typeof instance.getDataAtCell(index, colCount - 5) !== "string"
            ? instance.getDataAtCell(index, colCount - 5)
            : 0;
        let build =
          instance.getDataAtCell(index, colCount - 4) &&
          typeof instance.getDataAtCell(index, colCount - 4) !== "string"
            ? instance.getDataAtCell(index, colCount - 4)
            : 0;
        let implementation =
          instance.getDataAtCell(index, colCount - 2) &&
          typeof instance.getDataAtCell(index, colCount - 2) !== "string"
            ? instance.getDataAtCell(index, colCount - 2)
            : 0;
        percentTotal = percentTotal + analyze + design + build + implementation;
      }

      td.innerHTML = percentTotal;
    } else if (row === instance.countRows() - 1 && col === colCount - 3) {
      let percentTotal = 0;

      for (let index = 0; index < rowCount - 1; index++) {
        let analyze =
          instance.getDataAtCell(index, colCount - 6) &&
          typeof instance.getDataAtCell(index, colCount - 6) !== "string"
            ? instance.getDataAtCell(index, colCount - 6)
            : 0;
        let design =
          instance.getDataAtCell(index, colCount - 5) &&
          typeof instance.getDataAtCell(index, colCount - 5) !== "string"
            ? instance.getDataAtCell(index, colCount - 5)
            : 0;
        let build =
          instance.getDataAtCell(index, colCount - 4) &&
          typeof instance.getDataAtCell(index, colCount - 4) !== "string"
            ? instance.getDataAtCell(index, colCount - 4)
            : 0;

        percentTotal = percentTotal + analyze + design + build;
      }
      td.innerHTML = percentTotal;
    } else if (row === instance.countRows() - 1) {
      td.innerHTML = this.getTotal(instance, col).toFixed(0);
    }
  };
  data = [];
  editmode: boolean;
  columns = [
    {
      data: "required"
    },
    {
      data: "requiredcategory"
    },
    {
      data: "releaseno"
    },

    { data: "mainfunction" },
    {
      data: "workstream"
    },
    {
      data: "subfunction"
    },
    {
      data: "moduletype",
      type: "autocomplete",
      filter: true,
      trimDropdown: false,
      strick: false,
      source: []
    },
    {
      data: "descr"
    },
    {
      data: "sourcename"
    },
    {
      data: "target"
    },

    { data: "method" },
    {
      data: "difficulty"
    },
    {
      data: "neworexist"
    },
    {
      data: "analyse",
      type: "numeric",
      renderer: this.defaultValueRender
    },
    {
      data: "design",
      type: "numeric",
      renderer: this.defaultValueRender
    },
    {
      data: "buildunittest",
      type: "numeric",
      renderer: this.defaultValueRender
    },
    {
      data: "effort",
      renderer: this.defaultValueRender,
      type: "numeric"
      // editor: false
    },

    {
      data: "implementation",
      type: "numeric",
      renderer: this.defaultValueRender
    },
    {
      data: "effortimpl",
      renderer: this.defaultValueRender,
      type: "numeric"
      // editor: false
    }
  ];
  colHeaders = [
    "Required",
    "Required Category",
    "Release",
    "Main Function",
    "Work Stream",
    "Sub-function",
    "Module Type",
    "Description",
    "Source",
    "Target",
    "Method",
    "Difficulty",
    "New/Existing",
    "Analyze",
    "Design",
    "Build & Unit Test",
    "Effort",
    "Implementation",
    "Effort and Impl"
  ];
  settings: Handsontable.GridSettings = {
    rowHeaders: false,
    colHeaders: this.colHeaders,
    // colHeaders: true,
    dropdownMenu: false,
    startCols: 10,
    startRows: 10,
    contextMenu: [
      "row_above",
      "row_below",
      "remove_row",
      "clear_column",
      "undo",
      "redo",
      "cut",
      "copy"
      // "hidden_columns_hide",
      // "hidden_columns_show"
    ],
    cells: function(row, col, prop) {
      var cellProperties = {};
      const hotRef: Handsontable = this.instance;
      let rowCount = hotRef.countRows();
      let colCount = hotRef.countCols();
      if (row === rowCount - 1) {
        cellProperties["readOnly"] = true;
        cellProperties["className"] = "bg-secondary text-dark";
      } else {
        cellProperties["className"] = "text-body";
      }
      if (col === colCount - 1 || col === colCount - 3) {
        cellProperties["readOnly"] = true;
        // cellProperties["className"] = "font-weight-bold htRight text-body";
      }
      // cellProperties["renderer"] = this.defaultRowRender;
      return cellProperties;
    },
    filters: true,
    formulas: true,
    copyPaste: true
  };
  apiData: any;
  emptyRow = {
    required: "",
    requiredcategory: "",
    releaseno: "",
    mainfunction: "",
    workstream: "",
    subfunction: "",
    moduletype: "",
    descr: "",
    sourcename: "",
    target: "",
    method: "",
    difficulty: "",
    neworexist: "",
    analyse: 0,
    design: 0,
    buildunittest: 0,
    effort: 0,
    implementation: 0,
    effortimpl: 0
  };
  projectDetails: any;

  constructor(
    private newEstimationService: NewEstimateService,
    private snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private utils: UtilsService
  ) {}

  ngOnInit() {
    if (this.mode === "view" || this.mode === "approve") {
      this.settings.readOnly = true;

      this.settings.contextMenu = false;
    }
    this.getModuleTypes();

    this.apiData = [
      {
        required: " ",
        requiredcategory: "",
        releaseno: "",
        mainfunction: "",
        workstream: "",
        subfunction: "",
        moduletype: "",
        descr: "",
        sourcename: "",
        target: "",
        method: "",
        difficulty: "",
        neworexist: "",
        analyse: 0,
        design: 0,
        buildunittest: 0,
        effort: 0,
        implementation: 0,
        effortimpl: 0
      }
    ];
  }
  getTotal(instance, col) {
    let percentTotal = 0;
    instance.getDataAtProp(col).forEach(ele => {
      if (ele && typeof ele !== "string") {
        percentTotal = percentTotal + ele;
      }
    });
    return percentTotal;
  }
  getTableData() {
    this.data = this.apiData;
    this.setSummary();
  }
  handleAfterChange(event) {
    if (event.source === "edit" && this.mode !== "view") {
      // this.data.pop();
      // this.setSummary(this.data, this.data.length);
    }
  }
  saveDetailedEstimate() {
    this.data.pop();
    const detailedEstList = this.data.filter((element, index) => {
      console.log(element);
      return (
        (element.required && element.required !== "") ||
        (element.requiredcategory && element.requiredcategory !== "") ||
        (element.releaseno && element.releaseno !== "") ||
        (element.mainfunction && element.mainfunction !== "") ||
        (element.workstream && element.workstream !== "") ||
        (element.subfunction && element.subfunction !== "") ||
        (element.moduletype && element.moduletype !== "") ||
        (element.descr && element.descr !== "") ||
        (element.sourcename && element.sourcename !== "") ||
        (element.target && element.target !== "") ||
        (element.method && element.method !== "") ||
        (element.difficulty && element.difficulty !== "") ||
        (element.neworexist && element.neworexist !== "")
      );
    });
    console.log(JSON.stringify(detailedEstList));
    let obj = {
      id: this.projectDetails.id,
      payLoad: {
        detailedEstList
      }
    };
    this.newEstimationService.saveDetailedEstimate(obj).subscribe(res => {
      if (this.mode == "edit") {
        this.snackBar.open(
          "Detailed Estimation sheet updates successsfully",
          "DISMISS",
          {
            duration: 5000,
            verticalPosition: "top", // 'top' | 'bottom'
            horizontalPosition: "center",
            panelClass: ["success-snackbar"]
          }
        );
      } else {
        this.snackBar.open(
          "Detailed Estimation saved successsfully",
          "DISMISS",
          {
            duration: 5000,
            verticalPosition: "top", // 'top' | 'bottom'
            horizontalPosition: "center",
            panelClass: ["success-snackbar"]
          }
        );
      }
      this.djStepNext.emit();
      this.getAllDetailedEstimates();
    });
  }
  getAllDetailedEstimates() {
    let payLoad = {
      id: this.projectDetails.id
    };
    this.newEstimationService
      .getAllDetailedEstimates(payLoad)
      .subscribe(res => {
        if (res.length > 0) {
          this.data = res;
          if(this.data.length < 9) {
            for (let i = this.data.length; i < 9; i++) {
              this.data.push(JSON.parse(JSON.stringify(this.emptyRow)));
            }
          }
          this.setSummary();
        } else {
          for (let i = 0; i < 9; i++) {
            this.data.push(JSON.parse(JSON.stringify(this.emptyRow)));
          }
          this.setSummary();
        }
      });
  }
  setColumnTotal(data) {
    let dataSource = [];
    data.forEach((element, index) => {
      let size = ++index;
      element["effort"] = `=SUM(N${size}:P${size})`;
      element["effortimpl"] = `=SUM(Q${size}:R${size})`;
      dataSource.push(element);
    });
    this.data = dataSource;
  }
  setSummary() {
    let total = JSON.parse(JSON.stringify(this.emptyRow));
    total.required = "Total";
    this.data.push(total);
  }
  getModuleTypes() {
    this.newEstimationService.getDetailedModuleTypes().subscribe((res: any) => {
      this.columns[6].source = res;
      this.projectDetails = this.utils.getLocal("project");
      if (this.projectDetails.id) {
        this.getAllDetailedEstimates();
      } else {
        this.getTableData();
      }
    });
  }
  handleBeforeOnCellContextMenu(event) {
    const hotRef: Handsontable = this.detailedTable.getInstance();
    // console.log(event, event.CellCoords.row + 1, hotRef.countRows());
    if (event.CellCoords.row + 1 === hotRef.countRows()) {
      // console.log();
      event.event.stopImmediatePropagation();
    }
  }
  handleAfterCreateRow(event) {}
  handleAfterColumnSort(event) {
    this.setSummary();
  }
  handleBeforeColumnSort(event) {
    this.data.pop();
  }
}
