import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog,
  MatDialogConfig,
  MatSnackBar
} from "@angular/material";
import { ViewChild } from "@angular/core";
import { NewEstimateService } from "../../new-estimate.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ConfirmDialogComponent } from "src/app/shared/components/confirm-dialog/confirm-dialog.component";
import { CustomErrorStateMatcher } from "src/app/shared/other/custom-error-state-matcher";
import { ActivatedRoute } from "@angular/router";
import { UtilsService } from 'src/app/shared/services/utils/utils.service';

@Component({
  selector: "dj-assumptions",
  templateUrl: "./assumptions.component.html",
  styleUrls: ["./assumptions.component.scss"]
})
export class AssumptionsComponent implements OnInit {
  assumptionformgroup: FormGroup;
  Datas = [];
  editflag = false;
  estid;
  deletedArray = [];
  @Output() next = new EventEmitter();
  @Input() mode;
  DataRows: any[] = [
    {
      Assumption_Reference: "A10",
      Area: "Resource",
      Assumption: "Resources will be on board",
      Impact: "Project will be delayed",
      actions: ""
    },
    {
      Assumption_Reference: "A10",
      Area: "Resource",
      Assumption: "Resources will be on board",
      Impact: "Project will be delayed",
      actions: ""
    },
    {
      Assumption_Reference: "A10",
      Area: "Resource",
      Assumption: "Resources will be on board",
      Impact: "Project will be delayed",
      actions: ""
    },
    {
      Assumption_Reference: "A10",
      Area: "Resource",
      Assumption: "Resources will be on board",
      Impact: "Project will be delayed",
      actions: ""
    },
    {
      Assumption_Reference: "A10",
      Area: "Resource",
      Assumption: "Resources will be on board",
      Impact: "Project will be delayed",
      actions: ""
    },
    {
      Assumption_Reference: "A10",
      Area: "Resource",
      Assumption: "Resources will be on board",
      Impact: "Project will be delayed",
      actions: ""
    },
    {
      Assumption_Reference: "A10",
      Area: "Resource",
      Assumption: "Resources will be on board",
      Impact: "Project will be delayed",
      actions: false
    }
  ];

  displayedColumns: string[] = [
    "assumptionref",
    "area",
    "assumption",
    "impact",
    "actions"
  ];
  columnHeaders: string[] = [
    "Assumption Reference",
    "Area",
    "Assumption",
    "Impact",
    "Actions"
  ];
  dataSource: MatTableDataSource<any[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  errorState = false;
  errorMatcher = new CustomErrorStateMatcher();
  documentIndex: any;
  editmode: boolean;
  projectDetails: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private new_estimateService: NewEstimateService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private utils:UtilsService
  ) {}

  ngOnInit() {
    this.setDataSource(this.Datas);

    this.assumptionformgroup = new FormGroup({
      area: new FormControl("", Validators.required),
      assumption: new FormControl("", Validators.required),
      impact: new FormControl("", Validators.required)
    });
    this.assumptionformgroup.valueChanges.subscribe(value => {
      console.log(value);
    });
    if (this.mode === "view" || this.mode === "approve") {
      this.assumptionformgroup.controls["area"].disable();
      this.assumptionformgroup.controls["assumption"].disable();
      this.assumptionformgroup.controls["impact"].disable();
    }
    this.cancel();
    this.projectDetails = this.utils.getLocal('project');
    this.estimation(this.projectDetails.id);
    this.editflag = false;
  }
  setDataSource(dataSource) {
    this.dataSource = new MatTableDataSource(dataSource);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  skip() {
    this.next.emit(5);
  }
  estimation(val) {
    this.estid = val;
    console.log("emo", this.estid);
    this.getAssumptionById();
  }
  getAssumptionById() {
    this.new_estimateService
      .getAsumptionsById(this.estid)
      .subscribe((res: any) => {
        this.Datas = res;
        console.log("REF", this.Datas);
        this.dataSource = new MatTableDataSource(this.Datas);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });

    console.log("array", this.Datas);
  }
  deleteAssumption(val, val1) {
    // this.new_estimateService.deleteAsumption(val).subscribe(res=>{
    //   this.getAssumptionById()
    // })
    if (val.id != undefined) {
      val.isdelete = 1;
      this.deletedArray.push(val);
    }
    console.log("fromdelete", this.deletedArray);
    this.Datas.splice(val1, 1);
  }
  openDialog(e, e1) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: [
        "Cancel",
        "Delete",
        "Assumption Delete Confirmation",
        "This action will delete the selected Assumption permanently. Do you wish to continue?"
      ]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        console.log(e);
        this.deleteAssumption(e, e1);
        this.dataSource = new MatTableDataSource(this.Datas);
      } else {
      }
    });
  }
  openDialog1() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: [
        "Cancel",
        "Ok",
        "You'll lose all your changes, Are you sure you want to continue?"
      ]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.skip();
      } else {
      }
    });
  }
  editUsers(val, index) {
    console.log("edit", val);
    this.editflag = true;
    this.documentIndex = val.assumptionref;
    this.assumptionformgroup.setValue({
      area: val.area,
      assumption: val.assumption,
      impact: val.impact
    });
    // this.userFormgroup.controls["groupName"].enable();
    // this.userFormgroup.controls["role"].enable()

    // this.selectedUser = val;
  }
  cancel() {
    this.assumptionformgroup.reset();
    this.assumptionformgroup.markAsPristine();
    this.assumptionformgroup.markAsUntouched();
    this.assumptionformgroup.updateValueAndValidity();
    // this.isUpdate = false;
  }
  addDocuments() {
    let item = this.assumptionformgroup.value;

    if (this.editflag) {
      this.Datas.filter(ele => {
        if (ele["assumptionref"] === this.documentIndex) {
          ele["area"] = item["area"];
          ele["assumption"] = item["assumption"];
          ele["impact"] = item["impact"];
          return ele;
          // this.Datas[this.documentIndex]=documentRow;
        }
      });
      this.editflag = false;
    } else {
      let a = 1;
      if (this.Datas.length > 0) {
        let ob = this.Datas[this.Datas.length - 1];

        a = ob.assumptionref.slice(-2);
        a = ++a;
      }

      let documentItem = {
        assumptionref: `A0${a}`,
        area: item["area"],
        assumption: item["assumption"],
        impact: item["impact"],
        estid: this.estid
      };
      this.Datas.push(documentItem);
    }

    this.setDataSource(this.Datas);
    console.log(this.Datas);

    this.assumptionformgroup.reset();
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  continue() {
    var item = { assumptions: this.Datas.concat(this.deletedArray) };
    console.log(item);
    this.new_estimateService.saveAsumption(item).subscribe(
      res => {
        console.log("sucess", res);
        if(this.mode=='edit'){
          this.snackBar.open(
            "Assumption sheet updates successsfully",
            "DISMISS",
            {
              duration: 5000,
              verticalPosition: "top", // 'top' | 'bottom'
              horizontalPosition: "center",
              panelClass: ["success-snackbar"]
            }
          );
        }
        else{
            this.snackBar.open(
            "Assumptions saved successsfully",
            "DISMISS",
            {
              duration: 5000,
              verticalPosition: "top", // 'top' | 'bottom'
              horizontalPosition: "center",
              panelClass: ["success-snackbar"]
            }
          );
        }
        this.next.emit(5);
      },
      err => {
        console.log("assumption", err);
        this.snackBar.open(err.error.message, "DISMISS", {
          duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
        });
      }
    );
  }
  back() {
    this.next.emit(3);
  }
}
