import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedEstimatesComponent } from './detailed-estimates.component';

describe('DetailedEstimatesComponent', () => {
  let component: DetailedEstimatesComponent;
  let fixture: ComponentFixture<DetailedEstimatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedEstimatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedEstimatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
