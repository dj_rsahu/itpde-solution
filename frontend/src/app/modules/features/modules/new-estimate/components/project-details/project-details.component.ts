import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  AfterContentInit,
  AfterContentChecked
} from "@angular/core";
import { NewEstimateService } from "../../new-estimate.service";
import { ActivatedRoute } from "@angular/router";
import { UtilsService } from "src/app/shared/services/utils/utils.service";

import { ESTIMATE_CONSTANTS } from "src/app/configs/constants/APP_CONSTANTS";
import { AppStoreService } from "src/app/app-store.service";

@Component({
  selector: "dj-project-details",
  templateUrl: "./project-details.component.html",
  styleUrls: ["./project-details.component.scss"]
})
export class ProjectDetailsComponent implements OnInit, AfterContentChecked {
  project: any;
  @Input() mode;
  estCount: any;
  resourceTypeList = [
    { name: ESTIMATE_CONSTANTS.complete, class: "completed-chip" },
    { name: ESTIMATE_CONSTANTS.draft, class: "draft-chip" },
    { name: ESTIMATE_CONSTANTS.inprogress, class: "inprogress-chip" },
    { name: ESTIMATE_CONSTANTS.pending1, class: "pending-chip" }
  ];
  isMobile = false;
  estimationid: number;
  constructor(
    private _newEstimateService: NewEstimateService,
    private utils: UtilsService,
    private appStoreService: AppStoreService,
    private activatedRoute: ActivatedRoute
  ) {}
  ngOnInit() {
    this._newEstimateService.projectDetails.subscribe(
      (res) =>{
        if(res){
          this.project = res;
        }
        else {
         
        } 
      }
     )
    this.appStoreService.isMobile.subscribe(res => {
      this.isMobile = res;
    });
    this.activatedRoute.params.subscribe(params => {
      if (params["id"]) {
        this.estimationid = parseInt(params["id"]);
        this._newEstimateService
          .findEstimationById(this.estimationid)
          .subscribe(res => {
            // this.project = res;
            this._newEstimateService.setProjectSelectedDetails(res);
          });
      } else {
        this.estimationid = parseInt(this.utils.getLocal("project").id);
        this._newEstimateService
          .findEstimationById(this.estimationid)
          .subscribe(res => {
            this._newEstimateService.setProjectSelectedDetails(res);
          });
      }
    });
    this._newEstimateService.estCount.subscribe(res => {
      this.estCount = res;
    });
  }
  ngAfterContentChecked(): void {
    this.project = this.utils.getLocal("project");
  }
  getChipClass(name) {
    if (name) return this.resourceTypeList.find(ele => ele.name === name).class;
  }
}
