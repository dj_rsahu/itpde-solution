import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter
} from "@angular/core";
import * as _ from "lodash";

import { environment } from "src/environments/environment";
import Handsontable from "handsontable";
import { ActivatedRoute, Router } from "@angular/router";
import { UtilsService } from "src/app/shared/services/utils/utils.service";
import { EstimatesService } from "../../../estimates/estimates.service";
import { NewEstimateService } from "../../new-estimate.service";
import { Chart } from "chart.js";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatTableDataSource, MatDialog, MatDialogConfig } from "@angular/material";
import * as moment from "moment";
import { timer, Observable } from "rxjs";
import { Meta } from "@angular/platform-browser";
import html2canvas from "html2canvas";
import { resolve } from "url";
import { AppStoreService } from "src/app/app-store.service";
import { ConfirmDialogComponent } from 'src/app/core/layout/confirm-dialog/confirm-dialog.component';
@Component({
  selector: "dj-summary",
  templateUrl: "./summary.component.html",
  styleUrls: ["./summary.component.scss"]
})
export class SummaryComponent implements OnInit {
  @ViewChild("hltable") hltable;
  @ViewChild("summaryImage") summaryImage: ElementRef;
  @Output() next = new EventEmitter();
  @Input() mode;
  chart: Chart;
  chartDisplayType: string = "pie";
  aprroveGroup = new FormControl("");
  approveUser = new FormControl("");
  approversDataSource: MatTableDataSource<any> = new MatTableDataSource();
  aproversAddedList: any = [];
  handsonArray: any = [];
  graphLabels: any;
  graphDataSet: any;
  estimationid: any;
  approveUserList = [];
  currentUser: any;
  viewMode: boolean = false;
  approverForm: FormGroup;
  isSubmit;
  baseUrl: string = environment.baseUrl;
  approveRejectMsgs= {
    approve:{
      actions: [
        "Cancel",
        "Ok",
        "Approval Confirmation"
        ,"This action will approve the estimate. Do you wish to continue?"
      ]
    },
    reWork:{
      actions: [
        "Cancel",
        "Ok",
        "Rework Request"
        ,"Are you sure you want to send the estimate back for Rework? "+
        "This action would reset all approvals and the status will change back to ‘Draft’"
      ]
    }
  }
  percentageRenderer = function(instance, td, row, col, prop, value) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (value && typeof value !== "string") {
      td.innerHTML = value + "%";
    }
  };
  dollarRenderer = function(instance, td, row, col, prop, value) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (value && typeof value !== "string") {
      td.innerHTML = "$" + value.toFixed(2);
    } else if (value === "#VALUE!") {
      td.innerHTML = 0;
    }
  };
  defaultValueRender = function(instance, td, row, col, prop, value) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
  };
  errorHandler = function(instance, td, row, col, prop, value) {
    Handsontable.renderers.NumericRenderer.apply(this, arguments);
    if (value === "#VALUE!") {
      td.innerHTML = 0;
    }
    // if(row === instance.countRows()-1){
    //   let percentTotal = 0;
    //   for (let index = 0; index < instance.countRows()-1; index++) {
    //     if(typeof instance.getDataAtCell(index, col) !== "string"){
    //       percentTotal = percentTotal + instance.getDataAtCell(index, col);
    //     }
    //   }
    //   td.innerHTML = percentTotal;
    // }
  };
  matDisplayColumns: string[] = [
    "approvaltype",
    "approverName",
    "createddate",
    "status",
    "updateddate"
  ];
  matColumnHeaders: string[] = [
    "Approval Type",
    "Approver Name",
    "Submitted On",
    "Status",
    "Action"
  ];
  emptyRow = {
    groupName: "",
    costperday: "",
    actualspent: "",
    contingency: "",
    Grand_Total: ""
  };
  columns= [
    {
      data: "groupName",
      editor: false
    },
    {
      data: "costperday",
      editor: false,
      type: "numeric",
      numericFormat: {
        pattern: "$0,0.00",
        culture: "en-US"
      },
      width: "120px"
    },
    {
      data: "actualspent",
      type: "numeric",
      numericFormat: {
        pattern: "$0,0.00",
        culture: "en-US" // this is the default culture, set up for USD
      },
      width: "120px"
    },
    {
      data: "contingency",
      type: "numeric",
      renderer: this.percentageRenderer,
      width: "120px"
    },
    {
      data: "Grand_Total",
      editor: false,
      type: "numeric",
      numericFormat: {
        pattern: "$0,0.00",
        culture: "en-US" // this is the default culture, set up for USD
      },
      renderer: this.errorHandler,
      width: "120px"
    }
  ];
  columnHeaders: string[] = [
    "Group",
    "Estimates",
    "Actual Spent",
    "Contingency(%)",
    "Grand Total"
  ];
  settings: Handsontable.GridSettings = {
    rowHeaders: false,
    dropdownMenu: false,
    columnSorting: true,
    colHeaders: this.columnHeaders,
    cells: function(row, col, prop) {
      var cellProperties = {};
      let rowsCount = this.instance.countRows();

      if (row === rowsCount - 1) {
        cellProperties["readOnly"] = true;
        cellProperties["className"] = "bg-light-green";
      }
      else{
        cellProperties["readOnly"] = false;
        cellProperties["className"] = "";
      }
      if (col === 1 || col === 2 || col === 3 || col === 4) {
        cellProperties["className"] = "htRight";
      }
      if(col===1||col===0||col===4){
        cellProperties["readOnly"] = true;
      }
      return cellProperties;
    },
    contextMenu: false,
    filters: true,
    formulas: true,
    copyPaste: true
  };
  chartTotalValue: any[];
  submissionStatus: any;
  approversAddLimit: boolean;
  estimationDetails: any;
  showCreatedEstimation: boolean;
  submitBtnStatus: boolean = false;
  isMobile: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private utils: UtilsService,
    private estimateService: EstimatesService,
    private newEstimateSummary: NewEstimateService,
    private _router: Router,
    private readonly meta: Meta,
    private appStoreService: AppStoreService,
    private dialog: MatDialog
  ) {}
  ngOnInit() {
    this.currentUser = this.utils.getLocal("user");
    this.appStoreService.isMobile.subscribe(res => {
      this.isMobile = res;
    });

    this.approverForm = new FormGroup({
      aprroveGroup: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      approveUser: new FormControl(
        { value: "", disabled: true },
        Validators.required
      )
    });
    this.setReadOnlyMode(false);
    if (this.mode === "view"|| this.mode ===  "approve") {
      this.setReadOnlyMode(true);
    }

    this.activatedRoute.params.subscribe(params => {
      if (params["id"]) {
        this.estimationid = parseInt(params["id"]);
        this.getEstimationSummary(this.estimationid);
      } else {
        this.estimationid = parseInt(this.utils.getLocal("project").id);
        this.getEstimationSummary(this.estimationid);
      }
    });
    this.approverForm.controls["aprroveGroup"].valueChanges.subscribe(res => {
      if (res) {
        this.estimateService.getUsersByRole(res).subscribe(res => {
          this.approveUserList = res;
        });
      }
    });
  }
  handleAfterChange(event) {
    let summaryItems = this.handsonArray;

    this.setGraphDataset(summaryItems, this.estimationid);
    this.setGraphLabels(summaryItems, this.estimationid);
    this.setGraphRowTotal(summaryItems.slice(0, summaryItems.length - 1));
    this.setChartType(this.chartDisplayType);
  }
  setGraphRowTotal(summaryItems) {
    this.chartTotalValue = [];
    let rowItemList = [];
    let total = 0;
    let estimationCost = 0;
    summaryItems.map(ele => {
      estimationCost = estimationCost + ele["costperday"];
      total =
        total +
        ele["costperday"] * ele["contingency"] * 0.01 +
        ele["costperday"] +
        ele["actualspent"];

      rowItemList.push(
        ele["costperday"] * ele["contingency"] * 0.01 +
          ele["costperday"] +
          ele["actualspent"]
      );
    });
    rowItemList.map((item, index) => {
      this.chartTotalValue.push((item / total) * 100);
    });
    this.newEstimateSummary.setCount(estimationCost);
    console.log("this.chartTotalValue", this.chartTotalValue);
    //console.log("this.total", total);
  }
  getEstimationById(estimationId) {
    this.newEstimateSummary.findEstimationById(estimationId).subscribe(res => {
      this.newEstimateSummary.setProjectSelectedDetails(res);
      this.utils.setLocal("project", res);
      this.estimationDetails = this.utils.getLocal("project");
      if (this.estimationDetails.createdby === this.currentUser.id) {
        this.showCreatedEstimation = true;
      } else {
        this.showCreatedEstimation = false;
      }
      this.setSubmitStatus();
    });
  }
  getEstimationSummary(estimateId) {
    this.newEstimateSummary.fetchEstimationSummary(estimateId).subscribe(
      res => {
        this.submissionStatus = String(res["isSubmit"]) === "true";
        if (this.submissionStatus) {
          this.setReadOnlyMode(this.submissionStatus);
        }
        this.getEstimationById(estimateId);
        if (res["highlevelEstTimelineList"]) {
          this.setGrandTotal(
            res["highlevelEstTimelineList"],
            this.estimationid
          );
        }

        if (res["estApprovalList"]) {
          this.aproversAddedList = res["estApprovalList"];
          this.setApproverDataSource(this.aproversAddedList);
        } else {
          this.setSubmitStatus();
        }
      },
      err => {}
    );
  }
  setReadOnlyMode(status) {
    if (status) {
      this.viewMode = status;
      if (this.mode !== "view") {
        this.viewMode = false;
      }
      this.settings.readOnly = status;
      this.settings.contextMenu = false;
      this.approverForm.controls["aprroveGroup"].disable();
      this.approverForm.controls["approveUser"].disable();
    } else {
      this.approverForm.controls["aprroveGroup"].enable();
      this.approverForm.controls["approveUser"].enable();
    }
  }
  setGrandTotal(highLevelList, estimateId) {
    this.handsonArray = highLevelList.map((ele, index) => {
      let size = index + 1;
      ele["Grand_Total"] =
        "=SUM(B" +
        (index + 1) +
        ":C" +
        (index + 1) +
        ") + B" +
        (index + 1) +
        "*D" +
        (index + 1) +
        "*0.01";
      if (ele["contingency"] === null) {
        ele["contingency"] = 0;
      }
      if (ele["actualspent"] === null) {
        ele["actualspent"] = 0;
      }

      //ele["estid"] = estimateId;
      return ele;
    });

    this.setSummaryTotal(this.handsonArray);
  }
  setSummaryTotal(tableList: any) {
    let rowItem = this.emptyRow;
    let size = tableList.length > 1 ? tableList.length : 1;
    rowItem["groupName"] = "Sub Total";
    rowItem["costperday"] = `=SUM(B1:B${size})`;
    rowItem["actualspent"] = `=SUM(C1:C${size})`;
    rowItem["contingency"] = `NA`;
    rowItem["Grand_Total"] = `=SUM(E1:E${size})`;
    this.handsonArray[size] = JSON.parse(JSON.stringify(this.emptyRow));
  }
  setGraphDataset(tableList, estimateId) {
    this.graphDataSet = tableList.map(item => {
      let total =
        item["costperday"] +
        item["actualspent"] +
        item["costperday"] * item["contingency"] * 0.01;

      return total;
    });
  }
  setGraphLabels(tableList, estimateId) {
    this.graphLabels = tableList.map(item => {
      item["groupName"];
      return item["groupName"];
    });
  }
  setChartType(chartType) {
    this.chartDisplayType = chartType;
    // const hotRef: Handsontable = this.hltable.getInstance();
    Chart.plugins.register(ChartDataLabels);
    let fixedColor = [
      "#FFAF51",
      "#588DD5",
      "#C6E579",
      "#8C6AC4",
      "#93B7E3",
      "#FF6347",
      "#9BCA63",
      "#FCCE10",
      "#A092F1",
      "#bf360c"
    ];

    if (this.chart) {
      this.chart.destroy();
    }

    this.chart = new Chart("canvas", {
      type: chartType,
      data: {
        labels: this.graphLabels.slice(0, this.graphLabels.length - 1),
        datasets: [
          {
            data: this.graphDataSet.slice(0, this.graphDataSet.length - 1),
            backgroundColor: fixedColor,
            fill: true
          }
        ]
      },
      options: {
        responsive: true,
        layout: {
          padding: {
            top: 15,
            bottom: 12
          }
        },
        legend: {
          display:
            this.chartDisplayType === "pie"? true : false,
          position: "right",
          labels: {
            boxWidth: 10,
            fontSize: 10,
            padding:2
          }
        },
        tooltips: {
          enabled: true,
          callbacks: {
            label: (tooltipItem, data) => {
              let item = data.datasets[0].data[tooltipItem.index];
              return this.utils.currencyFormatter(item);
            }
          }
        },
        plugins: {
          datalabels: {
            anchor: this.chartDisplayType === "pie" ? "end" : "end",
            offset: this.chartDisplayType === "pie" ? 0 : -4,
            align: this.chartDisplayType === "pie" ? "end" : "top",
            formatter: (value, context) => {
              return this.chartTotalValue[context.dataIndex].toFixed(1) + "%";
            },
            color: "black"
          }
        }
      }
    });
  }
  addApprovers() {
    let idx = this.aproversAddedList.findIndex(
      item =>
        item.approvaltype === this.approverForm.controls["aprroveGroup"].value
    );
    if (idx === -1) {
      let approver = this.approverForm.controls["approveUser"].value;
      this.aproversAddedList.push({
        approvaltype: this.approverForm.controls["aprroveGroup"].value,
        approverName: approver.firstName + " " + approver.lastName,
        createddate: moment(new Date()).format("DD-MMM-YYYY"),
        status: "",
        updateddate: "",
        estId: this.estimationid,
        approverid: approver.id
      });
      this.setApproverDataSource(this.aproversAddedList);
    }
  }
  openDialog(data):Observable<any> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = data;
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    return dialogRef.afterClosed()
  }
  approveSummary(index, approveType) {
    let payLoad = JSON.parse(
      JSON.stringify(this.approversDataSource.data[index])
    );
    payLoad["status"] = approveType;
    delete payLoad["createddate"];
    delete payLoad["updateddate"];
    this.openDialog(this.approveRejectMsgs.approve).subscribe(res => {
      if (res) {
        this.setApproveOrRejectEmail(payLoad, approveType);
      }
    }); 
  }
  rejectSummary(index, approveType) {
    let payLoad = JSON.parse(
      JSON.stringify(this.approversDataSource.data[index])
    );
    payLoad["status"] = approveType;
    delete payLoad["createddate"];
    delete payLoad["updateddate"];
    this.openDialog(this.approveRejectMsgs.reWork).subscribe(res => {
      if (res) {
        this.setApproveOrRejectEmail(payLoad, approveType);
      }
    });
  }
  setApproveOrRejectEmail(payLoad, approveStatus) {
    this.newEstimateSummary
      .approveOrRejectEstimateApproval(payLoad)
      .subscribe(res => {
        this.getEstimationSummary(this.estimationid);
        setTimeout(() => {
          this.setEmailAttachement().then(
            data => {
              let payLoad = {
                approverType: res["approvaltype"],
                emailContent: data,
                estId: this.estimationid,
                statusType: approveStatus,
                approverId: this.findCurrentApprovalId(res["approvaltype"]),
                hyperLink:this.baseUrl.slice(0,-3) +'#'+ '/editEstimates/'+this.estimationid + '/approve/5'
              };
              this.newEstimateSummary
                .summaryEmail(payLoad)
                .subscribe(status => {
                  this._router.navigateByUrl("AllEstimates");
                });
            },
            err => {}
          );
        }, 300);
      });
  }
  findCurrentApprovalId(approvaltype) {
    let index = this.aproversAddedList.findIndex(ele => {
      return ele.approvaltype === approvaltype;
    });
    return this.aproversAddedList[index].approverid;
  }
  setEmailAttachement(): Promise<any> {
    return new Promise((resolve, reject) => {
      timer(500).subscribe(() => {
        this.meta.removeTag('name="viewport"');
        this.meta.addTag({ name: "viewport", content: "width=1440" });
      });
      setTimeout(() => {
        let summaryPage = this.summaryImage.nativeElement;
        html2canvas(summaryPage).then(
          canvas => {
            resolve(canvas.toDataURL());
            timer(500).subscribe(() => {
              this.meta.removeTag('name="viewport"');
              this.meta.addTag({
                name: "viewport",
                content: "width=device-width"
              });
            });
          },
          err => {
            reject();
          }
        );
      }, 1000);
    });
  }
  setApproverDataSource(dataSource) {
    if (dataSource) {
      this.approversAddLimit = dataSource.length >= 2 ? true : false;
      if (this.approversAddLimit) {
        this.approverForm.controls["aprroveGroup"].disable();
        this.approverForm.controls["approveUser"].disable();
      }

      dataSource.map(ele => {
        ele["createddate"] = moment(ele["createddate"]).format("DD-MMM-YYYY");
        ele["updateddate"] = ele["updateddate"]
          ? moment(ele["updateddate"]).format("DD-MMM-YYYY")
          : "";
        return ele;
      });
      this.approversDataSource = new MatTableDataSource(dataSource);
    } else {
      this.approversDataSource = new MatTableDataSource([]);
      this.approversAddLimit = false;
      this.approverForm.controls["aprroveGroup"].enable();
      this.approverForm.controls["approveUser"].enable();
    }
    this.setSubmitStatus();
  }
  setSubmitStatus() {
    if (this.currentUser.role !== "Admin" && this.currentUser.role !== "User") {
      this.submitBtnStatus = true;
    }

    // if (!this.showCreatedEstimation) {
    //   this.submitBtnStatus = true;
    // }
    if (this.approversDataSource && this.handsonArray) {
      if (
        this.approversDataSource.data.length >= 2 &&
        this.handsonArray.length > 0
      ) {
        this.submitBtnStatus = false;
      } else {
        this.submitBtnStatus = true;
      }
    }
  }
  submit(val) {
    let approvallList = JSON.parse(JSON.stringify(this.aproversAddedList));
    var payLoad = {
      highlevelEstTimelineList: this.handsonArray.slice(
        0,
        this.handsonArray.length - 1
      ),
      estApprovalList: approvallList.map(item => {
        delete item["createddate"];
        delete item["updateddate"];
        return item;
      }),
      isSubmit: val
    };
    this.newEstimateSummary.saveEstimationSummary(payLoad).subscribe(
      res => {
        this.getEstimationSummary(this.estimationid);
        this.setEmailAttachement().then(
          data => {
            let payLoad = {
              approverType: "",
              emailContent: data,
              estId: this.estimationid,
              statusType: "Submit",
              hyperLink:this.baseUrl.slice(0,-3) +'#'+ '/editEstimates/'+this.estimationid + '/approve/5'
            };
            this.newEstimateSummary.summaryEmail(payLoad).subscribe(status => {
              this._router.navigateByUrl("AllEstimates");
            });
          },
          err => {}
        );
      },
      err => {}
    );
  }
  saveSummary(val) {
    let approvallList = JSON.parse(JSON.stringify(this.aproversAddedList));
    var payLoad = {
      highlevelEstTimelineList: this.handsonArray.slice(
        0,
        this.handsonArray.length - 1
      ),
      estApprovalList: approvallList.map(item => {
        delete item["createddate"];
        delete item["updateddate"];
        return item;
      }),
      isSubmit: val,
      estSummaryContent: ""
    };
    this.newEstimateSummary.saveEstimationSummary(payLoad).subscribe(
      res => {
        this._router.navigateByUrl("AllEstimates");
      },
      err => {}
    );
  }
  handleAfterColumnSort(event) {
    this.setSummaryTotal(this.handsonArray);
  }
  handleBeforeColumnSort(event) {
    this.handsonArray.pop();
  }
  back() {
    this.next.emit(4);
  }
}
