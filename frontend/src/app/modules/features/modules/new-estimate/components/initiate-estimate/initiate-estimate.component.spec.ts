import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitiateEstimateComponent } from './initiate-estimate.component';

describe('InitiateEstimateComponent', () => {
  let component: InitiateEstimateComponent;
  let fixture: ComponentFixture<InitiateEstimateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitiateEstimateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitiateEstimateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
