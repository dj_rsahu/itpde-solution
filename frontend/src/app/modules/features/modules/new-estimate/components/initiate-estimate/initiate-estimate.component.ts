import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatSnackBar,
  MatDialogConfig,
  MatDialog,
  MatStepper
} from "@angular/material";
import { ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {
  ESTIMATE_CONSTANTS,
  CREATE_ESTIMATE
} from "src/app/configs/constants/APP_CONSTANTS";
import { NewEstimateService } from "../../new-estimate.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmDialogComponent } from "src/app/core/layout/confirm-dialog/confirm-dialog.component";
import { startWith, map } from "rxjs/operators";
import { EstimatesService } from "../../../estimates/estimates.service";
import { Observable } from "rxjs";
import { CustomErrorStateMatcher } from "src/app/shared/other/custom-error-state-matcher";
import { UtilsService } from "src/app/shared/services/utils/utils.service";
import { FeaturesService } from 'src/app/modules/features/features.service';
@Component({
  selector: "dj-initiate-estimate",
  templateUrl: "./initiate-estimate.component.html",
  styleUrls: ["./initiate-estimate.component.scss"]
})
export class InitiateEstimateComponent implements OnInit {
  @Output() djStepNext = new EventEmitter();
  @Input() mode;
  initialEstimateFormGroup: FormGroup;
  estimationStatus = CREATE_ESTIMATE.ESTIMATE_STATUS.draft;
  domainsList = [];
  userListFilter: Observable<string[]>;
  ProjectStatusList = [];
  DataRows: any[] = [];
  errorState = false;
  errorMatcher = new CustomErrorStateMatcher();
  displayColumnConfig = [
    { colName: "doc_title", mode: "all" },
    { colName: "doc_version", mode: "all" },
    { colName: "doc_location", mode: "all" },
    { colName: "actions", mode: "edit" }
  ];
  displayedColumns = [];
  columnHeaders: string[] = [
    "Document Title",
    "Version",
    "Document Location",
    "Actions"
  ];
  dataSource: MatTableDataSource<any[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  projectDetails;
  projectLeadList: any = [];
  isDocumentUpdate: boolean;
  documentRowIndex: any;
  disableBtn: boolean;
  message: string;
  editmode: boolean;

  documentDisabled: boolean = true;

  constructor(
    private newEstimationService: NewEstimateService,
    private _estimationService: EstimatesService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private utils: UtilsService,
    private _featuresService: FeaturesService
  ) {}

  ngOnInit() {
    this.displayedColumns = this.displayColumnConfig.map(ele => {
      return ele.colName;
    });
    this.getDomains();
    this.getProjectStatusList();
    this.isDocumentUpdate = false;
    this.disableBtn = true;
    this.initialEstimateFormGroup = new FormGroup({
      domainid: new FormControl("", Validators.required),
      est_project_name: new FormControl("", Validators.required),
      project_lead: new FormControl("", Validators.required),
      project_phase: new FormControl("", Validators.required),
      projectStatusOthers: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      durationInWeek: new FormControl("", Validators.required),
      estimationDocuments: new FormGroup({
        doc_title: new FormControl(""),
        doc_version: new FormControl(""),
        doc_location: new FormControl("")
      })
    });
    this.projectDetails = this.utils.getLocal("project");
    console.log(this.projectDetails);
    if (this.projectDetails) {
      setTimeout(() => {this.setProjectDetails(this.projectDetails)}, 1000)
    } else {
      this.activatedRoute.params.subscribe(params => {
        if (params["id"]) {
          this.newEstimationService
            .findEstimationById(params["id"])
            .subscribe(res => {
              this.newEstimationService.setProjectSelectedDetails(res);
              this.projectDetails = res;
              this.utils.setLocal("project", this.projectDetails);
              setTimeout(() => {this.setProjectDetails(this.projectDetails)}, 1000)
            });
        } else {
        }
      });
    }
    this.initialEstimateFormGroup
      .get("project_phase")
      .valueChanges.subscribe(res => {
        if (res) {
          if (res.id === 0) {
            this.initialEstimateFormGroup.controls[
              "projectStatusOthers"
            ].enable();
          } else {
            this.initialEstimateFormGroup.controls[
              "projectStatusOthers"
            ].disable();
          }
        }
      });
    const title = <FormControl>(
      this.initialEstimateFormGroup.get("estimationDocuments.doc_title")
    );
    title.valueChanges.subscribe(res => {
      if (title.value) {
        this.documentDisabled = false;
      } else {
        this.documentDisabled = true;
      }
    });
    const version = <FormControl>(
      this.initialEstimateFormGroup.get("estimationDocuments.doc_version")
    );
    // version.valueChanges.subscribe(res => {
    //   if (title.value && version.value && location.value) {
    //     this.documentDisabled = false;
    //   } else {
    //     this.documentDisabled = true;
    //   }
    // });
    // const location = <FormControl>(
    //   this.initialEstimateFormGroup.get("estimationDocuments.doc_location")
    // );
    // location.valueChanges.subscribe(res => {
    //   if (title.value && version.value && location.value) {
    //     this.documentDisabled = false;
    //   } else {
    //     this.documentDisabled = true;
    //   }
    // });

    this.dataSource = new MatTableDataSource(this.DataRows);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.userListFilter = this.initialEstimateFormGroup
      .get("project_lead")
      .valueChanges.pipe(
        startWith(""),
        map(value => this._filter(value))
      );
  }
  private _filter(value: string): any[] {
    if (value) {
      if (value.length > 1) {
        this._estimationService.searchUser(value).subscribe(data => {
          this.projectLeadList = data;
          if (this.projectLeadList.length > 0) {
          } else {
            this.disableBtn = true;
            this.message = "Please select valid user";
          }
        });
      } else {
        this.projectLeadList = [];
      }
    } else {
      this.projectLeadList = [];
    }
    return this.projectLeadList;
  }
  getDomains() {
    this.newEstimationService.getDomains().subscribe(res => {
      this.domainsList = res;
    });
  }
  getProjectStatusList() {
    this.newEstimationService.getProjectStatus().subscribe(res => {
      this.ProjectStatusList = res;
      this.ProjectStatusList.push({ id: 0, name: ESTIMATE_CONSTANTS.other });
    });
  }
  setProjectDetails(res) {
    if (this.projectDetails && this.projectDetails.id) {
      this.estimationStatus = res["estmation_status"];
      let projectStatusIndex = this.ProjectStatusList.findIndex(
        item => item.id === res["projectphaseid"]
      );
      this.initialEstimateFormGroup.controls["domainid"].setValue(
        res["domainid"]
      );
      this.initialEstimateFormGroup.controls["est_project_name"].setValue(
        res["est_project_name"]
      );
      this.initialEstimateFormGroup.controls["project_lead"].setValue(
        res["projectLeadUser"]
      );
      this.initialEstimateFormGroup.controls["project_phase"].patchValue(
        this.ProjectStatusList[projectStatusIndex]
      );
      this.initialEstimateFormGroup.controls["durationInWeek"].setValue(
        res["durationInWeek"]
      );
      this.initialEstimateFormGroup.controls["projectStatusOthers"].setValue(
        res["projectStatusOthers"]
      );

      if (this.mode === "view" || this.mode === "approve") {
        this.initialEstimateFormGroup.controls["domainid"].disable();
        this.initialEstimateFormGroup.controls["est_project_name"].disable();
        this.initialEstimateFormGroup.controls["project_lead"].disable();
        this.initialEstimateFormGroup.controls["project_phase"].disable();
        this.initialEstimateFormGroup.controls["durationInWeek"].disable();
        this.initialEstimateFormGroup.controls["projectStatusOthers"].disable();
      }
      this.setDataSource(res["estimationDocuments"]);
    }
  }
  setDataSource(rows) {
    this.DataRows = rows;
    this.dataSource = new MatTableDataSource(this.DataRows);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  saveEstimate() {
    
    let payLoad = this.initialEstimateFormGroup.value;
    let projectLead = this.initialEstimateFormGroup.controls["project_lead"]
      .value;
    let projectPhase = this.initialEstimateFormGroup.controls["project_phase"]
      .value;
    payLoad["estmation_status"] = CREATE_ESTIMATE.ESTIMATE_STATUS.draft;
    payLoad["project_lead"] = `${projectLead["id"]}`;
    payLoad["project_phase"] = projectPhase["id"];
    payLoad["estimationDocuments"] = this.DataRows;
    this.newEstimationService.saveEstimate(payLoad).subscribe(
      res => {
        // this.initialEstimateFormGroup.reset();
        this.projectDetails = res;
        this._featuresService
          .setEstimationEditMode(this.projectDetails.id, 0)
          .subscribe(res => {});
        
        this.utils.setLocal("project", this.projectDetails);
        //  this.reload();
        this.newEstimationService.setProjectSelectedDetails(res);
        this.snackBar.open("Intial Estimate Sheet updates successfully", "DISMISS", {
          duration: 10000,
          verticalPosition: "top", // 'top' | 'bottom'
          horizontalPosition: "center",
          panelClass: ["success-snackbar"]
        });
        this.djStepNext.emit();
      },
      err => {}
    );
  }
  updateEstimate() {
    let payLoad = this.initialEstimateFormGroup.value;
    let projectLead = this.initialEstimateFormGroup.controls["project_lead"]
      .value;
    let projectPhase = this.initialEstimateFormGroup.controls["project_phase"]
      .value;
    // payLoad["estmation_status"] = CREATE_ESTIMATE.ESTIMATE_STATUS.draft;
    payLoad["project_lead"] = `${projectLead["id"]}`;
    payLoad["project_phase"] = projectPhase["id"];
    payLoad["estimationDocuments"] = this.DataRows;
    payLoad["id"] = this.projectDetails.id;
    this.newEstimationService.updateEstimate(payLoad).subscribe(res => {
      this.utils.setLocal("project", res);
      this.projectDetails = res;
      setTimeout(() => {this.setProjectDetails(res)}, 1000)
      this.snackBar.open("Intial Estimate Sheet updates successfully", "DISMISS", {
        duration: 10000,
        verticalPosition: "top",
        horizontalPosition: "center",
        panelClass: ["success-snackbar"]
      });
      this.djStepNext.emit();
    });
  }
  addDocuments() {
    let item = this.initialEstimateFormGroup.value;
    this.DataRows.push({
      doc_title: item["estimationDocuments"].doc_title,
      doc_version: item["estimationDocuments"].doc_version,
      doc_location: item["estimationDocuments"].doc_location
    });
    this.resetDocument();
    this.setDataSource(this.DataRows);
  }
  resetDocument() {
    this.initialEstimateFormGroup.controls["estimationDocuments"].setValue({
      doc_title: "",
      doc_version: "",
      doc_location: ""
    });
    this.isDocumentUpdate = false;
  }
  updateDocuments() {
    let row = this.DataRows[this.documentRowIndex];
    let documents = this.initialEstimateFormGroup.controls[
      "estimationDocuments"
    ].value;
    row["doc_title"] = documents["doc_title"];
    row["doc_version"] = documents["doc_version"];
    row["doc_location"] = documents["doc_location"];
    this.DataRows[this.documentRowIndex] = row;
    this.setDataSource(this.DataRows);
    this.resetDocument();
  }
  editDocument(row, index) {
    this.isDocumentUpdate = true;
    this.documentRowIndex = index;
    this.initialEstimateFormGroup.controls["estimationDocuments"].setValue({
      doc_title: row["doc_title"],
      doc_version: row["doc_version"],
      doc_location: row["doc_location"]
    });
  }
  deleteDocument(row, index) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: ["Cancel", "Delete", "Delete Confirmation","This action will delete the document reference. Do you wish to continue?"]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.DataRows.splice(index, 1);
        this.setDataSource(this.DataRows);
      } else {
      }
    });
  }
  cancel() {
    this.router.navigate(["AllEstimates"]);
  }
  rest() {
    this.initialEstimateFormGroup.reset();
  }
  reload() {
    this.getProjectStatusList();
  }
  ownerFn = user => {
    this.disableBtn = false;
    return user ? user.firstName + " " + user.lastName : undefined;
  };
  getIndex(res) {
    let projectStatusIndex = this.ProjectStatusList.findIndex(
      item => item.id === res["projectphaseid"]
    );
    return projectStatusIndex;
  }
  getStorageItem() {}
  setStorageItem;
}
