import {
  Component,
  OnInit,
  OnChanges,
  AfterContentChecked,
  ViewChild,
  Output,
  EventEmitter,
  Input
} from "@angular/core";
import Handsontable from "handsontable";
import { NewEstimateService } from "../../new-estimate.service";
import { UtilsService } from "src/app/shared/services/utils/utils.service";
import { AppStoreService } from "src/app/app-store.service";
import { MatDialogConfig, MatDialog, MatSnackBar } from "@angular/material";
import { ConfirmDialogComponent } from "src/app/core/layout/confirm-dialog/confirm-dialog.component";

@Component({
  selector: "dj-hl-timelines-resources",
  templateUrl: "./hl-timelines-resources.component.html",
  styleUrls: ["./hl-timelines-resources.component.scss"]
})
export class HlTimelinesResourcesComponent
  implements OnInit, AfterContentChecked, OnChanges {
  @Output() djStepNext = new EventEmitter();
  @Input() mode;
  data = [];
  doubleClick = false;
  weeks = 10;
  @ViewChild("hltable") hltable;
  percentageValidator = (value, callback) => {
    setTimeout(function() {
      if (value >= 0 && value <= 100) {
        callback(true);
      } else {
        callback(false);
      }
    }, 500);
  };
  underscoreRenderer = function(instance, td, row, col, prop, value) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (value) {
      td.innerHTML = value.substring(0, value.lastIndexOf("_"));
    }
    Handsontable.renderers.AutocompleteRenderer.apply(this, arguments);
    if (value) {
      td.innerHTML = value.substring(0, value.lastIndexOf("_"));
    }
  };
  minValZeroValidator = (value, callback) => {
    setTimeout(function() {
      if (value >= 0) {
        callback(true);
      } else {
        callback(false);
      }
    }, 500);
  };
  daysRenderer = (instance, td, row, col, prop, value) => {
    //  Handsontable.renderers.TextRenderer.apply(this, [instance, td, row, col, prop, value]);
    if (typeof value === "string" || value === null) {
      td.innerHTML = 0;
    } else {
      td.innerHTML = value.toFixed(1);
    }
    if (row === instance.countRows() - 1) {
      td.innerHTML = this.getTotal(instance, col).toFixed(1);
    }
  };
  weekDollarRender = (instance, td, row, col, prop, value) => {
    // Handsontable.renderers.TextRenderer.apply(this, [instance, td, row, col, prop, value]);
    if (value === "#VALUE!" || value === "#REF!") {
      td.innerHTML = this.utils.currencyFormatter(0);
    } else {
      td.innerHTML = this.utils.currencyFormatter(value);
    }
    if (row === instance.countRows() - 1) {
      td.innerHTML = this.utils.currencyFormatter(this.getTotal(instance, col));
    }
  };
  percentageRenderer = function(
    instance: Handsontable,
    td,
    row,
    col,
    prop,
    value
  ) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (typeof value === "string") {
      td.innerHTML = 0;
    } else if (value && row !== instance.countRows() - 1) {
      td.innerHTML = value.toFixed(0) + "%";
    } else if (row === instance.countRows() - 1) {
      let percentTotal = 0;

      for (let index = 0; index < instance.countRows() - 1; index++) {
        if (typeof instance.getDataAtCell(index, col) !== "string") {
          percentTotal = percentTotal + instance.getDataAtCell(index, col);
        }
      }
      td.innerHTML = (percentTotal * 0.01).toFixed(2);
    }
  };
  totalRenderer = (instance, td, row, col, prop, value) => {
    // Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (value) {
    }
  };
  dollarRenderer = function(instance, td, row, col, prop, value) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (value === "#VALUE!") {
      td.innerHTML = 0;
    }
  };
  groupRenderer = function(
    instance,
    td,
    row,
    col,
    prop,
    value,
    cellProperties
  ) {
    if (value) {
      Handsontable.renderers.AutocompleteRenderer.apply(this, arguments);
      td.style.background = "#007833";
    }
  };
  columns = [];
  colHeaders = [];
  formulaeCells;
  settings: Handsontable.GridSettings = {
    rowHeaders: false,
    columnSorting: true,
    filter: true,
    dropdownMenu: false,
    mergeCells: true,
    contextMenu: [
      "row_above",
      "row_below",
      "remove_row",
      "clear_column",
      "undo",
      "redo",
      "cut",
      "copy"
    ],
    outsideClickDeselects: false,
    cells: (row, col, prop) => {
      var cellProperties = {};
      const hotRef: Handsontable = this.hltable
        ? this.hltable.getInstance()
        : undefined;
      if (hotRef) {
        let rowCount = hotRef.countRows();
        let colCount = hotRef.countCols();
        cellProperties["className"] = "htRight";
        if (row === rowCount - 1) {
          cellProperties["readOnly"] = true;
        } else {
          cellProperties["readOnly"] = false;
        }
        if (col > colCount - 5) {
          cellProperties["readOnly"] = true;
          cellProperties["className"] = "htRight text-body";
        }
        if (col === colCount - 1) {
          cellProperties["readOnly"] = true;
          cellProperties["className"] = "font-weight-bold htRight text-body";
        }
        if (col === 1 || col === 2 || col === 3 || col === 4) {
          cellProperties["className"] = "htLeft";
        }
        if (this.mode === "view" || this.mode === "approve") {
          cellProperties["readOnly"] = true;
        }
      }
      return cellProperties;
    },
    filters: true,
    hiddenColumns: {
      columns: [0],
      indicators: false
    },
    formulas: true,
    copyPaste: true
  };
  emptyRow = {
    systems: "",
    costperday: 0,
    highlevelestweeks: [],
    group: {
      groupname: ""
    },
    roleNameId: "",
    resourceNameId: ""
  };
  apiData;
  hotRef: Handsontable;
  projectDetails;
  isMobile = false;
  constructor(
    private _newEstimatesService: NewEstimateService,
    private snackBar: MatSnackBar,
    private utils: UtilsService,
    private appStoreService: AppStoreService,
    private dialog: MatDialog
  ) {}
  ngOnInit() {
    this.appStoreService.isMobile.subscribe(res => {
      this.isMobile = res;
    });
    if (this.mode === "view" || this.mode === "approve") {
      this.settings.readOnly = true;
      this.settings.contextMenu = false;
    }
    this.projectDetails = this.utils.getLocal("project");
    this.weeks = this.projectDetails.durationInWeek;
    this.getHl();
  }
  totalsRenerer(this, instance, td, row, col, prop, value) {
    Handsontable.renderers.TextRenderer.apply(this, [
      instance,
      td,
      row,
      col,
      prop,
      value
    ]);
    if (typeof value === "string" || value === null) {
      td.innerHTML = 0;
    } else {
      td.innerHTML = value.toFixed(1);
    }
    if (row === instance.countRows() - 1) {
      td.innerHTML = this.getTotal(instance, col).toFixed(1);
    }
  }
  getHl() {
    const cols: Handsontable.ColumnSettings[] = [
      {
        data: "id",
        type: "numeric"
      },
      {
        data: "groupName",
        type: "autocomplete",
        filter: true,
        strict: false,
        trimDropdown: false
      },
      {
        data: "role",
        type: "autocomplete",
        filter: true,
        strict: false,
        renderer: this.underscoreRenderer,
        trimDropdown: false
      },
      {
        data: "resource",
        type: "autocomplete",
        filter: true,
        renderer: this.underscoreRenderer,
        trimDropdown: false,
        source: [],
        strict: false
      },

      { data: "systems" },
      {
        data: "weeks",
        editor: false,
        validator: this.minValZeroValidator,
        renderer: this.daysRenderer,
        width: "90px"
      },
      {
        data: "days",
        editor: false,
        renderer: this.daysRenderer,
        width: "70px"
      },
      {
        data: "costperday",
        editor: false,
        type: "numeric",
        numericFormat: {
          pattern: "$0,0.00",
          culture: "en-US"
        },
        width: "100px"
      },
      {
        data: "subtotal",
        editor: false,
        renderer: this.weekDollarRender,
        width: "120px"
      }
    ];
    const headers = [
      "id",
      "Group",
      "Role",
      "Resources",
      "Details",
      "Total Weeks",
      "Days",
      "Cost Per Day",
      "Sub Total"
    ];
    this.apiData = [];
    this.data = [];
    this.columns = [...cols];
    this.colHeaders = JSON.parse(JSON.stringify(headers));
    this.settings.colHeaders = this.colHeaders;
    this.formulaeCells = {
      sumStart: this.numToAlpha(6),
      sumEnd: this.numToAlpha(this.weeks + 5),
      weeks: this.numToAlpha(this.weeks + 6),
      days: this.numToAlpha(this.weeks + 7),
      cost: this.numToAlpha(this.weeks + 8),
      subtotal: this.numToAlpha(this.weeks + 9)
    };
    if (this.projectDetails.id) {
      this._newEstimatesService
        .getHighlevelesttimeline(this.projectDetails.id)
        .subscribe((response: any[]) => {
          if (response.length > 0) {
            this.apiData = response.map(ele => {
              ele.groupName = ele.group ? ele.group.groupname : "";
              ele.role = ele.roleNameId;
              ele.resource = ele.resourceNameId;
              return ele;
            });
            if (this.apiData.length < 9) {
              for (let i = this.apiData.length; i < 9; i++) {
                this.apiData.push(JSON.parse(JSON.stringify(this.emptyRow)));
              }
            }
          } else {
            for (let i = 0; i < 9; i++) {
              this.apiData.push(JSON.parse(JSON.stringify(this.emptyRow)));
            }
            this.apiData.forEach(ele => {
              ele["groupName"] = ele.group.groupname;
              ele["role"] = ele.roleNameId;
              ele["resource"] = ele.resourceNameId;
              return ele;
            });
          }
          this.getColumnsAndHeadings();
          this._newEstimatesService.getRrdGroups().subscribe(res => {
            this.columns[1].source = res.map(group => {
              return group["groupname"];
            });
            // console.log(this.columns[1]);
            this.data = this.getTableData(this.apiData);
            this.getSumaryTableData();
          });
        });
    }
  }
  getSumaryTableData() {
    let summaryRow = {
      groupName: "Grand Total"
    };
    this.data.push(this.utils.clone(summaryRow));
  }
  getTotal(instance, col) {
    let percentTotal = 0;
    instance.getDataAtProp(col).forEach(ele => {
      if (ele && typeof ele !== "string") {
        percentTotal = percentTotal + ele;
      }
    });
    return percentTotal;
  }
  ngAfterContentChecked() {}
  ngOnChanges() {}
  handleAfterChange(event) {
    const hotRef: Handsontable = this.hltable
      ? this.hltable.getInstance()
      : undefined;

    if (event.source === "edit" && this.mode !== "view") {
      let count = hotRef
        ? hotRef.getSourceDataAtCell(
            hotRef.countRows() - 1,
            hotRef.countCols() - 1
          )
        : null;
      this._newEstimatesService.setCount(
        this.getTotal(hotRef, hotRef.countCols() - 1)
      );

      if (event.changes[0][1] === "groupName") {
        hotRef.setDataAtCell(event.changes[0][0], 2, null);
        hotRef.setDataAtCell(event.changes[0][0], 3, null);
      }
      if (event.changes[0][1] === "role") {
        hotRef.setDataAtCell(event.changes[0][0], 3, null);
      }
      if (event.changes[0][1] === "resource") {
        var rrdTransID = event.changes[0][3].split("_");
        // console.log(rrdTransID)
        if (rrdTransID && rrdTransID[rrdTransID.length - 1] !== "") {
          this._newEstimatesService
            .getRRDTransactionById(rrdTransID[rrdTransID.length - 1])
            .subscribe(res => {
              hotRef.setDataAtCell(
                event.changes[0][0],
                this.columns.length - 2,
                res["costperday"]
              );
            });
        }
      }
    }
    if (hotRef) {
      // hotRef.validateCells(valid => {});
    }
    this.data.pop();
    this.getSumaryTableData();
  }
  handleBeforeChange(event) {}
  handleAfterRender() {
    setTimeout(() => {
      const hotRef: Handsontable = this.hltable
        ? this.hltable.getInstance()
        : undefined;
      this._newEstimatesService.setCount(
        this.getTotal(hotRef, hotRef.countCols() - 1)
      );
    }, 1000);
  }
  handleBeforeOnCellMouseDown(event: Handsontable.wot.CellCoords) {
    const hotRef: Handsontable = this.hltable.getInstance();
    var now = new Date().getTime();
    if (
      !(
        hotRef.getCellMeta(event.row, event.col).lastClick &&
        now - hotRef.getCellMeta(event.row, event.col).lastClick < 300
      )
    ) {
      hotRef.setCellMeta(event.row, event.col, "lastClick", now);
      return;
    } else {
      // console.log("clicked");
      if (event.row !== hotRef.countRows() - 1) {
        if (event.col == 2 && this.mode !== "view") {
          if (
            this.hltable
              .getInstance()
              .getDataAtCell(event.row, event.col - 1) &&
            this.hltable
              .getInstance()
              .getDataAtCell(event.row, event.col - 1) != ""
          ) {
            this._newEstimatesService
              .getRrdGroupRoleByName(
                this.hltable
                  .getInstance()
                  .getDataAtCell(event.row, event.col - 1)
              )
              .subscribe(res => {
                hotRef.setCellMeta(
                  event.row,
                  event.col,
                  "source",
                  res.map(role => {
                    return role["role"] + "_" + role["rrdid"];
                  })
                );
                hotRef.selectCell(event.row, event.col);
                hotRef.getActiveEditor().beginEditing();
              });
          }
        }
        if (event.col == 3 && this.mode !== "view") {
          if (
            this.hltable
              .getInstance()
              .getDataAtCell(event.row, event.col - 1) &&
            this.hltable
              .getInstance()
              .getDataAtCell(event.row, event.col - 1) != ""
          ) {
            var groupID = this.hltable
              .getInstance()
              .getDataAtCell(event.row, event.col - 1)
              .split("_");
            if (groupID) {
              this._newEstimatesService
                .getRRDTransactionsByGroupId(groupID[groupID.length - 1])
                .subscribe(res => {
                  hotRef.setCellMeta(
                    event.row,
                    event.col,
                    "source",
                    res.map(resource => {
                      return (
                        resource["resourcename"] + "_" + resource["rrdtransid"]
                      );
                    })
                  );
                  hotRef.selectCell(event.row, event.col);
                  hotRef.getActiveEditor().beginEditing();
                });
            }
          }
        }
      }
    }
  }
  handleAfterCreateRow(event) {
    const hotRef: Handsontable = this.hltable.getInstance();
    const row = this.emptyRow;
    row["weeks"] =
      "=SUM(" +
      this.formulaeCells.sumStart +
      (event.index + 1) +
      ":" +
      this.formulaeCells.sumEnd +
      (event.index + 1) +
      ") *0.01";
    row["days"] = "=" + this.formulaeCells.weeks + (event.index + 1) + "*5";
    row["subtotal"] =
      "=" +
      this.formulaeCells.days +
      (event.index + 1) +
      "*" +
      this.formulaeCells.cost +
      (event.index + 1);
    // console.log(row);
    Object.keys(row).forEach(ele => {
      if (ele.includes("week_")) {
        hotRef.setDataAtRowProp(event.index, ele, row[ele]);
      } else {
        hotRef.setDataAtRowProp(event.index, ele, row[ele]);
      }
    });
  }
  handleBeforeOnCellContextMenu(event) {
    const hotRef: Handsontable = this.hltable.getInstance();
    if (event.CellCoords.row + 1 === hotRef.countRows()) {
      event.event.stopImmediatePropagation();
    }
  }
  save(next?) {
    let dataTemp = this.utils.clone(this.data);
    // console.log("hl array", this.getHlArray());
    dataTemp.pop();
    const obj = {
      highlevelesttimelines: this.getHlArray(dataTemp)
    };
    this._newEstimatesService
      .saveHighlevelesttimeline(obj, this.projectDetails.id)
      .subscribe(res => {
        if (this.mode == "edit") {
          this.snackBar.open(
            "Highlevel timelines sheet updates successsfully",
            "DISMISS",
            {
              duration: 5000,
              verticalPosition: "top", // 'top' | 'bottom'
              horizontalPosition: "center",
              panelClass: ["success-snackbar"]
            }
          );
        } else {
          this.snackBar.open(
            "Highlevel timelines estimates saved successsfully",
            "DISMISS",
            {
              duration: 5000,
              verticalPosition: "top", // 'top' | 'bottom'
              horizontalPosition: "center",
              panelClass: ["success-snackbar"]
            }
          );
        }
        // console.log(this.projectDetails);
        this.projectDetails.durationInWeek = this.weeks;
        this._newEstimatesService
          .updateEstimate(this.projectDetails)
          .subscribe(res => {
            this.utils.setLocal("project", res);
            this.projectDetails = res;
          });
        if (next) {
          this.djStepNext.emit();
        } else {
          this.getHl();
        }
      });
  }
  getHlArray(data) {
    // console.log("data", this.data);
    let tempArr = [];
    data.forEach(ele => {
      if (
        ele.resource &&
        ele.role &&
        ele.groupName &&
        ele.groupName !== "" &&
        ele.resource !== "" &&
        ele.role !== ""
      ) {
        var groupID = ele.role.split("_");
        var rrdTransID = ele.resource.split("_");
        let tempObj = {
          groupName: ele.groupName,
          resourceid: rrdTransID[rrdTransID.length - 1],
          systems: ele.systems,
          highlevelestweeks: [],
          id: ele.id,
          groupid: groupID[groupID.length - 1],
          costperday: ele.costperday
        };
        tempObj.highlevelestweeks = this.getWeeks(ele);
        // console.log("tempObj", tempObj);
        tempArr.push(tempObj);
      }
    });
    return tempArr;
  }
  getWeeks(ele) {
    return Object.keys(ele)
      .filter(str => {
        return str.includes("week_");
      })
      .map(str => {
        if (typeof ele[str].weekvalue === "string") {
          return { weekname: str, weekvalue: 0 };
        } else {
          return { weekname: str, weekvalue: ele[str].weekvalue };
        }
      });
  }
  getColumnsAndHeadings() {
    for (let i = 0; i < this.weeks; i++) {
      let weekColumn: Handsontable.ColumnSettings = {
        data: "week_" + i + ".weekvalue",
        type: "numeric",
        invalidCellClassNameString: "htInvalid",
        renderer: this.percentageRenderer,
        width: "60px"
      };
      this.columns.splice(-4, 0, weekColumn);
      this.colHeaders.splice(-4, 0, "WK " + (i + 1));
    }
  }
  getTableData(arrayOfRows) {
    return arrayOfRows.map((row, rowIndex) => {
      row.highlevelestweeks.forEach((week, index) => {
        row["week_" + index] = week;
      });
      for (let i = row.highlevelestweeks.length; i < this.weeks; i++) {
        row["week_" + i] = {
          weekname: 0,
          weekvalue: 0
        };
      }
      row["weeks"] =
        "=SUM(" +
        this.formulaeCells.sumStart +
        (rowIndex + 1) +
        ":" +
        this.formulaeCells.sumEnd +
        (rowIndex + 1) +
        ") *0.01";
      row["days"] = "=" + this.formulaeCells.weeks + (rowIndex + 1) + "*5";
      row["subtotal"] =
        "=" +
        this.formulaeCells.days +
        (rowIndex + 1) +
        "*" +
        this.formulaeCells.cost +
        (rowIndex + 1);
      return row;
    });
  }
  getSumRange(rowIndex, start, end) {
    return (
      "=SUM(" +
      this.numToAlpha(start) +
      rowIndex +
      ":" +
      this.numToAlpha(end) +
      rowIndex +
      ")"
    );
  }
  numToAlpha(num) {
    var alpha = "";

    for (var a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
      let myint = (num % b) / a;
      alpha = String.fromCharCode(myint + 65) + alpha;
    }
    return alpha;
  }
  addColumn() {
    this.weeks++;
    let data = {
      actions: [
        "Proceed",
        "Save & Proceed",
        "DO YOU WANT TO SAVE?",
        "This tab /page has unsaved data. Do you wish to save them?Your changes will be lost if you continue without saving",
        "Cancel"
      ]
    };
    this.openDialog(data);
    // this.getHl();
  }
  deleteColumn() {
    this.weeks--;
    let data = {
      actions: [
        "Proceed",
        "Save & Proceed",
        "DO YOU WANT TO SAVE?",
        "This tab /page has unsaved data. Do you wish to save them?Your changes will be lost if you continue without saving",
        "Cancel"
      ]
    };
    this.openDialog(data);
  }
  insertRow() {
    const hotRef: Handsontable = this.hltable.getInstance();
    const selectCell = hotRef.getSelected()
      ? hotRef.getSelected()
      : [[hotRef.countRows() - 1]];
    if (selectCell && selectCell[0][0] < hotRef.countRows() - 1) {
      hotRef.alter("insert_row", selectCell[0][0] + 1, 1);
    } else {
      this.snackBar.open("Select a valid row to insert row after", "DISMISS", {
        duration: 5000,
        verticalPosition: "top",
        horizontalPosition: "center",
        panelClass: ["warning-snackbar"]
      });
    }
  }
  removeRow() {
    const hotRef: Handsontable = this.hltable.getInstance();
    const selectCell = hotRef.getSelected();
    // console.log(selectCell);
    if (selectCell && selectCell[0][0] < hotRef.countRows() - 1) {
      hotRef.alter("remove_row", selectCell[0][0], 1);
    } else {
      this.snackBar.open("Select a row to remove", "DISMISS", {
        duration: 5000,
        verticalPosition: "top",
        horizontalPosition: "center",
        panelClass: ["warning-snackbar"]
      });
    }
  }
  openDialog(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = data;
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.save();
      } else if (res === false) {
        this.getHl();
      } else {
        // does nothing
      }
    });
  }
  handleAfterColumnSort(event) {
    this.getSumaryTableData();
    const hotRef: Handsontable = this.hltable.getInstance();
    hotRef.render();
  }
  handleBeforeColumnSort(event) {
    this.data.pop();
  }
  handleAfterFilter(conditionsStack) {
    // this.afterFilter.emit({ conditionsStack });
    // this.getSumaryTableData();
  }
  handleBeforeFilter(conditionsStack) {
    // this.beforeFilter.emit({ conditionsStack });
    // this.data.pop();
  }
  handleAfterRemoveRow(event) {
    // console.log(this.data.length);
    if (this.data.length === 1) {
      // this.data.unshift(this.utils.clone(this.emptyRow));
      // this.getSumaryTableData();
      const hotRef: Handsontable = this.hltable.getInstance();
      hotRef.alter("insert_row", 0, 1);
      hotRef.render();
    } else if (this.data.length === 0) {
      this.data.push(this.utils.clone(this.emptyRow));
      this.getSumaryTableData();
      const hotRef: Handsontable = this.hltable.getInstance();
      hotRef.render();
    }
  }
}
