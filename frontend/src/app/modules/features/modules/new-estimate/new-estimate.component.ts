import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  HostListener
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NewEstimateService } from "./new-estimate.service";
import { MatStepper } from "@angular/material";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog,
  MatDialogConfig
} from "@angular/material";
import * as moment from "moment";
import { ESTIMATE_CONSTANTS } from "src/app/configs/constants/APP_CONSTANTS";
import { ConfirmDialogComponent } from "src/app/shared/components/confirm-dialog/confirm-dialog.component";
import { FeaturesService } from "../../features.service";
import { UtilsService } from "src/app/shared/services/utils/utils.service";
import { AppStoreService } from "src/app/app-store.service";

@Component({
  selector: "dj-new-estimate",
  templateUrl: "./new-estimate.component.html",
  styleUrls: ["./new-estimate.component.scss"]
})
export class NewEstimateComponent implements OnInit, OnDestroy {
  @ViewChild("stepper") stepper: MatStepper;
  @ViewChild("stepOne") stepOne;
  projectId: any;
  isLinear = true;
  path;
  mode: any;
  stepIndex: any;
  projectDetails;
  @HostListener("window:beforeunload", ["$event"])
  doSomething($event) {
    $event.returnValue = "Your data will be lost!";
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private newEstimationService: NewEstimateService,
    private dialog: MatDialog,
    private router: Router,
    private _featuresService: FeaturesService,

    private utils: UtilsService,
    private _appStoreService: AppStoreService
  ) {}

  ngOnInit() {
    this._appStoreService.isHamburgerMenu.next(false);
    this.activatedRoute.params.subscribe(params => {
      this.projectId = params["id"]
        ? params["id"]
        : localStorage.getItem("projectId");

      this.mode = params["mode"] ? params["mode"] : "new";
      this.stepIndex = params["step"];
      if (this.projectId && this.stepIndex == 5) {
        this.newEstimationService
          .findEstimationById(params["id"])
          .subscribe(res => {
            this.newEstimationService.setProjectSelectedDetails(res);
            this.projectDetails = res;
            this.utils.setLocal("project", this.projectDetails);
          });
      }
      this.stepper.selectedIndex = this.stepIndex;
      if (this.mode === "new") {
        

        this.isLinear = true;
      } else {
        this.isLinear = false;
      }
      if (this.mode === "edit") {
        setTimeout(this.openSessionDialog, 25 * 60 * 1000);
      }
    });
  }

  continue(val) {
    this.stepper.selectedIndex = val;
  }
  djStepNext() {
    this.stepper.next();
  }
  ngOnDestroy() {
    this._appStoreService.isHamburgerMenu.next(true);
    localStorage.removeItem("project");
    this.newEstimationService.projectDetails.next(null);
    this.newEstimationService.estCount.next(0);
    this.newEstimationService.projectDetails.complete();
    console.log("destroyed");
    if (this.mode === "edit") {
      this._featuresService
        .setEstimationEditMode(this.projectId, 1)
        .subscribe(res => {});
    }
  }
  openSessionDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: [
        "Ignore",
        "Extend",
        "This session expires in 5 minutes. Do you want to extend?"
      ]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this._featuresService
          .increaseEstimationEditableDuration(this.projectId)
          .subscribe(res => {
            setTimeout(this.openSessionDialog, 25 * 60 * 1000);
          });
      } else {
        setTimeout(this.ignore, 1000 * 60 * 5);
      }
    });
  }
  ignore() {
    this.router.navigate(["/projects"]);
  }
}
