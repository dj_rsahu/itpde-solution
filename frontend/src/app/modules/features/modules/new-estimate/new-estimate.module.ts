import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NewEstimateRoutingModule } from "./new-estimate-routing.module";
import { NewEstimateComponent } from "./new-estimate.component";
import { InitiateEstimateComponent } from "./components/initiate-estimate/initiate-estimate.component";
import { HlTimelinesResourcesComponent } from "./components/hl-timelines-resources/hl-timelines-resources.component";
import { DetailedEstimatesComponent } from "./components/detailed-estimates/detailed-estimates.component";
import { TestScopeComponent } from "./components/test-scope/test-scope.component";
import { SummaryComponent } from "./components/summary/summary.component";
import { AssumptionsComponent } from "./components/assumptions/assumptions.component";
import {
  MatStepperModule,
  MatTooltipModule,
  MatSidenavModule,
  MatTabsModule,
  MatDividerModule,
  MatListModule,
  MatIconModule,
  MatButtonModule,
  MatMenuModule,
  MatSelectModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatCheckboxModule,
  MatButtonToggleModule,
  MatExpansionModule,
  MatToolbarModule,
  MatTableModule,
  MatPaginatorModule,
  MatFormFieldModule,
  MatInputModule,
  MatSnackBarModule,
  MatAutocompleteModule,
  MatChipsModule
} from "@angular/material";
import { ReactiveFormsModule } from "@angular/forms";
import { ProjectDetailsComponent } from "./components/project-details/project-details.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [
    NewEstimateComponent,
    InitiateEstimateComponent,
    HlTimelinesResourcesComponent,
    DetailedEstimatesComponent,
    TestScopeComponent,
    SummaryComponent,
    AssumptionsComponent,
    ProjectDetailsComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatTabsModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatCardModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatExpansionModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    NewEstimateRoutingModule,
    MatStepperModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    SharedModule,
    MatChipsModule
  ]
})
export class NewEstimateModule {}
