import { Injectable } from "@angular/core";
import { of, BehaviorSubject } from "rxjs";
import { ApiService } from "src/app/core/services/http/api.service";
import { NewEstimateModel } from "./components/new-estimate-model";
import { HttpClient, HttpParams } from "@angular/common/http";
import { UtilsService } from 'src/app/shared/services/utils/utils.service';

@Injectable({
  providedIn: "root"
})
export class NewEstimateService extends ApiService<NewEstimateModel, number> {
  projectDetails: BehaviorSubject<any> = new BehaviorSubject(null);
  estCount: BehaviorSubject<any> = new BehaviorSubject(0);
  constructor(_http: HttpClient,
    private utils: UtilsService) {
    super(_http);
  }
  setCount(count) {
    this.estCount.next(count);
  }
  getProjectDeatils(projectId) {
    return this.find("getEstimationById", projectId);
  }

  setProjectSelectedDetails(project) {
    this.utils.setLocal("project", project);
    this.projectDetails.next(project);
  }

  getProjectStatus() {
    return this.findAll("fetchProjectStatus");
  }

  saveEstimate(payLoad) {
    return this.save("saveEstimation", payLoad);
  }

  findEstimationById(payLoad) {
    return this.find("getEstimationById", payLoad);
  }

  updateEstimate(payLoad) {
    return this.update("updateEstimation", payLoad);
  }

  getDomains() {
    return this.findAll("domain/getDomains");
  }

  //Assumption
  getAsumptions() {
    return this.findAll("assumption/getAssumptions");
  }
  getAsumptionsById(payload) {
    return this.findOne("assumption/getAssumptions", payload);
  }
  saveAsumption(payload) {
    return this.save("assumption/save/assumption", payload);
  }
  updateAsumption(payload) {
    return this.update("assumption/update/assumption", payload);
  }
  deleteAsumption(payload) {
    return this.delete("assumption/delete/assumption", payload);
  }

  // RRD + HL Timelines
  saveHighlevelesttimeline(payload, id) {
    return this.save(
      "highleveltimeline/save/highlevelesttimeline/" + id,
      payload
    );
  }
  getHighlevelesttimeline(id) {
    return this.find("highleveltimeline/getHighlevelesttimeline", id);
  }

  getRrdGroups() {
    return this.findAll("rrdGroup/getRRDGroups");
  }
  getRrdGroupRoleByName(name: string) {
    let param = new HttpParams().append("search", name);
    return this.findAll(`rrdGroup/getRRDGroupRoles/${name}`);
  }
  getRRDTransactionsByGroupId(id: number) {
    let param = new HttpParams().append("search", name);
    return this.findAll(`rrdTransaction/getRRDTransactionsByGroupId/${id}`);
  }
  getRRDTransactionById(id: number) {
    let param = new HttpParams().append("search", name);
    return this.findAll(`rrdTransaction/getRRDTransactionById/${id}`);
  }
  saveRrd(payLoad) {
    return this.save("rrdTransaction/create/rrdTransaction", payLoad);
  }
  getAllRrds() {
    return this.findAll("rrdTransaction/getRRDTransactions");
  }

  updateRrd(payLoad) {
    return this.update("rrdTransaction/update/rrdTransaction", payLoad);
  }

  deleteRrd(id) {
    return this.delete("rrdTransaction/delete/rrdTransaction", id);
  }
  //summary apis

  fetchEstimationSummary(payload) {
    // let param = new HttpParams().append("estimationId", id);
    return this.find("fetchEstimationSummary", payload);
  }
  saveDetailedEstimate(obj) {
    return this.save(`saveDetailedEst/${obj.id}`, obj.payLoad);
  }
  getAllDetailedEstimates(obj) {
    return this.findAll(`getDetailedEst/${obj.id}`);
  }
  saveEstimationSummary(payload) {
    return this.save("saveEstimationSummary", payload);
  }
  getDetailedModuleTypes() {
    return this.findAll("getDetEstModuleType");
  }

  //Test Scope

  fetchTestScopeDetails(id) {
    return this.findAll(`fetchTestScope/${id}`);
  }
  saveOrUpdateTestScopeDetails(obj) {
    return this.save(`saveOrUpdateTestScope/${obj.id}`, obj.payLoad);
  }
  approveOrRejectEstimateApproval(payload) {
    return this.save("approveOrRejectEstimateApproval", payload);
  }
  summaryEmail(payload) {
    return this.save("sendEmailInSummary", payload);
  }
}
