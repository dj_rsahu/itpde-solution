import { TestBed } from '@angular/core/testing';

import { NewEstimateService } from './new-estimate.service';

describe('NewEstimateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewEstimateService = TestBed.get(NewEstimateService);
    expect(service).toBeTruthy();
  });
});
