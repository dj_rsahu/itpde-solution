import { ApiService } from "src/app/core/services/http/api.service";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { EstimatesModel } from "./estimates-model";

@Injectable({
  providedIn: "root"
})
export class EstimatesService extends ApiService<EstimatesModel, number> {
  resetPassword(payload: {}) {
    return this.save("resetPassword", payload);
  }

  constructor(_http: HttpClient) {
    super(_http);
  }

  getEstimatesByUser(id) {
    return this.findAll("getEstimationsByUserId");
  }
  getEstimations() {
    return this.findAll("getEstimations");
  }
  deleteEstimateById(id) {
    return this.delete("deleteEstimationDataById", id);
  }
  cloneEstimationById(id) {
    return this.save("cloneEstimation/" + id, {});
  }
  changeEstimationStatus(payLoad) {
    return this.update("updateEstimation", payLoad);
  }

  getRrdGroups() {
    return this.findAll("rrdGroup/getRRDGroups");
  }
  getRrdGroupRoleByName(name: string) {
    let param = new HttpParams().append("search", name);
    return this.findAll(`rrdGroup/getRRDGroupRoles/${name}`);
  }
  saveRrd(payLoad) {
    return this.save("rrdTransaction/create/rrdTransaction", payLoad);
  }
  getAllRrds() {
    return this.findAll("rrdTransaction/getRRDTransactions");
  }

  /// users
  getUserGroups() {
    return this.findAll("getUserGroups");
  }
  getUserGroupByUserId(id: any) {
    return this.find("getUserGroupByUserId", id);
  }
  getUsers() {
    return this.findAll(`getUsers`);
  }
  getUsersByRole(payLoad) {
    return this.findAll(`getUsersByRole/${payLoad}`);
  }
  updateUser(payLoad) {
    return this.update("updateUser", payLoad);
  }
  saveUser(payLoad) {
    console.log("inside estimates service", payLoad);
    return this.save("createUser", payLoad);
  }
  deleteUserById(id) {
    let param = new HttpParams().append("userId", id);
    return this.deleteByParam("deleteUserById", param);
  }
  updateRrd(payLoad) {
    return this.update("rrdTransaction/update/rrdTransaction", payLoad);
  }
  deleteRrd(id) {
    return this.delete("rrdTransaction/delete/rrdTransaction", id);
  }
  searchUser(name) {
    let param = new HttpParams().append("search", name);
    return this.findAllWithPram("getAllUsers", param);
  }
}
