import { Component, OnInit } from "@angular/core";
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogConfig, MatSnackBar } from "@angular/material";
import { ViewChild } from "@angular/core";
import { MatDividerModule } from "@angular/material/divider";
import { EstimatesService } from "../../estimates.service";
import { element } from "@angular/core/src/render3";
import { FormGroup, FormControl,Validators } from "@angular/forms";
import { ConfirmDialogComponent } from "src/app/shared/components/confirm-dialog/confirm-dialog.component";
import { PORTAL_ROLES } from 'src/app/configs/constants/APP_CONSTANTS';
import { JSONP_ERR_WRONG_METHOD } from '@angular/common/http/src/jsonp';
import { CustomErrorStateMatcher } from 'src/app/shared/other/custom-error-state-matcher';

@Component({
  selector: "dj-user-management",
  templateUrl: "./user-management.component.html",
  styleUrls: ["./user-management.component.scss"]
})
export class UserManagementComponent implements OnInit {
  userFormgroup: FormGroup;
  userGroupNames = [];
  editflag = false;
  editUsersObj = [];
  allUsers = [];
  userName;
  emailId;
  errorMatcher = new CustomErrorStateMatcher();
  portalRoles = [PORTAL_ROLES.admin, PORTAL_ROLES.testApprover,PORTAL_ROLES.overallApprover, PORTAL_ROLES.user,PORTAL_ROLES.noAccess];
  searchFormControl = new FormControl();
  foods = [
    { value: "steak-0", viewValue: "Thiru" },
    { value: "pizza-1", viewValue: "David" },
    { value: "tacos-2", viewValue: "Raj" }
  ];
  DataRows: any[] ;
  

  displayedColumns: string[] = [
    "username",
    "email",
    "groupName",
    "role",
    "createdDate",
    "updatedDate",
    "actions"
  ];
  columnHeaders: string[] = [
    "User Name",
    "Email ID",
    "Project Role",
    "Portal Role",
    "Created Date",
    "Updated Date",
    "Actions"
  ];
  dataSource: MatTableDataSource<any[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  selectedUser: any;

  constructor(private _estimationService: EstimatesService, private snackBar: MatSnackBar,private dialog: MatDialog) { }

  ngOnInit() {
    // this.userFormgroup = new FormGroup({
    //   firstName: new FormControl({ value: "", disabled: false }),
    //   lastName: new FormControl({ value: "", disabled: false }),
    //   email: new FormControl({ value: "", disabled: false }),
    //   groupName: new FormControl({ value: "", disabled: false }),
    //   role: new FormControl({ value: "", disabled: false })
    // });
    this.userFormgroup=new FormGroup({
      firstName:new FormControl("",Validators.required),
      lastName:new FormControl("",Validators.required),
      email:new FormControl("",[Validators.required,Validators.email]),
      groupName:new FormControl("",Validators.required),
      role:new FormControl("",Validators.required)
    });
    this.userFormgroup.valueChanges.subscribe(
      value => {
        console.log(value);
      }
    )
    this.getUsers();
    this.editflag=false;
    this.getAllUserGroups();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  clearSearch() {
    this.searchFormControl.setValue("");
    this.applyFilter(this.searchFormControl.value);
  }
cancel(){
  this.userFormgroup.reset();
    this.userFormgroup.markAsPristine();
    this.userFormgroup.markAsUntouched();
    this.userFormgroup.updateValueAndValidity();
    this.editflag=false;
    this.userFormgroup.controls["email"].enable();
}
  getAllUserGroups() {
    this._estimationService.getUserGroups().subscribe(res => {
      res.forEach((element: any) => {
        this.userGroupNames.push(element.groupName);
      });
    });
  }
  editUsers(val) {
    console.log(val.firstName);
    this.editflag = true;
    this.userFormgroup.setValue({
      firstName: val.firstName,
      lastName:val.lastName,
      email: val.email,
      groupName: val.groupName,
      role: val.role
    });
    // this.userFormgroup.controls["groupName"].enable();
    // this.userFormgroup.controls["role"].enable()
    this.userFormgroup.controls["email"].disable();
    this.selectedUser = val;
  }

  grantAcess(val) {
    
    // console.log(this.selectedUser);
    if(this.editflag){ 
      this.selectedUser.firstName=val.firstName;
      this.selectedUser.lastName=val.lastName;
      this.selectedUser.role = val.role;
    this.selectedUser.groupName = val.groupName;
      this._estimationService.updateUser(this.selectedUser).subscribe(res => {
        this.snackBar.open("User Updated Successfully", "DISMISS", {
          duration: 5000,
            verticalPosition: 'top', // 'top' | 'bottom'
            horizontalPosition: 'center',
            panelClass: ['success-snackbar']
        });
        this.getUsers();
    }
    ,
    err => {
      console.log("Update user ", err);
      this.snackBar.open(err.error.message, "DISMISS", {
        duration: 10000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
      });
    }
    );
  }
  else{
    this._estimationService.saveUser(val).subscribe(res=>{
      this.snackBar.open("User Added Successfully", "DISMISS", {
        duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['success-snackbar']
      });
      this.getUsers();
    },
    err => {
      console.log("save user ", err);
      this.snackBar.open(err.error.message, "DISMISS", {
        duration: 10000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
      });
    }
    )
  }
  this.userFormgroup.reset();
  this.userFormgroup.markAsPristine();
  this.userFormgroup.markAsUntouched();
  this.userFormgroup.updateValueAndValidity();
  }

  getUsers() {
    this._estimationService.getUsers().subscribe(res => {
      this.allUsers = res.map((element: any) => {
        element.username = element.firstName + " " + element.lastName;

        return element;
      });
      this.dataSource = new MatTableDataSource(this.allUsers);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    err => {
      console.log("Get users ", err);
      this.snackBar.open(err.error.message, "DISMISS", {
        duration: 10000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
      });
    }
    
    );
    console.log(this.allUsers);
  }
  deleteUser(val) {
    console.log(val)
    this._estimationService.deleteUserById(val.id).subscribe(res => {
      this.snackBar.open("User Deleted Successfully", "DISMISS", {
        duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['success-snackbar']
      });
      this.getUsers()
    },
    err => {
      console.log("Get users ", err);
      this.snackBar.open(err.error.message, "DISMISS", {
        duration: 10000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['error-snackbar']
      });
    }

    )
  }
  openDialog(e) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: ["Cancel", "Delete", "Delete User Confirmation","This action will delete the user permanently. Do you wish to continue?"]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.deleteUser(e);
      } else {
      }
    });
  }

  openDialogGrantAcc(e) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    if(this.editflag){
      dialogConfig.data = {
      actions: ["Cancel", "Confirm", "Modify User Role Confirmation"
      ,"This action will modify the role of the selected user. Do you wish to continue?"]
    };
  }
else{
  dialogConfig.data = {
    actions: ["Cancel", "Confirm", "Create User Confirmation"
    ,"This action will create a new user. Do you wish to continue?"]
  };
}
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.grantAcess(e);
      } else {
      }
    });
  }
}
