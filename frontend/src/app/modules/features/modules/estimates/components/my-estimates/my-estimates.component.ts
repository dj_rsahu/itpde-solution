import {
  Component,
  OnInit,
  ViewChild,
  OnChanges,
  AfterContentChecked,
  AfterContentInit
} from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  ThemePalette
} from "@angular/material";
import { FormControl } from "@angular/forms";
import { EstimatesService } from "../../estimates.service";
import { Router } from "@angular/router";
import { MatDialogConfig, MatDialog, MatSnackBar } from "@angular/material";
import { ConfirmDialogComponent } from "src/app/shared/components/confirm-dialog/confirm-dialog.component";
import { ESTIMATE_CONSTANTS } from "src/app/configs/constants/APP_CONSTANTS";
import * as moment from "moment";
import { AppStoreService } from "src/app/app-store.service";
import { FeaturesService } from "src/app/modules/features/features.service";

export interface ChipColor {
  name: string;
  color: ThemePalette;
}

@Component({
  selector: "dj-my-estimates",
  templateUrl: "./my-estimates.component.html",
  styleUrls: ["./my-estimates.component.scss"]
})
export class MyEstimatesComponent implements OnInit {
  searchFormControl = new FormControl();
  DataRows: any[] = [];
  currentUser;
  displayedColumns: string[] = [
    "domainName",
    "est_project_name",
    "projectLeadName",
    "createdDate",
    "updatedDate",
    "updatedByName",
    "estmation_status",
    "actions"
  ];
  columnHeaders: string[] = [
    "Domain",
    "Project Name",
    "Project Lead",
    "Created Date",
    "Updated Date",
    "Updated By",
    "Status",
    "Actions"
  ];
  resourceTypeList = [
    { name: ESTIMATE_CONSTANTS.complete, class: "completed-chip" },
    { name: ESTIMATE_CONSTANTS.draft, class: "draft-chip" },
    { name: ESTIMATE_CONSTANTS.inprogress, class: "inprogress-chip" },
    { name: ESTIMATE_CONSTANTS.pending1, class: "pending-chip" }
  ];
  dataSource: MatTableDataSource<any[]>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _estimatesService: EstimatesService,
    private _router: Router,
    private dialog: MatDialog,
    private appStoreService: AppStoreService,
    private _featuresService: FeaturesService
  ) {}

  ngOnInit() {
    this.getEstimates();
    this.appStoreService.user.subscribe(res => {
      this.currentUser = res;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  navMode(estimation, mode) {
    this._router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };
    this._router.navigateByUrl(
      "editEstimates/" + estimation.id + "/" + mode + "/" + 0
    );
    
  }

  getChipClass(name) {
    if (name) return this.resourceTypeList.find(ele => ele.name === name).class;
  }

  getEstimates() {
    this._estimatesService.getEstimations().subscribe(res => {
      this.DataRows = res.map(ele => {
        ele["createdDate"] = moment(ele["createdDate"]).format("DD-MMM-YYYY");
        ele["updatedDate"] = moment(ele["updatedDate"]).format("DD-MMM-YYYY");
        ele["actions"] = "";
        return ele;
      });
      this.dataSource = new MatTableDataSource(this.DataRows);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  newEstimates() {
    this._router.navigateByUrl("/NewEstimates");
  }

  clearSearch() {
    this.searchFormControl.setValue("");
    this.applyFilter(this.searchFormControl.value);
  }

  deleteEstimates(element) {
    console.log(element.id);
    this._estimatesService.deleteEstimateById(element.id).subscribe(res => {
      this.getEstimates();
    });
  }
  cloneEstimates(element) {
    console.log(element.id);
    this._estimatesService.cloneEstimationById(element.id).subscribe(res => {
      this.getEstimates();
    });
  }
  openDialogDelete(e) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: ["Cancel", "Delete", "Delete Confirmation","This action will delete the '"+e["est_project_name"] +"' permanently. Do you wish to continue?"]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.deleteEstimates(e);
      } else {
      }
    });
  }
  openDialogCopy(e) {
    console.log("element",e);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: ["Cancel", "Copy", "Create Copy Confirmation"
      ,"This action will create a copy of '"+e["est_project_name"] +"'. Do you wish to continue?" ]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.cloneEstimates(e);
      } else {
      }
    });
  }
}
