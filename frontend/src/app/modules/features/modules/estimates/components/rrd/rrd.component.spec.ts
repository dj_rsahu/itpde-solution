import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RRDComponent } from './rrd.component';

describe('RRDComponent', () => {
  let component: RRDComponent;
  let fixture: ComponentFixture<RRDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RRDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RRDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
