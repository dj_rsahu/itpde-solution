import {
  AfterViewInit,
  Component,
  ViewChild,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatTab
} from "@angular/material";
import { RrdTableDataSource } from "./rrd-table-datasource";
import { FormControl } from '@angular/forms';

@Component({
  selector: "dj-rrd-table",
  templateUrl: "./rrd-table.component.html",
  styleUrls: ["./rrd-table.component.css"]
})
export class RrdTableComponent implements OnInit, OnChanges {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  @Input() tableDataSource;
  @Output() updateEvent = new EventEmitter();
  @Output() deleteEvent = new EventEmitter();
  DataRows = [];
  searchFormControl = new FormControl();
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns: string[] = [
    "resourceTypeRole",
    "capacity",
    "resourcetype",
    "groupName",
    "rscRole",
    "costperday",
    "actions"
  ];
  columnHeaders: string[] = [
    "Resource Role",
    "Cap (%)",
    "Internal/External",
    "Group",
    "Role",
    "Cost ($)",
    "Actions"
  ];

  ngOnInit() {
    //this.dataSource = new MatTableDataSource(this.tableDataSource);
  }
  ngOnChanges() {
    this.getAllRrds();
  }
  clearSearch() {
    this.searchFormControl.setValue("");
    this.applyFilter(this.searchFormControl.value);
  }
  getAllRrds() {
    this.DataRows = this.tableDataSource.map(ele => {
      ele["actions"] = "";
      return ele;
    });
    console.log("datarows", this.DataRows);
    this.dataSource = new MatTableDataSource(this.DataRows);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  updateRrd(row) {
    this.updateEvent.emit(row);
  }
  deleteRrd(row) {
    this.deleteEvent.emit(row);
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
