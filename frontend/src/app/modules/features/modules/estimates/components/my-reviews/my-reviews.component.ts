import {
  Component,
  ViewChild,
  OnInit,
  Input,
  OnChanges,
  ViewEncapsulation
} from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog,
  MatDialogConfig
} from "@angular/material";
import { FormControl } from "@angular/forms";
import { EstimatesService } from "../../estimates.service";
import { Router } from "@angular/router";
import { ESTIMATE_CONSTANTS } from "src/app/configs/constants/APP_CONSTANTS";
import * as moment from "moment";
import { ConfirmDialogComponent } from "src/app/core/layout/confirm-dialog/confirm-dialog.component";
import { AppStoreService } from "src/app/app-store.service";
import { FeaturesService } from 'src/app/modules/features/features.service';

@Component({
  selector: "dj-my-reviews",
  templateUrl: "./my-reviews.component.html",
  styleUrls: ["./my-reviews.component.scss"]
})
export class MyReviewsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  searchFormControl = new FormControl();
  DataRows: any[] = [];
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns: string[] = [
    "domainName",
    "est_project_name",
    "projectLeadName",
    "createdDate",
    "updatedDate",
    "updatedByName",
    "estmation_status",
    "actions"
  ];
  columnHeaders: string[] = [
    "Domain",
    "Project Name",
    "Project Lead",

    "Created Date",
    "Updated Date",
    "Updated By",
    "Status",
    "Actions"
  ];
  currentUser: any;
  constructor(
    private _estimatesService: EstimatesService,
    private _router: Router,
    private dialog: MatDialog,
    private appStoreService: AppStoreService,
    private _featuresService: FeaturesService
  ) {}

  ngOnInit() {
    this.getReviews();
    this.appStoreService.user.subscribe(res => {
      this.currentUser = res;
    });
  }

  getReviews() {
    this._estimatesService.getEstimatesByUser(1).subscribe(res => {
      this.DataRows = res.map(ele => {
        ele["createdDate"] = moment(ele["createdDate"]).format("DD-MMM-YYYY");
        ele["updatedDate"] = moment(ele["updatedDate"]).format("DD-MMM-YYYY");
        ele["actions"] = "";
        return ele;
      });
      this.dataSource = new MatTableDataSource(this.DataRows);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  resourceTypeList = [
    { name: ESTIMATE_CONSTANTS.complete, class: "completed-chip" },
    { name: ESTIMATE_CONSTANTS.draft, class: "draft-chip" },
    { name: ESTIMATE_CONSTANTS.inprogress, class: "inprogress-chip" },
    { name: ESTIMATE_CONSTANTS.pending1, class: "pending-chip" }
  ];
  getChipClass(name) {
    return this.resourceTypeList.find(ele => ele.name === name).class;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  clearSearch() {
    this.searchFormControl.setValue("");
    this.applyFilter(this.searchFormControl.value);
  }
  navMode(estimation, mode) {
    this._router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    };
    if (mode === "edit") {
      this._featuresService
        .setEstimationEditMode(estimation.id, 0)
        .subscribe(res => {
          this._router.navigateByUrl(
            "editEstimates/" + estimation.id + "/" + mode + "/" + 0
          );
          
        });
    } else {
      this._router.navigateByUrl(
        "editEstimates/" + estimation.id + "/" + mode + "/" + 0
      );
    }
  }
  deleteEstimates(element) {
    console.log(element.id);
    this._estimatesService.deleteEstimateById(element.id).subscribe(res => {
      this.getReviews();
    });
  }
  cloneEstimates(element) {
    console.log(element.id);
    this._estimatesService.cloneEstimationById(element.id).subscribe(res => {
      this.getReviews();
    });
  }
  openDialogDelete(e) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: ["Cancel", "Delete", "Delete Confirmation","This action will delete the '"+e["est_project_name"]+"' permanently. Do you wish to continue?"]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.deleteEstimates(e);
      } else {
      }
    });
  }
  openDialogCopy(e) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: ["Cancel", "Copy", "Create Copy Confirmation"
      ,"This action will create a copy of '"+e["est_project_name"] +"'. Do you wish to continue?"]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.cloneEstimates(e);
      } else {
      }
    });
  }
}
