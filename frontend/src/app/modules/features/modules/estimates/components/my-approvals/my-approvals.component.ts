import {
  AfterViewInit,
  Component,
  ViewChild,
  Input,
  OnInit,
  OnChanges
} from "@angular/core";
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog,
  MatDialogConfig
} from "@angular/material";
import { FormControl } from "@angular/forms";
import { EstimatesService } from "../../estimates.service";
import { Router } from "@angular/router";
import * as moment from "moment";
import { ESTIMATE_CONSTANTS } from "src/app/configs/constants/APP_CONSTANTS";
import { ConfirmDialogComponent } from "src/app/shared/components/confirm-dialog/confirm-dialog.component";
@Component({
  selector: "dj-my-approvals",
  templateUrl: "./my-approvals.component.html",
  styleUrls: ["./my-approvals.component.scss"]
})
export class MyApprovalsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<any>;
  @Input() tableDataSource;
  searchFormControl = new FormControl();
  DataRows: any[] = [];
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns: string[] = [
    "domainName",
    "est_project_name",
    "projectLeadName",

    "createdDate",
    "updatedDate",
    "updatedByName",
    "estmation_status",
    "actions"
  ];
  columnHeaders: string[] = [
    "Domain",
    "Project Name",
    "Project Lead",

    "Created Date",
    "Updated Date",
    "Updated By",
    "Status",
    "Actions"
  ];
  constructor(
    private _estimatesService: EstimatesService,
    private _router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.getApprovals();
  }
  
  navMode(estimation, mode) {
    if (mode === "view") {
      this._router.navigateByUrl(
        "editEstimates/" + estimation.id + "/" + mode + "/" + 0
      );
    } else {
      this._router.navigateByUrl(
        "editEstimates/" + estimation.id + "/" + mode + "/" + 5
      );
    }
  }

  resourceTypeList = [
    { name: ESTIMATE_CONSTANTS.complete, class: "completed-chip" },
    { name: ESTIMATE_CONSTANTS.draft, class: "draft-chip" },
    { name: ESTIMATE_CONSTANTS.inprogress, class: "inprogress-chip" },
    { name: ESTIMATE_CONSTANTS.pending1, class: "pending-chip" }
  ];
  getChipClass(name) {
    return this.resourceTypeList.find(ele => ele.name === name).class;
  }
  openDialog(e) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: [
        "Cancel",
        "Approve",
        "Approval Confirmation","This action will approve the estimate. Do you wish to continue?"
      ]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.approve(e);
      } else {
      }
    });
  }
  route(val) {
    this._router.navigateByUrl("editEstimates/" + val.id + "/" + 5);
  }
  openReworkDialog(e) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: ["Cancel", "Rework","Rework Confirmation"
      ,"Are you sure you want to send the estimate back for Rework?"+
      "This action would reset all approvals and the status will change back to ‘Draft’"
 ]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.rework(e);
      } else {
      }
    });
  }

  getApprovals() {
    let user = JSON.parse(localStorage.getItem("user"));

    this._estimatesService.getEstimations().subscribe(res => {
      if (user.role == "Test Approver") {
        this.DataRows = res.filter(ele => {
          ele["createdDate"] = moment(ele["createdDate"]).format("DD-MMM-YYYY");
          ele["updatedDate"] = moment(ele["updatedDate"]).format("DD-MMM-YYYY");
          ele["actions"] = "";
          return ele["estmation_status"] == "Pending TA";
        });
      } else if (user.role == "Overall Approver") {
        this.DataRows = res.filter(ele => {
          ele["createdDate"] = moment(ele["createdDate"]).format("DD-MMM-YYYY");
          ele["updatedDate"] = moment(ele["updatedDate"]).format("DD-MMM-YYYY");
          ele["actions"] = "";
          return ele["estmation_status"] == "Pending OA";
        });
      } else if (user.role == "Admin") {
        this.DataRows = res.filter(ele => {
          ele["createdDate"] = moment(ele["createdDate"]).format("DD-MMM-YYYY");
          ele["updatedDate"] = moment(ele["updatedDate"]).format("DD-MMM-YYYY");
          ele["actions"] = "";
          return (
            ele["estmation_status"] == "Pending OA" ||
            ele["estmation_status"] == "Pending TA"
          );
        });
      } else if (user.role == "User") {
        this.DataRows = [];
      }
      // console.log("console", this.DataRows);
      this.dataSource = new MatTableDataSource(this.DataRows);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  approve(element) {
    element.estmation_status = ESTIMATE_CONSTANTS.complete;
    element.updatedDate = moment().format("YYYY-MM-DD HH:mm:ss");
    this._estimatesService.changeEstimationStatus(element).subscribe(res => {
      this.getApprovals();
    });
  }
  rework(element) {
    element.estmation_status = ESTIMATE_CONSTANTS.draft;
    element.updatedDate = moment().format("YYYY-MM-DD HH:mm:ss");
    this._estimatesService.changeEstimationStatus(element).subscribe(res => {
      this.getApprovals();
    });
  }
  clearSearch() {
    this.searchFormControl.setValue("");
    this.applyFilter(this.searchFormControl.value);
  }
}
