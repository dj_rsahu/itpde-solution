import { Component, OnInit, ViewChild } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  NgForm,
  FormGroupDirective
} from "@angular/forms";
import { EstimatesService } from "../../estimates.service";
import { ESTIMATE_CONSTANTS } from "src/app/configs/constants/APP_CONSTANTS";
import {
  MatDialogConfig,
  MatDialog,
  MatSnackBar,
  ErrorStateMatcher
} from "@angular/material";
import { ConfirmDialogComponent } from "src/app/core/layout/confirm-dialog/confirm-dialog.component";
import { Observable } from "rxjs";
import { startWith, map, debounceTime } from "rxjs/operators";
import { CustomErrorStateMatcher } from "src/app/shared/other/custom-error-state-matcher";

@Component({
  selector: "dj-rrd",
  templateUrl: "./rrd.component.html",
  styleUrls: ["./rrd.component.scss"]
})
export class RRDComponent implements OnInit {
  userListFilter: Observable<string[]>;
  userList = [];
  errorState = false;
  errorMatcher = new CustomErrorStateMatcher();
  tableDataSource = [];
  rrdFormgroup: FormGroup;
  groupNameList;
  roleList;
  resourceRoleList = [
    { name: ESTIMATE_CONSTANTS.known, value: 1 },
    { name: ESTIMATE_CONSTANTS.unKnown, value: 0 }
  ];
  resourceTypeList = [ESTIMATE_CONSTANTS.internal, ESTIMATE_CONSTANTS.external];
  isUpdate = false;
  selectedRrd: any;
  @ViewChild("f")
  yourForm: NgForm;
  disableBtn: boolean;
  message: string;
  constructor(
    private _estimationService: EstimatesService,
    private dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.isUpdate = false;
    this.disableBtn = true;
    this.getAllRrds();
    this.rrdFormgroup = new FormGroup({
      rrdgroupid: new FormControl("", Validators.required),
      groupOthers: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      roleName: new FormControl("", Validators.required),
      roleOthers: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      resourceid: new FormControl("", Validators.required),
      resourcenameOthers: new FormControl(
        { value: "", disabled: true },
        Validators.required
      ),
      capacity: new FormControl("", Validators.required),
      resourcetype: new FormControl("", Validators.required),
      costperday: new FormControl("", Validators.required)
    });
    this.rrdFormgroup.get("rrdgroupid").valueChanges.subscribe(res => {
      if (res) {
        if (res.groupname === ESTIMATE_CONSTANTS.other) {
          this.rrdFormgroup.controls["groupOthers"].enable();
          this.roleList = [{ role: ESTIMATE_CONSTANTS.other, rrdid: 0 }];
        } else {
          this.rrdFormgroup.controls["groupOthers"].disable();
          this.rrdFormgroup.controls["groupOthers"].setValue("");
          this._estimationService
            .getRrdGroupRoleByName(res.groupname)
            .subscribe(result => {
              this.roleList = result;
              this.roleList.push({
                role: ESTIMATE_CONSTANTS.other,
                rrdid: 0
              });
              if (this.isUpdate) {
                let roleIndex = this.roleList.findIndex(
                  item => item.role === this.selectedRrd.rscRole
                );
                this.rrdFormgroup.controls["roleName"].setValue(
                  this.roleList[roleIndex]
                );
              }
            });
        }
      }
    });
    this.rrdFormgroup.get("roleName").valueChanges.subscribe(res => {
      if (res) {
        if (res.role === ESTIMATE_CONSTANTS.other) {
          this.rrdFormgroup.controls["roleOthers"].enable();
        } else {
          this.rrdFormgroup.controls["roleOthers"].disable();
        }
      }
    });
    this.rrdFormgroup.get("resourceid").valueChanges.subscribe(res => {
      console.log(res);
      if (res === ESTIMATE_CONSTANTS.known) {
        this.rrdFormgroup.controls["resourcenameOthers"].enable();
      } else {
        this.rrdFormgroup.controls["resourcenameOthers"].disable();
        this.rrdFormgroup.controls["resourcenameOthers"].setValue("");
      }
    });
    this.userListFilter = this.rrdFormgroup
      .get("resourcenameOthers")
      .valueChanges.pipe(
        startWith(""),
        map(value => this._filter(value))
      );
  }
  private _filter(value: string): any[] {
    if (value) {
      if (value.length > 1) {
        this._estimationService.searchUser(value).subscribe(data => {
          this.userList = data;
          if (this.userList.length > 0) {
          } else {
            this.disableBtn = true;
            this.message = "Please select valid user";
          }
          // console.log("userlist", this.userList);
        });
      } else {
        this.userList = [];
      }
    } else {
      this.userList = [];
    }

    return this.userList;
  }
  getRrdGroups() {
    this._estimationService.getRrdGroups().subscribe(res => {
      this.groupNameList = res;
      this.groupNameList.push({
        groupname: ESTIMATE_CONSTANTS.other,
        rrdid: 0
      });
    });
  }
  createRrd() {
    let roleName = this.rrdFormgroup.controls["roleName"].value;
    let group = this.rrdFormgroup.controls["rrdgroupid"].value;
    this.rrdFormgroup.controls["rrdgroupid"].setValue(roleName["rrdid"]);
    if (this.rrdFormgroup.controls["resourceid"].value === ESTIMATE_CONSTANTS.known) {
      let resourceName = this.rrdFormgroup.controls["resourcenameOthers"].value;
      this.rrdFormgroup.controls["resourceid"].setValue(resourceName["id"]);
    }

    let obj = JSON.parse(JSON.stringify(this.rrdFormgroup.value));
    if (roleName["role"] === ESTIMATE_CONSTANTS.other) {
      if (group["groupname"] !== ESTIMATE_CONSTANTS.other) {
        obj["groupName"] = group["groupname"];
      }
    }
    // console.log(obj);

    this._estimationService.saveRrd(obj).subscribe(
      res => {
        this.snackBar.open("RRD created Successfully", "DISMISS", {
          duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['success-snackbar']
        });
        this.getAllRrds();
        //this.rrdFormgroup.reset();
      },
      err => {
        this.cancel();
      }
    );
  }
  editRrd(row) {
    this.isUpdate = true;
    this.selectedRrd = row;
    let groupIndex = this.groupNameList.findIndex(
      item => item.groupname === row.groupName
    );

    this.rrdFormgroup.controls["rrdgroupid"].setValue(
      this.groupNameList[groupIndex]
    );

    this.rrdFormgroup.controls["costperday"].setValue(row.costperday);
    this.rrdFormgroup.controls["capacity"].setValue(row.capacity);
    if (typeof row.resourceid !== 'string') {
      this.rrdFormgroup.controls["resourceid"].setValue(ESTIMATE_CONSTANTS.known);
      this.rrdFormgroup.controls["resourcenameOthers"].setValue(
        row.userDetails
      );
    } else {
      this.rrdFormgroup.controls["resourceid"].setValue(ESTIMATE_CONSTANTS.unKnown);
      this.rrdFormgroup.controls["resourcenameOthers"].setValue("");
    }

    this.rrdFormgroup.controls["resourcetype"].setValue(row.resourcetype);
  }
  updateRrd() {
    let roleName = this.rrdFormgroup.controls["roleName"].value;
    this.rrdFormgroup.controls["rrdgroupid"].setValue(roleName["rrdid"]);
    if (this.rrdFormgroup.controls["resourceid"].value === ESTIMATE_CONSTANTS.known) {
      let resourceName = this.rrdFormgroup.controls["resourcenameOthers"].value;
      this.rrdFormgroup.controls["resourceid"].setValue(resourceName["id"]);
    }

    let payLoad = this.rrdFormgroup.value;
    payLoad.rrdtransid = this.selectedRrd.rrdtransid;
    console.log(this.rrdFormgroup.value);
    this._estimationService.updateRrd(this.rrdFormgroup.value).subscribe(
      res => {
        this.snackBar.open("RRD updated Successfully", "DISMISS", {
          duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['success-snackbar']
        });
        this.getAllRrds();
        //this.rrdFormgroup.reset();
        //this.rrdFormgroup.reset();
        this.isUpdate = false;
      },
      err => {
        this.cancel();
      }
    );
  }
  // deleteRrd(row) {
  //   this._estimationService.deleteRrd(row.rrdtransid).subscribe(res => {
  //     this.getAllRrds();
  //   });
  // }
  deleteRrd(row) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      actions: ["Cancel", "Delete", "RRD Delete Confirmation","This action will delete the selected RRD permanently. Do you wish to continue?"]
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this._estimationService.deleteRrd(row.rrdtransid).subscribe(
          res => {
            this.snackBar.open("Deleted Successfully", "DISMISS", {
              duration: 5000,
          verticalPosition: 'top', // 'top' | 'bottom'
          horizontalPosition: 'center',
          panelClass: ['success-snackbar']
            });
            this.getAllRrds();
          },
          err => {
            this.cancel();
          }
        );
      } else {
      }
    });
  }
  getAllRrds() {
    this._estimationService.getAllRrds().subscribe(res => {
      this.tableDataSource = res;
      this.cancel();
      this.getRrdGroups();
    });
  }
  cancel() {
    this.rrdFormgroup.reset();
    this.rrdFormgroup.markAsPristine();
    this.rrdFormgroup.markAsUntouched();
    this.rrdFormgroup.updateValueAndValidity();
    this.isUpdate = false;
  }
  ownerFn = user => {
    this.disableBtn = false;
    return user ? user.firstName + " " + user.lastName : undefined;
  };
}
