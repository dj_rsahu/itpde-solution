import { Component, OnInit } from "@angular/core";
import { UtilsService } from "src/app/shared/services/utils/utils.service";
import { EstimatesService } from "../../estimates.service";
import { AppStoreService } from "src/app/app-store.service";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "dj-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.scss"]
})
export class UserProfileComponent implements OnInit {
  isEdit: boolean = false;
  uploadedProfile: any;
  model: any;
  passwordModel: any;
  disablePassword: boolean = true;
  disablebtn: boolean = true;
  currentHide: boolean = true;
  newPasswordHide: boolean = true;
  confirmHide: boolean = true;
  constructor(
    private utils: UtilsService,
    private snackBar: MatSnackBar,
    private _estimationService: EstimatesService,
    private appstore: AppStoreService
  ) {}

  ngOnInit() {
    this.passwordModel = {
      currentPassword: "",
      newPassword: "",
      confirmPassword: ""
    };
    this.model = this.utils.getLocal("user");
  }

  choosePicture(event: any) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      this.disablebtn = false;
      const reader = new FileReader();
      reader.onload = e => (this.model.pic = reader.result);

      reader.readAsDataURL(file);
    }
  }
  validatePassword() {
    if (this.model.password === this.passwordModel.currentPassword) {
      this.disablePassword = false;
    } else {
      this.disablePassword = true;
    }
  }
  comparePassword() {
    if (this.passwordModel.newPassword === this.passwordModel.confirmPassword) {
      this.disablebtn = false;
    } else {
      this.disablebtn = true;
    }
  }
  newPasswordChage() {
    this.passwordModel.confirmPassword = "";
  }
  updateProfile() {
    this.model.password = this.passwordModel.confirmPassword
      ? this.passwordModel.confirmPassword
      : this.model.password;
    console.log("model", this.model);
    this._estimationService.updateUser(this.model).subscribe(
      res => {
        this.appstore.setUserStateAfterlogin(res);
        this.utils.setLocal("user", res);
        this.snackBar.open("User Updated Successfully", "DISMISS", {
          duration: 5000,
          verticalPosition: "top", // 'top' | 'bottom'
          horizontalPosition: "center",
          panelClass: ["success-snackbar"]
        });
      },
      err => {
        this.snackBar.open(err.error.message, "DISMISS", {
          duration: 10000,
          verticalPosition: "top", // 'top' | 'bottom'
          horizontalPosition: "center",
          panelClass: ["error-snackbar"]
        });
      }
    );
    this.passwordModel = {
      currentPassword: "",
      newPassword: "",
      confirmPassword: ""
    };
    this.disablePassword=true;
    this.disablebtn=true;
  
  }

  onCancel(){
    this.passwordModel = {
      currentPassword: "",
      newPassword: "",
      confirmPassword: ""
    };
    this.disablePassword=true;
    this.disablebtn=true;
  
  }
}
