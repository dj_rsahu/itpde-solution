import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EstimatesRoutingModule } from "./estimates-routing.module";
import { EstimatesComponent } from "./estimates.component";
import { MyEstimatesComponent } from "./components/my-estimates/my-estimates.component";
import { MyApprovalsComponent } from "./components/my-approvals/my-approvals.component";
import { MyReviewsComponent } from "./components/my-reviews/my-reviews.component";
import {
  MatSidenavModule,
  MatTabsModule,
  MatDividerModule,
  MatListModule,
  MatIconModule,
  MatButtonModule,
  MatMenuModule,
  MatSelectModule,
  MatCardModule,
  MatTooltipModule,
  MatProgressSpinnerModule,
  MatCheckboxModule,
  MatButtonToggleModule,
  MatExpansionModule,
  MatToolbarModule,
  MatTableModule,
  MatPaginatorModule,
  MatFormFieldModule,
  MatInputModule,
  MatSortModule,
  MatAutocompleteModule,
  MatSnackBarModule,
  MatChipsModule
} from "@angular/material";
import { ReactiveFormsModule } from "@angular/forms";
import { RRDComponent } from "./components/rrd/rrd.component";
import { UserManagementComponent } from "./components/user-management/user-management.component";
import { RrdTableComponent } from "./components/rrd/rrd-table/rrd-table.component";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "src/app/shared/shared.module";
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';

@NgModule({
  declarations: [
    EstimatesComponent,
    MyEstimatesComponent,
    MyApprovalsComponent,
    MyReviewsComponent,
    RRDComponent,
    UserManagementComponent,
    RrdTableComponent,
    PageNotFoundComponent,
    UserProfileComponent
  ],
  imports: [
    CommonModule,
    EstimatesRoutingModule,
    MatSidenavModule,
    MatTabsModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatCardModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatExpansionModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSortModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    FormsModule,
    SharedModule,
    MatChipsModule
  ]
})
export class EstimatesModule {}
