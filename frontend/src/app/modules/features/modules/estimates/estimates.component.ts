import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { AppStoreService } from "src/app/app-store.service";

@Component({
  selector: "dj-estimates",
  templateUrl: "./estimates.component.html",
  styleUrls: ["./estimates.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class EstimatesComponent implements OnInit {
  sideNavMode: any;
  sideNavOpen: any;
  currentUser: any;
  changeText: any;
  sidenavWidth: number;
  isMobile: boolean;

  constructor(private appStoreService: AppStoreService) {}

  ngOnInit() {
    this.changeText = true;
    this.appStoreService.sideNavMode.subscribe(res => {
      this.sideNavMode = res;
    });
    this.appStoreService.sideNavOpen.subscribe(res => {
      this.sideNavOpen = res;
    });
    this.appStoreService.user.subscribe(res => {
      this.currentUser = res;
    });
    this.appStoreService.isMobile.subscribe(res => {
      this.isMobile = res;
    });
  }
  increase() {
    if (!this.isMobile) {
      this.sidenavWidth = 15;
      this.changeText = true;
    }
  }
  decrease() {
    if (!this.isMobile) {
      this.sidenavWidth = 4;
      this.changeText = false;
    }
  }
}
