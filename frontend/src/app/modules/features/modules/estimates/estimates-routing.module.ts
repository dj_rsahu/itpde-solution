import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MyApprovalsComponent } from "./components/my-approvals/my-approvals.component";
import { MyReviewsComponent } from "./components/my-reviews/my-reviews.component";
import { MyEstimatesComponent } from "./components/my-estimates/my-estimates.component";
import { EstimatesComponent } from "./estimates.component";
import { RRDComponent } from "./components/rrd/rrd.component";
import { UserManagementComponent } from "./components/user-management/user-management.component";
import { RoleGuard } from "src/app/role.guard";
import { AuthguardGuard } from "src/app/authguard.guard";
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';

const routes: Routes = [
  {
    path: "",
    component: EstimatesComponent,
    children: [
      {
        path: "MyApprovals",
        component: MyApprovalsComponent
      },
      {
        path: "MyReviews",
        component: MyReviewsComponent
      },
      {
        path: "AllEstimates",
        component: MyEstimatesComponent
      },
      {
        path: "RRD",
        component: RRDComponent,
        canActivate: [RoleGuard]
      },
      {
        path: "User",
        component: UserManagementComponent,
        canActivate: [RoleGuard]
      },
      {
        path:"UserProfile",
        component:UserProfileComponent
      }
      ,
      {
        path: "pagenotfound",
        component: PageNotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstimatesRoutingModule {}
