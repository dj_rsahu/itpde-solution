import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { FeaturesComponent } from './features.component';

const routes: Routes = [
  {
    path:"",
    component: FeaturesComponent,
    children:[
      {
        path: "",
        loadChildren: "./modules/estimates/estimates.module#EstimatesModule"
      },
      {
        path: "",
        loadChildren: "./modules/new-estimate/new-estimate.module#NewEstimateModule"
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule {}
