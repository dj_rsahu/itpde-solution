import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { ApiService } from "src/app/core/services/http/api.service";
import { FeaturesModel } from "./features.model";

@Injectable({
  providedIn: "root"
})
export class FeaturesService extends ApiService<FeaturesModel, number> {
  constructor(_http: HttpClient) {
    super(_http);
  }

  setEstimationEditMode(id, isEditable) {
    let param = new HttpParams().append("isEditable", isEditable);
    return this.saveAllWithPram("estimationEditable/" + id, param, {});
  }

  increaseEstimationEditableDuration(id) {
    return this.save("increaseEstimationEditableDuration/" + id, {});
  }
}
