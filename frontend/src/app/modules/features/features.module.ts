import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeaturesRoutingModule } from './features-routing.module';
import { FeaturesComponent } from './features.component';
import { MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, MatButtonModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({
  declarations: [FeaturesComponent],
  imports: [
    CommonModule, 
    FeaturesRoutingModule, 
    MatGridListModule,
     MatCardModule, 
     MatMenuModule,
     MatIconModule,
     MatButtonModule,
     LayoutModule,
     CoreModule
  ]
})
export class FeaturesModule { }
