package com.dj.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dj.entity.Projectstatus;
import com.dj.repository.ProjectStatusRepository;

@Service
public class ProjectStatusService {

	@Autowired
	private ProjectStatusRepository projectStatusRepository; 
	
	public List<Projectstatus> fetchProjectStatus(){
		return projectStatusRepository.findAll();
	}
	
	public Projectstatus insertProjectStatus(Projectstatus projectstatus) {
		return projectStatusRepository.save(projectstatus);
	}
	
	public Projectstatus getProjectStatusById(Integer projectPhaseId) {
		return projectStatusRepository.getProjectStatusById(projectPhaseId);
	}
}
