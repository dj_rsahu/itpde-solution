package com.dj.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dj.entity.User;
import com.dj.repository.UserRepository;

@Service
public class UserRepositoryService {
	@Autowired
	private UserRepository userRepository;

	public List<User> getAllUsers(String search) {
		return userRepository.getAllUsers(search);
	}

	public User getUserById(Integer userId) {
		return userRepository.findById(userId);
	}

}
