package com.dj.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dj.controller.TestScopeController;
import com.dj.dto.TestScopeDto;
import com.dj.entity.TestScope;
import com.dj.entity.User;
import com.dj.exception.ErrorResponse;
import com.dj.repository.TestScopeRepository;
import com.dj.utils.CommonUtils;

/**
 * @author ramesh.bandari
 *
 */
@Service
public class TestScopeService {
	@Autowired
	private TestScopeRepository testScopeRepository;

	private static final Logger logger = LoggerFactory.getLogger(TestScopeController.class);

	/**
	 * Save or Update test scope
	 * @param testScopeList
	 * @return
	 */
	public TestScopeDto saveOrUpdateScopeDetails(TestScopeDto testScopeDto, int estId) {
		User userObj = CommonUtils.getLoggedInUserDetails();
		logger.info("Test Scope Logged in user {} ", userObj.getFirstName());
		List<TestScope> testScopeRespList = new ArrayList<>();
		for (TestScope testScope : testScopeDto.getTestScopeListObj()) {
			if (testScope.getId() == null) {
				createTestScope(userObj, testScopeRespList, testScope, estId);
			} else {
				updateTestScope(userObj, testScopeRespList, testScope);
			}

		}
		return testScopeDto;

	}

	/**
	 * Creates test scope
	 * @param userObj
	 * @param testScopeRespList
	 * @param testScope
	 */
	public void createTestScope(User userObj, List<TestScope> testScopeRespList, TestScope testScope, int estId) {

		if (userObj != null) {
			testScope.setCreatedby(userObj.getId());
			//testScope.setUpdatedby(userObj.getId());
		}
		testScope.setCreateddate(new Date());
		testScope.setEstid(estId);
		//testScope.setUpdateddate(new Date());
		testScopeRespList.add(testScope);
		testScopeRepository.saveAll(testScopeRespList);
	}

	/**
	 * Update test scope
	 * @param userObj
	 * @param testScopeRespList
	 * @param testScope
	 */
	public void updateTestScope(User userObj, List<TestScope> testScopeRespList, TestScope testScope) {
		Optional<TestScope> testScopeObj = testScopeRepository.findById(testScope.getId());
		if(testScopeObj.isPresent()) {
			testScopeObj.get().setTsname(testScope.getTsname());
			testScopeObj.get().setTceffort(testScope.getTceffort());
			testScopeObj.get().setTsexeplan(testScope.getTsexeplan());
			testScopeObj.get().setInterfacerep(testScope.getInterfacerep());
			testScopeObj.get().setComments(testScope.getComments());
			testScopeObj.get().setTcrevised(testScope.getTcrevised());
			testScopeObj.get().setTdsetup(testScope.getTdsetup());
			testScopeObj.get().setTestexe(testScope.getTestexe());
			testScopeObj.get().setTotal(testScope.getTotal());
			testScopeObj.get().setTcnewreuse(testScope.getTcnewreuse());
			testScopeObj.get().setNooftc(testScope.getNooftc());
			testScopeObj.get().setExeprod(testScope.getExeprod());
		}
		else {
			throw new ErrorResponse("Id not found to update the TestScope details "+testScopeObj.get().getId(), HttpStatus.NOT_FOUND);
		}
		if (userObj != null) {
			testScopeObj.get().setUpdatedby(userObj.getId());
		}
		testScopeObj.get().setUpdateddate(new Date());
		testScopeRespList.add(testScopeObj.get());
		testScopeRepository.saveAll(testScopeRespList);
	}
	
	public List<TestScope> fetchTestScopeByEstId(Integer estid) {
		return testScopeRepository.fetchTestScopeDetails(estid);
	}
	
}
