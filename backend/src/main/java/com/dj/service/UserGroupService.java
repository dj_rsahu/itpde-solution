package com.dj.service;

import java.util.List;

import com.dj.entity.User;
import com.dj.entity.UserGroup;

public interface UserGroupService {
	public UserGroup saveUserGroup(UserGroup userGroup);

    public UserGroup findById(int userGroupId);
    
    public Boolean deleteUserGroupById(int userGroupId);
    
    
    public UserGroup updateUserGroup(UserGroup userGroup);
    
    public List<UserGroup> getUserGroups();
    
    public UserGroup getUserGroupBYName(String name);

	public UserGroup getUserGroupByUserId(int userId);

}
