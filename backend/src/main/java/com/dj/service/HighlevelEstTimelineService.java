package com.dj.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dj.dto.HighLevelEstTimeLineDto;
import com.dj.entity.Estimation;
import com.dj.entity.Highlevelesttimeline;
import com.dj.entity.Highlevelestweek;
import com.dj.entity.RRDGroup;
import com.dj.entity.User;
import com.dj.exception.ErrorResponse;
import com.dj.repository.HighlevelEstTimelineRepository;
import com.dj.repository.HighlevelestweekRepository;
import com.dj.utils.CommonUtils;
import com.dj.utils.DJConstants;
import com.dj.utils.StringFormatUtils;

@Service
public class HighlevelEstTimelineService {

	@Autowired
	private HighlevelEstTimelineRepository highlevelEstTimelineRepository;
	@Autowired
	private HighlevelestweekRepository highlevelestweekRepository;
	@Autowired
	private RRDGroupService rrdGroupService;
	@Autowired
	private EstimationService estimationService;
	@Autowired
	private UserRepositoryService userRepositoryService;

	private static final Logger logger = LoggerFactory.getLogger(HighlevelEstTimelineService.class);

	/**
	 * fetch all HighlevelEstTimelines
	 * 
	 * @return
	 */
	/*
	 * public List<Highlevelesttimeline> fetchHighlevelEstTimelines() { return
	 * highlevelEstTimelineRepository.fetchHighlevelEstTimelines((byte) 0); }
	 */

	public List<Highlevelesttimeline> fetchHighlevelEstTimelines(int estId) {
		// Highlevelesttimeline hlt = highlevelEstTimelineRepository.getOne(estId);
		return highlevelEstTimelineRepository.fetchHighlevelEstTimelines(estId, (byte) 0,
				DJConstants.ACTUAL_DAYS_IN_A_WEEK);
	}

	public Optional<Highlevelesttimeline> fetchHighlevelEstTimelinesById(int id) {
		Optional<Highlevelesttimeline> hlt = highlevelEstTimelineRepository.findById(id);
		if (hlt.isPresent()) {
			return hlt;
		}
		return null;
	}

	/**
	 * Save HLT data
	 * 
	 * @param hetList
	 * @return
	 */
	public List<Highlevelesttimeline> saveHighlevelEstTimelines(List<Highlevelesttimeline> hetList) {
		List<Highlevelesttimeline> hltListObj = new ArrayList<>();
		User userObj = CommonUtils.getLoggedInUserDetails();
		if (hetList != null && !hetList.isEmpty()) {
			for (Highlevelesttimeline hetObj : hetList) {
				if (hetObj.getId() == null) {
					saveHlt(userObj, hetObj);
					hltListObj.add(hetObj);
				} else {
					Optional<Highlevelesttimeline> hltObj = highlevelEstTimelineRepository.findById(hetObj.getId());
					if (hltObj.isPresent()) {
						if (hltObj.get() != null) {
							List<RRDGroup> rrdGroups = rrdGroupService
									.getRRDGroupsByGroupName(hltObj.get().getGroup().getGroupname());
							List<Integer> rrdGroupIds = new ArrayList<>();
							if (rrdGroups != null && !rrdGroups.isEmpty()) {
								for (RRDGroup rrdGroup : rrdGroups) {
									rrdGroupIds.add(rrdGroup.getRrdid());
								}

								List<Highlevelesttimeline> dbHighlevelesttimelines = highlevelEstTimelineRepository
										.getHighlevelesttimelinesByGroupIds(rrdGroupIds, hltObj.get().getEstid());
								if (dbHighlevelesttimelines != null && !dbHighlevelesttimelines.isEmpty()) {
									for (Highlevelesttimeline highlevelesttimeline : dbHighlevelesttimelines) {

										Estimation est = estimationService
												.findById(dbHighlevelesttimelines.get(0).getEstid());
										est.setUpdatedby(userObj.getId());
										est.setUpdatedDate(LocalDateTime.now());
										estimationService.saveEstimation(est);

										updateHlt(userObj, hetObj, highlevelesttimeline);

										hltListObj.add(highlevelesttimeline);
									}
								}
							}
						}

					} else {
						throw new ErrorResponse("Given Highlevel timeline Id not found to update ",
								HttpStatus.NOT_FOUND);
					}
				}
			}
		} else {
			throw new ErrorResponse("Nothing to save ", HttpStatus.BAD_REQUEST);
		}
		return highlevelEstTimelineRepository.saveAll(hltListObj);
	}

	private void updateHlt(User userObj, Highlevelesttimeline hetObj, Highlevelesttimeline hltObj) {
		hltObj.setActualspent(hetObj.getActualspent());
		hltObj.setContingency(hetObj.getContingency());
		hltObj.setUpdateddate(new Date());
		if (userObj != null) {
			hltObj.setUpdatedby(userObj.getId());
		}
		// highlevelEstTimelineRepository.save(hltObj);
	}

	private void saveHlt(User userObj, Highlevelesttimeline hetObj) {
		List<Highlevelesttimeline> hltListObj = new ArrayList<>();
		if (hetObj != null & hetObj.getEstid() == null) {
			throw new ErrorResponse("Estimation id cannot be null", HttpStatus.BAD_REQUEST);
		}
		if (userObj != null) {
			hetObj.setCreatedby(userObj.getId());
		}
		hetObj.setCreateddate(new Date());
		hltListObj.add(hetObj);

		// highlevelEstTimelineRepository.save(hetObj);
	}

	public List<Highlevelesttimeline> getHighlevelesttimelineByEstId(int estId) {
		return highlevelEstTimelineRepository.getHighlevelesttimelineByEstId(estId);
	}

	@Transactional
	public void createHighlevelesttimeline(HighLevelEstTimeLineDto highLevelEstTimeLineDto) {
		if (highLevelEstTimeLineDto != null) {
			List<Highlevelesttimeline> highlevelesttimelines = highLevelEstTimeLineDto.getHighlevelesttimelines();
			if (highlevelesttimelines != null && !highlevelesttimelines.isEmpty()) {
				User loggedInUser = CommonUtils.getLoggedInUserDetails();
				for (Highlevelesttimeline highlevelesttimeline : highlevelesttimelines) {
					if (loggedInUser != null) {
						highlevelesttimeline.setCreatedby(loggedInUser.getId());
						highlevelesttimeline.setUpdatedby(loggedInUser.getId());
					}
					highlevelesttimeline.setCreateddate(new Date());
					highlevelesttimeline.setUpdateddate(new Date());
					highlevelesttimeline.setIsdelete((byte) 0);
					Highlevelesttimeline highlevelesttimelinedb = highlevelEstTimelineRepository
							.save(highlevelesttimeline);
					if (highlevelesttimelinedb != null) {
						List<Highlevelestweek> highlevelestweeks = highlevelesttimeline.getHighlevelestweeks();
						if (highlevelestweeks != null && !highlevelestweeks.isEmpty()) {
							for (Highlevelestweek highlevelestweek : highlevelestweeks) {
								highlevelestweek.setEstid(highlevelesttimeline.getEstid());
								highlevelestweek.setHighlevelesttimelineid(highlevelesttimeline.getId());
								if (loggedInUser != null) {
									highlevelestweek.setCreatedby(loggedInUser.getId());
									highlevelestweek.setUpdatedby(loggedInUser.getId());
								}
								highlevelestweek.setCreateddate(new Date());
								highlevelestweek.setUpdateddate(new Date());
								highlevelestweek.setIsdelete((byte) 0);
							}
							highlevelestweekRepository.saveAll(highlevelestweeks);
						}
					}
				}
			}
		}
	}

	@Transactional
	public void updateHighlevelesttimeline(HighLevelEstTimeLineDto highLevelEstTimeLineDto) {
		if (highLevelEstTimeLineDto != null) {
			List<Highlevelesttimeline> highlevelesttimelines = highLevelEstTimeLineDto.getHighlevelesttimelines();
			if (highlevelesttimelines != null && !highlevelesttimelines.isEmpty()) {
				User loggedInUser = CommonUtils.getLoggedInUserDetails();
				for (Highlevelesttimeline highlevelesttimeline : highlevelesttimelines) {
					Highlevelesttimeline highlevelesttimelinedb = highlevelEstTimelineRepository
							.getHighlevelesttimelineById(highlevelesttimeline.getId());
					if (highlevelesttimelinedb != null) {
						highlevelesttimelinedb.setGroupid(highlevelesttimeline.getGroupid());
						highlevelesttimelinedb.setResourceid(highlevelesttimeline.getResourceid());
						highlevelesttimelinedb.setSystems(highlevelesttimeline.getSystems());
						if (loggedInUser != null) {
							highlevelesttimelinedb.setUpdatedby(loggedInUser.getId());
						}
						highlevelesttimelinedb.setUpdateddate(new Date());
						highlevelEstTimelineRepository.save(highlevelesttimelinedb);

						List<Highlevelestweek> highlevelestweeks = highlevelesttimeline.getHighlevelestweeks();
						if (highlevelestweeks != null && !highlevelestweeks.isEmpty()) {
							for (Highlevelestweek highlevelestweek : highlevelestweeks) {
								highlevelestweek.setEstid(highlevelesttimelinedb.getEstid());
								highlevelestweek.setHighlevelesttimelineid(highlevelesttimelinedb.getId());
								if (highlevelestweek.getHighlevelestweekid() == null) {
									if (loggedInUser != null) {
										highlevelestweek.setCreatedby(loggedInUser.getId());
										highlevelestweek.setUpdatedby(loggedInUser.getId());
									}
									highlevelestweek.setCreateddate(new Date());
									highlevelestweek.setUpdateddate(new Date());
									highlevelestweek.setIsdelete((byte) 0);
								} else {
									if (loggedInUser != null) {
										highlevelestweek.setUpdatedby(loggedInUser.getId());
									}
									highlevelestweek.setUpdateddate(new Date());
								}
							}
							highlevelestweekRepository.saveAll(highlevelestweeks);
						}
					}
				}
			}
		}

	}

	@Transactional
	public void deleteHighlevelesttimeline(HighLevelEstTimeLineDto highLevelEstTimeLineDto) {
		if (highLevelEstTimeLineDto != null) {
			List<Highlevelesttimeline> highlevelesttimelines = highLevelEstTimeLineDto.getHighlevelesttimelines();
			if (highlevelesttimelines != null && !highlevelesttimelines.isEmpty()) {
				if (highlevelesttimelines != null && !highlevelesttimelines.isEmpty()) {
					List<Integer> highlevelesttimelineIds = new ArrayList<>();
					for (Highlevelesttimeline highlevelesttimeline : highlevelesttimelines) {
						highlevelesttimelineIds.add(highlevelesttimeline.getId());
					}
					if (highlevelesttimelineIds != null && !highlevelesttimelineIds.isEmpty()) {
						highlevelEstTimelineRepository.deleteHighlevelesttimelineByIds(highlevelesttimelineIds);
						highlevelestweekRepository
								.deleteHighlevelestweeksByHighleveleesttimelineIds(highlevelesttimelineIds);
					}
				}
			}
		}
	}

	@Transactional
	public void saveHighlevelesttimeline(HighLevelEstTimeLineDto highLevelEstTimeLineDto, int estId) {
		// highleveltimelineValidations(highLevelEstTimeLineDto);
		User loggedInUser = CommonUtils.getLoggedInUserDetails();

		/*
		 * Estimation estimation = estimationService.findById(estId); if (estimation !=
		 * null) { if (estimation.getIseditable() == 0 && estimation.getIseditableby()
		 * != null && (estimation.getIseditableby() != loggedInUser.getId())) { User
		 * estimationUpdateUser =
		 * userRepositoryService.getUserById(estimation.getIseditableby()); throw new
		 * ErrorResponse("Concurrent users are not allowed to edit the same estimate, Currently "
		 * +estimationUpdateUser.getFirstName() + " " +
		 * estimationUpdateUser.getLastName()+" Session is In progress.",
		 * HttpStatus.NOT_FOUND); } } else { throw new
		 * ErrorResponse("Estimation not found", HttpStatus.NOT_FOUND); }
		 */

		if (highLevelEstTimeLineDto != null) {
			List<Highlevelesttimeline> highlevelesttimelines = highLevelEstTimeLineDto.getHighlevelesttimelines();
			if (highlevelesttimelines != null && !highlevelesttimelines.isEmpty()) {
				// Delete
				highLevelTimeLinesDelete(estId, highlevelesttimelines);

				// Add Or Update
				for (Highlevelesttimeline highlevelesttimeline : highlevelesttimelines) {
					if (highlevelesttimeline.getId() == null || highlevelesttimeline.getId() == 0) {
						highLevelTimeLinesCreate(estId, loggedInUser, highlevelesttimeline);
					} else {
						highLevelTimeLinesUpdate(estId, loggedInUser, highlevelesttimeline);
					}
				}
			}
		}
	}

	private void highleveltimelineValidations(HighLevelEstTimeLineDto highLevelEstTimeLineDto) {
		if (highLevelEstTimeLineDto != null) {
			List<Highlevelesttimeline> highlevelesttimelines = highLevelEstTimeLineDto.getHighlevelesttimelines();
			if (highlevelesttimelines != null && !highlevelesttimelines.isEmpty()) {
				for (Highlevelesttimeline highlevelesttimeline : highlevelesttimelines) {
					if (highlevelesttimeline.getGroupid() == null || highlevelesttimeline.getGroupid() == 0) {
						throw new ErrorResponse("Group id is mandatory", HttpStatus.NOT_ACCEPTABLE);
					}
					if (highlevelesttimeline.getResourceid() == null || highlevelesttimeline.getResourceid() == 0) {
						throw new ErrorResponse("Resource id is mandatory", HttpStatus.NOT_ACCEPTABLE);
					}
				}
			}
		}
	}

	@Transactional
	private void highLevelTimeLinesUpdate(int estId, User loggedInUser, Highlevelesttimeline highlevelesttimeline) {
		Highlevelesttimeline highlevelesttimelinedb = highlevelEstTimelineRepository
				.getHighlevelesttimelineById(highlevelesttimeline.getId());
		if (highlevelesttimelinedb != null) {
			highlevelesttimelinedb.setCostperday(highlevelesttimeline.getCostperday());
			highlevelesttimelinedb.setGroupid(highlevelesttimeline.getGroupid());
			highlevelesttimelinedb.setResourceid(highlevelesttimeline.getResourceid());
			highlevelesttimelinedb.setSystems(highlevelesttimeline.getSystems());
			if (loggedInUser != null) {
				highlevelesttimelinedb.setUpdatedby(loggedInUser.getId());
			}
			highlevelesttimelinedb.setUpdateddate(new Date());
			highlevelEstTimelineRepository.save(highlevelesttimelinedb);
			List<Integer> hltimelineIdsTemp = new ArrayList<>();
			hltimelineIdsTemp.add(highlevelesttimelinedb.getId());
			highlevelestweekRepository.deleteHighlevelestweeksByHighleveleesttimelineIds(hltimelineIdsTemp);
			List<Highlevelestweek> highlevelestweeks = highlevelesttimeline.getHighlevelestweeks();
			if (highlevelestweeks != null && !highlevelestweeks.isEmpty()) {
				for (Highlevelestweek highlevelestweek : highlevelestweeks) {
					highlevelestweek.setEstid(estId);
					highlevelestweek.setHighlevelesttimelineid(highlevelesttimelinedb.getId());
					if (loggedInUser != null) {
						highlevelestweek.setCreatedby(loggedInUser.getId());
						highlevelestweek.setUpdatedby(loggedInUser.getId());
					}
					highlevelestweek.setCreateddate(new Date());
					highlevelestweek.setUpdateddate(new Date());
					highlevelestweek.setIsdelete((byte) 0);

				}
				highlevelestweekRepository.saveAll(highlevelestweeks);
			}
		}
	}

	@Transactional
	private void highLevelTimeLinesCreate(int estId, User loggedInUser, Highlevelesttimeline highlevelesttimeline) {
		if (loggedInUser != null) {
			highlevelesttimeline.setCreatedby(loggedInUser.getId());
			highlevelesttimeline.setUpdatedby(loggedInUser.getId());
		}
		highlevelesttimeline.setEstid(estId);
		highlevelesttimeline.setCreateddate(new Date());
		highlevelesttimeline.setUpdateddate(new Date());
		highlevelesttimeline.setIsdelete((byte) 0);
		Highlevelesttimeline highlevelesttimelinedb = highlevelEstTimelineRepository.save(highlevelesttimeline);
		if (highlevelesttimelinedb != null) {
			List<Highlevelestweek> highlevelestweeks = highlevelesttimeline.getHighlevelestweeks();
			if (highlevelestweeks != null && !highlevelestweeks.isEmpty()) {
				for (Highlevelestweek highlevelestweek : highlevelestweeks) {
					highlevelestweek.setEstid(estId);
					highlevelestweek.setHighlevelesttimelineid(highlevelesttimeline.getId());
					if (loggedInUser != null) {
						highlevelestweek.setCreatedby(loggedInUser.getId());
						highlevelestweek.setUpdatedby(loggedInUser.getId());
					}
					highlevelestweek.setCreateddate(new Date());
					highlevelestweek.setUpdateddate(new Date());
					highlevelestweek.setIsdelete((byte) 0);
				}
				highlevelestweekRepository.saveAll(highlevelestweeks);
			}
		}
	}

	@Transactional
	private void highLevelTimeLinesDelete(int estId, List<Highlevelesttimeline> highlevelesttimelines) {
		List<Integer> hltimelineIds = new ArrayList<>();
		for (Highlevelesttimeline highlevelesttimeline : highlevelesttimelines) {
			if (highlevelesttimeline.getId() != null && highlevelesttimeline.getId() != 0) {
				hltimelineIds.add(highlevelesttimeline.getId());
			}
		}
		if (hltimelineIds != null && !hltimelineIds.isEmpty()) {
			List<Highlevelesttimeline> highlevelesttimelinesDB = highlevelEstTimelineRepository
					.getHighLevelTimelinesNotInIds(hltimelineIds, estId);
			if (highlevelesttimelinesDB != null && !highlevelesttimelinesDB.isEmpty()) {
				List<Integer> hltimelineIdsNotExistedInDB = new ArrayList<>();
				for (Highlevelesttimeline highlevelesttimelinetmp : highlevelesttimelinesDB) {
					hltimelineIdsNotExistedInDB.add(highlevelesttimelinetmp.getId());
				}
				if (hltimelineIdsNotExistedInDB != null && !hltimelineIdsNotExistedInDB.isEmpty()) {
					highlevelEstTimelineRepository.deleteHighlevelesttimelineByIds(hltimelineIdsNotExistedInDB);
					highlevelestweekRepository
							.deleteHighlevelestweeksByHighleveleesttimelineIds(hltimelineIdsNotExistedInDB);
				}
			}
		}
	}

}
