package com.dj.service;

import java.util.Date;
import java.util.List;

import com.dj.entity.Estimation;
import com.dj.entity.User;


public interface EstimationService {
	public Estimation saveEstimation(Estimation estimation);

    public Estimation findById(int estimationId);
    
    public Boolean deleteEstimationById(int estimationId);
    
    
    public Estimation updateEstimation(Estimation estimation);
    
    public List<Estimation> getEstimations();
    
    public List<Estimation> getEstimationsByUserId(int createdBy);
    
    public List<Estimation> getEstimationStatusByRole(User userObj);

	public void deleteEstimationDataById(int estimationId);

	public Estimation cloneEstimation(int estimationId);

	public List<Estimation> getEstimationsByIsEditable(byte iseditable, byte isdelete);
	public void updateEstimationStatus(Estimation estimation);

	public void updateIseditableFlagAndIseditableDate(int estId, byte b, Date iseditableDate, Integer loggedInUserId);

	public Estimation makeEstimationEditableOrNonEditable(int estimationId, byte isEditable);

	public Estimation increaseEstimationEditableDuration(int estimationId);

	public void estimationSave(Estimation estimation);


}
