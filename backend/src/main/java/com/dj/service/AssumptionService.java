package com.dj.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dj.dto.AssumptionDto;
import com.dj.entity.Assumption;
import com.dj.entity.User;
import com.dj.exception.ErrorResponse;
import com.dj.repository.AssumptionRepository;
import com.dj.utils.CommonUtils;
import com.dj.utils.DJConstants;
import com.dj.utils.StringFormatUtils;

@Service
public class AssumptionService {

	private static final Logger logger = LoggerFactory.getLogger(AssumptionService.class);

	@Autowired
	private AssumptionRepository assumptionRepository;

	public List<Assumption> getAssumptions(String search) {
		return assumptionRepository.getAssumptions(search);
	}

	@Transactional
	public void createAssumption(Assumption assumption) {
		assumptionValidations(assumption);
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		if (loggedInUser != null) {
			assumption.setCreatedby(loggedInUser.getId());
			assumption.setUpdatedby(loggedInUser.getId());
		}
		assumption.setCreateddate(new Date());
		assumption.setUpdateddate(new Date());
		assumption.setIsdelete((byte) 0);
		/*
		 * Assumption assumptionTemp =
		 * assumptionRepository.getLastAssumptionByEstId(assumption.getEstid()); if
		 * (assumptionTemp == null) { assumption.setAssumptionref(DJConstants.
		 * ASSUMPTION_REFERENCE_STARTING_LETTER_AND_NUMBER); } else { if
		 * (assumptionTemp.getAssumptionref() != null &&
		 * !StringFormatUtils.isEmpty(assumptionTemp.getAssumptionref())) { String
		 * refNumber = assumptionTemp.getAssumptionref().substring(1,
		 * assumptionTemp.getAssumptionref().length()); try { Integer refNum =
		 * Integer.parseInt(refNumber); refNum = refNum + 1; String refNumStr = ""; if
		 * (refNum < 10) { refNumStr = DJConstants.ASSUMPTION_REFERENCE_STARTING_LETTER
		 * + "0" + refNum; } else { refNumStr =
		 * DJConstants.ASSUMPTION_REFERENCE_STARTING_LETTER + "" + refNum; }
		 * assumption.setAssumptionref(refNumStr); } catch (Exception e) {
		 * logger.info("Parsing exception: " + e); } } }
		 */
		assumptionRepository.save(assumption);
	}

	private void assumptionValidations(Assumption assumption) {
		if (assumption.getEstid() == null || assumption.getEstid() == 0) {
			throw new ErrorResponse("Estimation id is mandatory", HttpStatus.NOT_ACCEPTABLE);
		}
		if (assumption.getArea() == null || StringFormatUtils.isEmpty(assumption.getArea())) {
			throw new ErrorResponse("Area is mandatory", HttpStatus.NOT_ACCEPTABLE);
		}

		if (assumption.getAssumption() == null || StringFormatUtils.isEmpty(assumption.getAssumption())) {
			throw new ErrorResponse("Assemption is mandatory", HttpStatus.NOT_ACCEPTABLE);
		}

		if (assumption.getImpact() == null || StringFormatUtils.isEmpty(assumption.getImpact())) {
			throw new ErrorResponse("Impact is mandatory", HttpStatus.NOT_ACCEPTABLE);
		}
	}

	@Transactional
	public Assumption updateAssumption(Assumption assumption) {
		assumptionValidations(assumption);
		Assumption assumptiondb = assumptionRepository.getAssumptionById(assumption.getId());
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		assumptiondb.setArea(assumption.getArea());
		assumptiondb.setAssumption(assumption.getAssumption());
		assumptiondb.setImpact(assumption.getImpact());
		if (loggedInUser != null) {
			assumptiondb.setUpdatedby(loggedInUser.getId());
		}
		assumptiondb.setUpdateddate(new Date());
		assumptionRepository.save(assumptiondb);
		return assumptiondb;
	}

	@Transactional
	public Assumption deleteAssumption(int assumptionId) {
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		Assumption assumption = assumptionRepository.getAssumptionById(assumptionId);
		if (assumption != null) {
			assumption.setIsdelete((byte) 1);
			if (loggedInUser != null) {
				assumption.setUpdatedby(loggedInUser.getId());
				assumption.setUpdateddate(new Date());
			}
		}
		assumptionRepository.save(assumption);
		return assumption;
	}

	public List<Assumption> getAssumptionsByEstId(int estId, String search) {
		return assumptionRepository.getAssumptionsByEstId(estId, search);
	}
	
	@Transactional
	public void saveAssumption(AssumptionDto assumptionDto) {
		if (assumptionDto != null) {
			List<Assumption> assumptions = assumptionDto.getAssumptions();
			List<Assumption> assumptionsTemp = new ArrayList<>();
			if (assumptions != null && !assumptions.isEmpty()) {
				User loggedInUser = CommonUtils.getLoggedInUserDetails();
				for (Assumption assumption : assumptions) {
					assumptionValidations(assumption);
					if (assumption.getId() == null || assumption.getId() == 0) {
						if (loggedInUser != null) {
							assumption.setCreatedby(loggedInUser.getId());
							assumption.setUpdatedby(loggedInUser.getId());
						}
						assumption.setCreateddate(new Date());
						assumption.setUpdateddate(new Date());
						assumption.setIsdelete((byte) 0);
						/*
						 * Assumption assumptionTemp = assumptionRepository
						 * .getLastAssumptionByEstId(assumption.getEstid()); if (assumptionTemp == null)
						 * { assumption.setAssumptionref(DJConstants.
						 * ASSUMPTION_REFERENCE_STARTING_LETTER_AND_NUMBER); } else { if
						 * (assumptionTemp.getAssumptionref() != null &&
						 * !StringFormatUtils.isEmpty(assumptionTemp.getAssumptionref())) { String
						 * refNumber = assumptionTemp.getAssumptionref().substring(1,
						 * assumptionTemp.getAssumptionref().length()); try { Integer refNum =
						 * Integer.parseInt(refNumber); refNum = refNum + 1; String refNumStr = ""; if
						 * (refNum < 10) { refNumStr = DJConstants.ASSUMPTION_REFERENCE_STARTING_LETTER
						 * + "0" + refNum; } else { refNumStr =
						 * DJConstants.ASSUMPTION_REFERENCE_STARTING_LETTER + "" + refNum; }
						 * assumption.setAssumptionref(refNumStr); } catch (Exception e) {
						 * logger.info("Parsing exception: " + e); } } }
						 */
						assumptionsTemp.add(assumption);
					} else {
						if(assumption.getIsdelete() == 0) {
							Assumption assumptiondb = null;
							assumptiondb = assumptionRepository.getAssumptionById(assumption.getId());
							if(assumptiondb != null) {
								assumptiondb.setArea(assumption.getArea());
								assumptiondb.setAssumption(assumption.getAssumption());
								assumptiondb.setImpact(assumption.getImpact());
								if (loggedInUser != null) {
									assumptiondb.setUpdatedby(loggedInUser.getId());
								}
								assumptiondb.setUpdateddate(new Date());
								assumptionsTemp.add(assumptiondb);
							}
						} else {
							Assumption assumptiondb = null;
							assumptiondb = assumptionRepository.getAssumptionById(assumption.getId());
							if (assumptiondb != null) {
								assumptiondb.setIsdelete((byte) 1);
								if (loggedInUser != null) {
									assumptiondb.setUpdatedby(loggedInUser.getId());
									assumptiondb.setUpdateddate(new Date());
								}
								assumptionsTemp.add(assumptiondb);
							}
						}
					}
				}
				if(assumptionsTemp != null && !assumptionsTemp.isEmpty()) {
					assumptionRepository.saveAll(assumptionsTemp);
				}
			}
		}
	}

	public Integer getEstimationsCountByEstimationId(int estimationId) {
		return assumptionRepository.getEstimationsCountByEstimationId(estimationId);
	}

}
