package com.dj.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dj.entity.User;
import com.dj.entity.UserGroup;

import com.dj.repository.UserGroupRepository;
import com.dj.utils.CommonUtils;

@Service
public class UserGroupServiceImpl implements UserGroupService{
	 @Autowired
	  UserGroupRepository userGroupRepository; 

	@Override
	@Transactional
	public UserGroup saveUserGroup(UserGroup userGroup) {
		return userGroupRepository.save(userGroup);
		
	}

	@Override
	@Transactional
	public UserGroup findById(int userGroupId) {
		return userGroupRepository.findById(userGroupId);
	}

	@Override
	@Transactional
	public Boolean deleteUserGroupById(int userGroupId) {
		userGroupRepository.deleteById(userGroupId);
		return true ;
	}

	@Override
	@Transactional
	public UserGroup updateUserGroup(UserGroup userGroup) {
		return userGroupRepository.save(userGroup);
	}

	@Override
	@Transactional
	public List<UserGroup> getUserGroups() {
		return userGroupRepository.findAll();
	}
     
	@Override
	@Transactional
	public UserGroup getUserGroupBYName(String name) {
		
		UserGroup group= userGroupRepository.findBYName(name);
		if(group==null) {
			UserGroup userGroupObj = new UserGroup();
			userGroupObj.setGroupName(name);
			userGroupObj.setCreatedby(CommonUtils.getLoggedInUserDetails().getId());
			userGroupRepository.save(userGroupObj);
			return userGroupObj;
		}
		return group;
	}

	@Override
	public UserGroup getUserGroupByUserId(int userId) {
		// TODO Auto-generated method stub
		return userGroupRepository.getUserGroupByUserId(userId);
	}

}
