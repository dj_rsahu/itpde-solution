package com.dj.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dj.entity.EstimatedDocuments;
import com.dj.entity.User;
import com.dj.exception.ErrorResponse;
import com.dj.repository.EstimationDocumentRepository;
import com.dj.utils.CommonUtils;

@Service
public class EstimationDocumentImpl implements EstimationDocumentService {
	
	 @Autowired
	  EstimationDocumentRepository estimationDocumentRepository; 

	@Override
	@Transactional
	public EstimatedDocuments saveEstimatedDocument(EstimatedDocuments estimatedDocument) {
		estimatedDocument.setCreatedby( CommonUtils.getLoggedInUserDetails().getId());
		return estimationDocumentRepository.save(estimatedDocument);
	}

	@Override
	@Transactional
	public EstimatedDocuments findById(int estimationId) {
		
		return estimationDocumentRepository.findById(estimationId);
	}

	@Override
	@Transactional
	public void deleteEstimatedDocumentById(int id) {
		if(estimationDocumentRepository.findById(id) != null){
			estimationDocumentRepository.deleteById(id);
		}else {
			throw new ErrorResponse("Id not found to delete "+id, HttpStatus.NOT_FOUND);
		}
	}

	@Override
	@Transactional
	public List<EstimatedDocuments> updateEstimatedDocuments(List<EstimatedDocuments> estimatedDocumentList) {
		User user = CommonUtils.getLoggedInUserDetails();
		List<EstimatedDocuments> estDocsList = new ArrayList<>();
		for (EstimatedDocuments estimatedDocuments : estimatedDocumentList) {
			EstimatedDocuments estDocsObj = estimationDocumentRepository.findById(estimatedDocuments.getId());
			if(estDocsObj != null){
				estDocsObj.setDoc_location(estimatedDocuments.getDoc_location());
				estDocsObj.setDoc_version(estimatedDocuments.getDoc_version());
				estDocsObj.setDoc_title(estimatedDocuments.getDoc_title());
				estDocsObj.setUpdatedby(user.getId());
				estDocsObj.setUpdatedDate(LocalDateTime.now());
				estDocsList.add(estDocsObj);
				
			}
		}
		return estimationDocumentRepository.saveAll(estDocsList);
	}

	@Override
	@Transactional
	public List<EstimatedDocuments> getEstimatedDocuments() {
		
		return estimationDocumentRepository.findAll();
	}

}
