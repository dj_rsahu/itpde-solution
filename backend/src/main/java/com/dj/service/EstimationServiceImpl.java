package com.dj.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dj.entity.Assumption;
import com.dj.entity.DetailedEstimates;
import com.dj.entity.Domain;
import com.dj.entity.EstimatedDocuments;
import com.dj.entity.Estimation;
import com.dj.entity.Highlevelesttimeline;
import com.dj.entity.Highlevelestweek;
import com.dj.entity.Projectstatus;
import com.dj.entity.TestScope;
import com.dj.entity.User;
import com.dj.exception.ErrorResponse;
import com.dj.repository.AssumptionRepository;
import com.dj.repository.DetailedEstRepository;
import com.dj.repository.DomainRepository;
import com.dj.repository.EstimationDocumentRepository;
import com.dj.repository.EstimationRepository;
import com.dj.repository.HighlevelEstTimelineRepository;
import com.dj.repository.HighlevelestweekRepository;
import com.dj.repository.ProjectStatusRepository;
import com.dj.repository.TestScopeRepository;
import com.dj.repository.UserRepository;
import com.dj.utils.CommonUtils;
import com.dj.utils.DJConstants;
import com.dj.utils.StringFormatUtils;

@Service
public class EstimationServiceImpl implements EstimationService {
	@Autowired
	EstimationRepository estimationRepository;

	@Autowired
	private ProjectStatusRepository projectStatusRepository;

	@Autowired
	private EstimationDocumentRepository estimationDocumentRepository;
	@Autowired
	private DomainRepository domainRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private HighlevelEstTimelineRepository highlevelEstTimelineRepository;
	@Autowired
	private HighlevelestweekRepository highlevelestweekRepository;
	@Autowired
	private DetailedEstRepository detailedEstRepository;
	@Autowired
	private TestScopeRepository testScopeRepository;
	@Autowired
	private AssumptionRepository assumptionRepository;

	@Override
	@Transactional
	public Estimation saveEstimation(Estimation estimation) {
		if (estimation != null) {
			// Set domain name
			Optional<Domain> domain = domainRepository.findById(estimation.getDomainid());
			if (domain.isPresent()) {
				estimation.setDomainName(domain.get().getName());
			}

			// Set projectLeadUser object
			Integer projectLeadId = estimation.getProject_lead();
			User projectLeadUserObj = userRepository.findById(projectLeadId);
			if (projectLeadUserObj != null) {
				estimation.setProjectLeadUser(projectLeadUserObj);
			}
			estimation.setIseditable((byte) 1);
			// estimation.setProject_phase("0");
			// Set project phase name
			/*
			 * Integer projectPhaseId = estimation.getProjectphaseid(); if(projectPhaseId !=
			 * null) { Optional<Projectstatus> projStatus =
			 * projectStatusRepository.findById(projectPhaseId); if(projStatus.isPresent())
			 * { estimation.setProjectPhaseName(projStatus.get().getName());
			 * estimation.setProjectphaseid(projStatus.get().getId()); } }
			 */

			// Set audit columns
			User user = CommonUtils.getLoggedInUserDetails();
			if (user != null) {
				estimation.setCreatedby(user.getId());
				estimation.setCreatedDate(LocalDateTime.now());
				estimation.setUpdatedby(user.getId());
				estimation.setUpdatedDate(LocalDateTime.now());
			}

			// Set project status
			Projectstatus projectStatusObj = null;
			if (estimation.getProjectStatusOthers() != null) {
				Projectstatus findProjectStatus = projectStatusRepository
						.findProjectStatus(estimation.getProjectStatusOthers());
				if (findProjectStatus != null) {
					estimation.setProjectphaseid(findProjectStatus.getId());
				} else {
					projectStatusObj = prepareProjectStatus(estimation, user);
					if (projectStatusObj != null) {
						estimation.setProjectphaseid(projectStatusObj.getId());
					}
				}
			} else {
				if (estimation.getProject_phase() != null
						&& !StringFormatUtils.isEmpty(estimation.getProject_phase())) {
					estimation.setProjectphaseid(Integer.parseInt(estimation.getProject_phase()));
				} else {
					estimation.setProjectphaseid(0);
				}
			}

			estimationRepository.save(estimation);
			List<EstimatedDocuments> estDocs = estimation.getEstimationDocuments();
			prepareEstDocuments(user, estDocs);
		} else {
			throw new ErrorResponse("Estimation object cannot be null", HttpStatus.BAD_REQUEST);
		}
		return estimation;

	}

	private void prepareEstDocuments(User user, List<EstimatedDocuments> estDocs) {
		if (estDocs != null && !estDocs.isEmpty()) {
			for (EstimatedDocuments estimatedDocuments : estDocs) {
				if (user != null) {
					estimatedDocuments.setCreatedby(user.getId());
					estimatedDocuments.setCreatedDate(LocalDateTime.now());
					estimatedDocuments.setUpdatedby(user.getId());
					estimatedDocuments.setUpdatedDate(LocalDateTime.now());
					estimationDocumentRepository.save(estimatedDocuments);
				}
			}
		}
	}

	private Projectstatus prepareProjectStatus(Estimation estimation, User userObj) {
		Projectstatus projectStatusObj = null;
		Projectstatus projStatus = new Projectstatus();
		if (userObj != null) {
			projStatus.setCreatedby(userObj.getId());
			projStatus.setCreateddate(new Date());
			projStatus.setUpdatedby(userObj.getId());
			projStatus.setUpdateddate(new Date());
		}
		projStatus.setName(estimation.getProjectStatusOthers());
		
		return projectStatusRepository.save(projStatus);
	}

	@Override
	@Transactional
	public Estimation findById(int estimationId) {

		return estimationRepository.findById(estimationId);
	}

	@Override
	@Transactional
	public Boolean deleteEstimationById(int estimationId) {
		Estimation estimation = estimationRepository.findById(estimationId);
		if (estimation != null) {
			List<EstimatedDocuments> estDocsList = estimation.getEstimationDocuments();
			for (EstimatedDocuments estimatedDocuments : estDocsList) {
				estimationDocumentRepository.delete(estimatedDocuments);
			}
		} else {
			throw new ErrorResponse("Id not found to delete " + estimationId, HttpStatus.NOT_FOUND);
		}
		return true;
	}

	@Override
	@Transactional
	public Estimation updateEstimation(Estimation estimation) {
		User useObj = CommonUtils.getLoggedInUserDetails();

		Estimation estimationObj = estimationRepository.findById(estimation.getId());
		estimationObj.setEst_project_name(estimation.getEst_project_name());
		//estimationObj.setEstmation_status(estimationObj.getEstmation_status());
		estimationObj.setEstimation_cost(estimation.getEstimation_cost());
		estimationObj.setProject_lead(estimation.getProject_lead());
		Projectstatus projectStatusObj = null;
		if (estimation.getProjectStatusOthers() != null) {
			Projectstatus findProjectStatus = projectStatusRepository
					.findProjectStatus(estimation.getProjectStatusOthers());
			if (findProjectStatus != null) {
				estimationObj.setProjectphaseid(findProjectStatus.getId());
			} else {
				projectStatusObj = prepareProjectStatus(estimation, useObj);
				if (projectStatusObj != null) {
					estimationObj.setProjectphaseid(projectStatusObj.getId());
				}
			}
		} else {
			if (estimation.getProject_phase() != null && !StringFormatUtils.isEmpty(estimation.getProject_phase())) {
				estimationObj.setProjectphaseid(Integer.parseInt(estimation.getProject_phase()));
			} else {
				estimationObj.setProjectphaseid(0);
			}
		}
		estimationObj.setDurationInWeek(estimation.getDurationInWeek());
		estimationObj.setDomainid(estimation.getDomainid());
		estimationObj.setUpdatedby(useObj.getId());
		estimationObj.setUpdatedDate(LocalDateTime.now());
		estimationRepository.save(estimationObj);

		if (estimationObj != null) {
			List<EstimatedDocuments> estimatedDocuments = estimationObj.getEstimationDocuments();

			deleteIfRecordIsInDBButNotInRequest(estimationObj, estimatedDocuments);

			if (estimatedDocuments != null && !estimatedDocuments.isEmpty()) {
				for (EstimatedDocuments estimatedDocumentsTemp : estimatedDocuments) {
					EstimatedDocuments dbEstDoc = null;
					if (estimatedDocumentsTemp.getId() != null) {
						dbEstDoc = estimationDocumentRepository.findById(estimatedDocumentsTemp.getId());
					}

					if (dbEstDoc != null) {
						dbEstDoc.setDoc_location(estimatedDocumentsTemp.getDoc_location());
						dbEstDoc.setDoc_version(estimatedDocumentsTemp.getDoc_version());
						dbEstDoc.setDoc_title(estimatedDocumentsTemp.getDoc_title());
						if (useObj != null) {
							dbEstDoc.setUpdatedby(useObj.getId());
						}
						dbEstDoc.setUpdatedDate(LocalDateTime.now());
						estimationDocumentRepository.save(dbEstDoc);
					} else {
						if (useObj != null) {
							estimatedDocumentsTemp.setCreatedby(useObj.getId());
							estimatedDocumentsTemp.setUpdatedby(useObj.getId());
						}
						estimatedDocumentsTemp.setCreatedDate(LocalDateTime.now());
						estimatedDocumentsTemp.setUpdatedDate(LocalDateTime.now());
						estimationDocumentRepository.save(estimatedDocumentsTemp);
					}

				}

			}
			
			User projectLead = userRepository.findById(estimation.getProject_lead());
			estimationObj.setProjectLeadUser(projectLead);
			
			Projectstatus projectstatus = projectStatusRepository.getProjectStatusById(estimationObj.getProjectphaseid());
			estimationObj.setProjectStatusObj(projectstatus);
		}
	return estimationObj;
	}

	private void deleteIfRecordIsInDBButNotInRequest(Estimation estimation,
			List<EstimatedDocuments> estimatedDocuments) {
		Estimation dbEstimation = estimationRepository.findById(estimation.getId());
		if (dbEstimation != null) {
			List<EstimatedDocuments> dbEstDocsList = estimationDocumentRepository
					.findEstDocsListByEstId(dbEstimation.getId());
			List<EstimatedDocuments> newDataList = estimatedDocuments;
			List<EstimatedDocuments> oldDataList = dbEstDocsList;

			List<EstimatedDocuments> deletedList = new ArrayList<>(oldDataList);
			deletedList.removeAll(newDataList);// remove estimation which is not coming in input but it is there in db

			if (deletedList != null && !deletedList.isEmpty()) {
				for (EstimatedDocuments estdoc : deletedList) {
					estimationDocumentRepository.delete(estdoc);
				}
			}

		}
	}

	@Override
	@Transactional
	public List<Estimation> getEstimations() {
		return estimationRepository.getEstimations();
	}

	@Override
	@Transactional
	public List<Estimation> getEstimationsByUserId(int createdBy) {
		return estimationRepository.getEstimationByUserId(createdBy);
	}

	@Override
	public List<Estimation> getEstimationStatusByRole(User user) {
		List<String> statuses = new ArrayList<String>();
		if (user.getRole().equals(DJConstants.ADMIN)) {
			statuses.add(DJConstants.PENDING_OA);
			statuses.add(DJConstants.PENDING_TA);
			return estimationRepository.getEstimationStatusByRole(statuses);
		} else if (user.getRole().equals(DJConstants.TEST_APPROVER)) {
			statuses.add(DJConstants.PENDING_TA);
			return estimationRepository.getEstimationStatusByRole(statuses);
		} else if (user.getRole().equals(DJConstants.OVERALL_APPROVER)) {
			statuses.add(DJConstants.PENDING_OA);
			return estimationRepository.getEstimationStatusByRole(statuses);
		}
		return new ArrayList<Estimation>();
	}

	@Override
	@Transactional
	public void deleteEstimationDataById(int estimationId) {
		estimationRepository.deleteEstimationById(estimationId);
		/*
		 * List<Highlevelesttimeline> highlevelesttimelines =
		 * highlevelEstTimelineRepository.getHighlevelesttimelineByEstId(estimationId);
		 * List<Integer> highlevelesttimelineIds = new ArrayList<>();
		 * if(highlevelesttimelines != null && !highlevelesttimelines.isEmpty()) { for
		 * (Highlevelesttimeline highlevelesttimeline : highlevelesttimelines) {
		 * highlevelesttimelineIds.add(highlevelesttimeline.getId()); } }
		 * if(!highlevelesttimelineIds.isEmpty()) {
		 * highlevelEstTimelineRepository.deleteHighlevelesttimelineByIds(
		 * highlevelesttimelineIds); highlevelestweekRepository
		 * .deleteHighlevelestweeksByHighleveleesttimelineIds(highlevelesttimelineIds);
		 * } detailedEstRepository.deleteDetailedEstimationsByEstId(estimationId);
		 * testScopeRepository.deleteTestscopesByEstId(estimationId);
		 * assumptionRepository.deleteAssumptionsByEstId(estimationId);
		 */
	}

	@Override
	@Transactional
	public Estimation cloneEstimation(int estimationId) {
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		Estimation estimation = estimationRepository.findById(estimationId);
		Estimation estimationTemp = new Estimation();
		prepareCreateEstimationResponse(loggedInUser, estimation, estimationTemp);
		estimationRepository.save(estimationTemp);
		List<EstimatedDocuments> estimatedDocuments = estimationDocumentRepository.findEstDocsListByEstId(estimationId);
		List<EstimatedDocuments> estimatedDocumentsTemp = new ArrayList<>();
		if (estimatedDocuments != null && !estimatedDocuments.isEmpty()) {
			for (EstimatedDocuments estimatedDocument : estimatedDocuments) {
				EstimatedDocuments estimatedDocumentTemp = new EstimatedDocuments();
				estimatedDocumentTemp.setEstimation(estimationTemp);
				estimatedDocumentTemp.setDoc_title(DJConstants.ESTIMATION_COPY_NAME+""+estimatedDocument.getDoc_title());
				estimatedDocumentTemp.setDoc_version(estimatedDocument.getDoc_version());
				estimatedDocumentTemp.setDoc_location(estimatedDocument.getDoc_location());
				estimatedDocumentTemp.setCreatedby(loggedInUser.getId());
				estimatedDocumentTemp.setCreatedDate(LocalDateTime.now());
				estimatedDocumentTemp.setUpdatedby(loggedInUser.getId());
				estimatedDocumentTemp.setUpdatedDate(LocalDateTime.now());
				estimatedDocumentsTemp.add(estimatedDocumentTemp);
			}
			estimationDocumentRepository.saveAll(estimatedDocumentsTemp);
		}
		List<Highlevelesttimeline> highlevelesttimelines = highlevelEstTimelineRepository
				.getHighlevelesttimelineByEstId(estimationId);
		if (highlevelesttimelines != null && !highlevelesttimelines.isEmpty()) {
			for (Highlevelesttimeline highlevelesttimeline : highlevelesttimelines) {
				Highlevelesttimeline highlevelesttimelineTemp = new Highlevelesttimeline();
				prepareCreateHighleveltimelineResponse(loggedInUser, estimationTemp, highlevelesttimeline,
						highlevelesttimelineTemp);
				highlevelEstTimelineRepository.save(highlevelesttimelineTemp);
				List<Highlevelestweek> highlevelestweeks = highlevelesttimeline.getHighlevelestweeks();
				if (highlevelestweeks != null && !highlevelestweeks.isEmpty()) {
					for (Highlevelestweek highlevelestweek : highlevelestweeks) {
						Highlevelestweek highlevelestweekTemp = new Highlevelestweek();
						prepareCreateHighlevelweekResponse(loggedInUser, estimationTemp, highlevelesttimelineTemp,
								highlevelestweek, highlevelestweekTemp);
						highlevelestweekRepository.save(highlevelestweekTemp);
					}
				}

			}
		}
		List<DetailedEstimates> detailedEstimates = detailedEstRepository.getDetailedEstByEstId(estimationId);
		List<DetailedEstimates> detailedEstimatesTemp = new ArrayList<>();
		if (detailedEstimates != null && !detailedEstimates.isEmpty()) {
			for (DetailedEstimates detailedEstimate : detailedEstimates) {
				prepareCreateDetailedEstimateResponse(loggedInUser, estimationTemp, detailedEstimatesTemp,
						detailedEstimate);
			}
			detailedEstRepository.saveAll(detailedEstimatesTemp);
		}
		List<TestScope> testScopes = testScopeRepository.fetchTestScopeDetails(estimationId);
		List<TestScope> testScopesTemp = new ArrayList<>();
		if (testScopes != null && !testScopes.isEmpty()) {
			for (TestScope testScope : testScopes) {
				prepareCreateTestscopeResponse(loggedInUser, estimationTemp, testScopesTemp, testScope);
			}
			testScopeRepository.saveAll(testScopesTemp);
		}
		List<Assumption> assumptions = assumptionRepository.getAssumptionsByEstId(estimationId, "");
		List<Assumption> assumptionsTemp = new ArrayList<>();
		if (assumptions != null && !assumptions.isEmpty()) {
			for (Assumption assumption : assumptions) {
				prepareCreateAssumptionResponse(loggedInUser, estimationTemp, assumptionsTemp, assumption);
			}
			assumptionRepository.saveAll(assumptionsTemp);
		}
		return estimation;
	}

	private void prepareCreateTestscopeResponse(User loggedInUser, Estimation estimationTemp,
			List<TestScope> testScopesTemp, TestScope testScope) {
		TestScope testScopeTemp = new TestScope();
		testScopeTemp.setEstid(estimationTemp.getId());
		testScopeTemp.setTsname(testScope.getTsname());
		testScopeTemp.setTsexeplan(testScope.getTsexeplan());
		testScopeTemp.setTceffort(testScope.getTceffort());
		testScopeTemp.setTdsetup(testScope.getTdsetup());
		testScopeTemp.setTestexe(testScope.getTestexe());
		testScopeTemp.setTotal(testScope.getTotal());
		testScopeTemp.setTcnewreuse(testScope.getTcnewreuse());
		testScopeTemp.setNooftc(testScope.getNooftc());
		testScopeTemp.setExeprod(testScope.getExeprod());
		testScopeTemp.setTcrevised(testScope.getTcrevised());
		testScopeTemp.setComments(testScope.getComments());
		testScopeTemp.setInterfacerep(testScope.getInterfacerep());
		testScopeTemp.setCreatedby(loggedInUser.getId());
		testScopeTemp.setCreateddate(new Date());
		testScopeTemp.setUpdatedby(loggedInUser.getId());
		testScopeTemp.setUpdateddate(new Date());
		testScopesTemp.add(testScopeTemp);
	}

	private void prepareCreateAssumptionResponse(User loggedInUser, Estimation estimationTemp,
			List<Assumption> assumptionsTemp, Assumption assumption) {
		Assumption assumptionTemp = new Assumption();
		assumptionTemp.setArea(assumption.getArea());
		assumptionTemp.setAssumption(assumption.getAssumption());
		assumptionTemp.setImpact(assumption.getImpact());
		Assumption assumptionDb = assumptionRepository.getLastAssumptionByEstId(assumption.getEstid());
		if (assumptionDb == null) {
			assumptionTemp.setAssumptionref(DJConstants.ASSUMPTION_REFERENCE_STARTING_LETTER_AND_NUMBER);
		} else {
			if (assumptionDb.getAssumptionref() != null
					&& !StringFormatUtils.isEmpty(assumptionDb.getAssumptionref())) {
				String refNumber = assumptionDb.getAssumptionref().substring(1,
						assumptionDb.getAssumptionref().length());
				try {
					Integer refNum = Integer.parseInt(refNumber);
					refNum = refNum + 1;
					String refNumStr = "";
					if (refNum < 10) {
						refNumStr = DJConstants.ASSUMPTION_REFERENCE_STARTING_LETTER + "0" + refNum;
					} else {
						refNumStr = DJConstants.ASSUMPTION_REFERENCE_STARTING_LETTER + "" + refNum;
					}
					assumptionTemp.setAssumptionref(refNumStr);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}
		assumptionTemp.setEstid(estimationTemp.getId());
		assumptionTemp.setCreatedby(loggedInUser.getId());
		assumptionTemp.setCreateddate(new Date());
		assumptionTemp.setUpdatedby(loggedInUser.getId());
		assumptionTemp.setUpdateddate(new Date());
		assumptionTemp.setIsdelete((byte) 0);
		assumptionsTemp.add(assumptionTemp);
	}

	private void prepareCreateDetailedEstimateResponse(User loggedInUser, Estimation estimationTemp,
			List<DetailedEstimates> detailedEstimatesTemp, DetailedEstimates detailedEstimate) {
		DetailedEstimates detailedEstimateTemp = new DetailedEstimates();
		detailedEstimateTemp.setEstid(estimationTemp.getId());
		detailedEstimateTemp.setDetestmoduletypeid(detailedEstimate.getDetestmoduletypeid());
		detailedEstimateTemp.setRequired(detailedEstimate.getRequired());
		detailedEstimateTemp.setRequiredcategory(detailedEstimate.getRequiredcategory());
		detailedEstimateTemp.setReleaseno(detailedEstimate.getReleaseno());
		detailedEstimateTemp.setMainfunction(detailedEstimate.getMainfunction());
		detailedEstimateTemp.setWorkstream(detailedEstimate.getWorkstream());
		detailedEstimateTemp.setSubfunction(detailedEstimate.getSubfunction());
		detailedEstimateTemp.setDescr(detailedEstimate.getDescr());
		detailedEstimateTemp.setSourcename(detailedEstimate.getSourcename());
		detailedEstimateTemp.setTarget(detailedEstimate.getTarget());
		detailedEstimateTemp.setMethod(detailedEstimate.getMethod());
		detailedEstimateTemp.setDifficulty(detailedEstimate.getDifficulty());
		detailedEstimateTemp.setNeworexist(detailedEstimate.getNeworexist());
		detailedEstimateTemp.setAnalyse(detailedEstimate.getAnalyse());
		detailedEstimateTemp.setDesign(detailedEstimate.getDesign());
		detailedEstimateTemp.setBuildunittest(detailedEstimate.getBuildunittest());
		detailedEstimateTemp.setEffort(detailedEstimate.getEffort());
		detailedEstimateTemp.setImplementation(detailedEstimate.getImplementation());
		detailedEstimateTemp.setEffortimpl(detailedEstimate.getEffortimpl());
		detailedEstimateTemp.setCreatedby(loggedInUser.getId());
		detailedEstimateTemp.setCreateddate(new Date());
		detailedEstimateTemp.setUpdatedby(loggedInUser.getId());
		detailedEstimateTemp.setUpdateddate(new Date());
		detailedEstimatesTemp.add(detailedEstimateTemp);
	}

	private void prepareCreateHighlevelweekResponse(User loggedInUser, Estimation estimationTemp,
			Highlevelesttimeline highlevelesttimelineTemp, Highlevelestweek highlevelestweek,
			Highlevelestweek highlevelestweekTemp) {
		highlevelestweekTemp.setEstid(estimationTemp.getId());
		highlevelestweekTemp.setHighlevelesttimelineid(highlevelesttimelineTemp.getId());
		highlevelestweekTemp.setWeekname(highlevelestweek.getWeekname());
		highlevelestweekTemp.setWeekvalue(highlevelestweek.getWeekvalue());
		highlevelestweekTemp.setCreatedby(loggedInUser.getId());
		highlevelestweekTemp.setCreateddate(new Date());
		highlevelestweekTemp.setUpdatedby(loggedInUser.getId());
		highlevelestweekTemp.setUpdateddate(new Date());
		highlevelestweekTemp.setIsdelete((byte) 0);
	}

	private void prepareCreateHighleveltimelineResponse(User loggedInUser, Estimation estimationTemp,
			Highlevelesttimeline highlevelesttimeline, Highlevelesttimeline highlevelesttimelineTemp) {
		highlevelesttimelineTemp.setGroupid(highlevelesttimeline.getGroupid());
		highlevelesttimelineTemp.setResourceid(highlevelesttimeline.getResourceid());
		highlevelesttimelineTemp.setSystems(highlevelesttimeline.getSystems());
		highlevelesttimelineTemp.setActualspent(highlevelesttimeline.getActualspent());
		highlevelesttimelineTemp.setContingency(highlevelesttimeline.getContingency());
		highlevelesttimelineTemp.setCostperday(highlevelesttimeline.getCostperday());
		highlevelesttimelineTemp.setId(null);
		highlevelesttimelineTemp.setEstid(estimationTemp.getId());
		highlevelesttimelineTemp.setCreatedby(loggedInUser.getId());
		highlevelesttimelineTemp.setCreateddate(new Date());
		highlevelesttimelineTemp.setUpdatedby(loggedInUser.getId());
		highlevelesttimelineTemp.setUpdateddate(new Date());
		highlevelesttimelineTemp.setIsdelete((byte) 0);
	}

	private void prepareCreateEstimationResponse(User loggedInUser, Estimation estimation, Estimation estimationTemp) {
		estimationTemp.setDomainid(estimation.getDomainid());
		if (estimation.getProjectphaseid() != null) {
			estimationTemp.setProjectphaseid(estimation.getProjectphaseid());
		}
		estimationTemp
				.setEst_project_name(DJConstants.ESTIMATION_COPY_NAME + "" + estimation.getEst_project_name());
		if (estimation.getEstimation_cost() != null) {
			estimationTemp.setEstimation_cost(estimation.getEstimation_cost());
		}
		estimationTemp.setIseditable((byte) 1);
		estimationTemp.setProject_lead(estimation.getProject_lead());
		estimationTemp.setEstmation_status(DJConstants.DRAFT_STATUS);
		estimationTemp.setDurationInWeek(estimation.getDurationInWeek());
		estimationTemp.setCreatedby(loggedInUser.getId());
		estimationTemp.setUpdatedby(loggedInUser.getId());
		estimationTemp.setCreatedDate(LocalDateTime.now());
		estimationTemp.setUpdatedDate(LocalDateTime.now());
		estimationTemp.setIsdelete((byte) 0);
	}

	@Override
	public List<Estimation> getEstimationsByIsEditable(byte iseditable, byte isdelete) {
		return estimationRepository.getEstimationsByIsEditable(iseditable, isdelete);
	}

	@Transactional
	public void updateEstimationStatus(Estimation estimation) {
		estimationRepository.save(estimation);
	}

	@Override
	@Transactional
	public void updateIseditableFlagAndIseditableDate(int estId, byte iseditable, Date iseditableDate, Integer loggedInUserId) {
		estimationRepository.updateIseditableFlagAndIseditableDate(estId, iseditable, iseditableDate,loggedInUserId);
	}

	@Override
	@Transactional
	public Estimation makeEstimationEditableOrNonEditable(int estimationId, byte isEditable) {
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		Estimation estimation = estimationRepository.findById(estimationId);
		Integer loggedInUserId = null;
		Date iseditableDate = null;
		if (isEditable == 0) {
			if(estimation != null) {
				if(estimation.getIseditable() == 1) {
					iseditableDate = new Date();
					loggedInUserId = loggedInUser != null ? loggedInUser.getId() : null;
					estimationRepository.updateIseditableFlagAndIseditableDate(estimationId, isEditable, iseditableDate,
							loggedInUserId);
					estimation.setIseditable(isEditable);
					estimation.setIseditabledate(iseditableDate);
					estimation.setIseditableby(loggedInUserId);
				} else {
					if(loggedInUser.getId() != estimation.getIseditableby()) {
						User estimationUpdateUser = userRepository.findById(estimation.getIseditableby());
						//throw new ErrorResponse("Concurrent users are not allowed to edit the same estimate, Currently "+estimationUpdateUser.getFirstName()+" "+estimationUpdateUser.getLastName()+" Session is In progress.", HttpStatus.IM_USED);
				
						throw new ErrorResponse("The estimate is locked for editing by "+estimationUpdateUser.getFirstName()+" "+estimationUpdateUser.getLastName()+". Estimate can be viewed in the read-only mode", HttpStatus.IM_USED);
					}
				}
			} else {
				throw new ErrorResponse("Estimation not found", HttpStatus.NOT_FOUND);
			}
		} else {
			if(loggedInUser.getId() == estimation.getIseditableby()) {
				estimationRepository.updateIseditableFlagAndIseditableDate(estimationId, isEditable, iseditableDate,
						loggedInUserId);
				estimation.setIseditable(isEditable);
				estimation.setIseditabledate(iseditableDate);
				estimation.setIseditableby(loggedInUserId);
			} else {
				throw new ErrorResponse("Unable to make this estimation editable", HttpStatus.IM_USED);
			}
		}
		return estimation;
	}

	@Override
	@Transactional
	public Estimation increaseEstimationEditableDuration(int estimationId) {
		Estimation estimation = estimationRepository.findById(estimationId);
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		if (estimation != null) {
			if (estimation.getIseditable() == 0) {
				try {
					final long ONE_MINUTE_IN_MILLIS = DJConstants.ONE_MIN_IN_MILLIS;// millisecs

					long timeInMilliSeconds = estimation.getIseditabledate().getTime();
					Date afterAddingMins = new Date(timeInMilliSeconds
							+ (DJConstants.ESTIMATION_IS_EDITABLE_DURATION_IN_MINUTES * ONE_MINUTE_IN_MILLIS));
					estimationRepository.updateIseditableFlagAndIseditableDate(estimationId, (byte) 0, afterAddingMins,
							loggedInUser != null ? loggedInUser.getId() : null);
					estimation.setIseditable((byte) 0);
					estimation.setIseditabledate(afterAddingMins);
					estimation.setIseditableby(loggedInUser != null ? loggedInUser.getId() : null);
				} catch (Exception e) {
					throw new ErrorResponse("Session is already expired", HttpStatus.INTERNAL_SERVER_ERROR);
				}
			} else {
				throw new ErrorResponse("Session is already expired", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			throw new ErrorResponse("Estimation not found with id " + estimationId, HttpStatus.NOT_FOUND);
		}
		return estimation;
	}

	@Override
	public void estimationSave(Estimation estimation) {
		estimationRepository.save(estimation);
	}

}
