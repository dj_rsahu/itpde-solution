package com.dj.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dj.dto.EmailTriggerDto;
import com.dj.dto.EstimateSummeryDto;
import com.dj.entity.EstApprovals;
import com.dj.entity.Estimation;
import com.dj.entity.User;
import com.dj.exception.ErrorResponse;
import com.dj.repository.EstApprovalRepository;
import com.dj.repository.UserRepository;
import com.dj.utils.CommonUtils;
import com.dj.utils.DJConstants;
import com.dj.utils.EmailUtils;

@Service
public class EstApprovalService {

	@Autowired
	private EstApprovalRepository estApprovalRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EstimationService estimationService;

	EmailUtils emailUtils = new EmailUtils();

	private static final Logger logger = LoggerFactory.getLogger(EstApprovalService.class);

	/**
	 * Fetch estimate approvals
	 * 
	 * @return
	 */
	/*
	 * public List<EstApprovals> fetchEstApprovals(){ return
	 * estApprovalRepository.fetchEstApprovals((byte) 0); }
	 */

	/**
	 * Retrieves estimation approval by estimation id
	 * 
	 * @param estimationId
	 * @return
	 */
	public List<EstApprovals> fetchEstApprovalsByEstimationId(int estimationId) {
		List<EstApprovals> estApprList = estApprovalRepository.fetchEstApprovalsByEstId(estimationId, (byte) 0);
		if (estApprList != null && !estApprList.isEmpty()) {
			for (EstApprovals estApprovals : estApprList) {
				User userObj = userRepository.findById(estApprovals.getApproverid());
				if (userObj != null) {
					String firstName = userObj.getFirstName() != null ? userObj.getFirstName() : "";
					String lastName = userObj.getLastName() != null ? userObj.getLastName() : "";
					estApprovals.setApproverName(firstName + " " + lastName);
					estApprovals.setApprovaltype(userObj.getRole());
				}
			}
		}

		return estApprovalRepository.fetchEstApprovalsByEstId(estimationId, (byte) 0);
	}

	/**
	 * @param estimationId
	 * @return
	 */
	public List<EstApprovals> fetchEstApprovalsByEstimationIdWithoutflag(int estimationId) {
		List<EstApprovals> estApprList = estApprovalRepository.fetchEstApprovalsByEstIdwithoutflag(estimationId);
		if (estApprList != null && !estApprList.isEmpty()) {
			for (EstApprovals estApprovals : estApprList) {
				User userObj = userRepository.findById(estApprovals.getApproverid());
				if (userObj != null) {
					String firstName = userObj.getFirstName() != null ? userObj.getFirstName() : "";
					String lastName = userObj.getLastName() != null ? userObj.getLastName() : "";
					estApprovals.setApproverName(firstName + " " + lastName);
					estApprovals.setApprovaltype(userObj.getRole());
				}
			}
		}

		return estApprList;
	}

	/**
	 * Save estimate approvals list
	 * 
	 * @param estApprovalList
	 * @return
	 */
	public List<EstApprovals> saveEstApprovals(EstimateSummeryDto estimateSummeryDto) {
		List<EstApprovals> estApprlistObj = new ArrayList<>();
		User userObj = CommonUtils.getLoggedInUserDetails();
		if (estimateSummeryDto.getEstApprovalList() != null && !estimateSummeryDto.getEstApprovalList().isEmpty()) {
			for (EstApprovals estApproval : estimateSummeryDto.getEstApprovalList()) {

				if (estApproval != null && estApproval.getEstId() == null) {
					throw new ErrorResponse("Estimation id cannot be null", HttpStatus.BAD_REQUEST);
				}
				Optional<EstApprovals> estApprovalDb = null;
				if (estApprovalRepository.findEstApprovalById(estApproval.getEstapprovalid()) > 0) {
					estApprovalDb = estApprovalRepository.findById(estApproval.getEstapprovalid());
				}
				if (userObj != null) {
					estApproval.setCreatedby(userObj.getId());
				}

				if (estApprovalRepository.findEstApprovalById(estApproval.getEstapprovalid()) == 0) {
					estApproval.setCreateddate(new Date());
				} else {
					estApproval.setCreateddate(estApprovalDb.get().getCreateddate());
				}

				if ((estApproval.getStatus() == null || estApproval.getStatus() == "")
						&& estimateSummeryDto.getIsSubmit() != null
						&& estimateSummeryDto.getIsSubmit().equalsIgnoreCase(DJConstants.TRUE)) {
					if (estApproval.getApprovaltype().equalsIgnoreCase(DJConstants.TEST_APPROVER)) {
						estApproval.setStatus(DJConstants.PENDING_STATUS);
					}

				} else {
					estApproval.setStatus(estApproval.getStatus());
					estApproval.setUpdatedby(userObj.getId());
					estApproval.setUpdateddate(new Date());
				}
				estApprlistObj.add(estApproval);
			}
		} else {
			logger.info("Estimate approval list is null");
			if (estimateSummeryDto.getIsSubmit().equalsIgnoreCase(DJConstants.TRUE)) {
				throw new ErrorResponse("Estimate approval list is null or empty ", HttpStatus.BAD_REQUEST);
			}
		}

		if (estApprlistObj != null && !estApprlistObj.isEmpty()) {
			Estimation estimation = estimationService.findById(estApprlistObj.get(0).getEstId());
			if (estimateSummeryDto.getIsSubmit() != null
					&& estimateSummeryDto.getIsSubmit().equalsIgnoreCase(DJConstants.TRUE)) {
				estimation.setEstmation_status(DJConstants.PENDING_TA);
			}
			estimation.setUpdatedby(userObj.getId());
			estimation.setUpdatedDate(LocalDateTime.now());
			estimation.setIsSubmit(estimateSummeryDto.getIsSubmit());
			estimationService.estimationSave(estimation);
		}

		List<EstApprovals> estApprovals = estApprovalRepository.saveAll(estApprlistObj);

		/*
		 * if(estimateSummeryDto.getIsSubmit() == DJConstants.TRUE) {
		 * CompletableFuture.runAsync(() -> { approvalSubmitEmails(estApprlistObj,
		 * estApprovals, estimateSummeryDto.getEstSummaryContent()); }); }
		 */
		return estApprovals;
	}

	public void approvalSubmitEmails(List<EstApprovals> estApprlistObj, List<EstApprovals> estApprovals,
			String estSummaryContent) {
		// Upon successful estimation submit email has to send to Submitted User & Test
		// Approver
		if (estApprovals != null && !estApprovals.isEmpty()) {
			Estimation estimation = estimationService.findById(estApprlistObj.get(0).getEstId());
			String msg = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" + estSummaryContent
					+ "\"/></div></td></tr>" + DJConstants.EMAIL_FOOTER_CONTENT;
			User estCreatedUser = userRepository.findById(estimation.getCreatedby());
			if (estimation != null) {
				// msg = "";
				emailUtils.sendEstimationSummaryEmail(msg, estCreatedUser.getEmail(), null,
						estimation.getEst_project_name(), DJConstants.TA_SUBJECT);
			}
			for (EstApprovals estApproval : estApprlistObj) {
				if (estApproval.getApprovaltype().equalsIgnoreCase(DJConstants.TEST_APPROVER)) {
					User testApprover = userRepository.findById(estApproval.getApproverid());
					emailUtils.sendEstimationSummaryEmail(msg, testApprover.getEmail(), null,
							estimation.getEst_project_name(), DJConstants.TA_SUBJECT);
				}
			}
		}
	}

	/**
	 * Creates estimation approval
	 * 
	 * @param estApprovalList
	 * @return
	 */
	public List<EstApprovals> createEstApprovals(List<EstApprovals> estApprovalList) {
		List<EstApprovals> estApprovalListRespList = new ArrayList<>();
		User userObj = CommonUtils.getLoggedInUserDetails();
		for (EstApprovals estApprovals : estApprovalList) {
			if (userObj != null) {
				estApprovals.setCreatedby(userObj.getId());
			}
			if (estApprovals.getStatus() == null && estApprovals.getStatus().isEmpty()) {
				estApprovals.setStatus(DJConstants.PENDING_STATUS);
			} else {
				estApprovals.setStatus(estApprovals.getStatus());
				estApprovals.setUpdatedby(userObj.getId());
				estApprovals.setUpdateddate(new Date());
			}
			estApprovals.setCreateddate(new Date());
			estApprovalListRespList.add(estApprovals);
		}
		return estApprovalRepository.saveAll(estApprovalListRespList);
	}

	/**
	 * Approve or reject the request
	 * 
	 * @param estApproval
	 * @return
	 */
	public EstApprovals approveOrRejectRequest(EstApprovals estApproval) {
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		Optional<EstApprovals> estApprovalDb = estApprovalRepository.findById(estApproval.getEstapprovalid());
		if (!estApprovalDb.isPresent()) {
			throw new ErrorResponse("Id not found to perform action on approval request", HttpStatus.BAD_REQUEST);
		}
		Estimation estimation = estimationService.findById(estApproval.getEstId());
		if (estApproval.getStatus() != null && estApproval.getStatus().equalsIgnoreCase(DJConstants.APPROVED_STATUS)) {
			if (estApprovalDb.get().getApprovaltype().equalsIgnoreCase(DJConstants.TEST_APPROVER)) {
				estimation.setUpdatedby(loggedInUser.getId());
				estimation.setUpdatedDate(LocalDateTime.now());
				estimation.setEstmation_status(DJConstants.PENDING_OA);
				estimationService.updateEstimationStatus(estimation);
				EstApprovals est = estApprovalRepository.fetchEstApprovalByRole(estApproval.getEstId(),
						DJConstants.OVERALL_APPROVER);
				est.setStatus(DJConstants.PENDING_STATUS);
				estApprovalRepository.save(est);
			}

			if (estApprovalDb.get().getApprovaltype().equalsIgnoreCase(DJConstants.OVERALL_APPROVER)) {
				estimation.setUpdatedby(loggedInUser.getId());
				estimation.setUpdatedDate(LocalDateTime.now());
				estimation.setEstmation_status(DJConstants.APPROVED_STATUS);
				estimationService.updateEstimationStatus(estimation);
			}
			estApprovalDb.get().setStatus(estApproval.getStatus());
			estApprovalDb.get().setUpdatedby(loggedInUser.getId());
			estApprovalDb.get().setUpdateddate(new Date());
		}

		if (estApproval.getStatus() != null && estApproval.getStatus().equalsIgnoreCase(DJConstants.DRAFT_STATUS)) {
			estimation.setUpdatedby(loggedInUser.getId());
			estimation.setUpdatedDate(LocalDateTime.now());
			estimation.setEstmation_status(DJConstants.DRAFT_STATUS);
			estimation.setIsSubmit(DJConstants.False);
			estimationService.updateEstimationStatus(estimation);
			estApprovalDb.get().setStatus(DJConstants.DRAFT_STATUS);
			estApprovalDb.get().setUpdatedby(loggedInUser.getId());
			estApprovalDb.get().setUpdateddate(new Date());
			estApprovalDb.get().setIsdelete((byte) 1);
			List<EstApprovals> estApprovals = estApprovalRepository
					.fetchEstApprovalsByEstId(estApprovalDb.get().getEstId(), (byte) 0);
			if (estApprovals != null && !estApprovals.isEmpty()) {
				for (EstApprovals estApprovalTemp : estApprovals) {
					if (estApprovalTemp.getEstapprovalid() != estApprovalDb.get().getEstapprovalid()) {
						estApprovalTemp.setStatus(DJConstants.DRAFT_STATUS);
						estApprovalTemp.setUpdatedby(loggedInUser.getId());
						estApprovalTemp.setUpdateddate(new Date());
						estApprovalTemp.setIsdelete((byte) 1);
						estApprovalRepository.save(estApprovalTemp);
					}
				}
			}
		}
		EstApprovals estApprovals = estApprovalRepository.save(estApprovalDb.get());
		/*
		 * CompletableFuture.runAsync(() -> { approvalOrRejectEmails(estApproval,
		 * estApprovalDb, estimation, estCreatedByUser, estApprovals); });
		 */
		return estApprovals;
	}

	/*
	 * public void approvalOrRejectEmails(EstApprovals estApproval,
	 * Optional<EstApprovals> estApprovalDb, Estimation estimation, User
	 * estCreatedByUser, EstApprovals estApprovals) { String msg; String
	 * estSummaryContent = estApproval.getEstSummaryContent(); if (estApprovals !=
	 * null) { if (estApproval.getStatus() != null &&
	 * estApproval.getStatus().equalsIgnoreCase(DJConstants.APPROVED_STATUS)) {
	 * 
	 * if (estApprovalDb.get().getApprovaltype().equalsIgnoreCase(DJConstants.
	 * TEST_APPROVER)) {
	 * 
	 * msg = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
	 * estSummaryContent + "\"/></div></td></tr>" +
	 * DJConstants.EMAIL_FOOTER_CONTENT; emailUtils.sendEstimationSummaryEmail(msg,
	 * estCreatedByUser.getEmail(), null, estimation.getEst_project_name(),
	 * DJConstants.OA_SUBJECT);
	 * 
	 * User testApprover = userRepository.findById(estApproval.getApproverid()); msg
	 * = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
	 * estSummaryContent + "\"/></div></td></tr>" +
	 * DJConstants.EMAIL_FOOTER_CONTENT;
	 * emailUtils.sendEstimationSummaryEmailApproved(msg, testApprover.getEmail(),
	 * null, estimation.getEst_project_name(), DJConstants.TA_SUBJECT);
	 * 
	 * EstApprovals overallApproval = estApprovalRepository
	 * .getOverallEstApprovalsByEstId(estApproval.getEstId(),
	 * DJConstants.OVERALL_APPROVER); if (overallApproval != null) { User
	 * overallApprover = userRepository.findById(overallApproval.getApproverid());
	 * msg = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
	 * estSummaryContent + "\"/></div></td></tr>" +
	 * DJConstants.EMAIL_FOOTER_CONTENT; emailUtils.sendEstimationSummaryEmail(msg,
	 * overallApprover.getEmail(), null, estimation.getEst_project_name(),
	 * DJConstants.OA_SUBJECT); } }
	 * 
	 * if (estApprovalDb.get().getApprovaltype().equalsIgnoreCase(DJConstants.
	 * OVERALL_APPROVER)) {
	 * 
	 * msg = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
	 * estSummaryContent + "\"/></div></td></tr>" +
	 * DJConstants.EMAIL_FOOTER_CONTENT;
	 * emailUtils.sendEstimationSummaryEmailApproved(msg,
	 * estCreatedByUser.getEmail(), null, estimation.getEst_project_name(), "");
	 * 
	 * User overallApprover = userRepository.findById(estApproval.getApproverid());
	 * msg = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
	 * estSummaryContent + "\"/></div></td></tr>" +
	 * DJConstants.EMAIL_FOOTER_CONTENT;
	 * emailUtils.sendEstimationSummaryEmailApproved(msg,
	 * overallApprover.getEmail(), null, estimation.getEst_project_name(), ""); } }
	 * 
	 * if (estApproval.getStatus() != null &&
	 * estApproval.getStatus().equalsIgnoreCase(DJConstants.DRAFT_STATUS)) { if
	 * (estApprovalDb.get().getApprovaltype().equalsIgnoreCase(DJConstants.
	 * TEST_APPROVER)) {
	 * 
	 * msg = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
	 * estSummaryContent + "\"/></div></td></tr>" +
	 * DJConstants.EMAIL_FOOTER_CONTENT;
	 * emailUtils.sendEstimationSummaryEmailRework(msg, estCreatedByUser.getEmail(),
	 * null, estimation.getEst_project_name(), DJConstants.TA_SUBJECT);
	 * 
	 * User testApprover = userRepository.findById(estApproval.getApproverid()); msg
	 * = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
	 * estSummaryContent + "\"/></div></td></tr>" +
	 * DJConstants.EMAIL_FOOTER_CONTENT;
	 * emailUtils.sendEstimationSummaryEmailRework(msg, testApprover.getEmail(),
	 * null, estimation.getEst_project_name(), DJConstants.TA_SUBJECT); }
	 * 
	 * if (estApprovalDb.get().getApprovaltype().equalsIgnoreCase(DJConstants.
	 * OVERALL_APPROVER)) {
	 * 
	 * msg = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
	 * estSummaryContent + "\"/></div></td></tr>" +
	 * DJConstants.EMAIL_FOOTER_CONTENT;
	 * emailUtils.sendEstimationSummaryEmailRework(msg, estCreatedByUser.getEmail(),
	 * null, estimation.getEst_project_name(), DJConstants.OA_SUBJECT);
	 * 
	 * User overallApprover = userRepository.findById(estApproval.getApproverid());
	 * msg = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
	 * estSummaryContent + "\"/></div></td></tr>" +
	 * DJConstants.EMAIL_FOOTER_CONTENT;
	 * emailUtils.sendEstimationSummaryEmailRework(msg, overallApprover.getEmail(),
	 * null, estimation.getEst_project_name(), DJConstants.OA_SUBJECT);
	 * 
	 * } } } }
	 */

	public void sendEmailOnSubmit(Estimation estimation, EmailTriggerDto emailTriggerDto, User estCreatedUser,
			EstApprovals estApproval) {
		String msg = DJConstants.EMAIL_HEADER_CONTENT
				+ "<tr> <td width='100%' align='center'> <table width='100%' border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center'> <table border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center' height='75' class='button' style='border-radius: 3px;' bgcolor='#66bb6a'></td></tr></table> </td></tr></table> </td></tr><tr> <td width='100%' align='center'><img src='"
				+ emailTriggerDto.getEmailContent() + "'> </div></td></tr>" + DJConstants.EMAIL_FOOTER_CONTENT;
		if (estimation != null) {

			/*
			 * emailUtils.sendEstimationSummaryEmail(msg, estCreatedUser.getEmail(), null,
			 * estimation.getEst_project_name(), DJConstants.TA_SUBJECT);
			 */

			if (estApproval.getApprovaltype().equalsIgnoreCase(DJConstants.TEST_APPROVER)) {
				User testApprover = userRepository.findById(estApproval.getApproverid());
				emailUtils.sendEstimationSummaryEmail(msg, testApprover.getEmail(), estCreatedUser.getEmail(),
						estimation.getEst_project_name(), DJConstants.TA_SUBJECT);
			}

		}
	}

	public void sendEmailOnApproval(Estimation estimation, EmailTriggerDto emailTriggerDto, User estCreatedUser,
			EstApprovals estApproval) {

		if (emailTriggerDto.getApproverType().equalsIgnoreCase(DJConstants.TEST_APPROVER)) {

			String msg = DJConstants.EMAIL_HEADER_CONTENT
					+ "<tr> <td align='center'> <table width='100%' border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center'> <table border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center' height='45' class='button' style='border-radius: 3px;' bgcolor='#66bb6a'><a href='"
					+ emailTriggerDto.getHyperLink()
					+ "'>Please click here to visit the estimate &rarr;</a></td></tr></table> </td></tr></table> </td></tr><tr> <td align='center'><img width='900' src='"
					+ emailTriggerDto.getEmailContent() + "'> </div></td></tr>" + DJConstants.EMAIL_FOOTER_CONTENT;
			/*
			 * emailUtils.sendEstimationSummaryEmail(msg, estCreatedUser.getEmail(), null,
			 * estimation.getEst_project_name(), DJConstants.OA_SUBJECT);
			 */

			User testApprover = userRepository.findById(estApproval.getApproverid());
			/*
			 * msg = DJConstants.EMAIL_HEADER_CONTENT + "<tr><td><img src=\"" +
			 * emailTriggerDto.getEmailContent() + "\"/></div></td></tr>" +
			 * DJConstants.EMAIL_FOOTER_CONTENT;
			 * emailUtils.sendEstimationSummaryEmailApproved(msg, testApprover.getEmail(),
			 * null, estimation.getEst_project_name(), DJConstants.TA_SUBJECT);
			 */

			EstApprovals overallApproval = estApprovalRepository.getOverallEstApprovalsByEstId(estApproval.getEstId(),
					DJConstants.OVERALL_APPROVER);

			if (overallApproval != null) {
				User overallApprover = userRepository.findById(overallApproval.getApproverid());

				emailUtils.sendEstimationSummaryEmailTAApproved(msg, overallApprover.getEmail(),
						testApprover.getEmail(), estCreatedUser.getEmail(), estimation.getEst_project_name(),
						DJConstants.OA_SUBJECT);
			}
		}

		if (emailTriggerDto.getApproverType().equalsIgnoreCase(DJConstants.OVERALL_APPROVER)) {
			String msg = DJConstants.EMAIL_HEADER_CONTENT
					+ "<tr> <td align='center'> <table width='100%' border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center'> <table border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center' height='45' class='button' style='border-radius: 3px;' bgcolor='#66bb6a'><a href='"
					+ emailTriggerDto.getHyperLink()
					+ "'>Please click here to visit the estimate &rarr;</a></td></tr></table> </td></tr></table> </td></tr><tr> <td align='center'><img width='900' src='"
					+ emailTriggerDto.getEmailContent() + "'> </div></td></tr>" + DJConstants.EMAIL_FOOTER_CONTENT;
			/*
			 * emailUtils.sendEstimationSummaryEmailOAApproved(msg,
			 * estCreatedUser.getEmail(), null,, estimation.getEst_project_name(),
			 * DJConstants.OA_SUBJECT);
			 */

			User overallApprover = userRepository.findById(emailTriggerDto.getApproverId());
			EstApprovals testApproval = estApprovalRepository.getTestEstApprovalsByEstId(estApproval.getEstId(),
					DJConstants.TEST_APPROVER);
			User testApprover = userRepository.findById(testApproval.getApproverid());
			msg = DJConstants.EMAIL_HEADER_CONTENT
					+ "<tr> <td align='center'> <table width='100%' border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center'> <table border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center' height='45' class='button' style='border-radius: 3px;' bgcolor='#66bb6a'><a href='"
					+ emailTriggerDto.getHyperLink()
					+ "'>Please click here to visit the estimate &rarr;</a></td></tr></table> </td></tr></table> </td></tr><tr> <td align='center'><img width='900' src='"
					+ emailTriggerDto.getEmailContent() + "'> </div></td></tr>" + DJConstants.EMAIL_FOOTER_CONTENT;
			emailUtils.sendEstimationSummaryEmailOAApproved(msg, estCreatedUser.getEmail(), overallApprover.getEmail(),
					testApprover.getEmail(), estimation.getEst_project_name(), DJConstants.OA_SUBJECT);
		}

	}

	public void sendEmailOnRework(Estimation estimation, EmailTriggerDto emailTriggerDto, User estCreatedUser,
			EstApprovals estApproval) {
		if (emailTriggerDto.getApproverType().equalsIgnoreCase(DJConstants.TEST_APPROVER)) {
			String msg = DJConstants.EMAIL_HEADER_CONTENT
					+ "<tr> <td align='center'> <table width='100%' border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center'> <table border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center' height='45' class='button' style='border-radius: 3px;' bgcolor='#66bb6a'><a href='"
					+ emailTriggerDto.getHyperLink()
					+ "'>Please click here to visit the estimate &rarr;</a></td></tr></table> </td></tr></table> </td></tr><tr> <td align='center'><img width='900' src='"
					+ emailTriggerDto.getEmailContent() + "'> </div></td></tr>" + DJConstants.EMAIL_FOOTER_CONTENT;
			/*
			 * emailUtils.sendEstimationSummaryEmailRework(msg, estCreatedUser.getEmail(),
			 * null, estimation.getEst_project_name(), DJConstants.TA_SUBJECT);
			 */
			User testApprover = userRepository.findById(emailTriggerDto.getApproverId());

			emailUtils.sendEstimationSummaryEmailRework(msg, estCreatedUser.getEmail(), testApprover.getEmail(),
					estimation.getEst_project_name(), DJConstants.TA_SUBJECT);
		}

		if (emailTriggerDto.getApproverType().equalsIgnoreCase(DJConstants.OVERALL_APPROVER)) {

			String msg = DJConstants.EMAIL_HEADER_CONTENT
					+ "<tr> <td align='center'> <table width='100%' border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center'> <table border='0' cellspacing='0' cellpadding='0'> <tr> <td align='center' height='45' class='button' style='border-radius: 3px;' bgcolor='#66bb6a'><a href='"
					+ emailTriggerDto.getHyperLink()
					+ "'>Please click here to visit the estimate &rarr;</a></td></tr></table> </td></tr></table> </td></tr><tr> <td align='center'><img width='900' src='"
					+ emailTriggerDto.getEmailContent() + "'> </div></td></tr>" + DJConstants.EMAIL_FOOTER_CONTENT;
			/*
			 * emailUtils.sendEstimationSummaryEmailRework(msg, estCreatedUser.getEmail(),
			 * null, estimation.getEst_project_name(), DJConstants.OA_SUBJECT);
			 */

			User overallApprover = userRepository.findById(emailTriggerDto.getApproverId());

			emailUtils.sendEstimationSummaryEmailRework(msg, estCreatedUser.getEmail(), overallApprover.getEmail(),
					estimation.getEst_project_name(), DJConstants.OA_SUBJECT);

		}

	}
}
