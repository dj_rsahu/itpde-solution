package com.dj.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dj.entity.RRDGroup;
import com.dj.repository.RRDGroupRepository;

@Service
public class RRDGroupService {
	
	@Autowired
	private RRDGroupRepository rrdGroupRepository;

	private static final Logger logger = LoggerFactory.getLogger(RRDGroupService.class);

	public List<RRDGroup> getAllRRDGroups(String search) {
		return rrdGroupRepository.getAllRRDGroups(search);
	}

	public List<RRDGroup> getAllRRDGroupRoles(String groupName) {
		return rrdGroupRepository.getAllRRDGroupRoles(groupName);
	}

	@Transactional
	public RRDGroup createRRDGroup(RRDGroup rrdGroup) {
		RRDGroup rrdGroupTemp = rrdGroupRepository.save(rrdGroup);
		return rrdGroupTemp;
	}

	public RRDGroup getRRDGroupByName(String groupName) {
		return rrdGroupRepository.getRRDGroupByName(groupName);
	}

	public RRDGroup getRRDGroupByNameAndRole(String groupName, String role) {
		return rrdGroupRepository.getRRDGroupByNameAndRole(groupName,role);
	}

	public RRDGroup getRRDGroupById(Integer rrdgroupid) {
		return rrdGroupRepository.getRRDGroupById(rrdgroupid);
	}

	public List<RRDGroup> getRRDGroups() {
		return rrdGroupRepository.getRRDGroups();
	}

	public List<RRDGroup> getRRDGroupsByGroupName(String groupname) {
		return rrdGroupRepository.getRRDGroupsByGroupName(groupname);
	}
	
}
