package com.dj.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.dj.entity.User;

public interface UserService {
   
	public User saveUser(User user);

    public User findByEmail(String email);
    
    public User findById(int userId);
    
    public Boolean deleteUserById(int userId);
    
    public Boolean deleteUserByemail(String email);
    
    public User updateUser(User user);
    
    public List<User> getUsers();

	public void saveImage(MultipartFile imageFile,int id) throws Exception;
	
	public String getImage(int id) throws Exception;
	
	public User findByEmailAnddDelFlag(String email, byte isdelete);
	
	public List<User> fetchAllNonDeletedUsers();

	public User changePassword(User user);

	public List<User> fetchUsersByRole(String role);
}