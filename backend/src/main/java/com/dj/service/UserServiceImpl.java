package com.dj.service;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.dj.entity.User;
import com.dj.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	
	String FORMAT = "ddMMyyHHmmss";
//	LocalDateTime date = LocalDateTime.now();
	/*System.out.println("Today " + date.format(DateTimeFormatter.ofPattern(FORMAT)));*/

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public User saveUser(User user) {
		return userRepository.save(user);
	}
	
	@Override
	@Transactional
	public User updateUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User findById(int userId) {
		// TODO Auto-generated method stub
		return userRepository.findById(userId);
	}
	
	@Override
	@Transactional
	public List<User> getUsers() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	@Transactional
	public Boolean deleteUserById(int userId) {
		userRepository.deleteUserById(userId);
		return true;
	}

	@Override
	@Transactional
	public Boolean deleteUserByemail(String email) {
		userRepository.deleteUserByEmail(email);
		return true;
	}

	@Override
	public void saveImage(MultipartFile imageFile,int id) throws Exception {
		/*Properties props = new Properties();
		InputStream inputStream = new FileInputStream(new File("config.properties"));
		props.load(inputStream);*/
		
//		Prod
//		String tomcatRoot = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\ROOT";
//		String tomcatHost="http://besconnect.qentelli.com:8080";
		
//		Dev
		String tomcatHost = "http://teddev.qentelli.com:8182";
		String tomcatRoot = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0_Tomcat_bes\\webapps\\ROOT";
		
		/*String tomcatHost = props.getProperty("thost");
		String tomcatRoot = props.getProperty("troot");*/
		
		LocalDateTime date = LocalDateTime.now();
		String timeStamp=date.format(DateTimeFormatter.ofPattern(FORMAT));
		Path currentRelativePath = Paths.get("");
		String projectPath = currentRelativePath.toAbsolutePath().toString();
//		String folder ="\\src\\main\\resources\\static\\photos\\";
//		String folder ="/photos/";
		String imageName = "/photos/"+Integer.toString(id)+"_"+timeStamp+"_"+imageFile.getOriginalFilename();
		byte[] bytes=imageFile.getBytes();
		Path srcImagePath =Paths.get((projectPath+imageName));
		System.out.println("path "+srcImagePath);
		Files.write(srcImagePath, bytes);
		
//		String destFolder = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\ROOT";
//		String destFolder = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0_Tomcat_bes\\webapps\\ROOT";
//		Path srcDestPath =Paths.get((destFolder+imageName));
		Path srcDestPath =Paths.get((tomcatRoot+imageName));
		Files.copy(srcImagePath, srcDestPath);
		User user =userRepository.findById(id);
		String responsePath=tomcatHost+imageName;
//		String responsePath="http://teddev.qentelli.com:8182"+imageName;
//		String responsePath="http://besconnect.qentelli.com:8080"+imageName;
		
		user.setPic(responsePath.toString());
		userRepository.save(user);
	}

	@Override
	public String getImage(int id) throws Exception {
		User user = userRepository.findById(id);
		/*Path currentRelativePath = Paths.get("");
		String projectPath = currentRelativePath.toAbsolutePath().toString();
		logger.info("Current relative path is: " + projectPath);*/
		String path = user.getPic();
		return path.toString();
	}

	@Override
	public User findByEmailAnddDelFlag(String  email, byte isdelete) {
		User user = userRepository.findByEmailAnddDelFlag(email, isdelete);
		return user;
	}

	@Override
	public List<User> fetchAllNonDeletedUsers(){
		return userRepository.fetchAllNonDeletedUsers((byte)0);
	}

	@Override
	@Transactional
	public User changePassword(User user) {
		
		return userRepository.save(user);
	}

	@Override
	public List<User> fetchUsersByRole(String role) {
		
		return userRepository.fetchUsersByRole(role);
	}
}