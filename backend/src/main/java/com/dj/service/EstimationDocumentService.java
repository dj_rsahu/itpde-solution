package com.dj.service;

import java.util.List;

import com.dj.entity.EstimatedDocuments;


public interface EstimationDocumentService {
	public EstimatedDocuments saveEstimatedDocument(EstimatedDocuments estimatedDocument);

    public EstimatedDocuments findById(int estimationId);
    
    public void deleteEstimatedDocumentById(int estimationId);
    
    
    public List<EstimatedDocuments> updateEstimatedDocuments(List<EstimatedDocuments> estimatedDocument);
    
    public List<EstimatedDocuments> getEstimatedDocuments();
}
