package com.dj.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.dj.entity.RRDGroup;
import com.dj.entity.RRDTransaction;
import com.dj.entity.User;
import com.dj.exception.ErrorResponse;
import com.dj.repository.RRDTransactionRepository;
import com.dj.utils.CommonUtils;
import com.dj.utils.DJConstants;
import com.dj.utils.StringFormatUtils;

@Service
public class RRDTransactionService {
	private static final Integer NULL = null;
	@Autowired
	private RRDTransactionRepository rrdTransactionRepository;
	@Autowired
	private RRDGroupService rrdGroupService;
	@Autowired
	private UserRepositoryService userRepositoryService;

	@Transactional
	public void createRRDTransaction(RRDTransaction rrdTransaction) {
		rrdTransactionValidations(rrdTransaction);
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		if (rrdTransaction.getRrdgroupid() == null || rrdTransaction.getRrdgroupid() == 0) {
			RRDGroup rrdGroup = new RRDGroup();
			if (rrdTransaction.getGroupName() == null || StringFormatUtils.isEmpty(rrdTransaction.getGroupName())
					|| rrdTransaction.getGroupName().equalsIgnoreCase(DJConstants.OTHERS)) {
				rrdGroup.setGroupname(rrdTransaction.getGroupOthers());
			} else {
				rrdGroup.setGroupname(rrdTransaction.getGroupName());
			}
			rrdGroup.setRole(rrdTransaction.getRoleOthers());
			if (loggedInUser != null) {
				rrdGroup.setCreatedby(loggedInUser.getId());
				rrdGroup.setUpdatedby(loggedInUser.getId());
			}
			rrdGroup.setCreateddate(new Date());
			rrdGroup.setUpdateddate(new Date());
			rrdGroup.setIsdelete((byte) 0);
			rrdGroup = rrdGroupService.createRRDGroup(rrdGroup);
			rrdTransaction.setRrdgroupid(rrdGroup.getRrdid());
		} else {
			if (rrdTransaction.getRoleOthers() != null && !StringFormatUtils.isEmpty(rrdTransaction.getRoleOthers())) {
				RRDGroup rrdGroupdb = rrdGroupService.getRRDGroupById(rrdTransaction.getRrdgroupid());
				RRDGroup rrdGroup = new RRDGroup();
				rrdGroup.setGroupname(rrdGroupdb.getGroupname());
				rrdGroup.setRole(rrdTransaction.getRoleOthers());
				if (loggedInUser != null) {
					rrdGroup.setCreatedby(loggedInUser.getId());
					rrdGroup.setUpdatedby(loggedInUser.getId());
				}
				rrdGroup.setCreateddate(new Date());
				rrdGroup.setUpdateddate(new Date());
				rrdGroup.setIsdelete((byte) 0);
				rrdGroup = rrdGroupService.createRRDGroup(rrdGroup);
				rrdTransaction.setRrdgroupid(rrdGroup.getRrdid());
			}
		}

		/*
		 * if (rrdTransaction.getResourcename().equalsIgnoreCase("KNOWN")) {
		 * rrdTransaction.setResourcename(rrdTransaction.getResourcenameOthers()); }
		 */
		if (rrdTransaction.getRrdgroupid() != null && rrdTransaction.getRrdgroupid() != 0) {
			RRDGroup rrdGroup = rrdGroupService.getRRDGroupById(rrdTransaction.getRrdgroupid());
			if (rrdGroup != null) {
				String rsrcName = "";
				if (rrdTransaction.getResourceid() == null || rrdTransaction.getResourceid() == 0) {
					rsrcName = DJConstants.RESOURCE_TYPE_UNKNOWN;
					rrdTransaction.setResourcename(rsrcName);
				} else {
					User user = userRepositoryService.getUserById(rrdTransaction.getResourceid());
					if (user != null) {
						rsrcName = user.getFirstName() + " " + user.getLastName();
						rrdTransaction.setResourcename(rsrcName);
					}
				}
			}
		}

		rrdTransaction.setCreateddate(new Date());
		rrdTransaction.setUpdateddate(new Date());
		rrdTransaction.setIsdelete((byte) 0);
		if (loggedInUser != null) {
			rrdTransaction.setCreatedby(loggedInUser.getId());
			rrdTransaction.setUpdatedby(loggedInUser.getId());
		}
		RRDTransaction rrdTransactionTemp = rrdTransactionRepository
				.getRRDTransactionByGroupIdAndResourceIdAndResourceType(rrdTransaction.getRrdgroupid(),
						rrdTransaction.getResourceid(), rrdTransaction.getResourcetype(), null);
		if (rrdTransactionTemp != null) {
			throw new ErrorResponse("Resource and Role to be added already exists. Please check your data.", HttpStatus.NOT_ACCEPTABLE);
		}
		if (rrdTransaction.getResourceid() == 0) {
			rrdTransaction.setResourceid(NULL);
		}
		rrdTransactionRepository.save(rrdTransaction);
	}

	private void rrdTransactionValidations(RRDTransaction rrdTransaction) {
		if (rrdTransaction.getRrdgroupid() == null || rrdTransaction.getRrdgroupid() == 0) {
			if (rrdTransaction.getGroupName() == null || StringFormatUtils.isEmpty(rrdTransaction.getGroupName())
					|| rrdTransaction.getGroupName().equalsIgnoreCase(DJConstants.OTHERS)) {
				if ((rrdTransaction.getGroupOthers() == null
						|| StringFormatUtils.isEmpty(rrdTransaction.getGroupOthers()))) {
					String msg = "Group";
					String isOr = " is ";
					if ((rrdTransaction.getRoleOthers() == null
							|| StringFormatUtils.isEmpty(rrdTransaction.getRoleOthers()))) {
						msg = msg + "and role";
						isOr = " are ";
					} else {
						RRDGroup rrdGroupRole = rrdGroupService.getRRDGroupByNameAndRole(rrdTransaction.getGroupName(),
								rrdTransaction.getRoleOthers());
						if (rrdGroupRole != null) {
							throw new ErrorResponse(
									"Role is already existed in the group " + rrdTransaction.getGroupName()
											+ " with the name " + rrdTransaction.getRoleOthers(),
									HttpStatus.NOT_ACCEPTABLE);
						}
					}
					throw new ErrorResponse(msg + "" + isOr + " mandatory", HttpStatus.NOT_ACCEPTABLE);
				} else {
					RRDGroup rrdGroup = rrdGroupService.getRRDGroupByName(rrdTransaction.getGroupOthers());
					if (rrdGroup != null) {
						throw new ErrorResponse("Group is already existed with the name " + rrdTransaction.getGroupOthers(),
								HttpStatus.NOT_ACCEPTABLE);
					}
					if (rrdTransaction.getRoleOthers() != null
							|| !StringFormatUtils.isEmpty(rrdTransaction.getRoleOthers())) {
						RRDGroup rrdGroupRole = rrdGroupService.getRRDGroupByNameAndRole(rrdTransaction.getGroupOthers(),
								rrdTransaction.getRoleOthers());
						if (rrdGroupRole != null) {
							throw new ErrorResponse(
									"Role is already existed in the group " + rrdTransaction.getGroupOthers()
											+ " with the name " + rrdTransaction.getRoleOthers(),
									HttpStatus.NOT_ACCEPTABLE);
						}
					}
				}
			}
		} else {
			RRDGroup rrdGroup = rrdGroupService.getRRDGroupById(rrdTransaction.getRrdgroupid());
			RRDGroup rrdGroupRole = rrdGroupService.getRRDGroupByNameAndRole(rrdGroup.getGroupname(),
					rrdTransaction.getRoleOthers());
			if (rrdGroupRole != null) {
				throw new ErrorResponse("Role is already existed in the group " + rrdTransaction.getGroupOthers()
						+ " with the name " + rrdTransaction.getRoleOthers(), HttpStatus.NOT_ACCEPTABLE);
			}
		}

		if (rrdTransaction.getRoleOthers() != null && !StringFormatUtils.isEmpty(rrdTransaction.getRoleOthers())) {
			RRDGroup rrdGroupRole = rrdGroupService.getRRDGroupByNameAndRole(rrdTransaction.getGroupOthers(),
					rrdTransaction.getRoleOthers());
			if (rrdGroupRole != null) {
				throw new ErrorResponse("Role is already existed in the group " + rrdTransaction.getGroupOthers()
						+ " with the name " + rrdTransaction.getRoleOthers(), HttpStatus.NOT_ACCEPTABLE);
			}
		}

		/*
		 * if (rrdTransaction.getResourcename() == null ||
		 * StringFormatUtils.isEmpty(rrdTransaction.getResourcename())) { throw new
		 * ErrorResponse("Resource role is mandatory", HttpStatus.NOT_ACCEPTABLE); }
		 * else { if (rrdTransaction.getResourcename().equalsIgnoreCase("KNOWN")) {
		 * if(rrdTransaction.getResourcenameOthers() == null ||
		 * StringFormatUtils.isEmpty(rrdTransaction.getResourcenameOthers())) { throw
		 * new ErrorResponse("Resource role is mandatory", HttpStatus.NOT_ACCEPTABLE); }
		 * } }
		 */
		/*
		 * if (rrdTransaction.getResourceid() == null) { throw new
		 * ErrorResponse("Resource role is mandatory", HttpStatus.NOT_ACCEPTABLE); }
		 */
		if (rrdTransaction.getCapacity() == null || rrdTransaction.getCapacity() == 0) {
			throw new ErrorResponse("Capacity is mandatory", HttpStatus.NOT_ACCEPTABLE);
		}
		if (rrdTransaction.getResourcetype() == null || StringFormatUtils.isEmpty(rrdTransaction.getResourcetype())) {
			throw new ErrorResponse("Resource type Internal / External is mandatory", HttpStatus.NOT_ACCEPTABLE);
		}
		if (rrdTransaction.getCostperday() == null || rrdTransaction.getCostperday() == 0) {
			throw new ErrorResponse("Cost per day is mandatory", HttpStatus.NOT_ACCEPTABLE);
		}

	}

	public List<RRDTransaction> getAllRRDTransactions(String search) {
		List<RRDTransaction> rrdTransactions = rrdTransactionRepository.getAllRRDTransactions(search);
		if (rrdTransactions != null && !rrdTransactions.isEmpty()) {
			List<RRDGroup> rrdGroups = rrdGroupService.getRRDGroups();
			if (rrdGroups != null && !rrdGroups.isEmpty()) {
				for (RRDTransaction rrdTransaction : rrdTransactions) {
					for (RRDGroup rrdGroup : rrdGroups) {
						if (rrdTransaction.getRrdgroupid().intValue() == rrdGroup.getRrdid().intValue()) {
							rrdTransaction.setRrdGroup(rrdGroup);
							rrdTransaction
									.setResourceTypeRole(rrdTransaction.getRsrcName() + " - " + rrdGroup.getRole());
							rrdTransaction.setRscRole(rrdGroup.getRole());
							rrdTransaction.setGroupName(rrdGroup.getGroupname());
						}
					}
				}
			}
		}
		return rrdTransactions;
	}

	@Transactional
	public RRDTransaction updateRRDTransaction(RRDTransaction rrdTransaction) {
		rrdTransactionValidations(rrdTransaction);
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		RRDTransaction rrdTransactiondb = rrdTransactionRepository
				.getRRDTransactionById(rrdTransaction.getRrdtransid());
		if (rrdTransaction.getRrdgroupid() == null || rrdTransaction.getRrdgroupid() == 0) {
			RRDGroup rrdGroup = new RRDGroup();
			if (rrdTransaction.getGroupName() == null || StringFormatUtils.isEmpty(rrdTransaction.getGroupName())
					|| rrdTransaction.getGroupName().equalsIgnoreCase(DJConstants.OTHERS)) {
				rrdGroup.setGroupname(rrdTransaction.getGroupOthers());
			} else {
				rrdGroup.setGroupname(rrdTransaction.getGroupName());
			}
			rrdGroup.setRole(rrdTransaction.getRoleOthers());
			if (loggedInUser != null) {
				rrdGroup.setUpdatedby(loggedInUser.getId());
			}
			rrdGroup.setCreateddate(new Date());
			rrdGroup.setUpdateddate(new Date());
			rrdGroup.setIsdelete((byte) 0);
			rrdGroup = rrdGroupService.createRRDGroup(rrdGroup);
			rrdTransaction.setRrdgroupid(rrdGroup.getRrdid());
		}
		/*
		 * if (rrdTransaction.getResourcename().equalsIgnoreCase("KNOWN")) {
		 * rrdTransaction.setResourcename(rrdTransaction.getResourcenameOthers()); }
		 */
		if (rrdTransaction.getRrdgroupid() != null && rrdTransaction.getRrdgroupid() != 0) {
			RRDGroup rrdGroup = rrdGroupService.getRRDGroupById(rrdTransaction.getRrdgroupid());
			if (rrdGroup != null) {
				String rsrcName = "";
				if (rrdTransaction.getResourceid() == null || rrdTransaction.getResourceid() == 0) {
					rsrcName = DJConstants.RESOURCE_TYPE_UNKNOWN;
					rrdTransaction.setResourcename(rsrcName + "-" + rrdGroup.getRole());
				} else {
					User user = userRepositoryService.getUserById(rrdTransaction.getResourceid());
					if (user != null) {
						rsrcName = user.getFirstName() + " " + user.getLastName();
						rrdTransaction.setResourcename(rsrcName + "-" + rrdGroup.getRole());
					}
				}
			}
		}
		rrdTransactiondb.setCostperday(rrdTransaction.getCostperday());
		rrdTransactiondb.setRrdgroupid(rrdTransaction.getRrdgroupid());
		rrdTransactiondb.setResourceid(rrdTransaction.getResourceid());
		rrdTransactiondb.setResourcetype(rrdTransaction.getResourcetype());
		rrdTransactiondb.setCostperday(rrdTransaction.getCostperday());
		rrdTransactiondb.setCapacity(rrdTransaction.getCapacity());
		if (loggedInUser != null) {
			rrdTransactiondb.setUpdatedby(loggedInUser.getId());
		}
		rrdTransactiondb.setUpdateddate(new Date());
		RRDTransaction rrdTransactionTemp = rrdTransactionRepository
				.getRRDTransactionByGroupIdAndResourceIdAndResourceType(rrdTransaction.getRrdgroupid(),
						rrdTransaction.getResourceid(), rrdTransaction.getResourcetype(),
						rrdTransaction.getRrdtransid());
		if (rrdTransactionTemp != null
				&& rrdTransaction.getRrdtransid().intValue() != rrdTransactionTemp.getRrdtransid().intValue()) {
			throw new ErrorResponse("RRD Transaction already existed with these details", HttpStatus.NOT_ACCEPTABLE);
		}
		if (rrdTransaction.getResourceid() == 0) {
			rrdTransactiondb.setResourceid(null);
		}
		rrdTransactionRepository.save(rrdTransactiondb);
		return rrdTransactiondb;
	}

	@Transactional
	public RRDTransaction deleteRRDTransaction(int rrdTransactionId) {
		User loggedInUser = CommonUtils.getLoggedInUserDetails();
		RRDTransaction rrdTransaction = rrdTransactionRepository.getRRDTransactionById(rrdTransactionId);
		if (rrdTransaction != null) {
			rrdTransaction.setIsdelete((byte) 1);
			if (loggedInUser != null) {
				rrdTransaction.setUpdatedby(loggedInUser.getId());
				rrdTransaction.setUpdateddate(new Date());
			}
		}
		rrdTransactionRepository.save(rrdTransaction);
		return rrdTransaction;
	}

	public List<RRDTransaction> getRRDTransactionsByGroupId(String search, int groupId) {
		return rrdTransactionRepository.getRRDTransactionsByGroupId(search, groupId);
	}

	public RRDTransaction getRRDTransactionById(int rrdTransId) {
		return rrdTransactionRepository.getRRDTransactionById(rrdTransId);
	}

}
