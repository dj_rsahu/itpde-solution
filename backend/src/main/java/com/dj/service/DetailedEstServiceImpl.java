package com.dj.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dj.controller.DetailedEstController;
import com.dj.dto.DetailedEstDto;
import com.dj.entity.DetailedEstModType;
import com.dj.entity.DetailedEstimates;
import com.dj.entity.User;
import com.dj.repository.DetailedEstModTypeRepository;
import com.dj.repository.DetailedEstRepository;
import com.dj.utils.CommonUtils;

@Service
public class DetailedEstServiceImpl implements DetailedEstService {
	private static final Logger logger = LoggerFactory.getLogger(DetailedEstController.class);

	@Autowired
	DetailedEstRepository detailedEstRepository;
	
	@Autowired
	DetailedEstModTypeRepository detailedEstModTypeRepository;

	/**
	 * detailedEstDto estId
	 */
	@Override
	@Transactional
	public void saveDetailedEst(DetailedEstDto detailedEstDto, int estId) {
		logger.info("DetailedEstServiceImpl saveDetailedEst() ");
		List<DetailedEstimates> detailedEstList = detailedEstDto.getDetailedEstList();
		if (detailedEstList != null && !detailedEstList.isEmpty()) {
			User loggedInUser = CommonUtils.getLoggedInUserDetails();
			// delete detailed estimates
			deleteDetailedEst(estId, detailedEstList);

			// Add or Update detailed estimates
			for (DetailedEstimates detailedEst : detailedEstList) {
				if (detailedEst.getId() == null || detailedEst.getId() == 0) {
					createDetailedEst(estId, loggedInUser, detailedEst);
				} else {
					updateDetailedEst(loggedInUser, detailedEst);
				}
			}
		}
	}

	private void createDetailedEst(int estId, User loggedInUser, DetailedEstimates detailedEst) {
		List<DetailedEstimates> saveDetailedEstList = new ArrayList<>();
		if (detailedEst != null) {
			if (loggedInUser != null) {
				detailedEst.setCreatedby(loggedInUser.getId());
				detailedEst.setCreateddate(new Date());
				detailedEst.setUpdatedby(loggedInUser.getId());
				detailedEst.setUpdateddate(new Date());
			}
			detailedEst.setEstid(estId);
			DetailedEstModType detailedEstModType = detailedEstModTypeRepository.getModuleTypeIdByName(detailedEst.getModuletype());
			if(detailedEstModType ==  null) {
				DetailedEstModType detailedEstModTypeObj = new DetailedEstModType();
				detailedEstModTypeObj.setModuletype(detailedEst.getModuletype());
				detailedEstModTypeObj.setCreatedby(loggedInUser.getId());
				detailedEstModTypeObj.setCreateddate(new Date());
				detailedEstModTypeObj.setUpdatedby(loggedInUser.getId());
				detailedEstModTypeObj.setUpdateddate(new Date());
				DetailedEstModType saveDetailedEstModType = detailedEstModTypeRepository.save(detailedEstModTypeObj);
				detailedEst.setDetestmoduletypeid(saveDetailedEstModType.getId());
			}else {
				detailedEst.setDetestmoduletypeid(detailedEstModType.getId());
			}
			detailedEst.setRequired(detailedEst.getRequired());
			detailedEst.setRequiredcategory(detailedEst.getRequiredcategory());
			detailedEst.setReleaseno(detailedEst.getReleaseno());
			detailedEst.setMainfunction(detailedEst.getMainfunction());
			detailedEst.setWorkstream(detailedEst.getWorkstream());
			detailedEst.setSubfunction(detailedEst.getSubfunction());
			detailedEst.setDescr(detailedEst.getDescr());
			detailedEst.setSourcename(detailedEst.getSourcename());
			detailedEst.setTarget(detailedEst.getTarget());
			detailedEst.setMethod(detailedEst.getMethod());
			detailedEst.setDifficulty(detailedEst.getDifficulty());
			detailedEst.setNeworexist(detailedEst.getNeworexist());
			detailedEst.setAnalyse(detailedEst.getAnalyse());
			detailedEst.setDesign(detailedEst.getDesign());
			detailedEst.setBuildunittest(detailedEst.getBuildunittest());
			detailedEst.setEffort(detailedEst.getEffort());
			detailedEst.setImplementation(detailedEst.getImplementation());
			detailedEst.setEffortimpl(detailedEst.getEffortimpl());
			saveDetailedEstList.add(detailedEst);
		}
		detailedEstRepository.saveAll(saveDetailedEstList);
	}

	private void updateDetailedEst(User loggedInUser, DetailedEstimates detailedEst) {
		List<DetailedEstimates> updateDetailedEstList = new ArrayList<>();
		DetailedEstModType saveDetailedEstModType = new DetailedEstModType();
		DetailedEstimates detailedEstById = detailedEstRepository.getDetailedEstimatesById(detailedEst.getId());
		if (detailedEstById != null) {
			DetailedEstModType detailedEstModType = detailedEstModTypeRepository.getModuleTypeIdByName(detailedEst.getModuletype());
			if(detailedEstModType ==  null) {
				DetailedEstModType detailedEstModTypeObj = new DetailedEstModType();
				detailedEstModTypeObj.setModuletype(detailedEst.getModuletype());
				detailedEstModTypeObj.setCreatedby(loggedInUser.getId());
				detailedEstModTypeObj.setCreateddate(new Date());
				detailedEstModTypeObj.setUpdatedby(loggedInUser.getId());
				detailedEstModTypeObj.setUpdateddate(new Date());
				saveDetailedEstModType = detailedEstModTypeRepository.save(detailedEstModTypeObj);
				detailedEst.setDetestmoduletypeid(saveDetailedEstModType.getId());
			}
			else {
				detailedEstById.setDetestmoduletypeid(detailedEstModType.getId());
			}
			detailedEstById.setRequired(detailedEst.getRequired());
			detailedEstById.setRequiredcategory(detailedEst.getRequiredcategory());
			detailedEstById.setReleaseno(detailedEst.getReleaseno());
			detailedEstById.setMainfunction(detailedEst.getMainfunction());
			detailedEstById.setWorkstream(detailedEst.getWorkstream());
			detailedEstById.setSubfunction(detailedEst.getSubfunction());
			detailedEstById.setDescr(detailedEst.getDescr());
			detailedEstById.setSourcename(detailedEst.getSourcename());
			detailedEstById.setTarget(detailedEst.getTarget());
			detailedEstById.setMethod(detailedEst.getMethod());
			detailedEstById.setDifficulty(detailedEst.getDifficulty());
			detailedEstById.setNeworexist(detailedEst.getNeworexist());
			detailedEstById.setAnalyse(detailedEst.getAnalyse());
			detailedEstById.setDesign(detailedEst.getDesign());
			detailedEstById.setBuildunittest(detailedEst.getBuildunittest());
			detailedEstById.setEffort(detailedEst.getEffort());
			detailedEstById.setImplementation(detailedEst.getImplementation());
			detailedEstById.setEffortimpl(detailedEst.getEffortimpl());
			detailedEstById.setUpdatedby(loggedInUser.getId());
			detailedEstById.setUpdateddate(new Date());
			updateDetailedEstList.add(detailedEstById);
		}
		detailedEstRepository.saveAll(updateDetailedEstList);
	}

	private void deleteDetailedEst(int estId, List<DetailedEstimates> detailedEstList) {

		List<Integer> detailedEstIds = new ArrayList<>();
		if (detailedEstList != null && !detailedEstList.isEmpty()) {
			for (DetailedEstimates detailedEst : detailedEstList) {
				if (detailedEst.getId() != null && detailedEst.getId() != 0) {
					detailedEstIds.add(detailedEst.getId());
				}
			}
		}
		if(detailedEstIds != null && !detailedEstIds.isEmpty()) {
			List<DetailedEstimates> detailedEstsDB = detailedEstRepository.getDetailedEstNotInIds(detailedEstIds,estId);
			if(detailedEstsDB != null && !detailedEstsDB.isEmpty()) {
				List<Integer> detailedEstIdsNotExistedInDB = new ArrayList<>();
				for (DetailedEstimates detailedEsttmp : detailedEstsDB) {
					detailedEstIdsNotExistedInDB.add(detailedEsttmp.getId());
				}
				if(detailedEstIdsNotExistedInDB != null && !detailedEstIdsNotExistedInDB.isEmpty()) {
					detailedEstRepository.deleteDetailedEstByIds(detailedEstIdsNotExistedInDB);
				}
			}
		}
	}

	@Override
	@Transactional
	public List<DetailedEstimates> getDetailedEstByEstId(int estId) {
		List<DetailedEstimates> detailedEstByEstList = detailedEstRepository.getDetailedEstByEstId(estId);
		for (DetailedEstimates detailedEstimates : detailedEstByEstList) {
			DetailedEstModType detailedEstModType = detailedEstModTypeRepository.getModuleTypeById(detailedEstimates.getDetestmoduletypeid());
			detailedEstimates.setModuletype(detailedEstModType.getModuletype());
		}
		return detailedEstByEstList;
	}

	@Override
	public List<String> getDetEstModuleType() {
		List<String> moduleTypeList = new ArrayList<>();
		List<DetailedEstModType> detEstModuleTypes = detailedEstModTypeRepository.getDetEstModuleTypes();
		for (DetailedEstModType detailedEstModType : detEstModuleTypes) {
			moduleTypeList.add(detailedEstModType.getModuletype());
		} 
		return moduleTypeList;
	}

}
