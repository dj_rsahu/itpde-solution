package com.dj.service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Service;

import com.dj.entity.Estimation;
import com.dj.utils.DJConstants;

/**
 * 
 * @author rameshbandari : Purpose of this class is to invoke tool's api call to
 *         fetch & insert the api response periodically in database
 */
@Service
public class ScheduleConfigRepositoryService implements SchedulingConfigurer {

	private static final Logger logger = LoggerFactory.getLogger(ScheduleConfigRepositoryService.class);
	ScheduledTaskRegistrar scheduledTaskRegistrar;

	@SuppressWarnings("rawtypes")
	private ScheduledFuture future;

	@Autowired
	private EstimationService estimationService;

	@Bean
	public TaskScheduler poolScheduler() {
		ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
		scheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
		scheduler.setPoolSize(1);
		scheduler.initialize();
		return scheduler;
	}

	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {

		taskRegistrar.addTriggerTask(
				() -> executeJob(DJConstants.ESTIMATION_IS_EDITABLE_REFRESH_DURATION_IN_MILLI_SECONDS), t -> {
					Calendar nextExecutionTime = new GregorianCalendar();
					Date lastActualExecutionTime = t.lastActualExecutionTime();
					nextExecutionTime.setTime(lastActualExecutionTime != null ? lastActualExecutionTime : new Date());
					nextExecutionTime.add(Calendar.SECOND,
							DJConstants.ESTIMATION_IS_EDITABLE_REFRESH_DURATION_IN_MILLI_SECONDS / 1000);
					return nextExecutionTime.getTime();
				});

	}

	private void executeJob(Integer timeInterval) {
		List<Estimation> estimations = estimationService.getEstimationsByIsEditable((byte) 0, (byte) 0);
		if (estimations != null && !estimations.isEmpty()) {
			for (Estimation estimation : estimations) {
				Date currentDateTime = new Date();
				long diffInMillies = Math.abs(currentDateTime.getTime() - estimation.getIseditabledate().getTime());
				long seconds = diffInMillies / 1000;
				if (seconds >= DJConstants.SESSION_EXPIRY_IN_SECONDS) {
					Date iseditableDate = null;
					estimationService.updateIseditableFlagAndIseditableDate(estimation.getId(), (byte) 1,
							iseditableDate,null);
				}
			}
		}
	}

	/**
	 * If a job should run on specific time interval
	 */
	public void scheduleFixed() {
		logger.info("scheduleFixed: Next execution time of this will always be 5 seconds");
	}

	/**
	 * @param mayInterruptIfRunning {@code true} if the thread executing this task
	 *                              should be interrupted; otherwise, in-progress
	 *                              tasks are allowed to complete
	 */
	public void cancelTasks(boolean mayInterruptIfRunning) {
		logger.info("Cancelling all tasks");
		future.cancel(mayInterruptIfRunning); // set to false if you want the running task to be completed first.
	}

	/**
	 * Re-activate the scheduler
	 */
	public void activateScheduler() {
		logger.info("Re-Activating Scheduler");
		configureTasks(scheduledTaskRegistrar);
	}

}
