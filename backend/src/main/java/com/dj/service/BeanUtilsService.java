package com.dj.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * Purpose of this class is to provide a object for non-spring managed classes 
 * @author ramesh
 *
 */
@Service
public class BeanUtilsService implements ApplicationContextAware {
	
	private static ApplicationContext context;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		 context = applicationContext;
	}

	public static <T> T getBean(Class<T> beanClass) {
        return context.getBean(beanClass);
    }
}
