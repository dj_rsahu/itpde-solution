package com.dj.service;

import java.util.List;

import com.dj.dto.DetailedEstDto;
import com.dj.entity.DetailedEstimates;

public interface DetailedEstService {

	public void saveDetailedEst(DetailedEstDto detailedEstDto, int estId);

	public List<DetailedEstimates> getDetailedEstByEstId(int estId);

	public List<String> getDetEstModuleType();

}
