package com.dj.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dj.entity.Domain;
import com.dj.repository.DomainRepository;

@Service
public class DomainService {
	@Autowired
	private DomainRepository domainRepository;

	public List<Domain> getDomains() {
		return (List<Domain>) domainRepository.findAll();
	}

}
