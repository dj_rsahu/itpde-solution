package com.dj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dj.entity.RRDGroup;
import com.dj.service.RRDGroupService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/dj/rrdGroup")
@CrossOrigin(origins = "*")
@Api(value="RRD Group", description="RRD Groups Controller")
public class RRDGroupController {
	
	@Autowired
	private RRDGroupService rrdGroupService;

	@ApiOperation(value = "Get RRD Groups")
	@GetMapping("/getRRDGroups")
	public ResponseEntity<List<RRDGroup>> getAllRRDGroups(
			@RequestParam(name = "search", required = false) String search) {
		List<RRDGroup> rrdGroups = rrdGroupService.getAllRRDGroups(search);
		return new ResponseEntity<>(rrdGroups, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Get RRD Group Roles")
	@GetMapping("/getRRDGroupRoles/{groupName}")
	public ResponseEntity<List<RRDGroup>> getAllRRDGroupRoles(@PathVariable("groupName") String groupName) {
		List<RRDGroup> rrdGroups = rrdGroupService.getAllRRDGroupRoles(groupName);
		return new ResponseEntity<>(rrdGroups, HttpStatus.OK);
	}
}
