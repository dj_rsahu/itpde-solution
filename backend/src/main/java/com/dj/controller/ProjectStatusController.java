package com.dj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dj.dto.EstimateSummeryDto;
import com.dj.entity.EstApprovals;
import com.dj.entity.Highlevelesttimeline;
import com.dj.entity.Projectstatus;
import com.dj.service.ProjectStatusService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/dj")
@Api(value = "projectstatus", description = "Operations pertaining to insert and retrieve the project status")
public class ProjectStatusController {
	
	/**
	 * @author ramesh.bandari
	 *    
	 */
	
	@Autowired
	private ProjectStatusService projectStatusService;
	
	@ApiOperation(value = "Fetch project status")
	@GetMapping("/fetchProjectStatus")
	public ResponseEntity<List<Projectstatus>> fetchProjectStatus() {
		List<Projectstatus> projectStatusList = projectStatusService.fetchProjectStatus();
		return new ResponseEntity<List<Projectstatus>>(projectStatusList, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Insert project status")
	@GetMapping("/insertProjectStatus")
	public ResponseEntity<Projectstatus> insertProjectStatus(@RequestBody Projectstatus projectstatus) {
		Projectstatus projectStatus = projectStatusService.insertProjectStatus(projectstatus);
		return new ResponseEntity<Projectstatus>(projectStatus, HttpStatus.OK);
	}
}
