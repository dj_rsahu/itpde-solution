package com.dj.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dj.dto.EmailTriggerDto;
import com.dj.dto.EstimateSummeryDto;
import com.dj.entity.EstApprovals;
import com.dj.entity.Estimation;
import com.dj.entity.Highlevelesttimeline;
import com.dj.entity.User;
import com.dj.exception.ErrorResponse;
import com.dj.repository.EstApprovalRepository;
import com.dj.repository.HighlevelEstTimelineRepository;
import com.dj.repository.UserRepository;
import com.dj.service.EstApprovalService;
import com.dj.service.EstimationService;
import com.dj.service.HighlevelEstTimelineService;
import com.dj.utils.CommonUtils;
import com.dj.utils.DJConstants;
import com.dj.utils.EmailUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author ramesh.bandari
 *         <p>
 *         Inserts and retrieves the Estimation Summary list objects and binds
 *         it to EstimateSummeryDto
 *         </p>
 *
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/dj")
@Api(value = "estimationsummary", description = "Operations pertaining to insert and retrieves the estimation summary")
public class EstimationSummaryController {

	@Autowired
	private HighlevelEstTimelineService highlevelEstTimelineService;
	@Autowired
	private HighlevelEstTimelineRepository highlevelEstTimelineRepository;
	@Autowired
	private EstApprovalService estApprovalService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EstimationService estimationService;
	@Autowired
	private EstApprovalRepository estApprovalRepository;

	EmailUtils emailUtils = new EmailUtils();

	private static final Logger logger = LoggerFactory.getLogger(EstimationSummaryController.class);

	/**
	 * Saves the estimation summary
	 * 
	 * @param estimateSummeryDto
	 * @return
	 */
	@ApiOperation(value = "insert estimation summary")
	@PostMapping("/saveEstimationSummary")
	public ResponseEntity<EstimateSummeryDto> saveEstimationSummary(
			@RequestBody EstimateSummeryDto estimateSummeryDto) {
		EstimateSummeryDto responseDto = new EstimateSummeryDto();
		User userObj = CommonUtils.getLoggedInUserDetails();
		if (estimateSummeryDto == null) {
			throw new ErrorResponse("estimateSummeryDto is null", HttpStatus.BAD_REQUEST);
		}
		if (estimateSummeryDto.getHighlevelEstTimelineList() != null) {
			List<Highlevelesttimeline> hltList = highlevelEstTimelineService
					.saveHighlevelEstTimelines(estimateSummeryDto.getHighlevelEstTimelineList());
			if (hltList != null && !hltList.isEmpty()) {
				responseDto.setHighlevelEstTimelineList(hltList);
			}
		} else {
			logger.info("HighlevelEstTimelineList object is null");
		}

		if (estimateSummeryDto.getEstApprovalList() != null) {
			List<EstApprovals> estApprList = estApprovalService.saveEstApprovals(estimateSummeryDto);
			if (estApprList != null && !estApprList.isEmpty()) {
				responseDto.setEstApprovalList(estApprList);
			}
		} else {
			logger.info("EstApprovalList object is null");
		}
		
		return new ResponseEntity<>(responseDto, HttpStatus.OK);

	}

	/**
	 * Updates the estimation summary
	 * 
	 * @param highlevelesttimelineList
	 * @return
	 */
	@ApiOperation(value = "update estimation summary")
	@PutMapping("/updateEstimationSummary")
	public ResponseEntity<List<Highlevelesttimeline>> updateEstimationSummary(
			@RequestBody List<Highlevelesttimeline> highlevelesttimelineList) {
		List<Highlevelesttimeline> highlevelesttimelineListObj = new ArrayList<>();
		if (highlevelesttimelineList == null || !highlevelesttimelineList.isEmpty()) {
			throw new ErrorResponse("estimateSummeryDto is null or empty ", HttpStatus.BAD_REQUEST);
		}
		for (Highlevelesttimeline highlevelesttimeline : highlevelesttimelineList) {
			Highlevelesttimeline hltObj = highlevelEstTimelineRepository
					.getHighlevelesttimelineById(highlevelesttimeline.getId());
			hltObj.setActualspent(highlevelesttimeline.getActualspent());
			hltObj.setContingency(highlevelesttimeline.getContingency());
			highlevelesttimelineListObj.add(hltObj);
		}
		List<Highlevelesttimeline> hltRespList = highlevelEstTimelineRepository.saveAll(highlevelesttimelineListObj);
		return new ResponseEntity<List<Highlevelesttimeline>>(hltRespList, HttpStatus.OK);
	}

	/**
	 * Submits for the estimation approval
	 * 
	 * @param estApprovalsList
	 * @return
	 */
	@ApiOperation(value = "Submits the estimation approval")
	@PostMapping("/submitEstApproval")
	public ResponseEntity<List<EstApprovals>> submitEstimationApproval(
			@RequestBody List<EstApprovals> estApprovalsList) {

		if (estApprovalsList == null || !estApprovalsList.isEmpty()) {
			throw new ErrorResponse("estApprovalsList is null or empty ", HttpStatus.BAD_REQUEST);
		}
		List<EstApprovals> estAppList = estApprovalService.createEstApprovals(estApprovalsList);
		return new ResponseEntity<List<EstApprovals>>(estAppList, HttpStatus.OK);
	}

	/**
	 * Retrieves the estimation summary
	 * 
	 * @param estId
	 * @return
	 */
	@ApiOperation(value = "Fetch estimation summary")
	@GetMapping("/fetchEstimationSummary/{estId}")
	public ResponseEntity<EstimateSummeryDto> fetchEstimationSummary(@PathVariable("estId") int estId) {
		EstimateSummeryDto responseDto = new EstimateSummeryDto();
		List<Highlevelesttimeline> hltList = highlevelEstTimelineService.fetchHighlevelEstTimelines(estId);
		Estimation estimation = estimationService.findById(estId);
		responseDto.setIsSubmit(estimation.getIsSubmit());
		if (estimation != null) {
			User user = userRepository.findById(estimation.getCreatedby());
			if (user != null) {
				responseDto.setEstCreatedName(user.getFirstName() + " " + user.getLastName());
			}
		}
		if (hltList != null && !hltList.isEmpty()) {
			responseDto.setHighlevelEstTimelineList(hltList);
		}
		List<EstApprovals> estApprList = estApprovalService.fetchEstApprovalsByEstimationId(estId);
		if (estApprList != null && !estApprList.isEmpty()) {
			responseDto.setEstApprovalList(estApprList);
			/*
			 * EstApprovals estApp = estApprList.get(0);
			 * 
			 * User user = userRepository.findById(estApp.getCreatedby()); if (user != null)
			 * { responseDto.setEstCreatedName(user.getFirstName() + " " +
			 * user.getLastName()); }
			 */

		}
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@ApiOperation(value = "Fetch estimation summary")
	@GetMapping("/fetchEstimationApproval/{estId}")
	public ResponseEntity<EstimateSummeryDto> fetchEstimationApproval(@PathVariable("estId") int estId) {
		EstimateSummeryDto responseDto = new EstimateSummeryDto();
		List<EstApprovals> estApprList = estApprovalService.fetchEstApprovalsByEstimationId(estId);
		if (estApprList != null && !estApprList.isEmpty()) {
			responseDto.setEstApprovalList(estApprList);
			EstApprovals estApp = estApprList.get(0);
			User user = userRepository.findById(estApp.getCreatedby());
			if (user != null) {
				responseDto.setEstCreatedName(user.getFirstName() + " " + user.getLastName());
			}
		}
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	/**
	 * Approve or rejects the request
	 * 
	 * @param estApproval
	 * @return
	 */
	@ApiOperation(value = "Approve or Reject the estimation approval request")
	@PostMapping("/approveOrRejectEstimateApproval")
	public ResponseEntity<EstApprovals> approveOrRejectRequest(@RequestBody EstApprovals estApproval) {
		if (estApproval == null) {
			throw new ErrorResponse("Estimation object is null", HttpStatus.BAD_REQUEST);
		}
		EstApprovals estApprObj = estApprovalService.approveOrRejectRequest(estApproval);
		return new ResponseEntity<EstApprovals>(estApprObj, HttpStatus.OK);
	}

	@ApiOperation(value = "Trigger Email Based on approval status and approver Type")
	@PostMapping("/sendEmailInSummary")
	public ResponseEntity<Estimation> sendEmail(@RequestBody EmailTriggerDto emailTriggerDto) {

		if (emailTriggerDto == null) {
			throw new ErrorResponse("Email Request can't be Empty", HttpStatus.BAD_REQUEST);
		}

		if (emailTriggerDto.getEstId() == null) {
			throw new ErrorResponse("Estimation cant be Empty", HttpStatus.BAD_REQUEST);
		}
		Estimation estimation = estimationService.findById(emailTriggerDto.getEstId());
		User estCreatedUser = userRepository.findById(estimation.getCreatedby());
		List<EstApprovals> estApprovals = estApprovalService
				.fetchEstApprovalsByEstimationIdWithoutflag(estimation.getId());
		if (estApprovals != null && !estApprovals.isEmpty()) {
			for (EstApprovals estApproval : estApprovals) {
				// Email Trigger on Submit
				if (emailTriggerDto.getStatusType().equalsIgnoreCase(DJConstants.Submit)) {
					estApprovalService.sendEmailOnSubmit(estimation, emailTriggerDto, estCreatedUser, estApproval);
					return new ResponseEntity<Estimation>(estimation, HttpStatus.OK);
				}
				// Email Trigger on Approved
				if (emailTriggerDto.getStatusType() != null
						&& emailTriggerDto.getStatusType().equalsIgnoreCase(DJConstants.APPROVED_STATUS)) {
					estApprovalService.sendEmailOnApproval(estimation, emailTriggerDto, estCreatedUser, estApproval);

					return new ResponseEntity<Estimation>(estimation, HttpStatus.OK);
				}
				// Email Trigger on Rework/Reject
				if (emailTriggerDto.getStatusType() != null
						&& emailTriggerDto.getStatusType().equalsIgnoreCase(DJConstants.DRAFT_STATUS)) {
					estApprovalService.sendEmailOnRework(estimation, emailTriggerDto, estCreatedUser, estApproval);
					return new ResponseEntity<Estimation>(estimation, HttpStatus.OK);
				}

			}

		}
		return new ResponseEntity<Estimation>(estimation, HttpStatus.NOT_ACCEPTABLE);

	}

	@ApiOperation(value = "Test Email")
	@GetMapping("/testEmail")
	public void test() {
		EmailUtils emailUtils = new EmailUtils();
		String msg = "<html xmlns='http://www.w3.org/1999/xhtml'><head> <meta http-equiv='Content-Type' content='text/html; charset=utf-8'> <title></title> <style type='text/css'> body{margin: 0; padding: 0; min-width: 100% !important;}img{/* height: auto; */ max-height: 44px; width: auto;}.content{width: 100%; max-width: 600px;}.header{padding: 40px 30px 20px 30px;}.innerpadding{padding: 30px 30px 30px 30px;}.borderbottom{border-bottom: 1px solid #f2eeed;}.subhead{font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}.h1, .h2, .bodycopy{color: #153643; font-family: sans-serif;}.h1{font-size: 33px; line-height: 38px; font-weight: bold;}.h2{padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}.bodycopy{font-size: 16px; line-height: 22px;}.button{text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}.button a{color: #ffffff; text-decoration: none;}.footer{padding: 20px 30px 15px 30px;}.footercopy{font-family: sans-serif; font-size: 14px; color: #ffffff;}.footercopy a{color: #ffffff; text-decoration: underline;}@media only screen and (max-width: 550px), screen and (max-device-width: 550px){body[yahoo] .hide{display: none !important;}body[yahoo] .buttonwrapper{background-color: transparent !important;}body[yahoo] .button{padding: 0px !important;}body[yahoo] .button a{background-color: #66bb6a; padding: 15px 15px 13px !important;}body[yahoo] .unsubscribe{display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none !important; font-weight: bold;}}/*@media only screen and (min-device-width: 601px){.content{width: 600px !important;}.col425{width: 425px!important;}.col380{width: 380px!important;}}*/ </style></head><body yahoo='' bgcolor='#f6f8f1'> <table width='100%' bgcolor='#f6f8f1' border='0' cellpadding='0' cellspacing='0'> <tbody> <tr> <td> <table bgcolor='#ffffff' class='content' align='center' cellpadding='0' cellspacing='0' border='0'> <tbody> <tr> <td class='innerpadding borderbottom'> <table width='100%' border='0' cellspacing='0' cellpadding='0'> <tbody> <tr> <td class='h1'> Hi Rakesh </td></tr><tr> <td class='bodycopy'> Your estimation  is approved at Test Approver. Currently pending for Overall Approvals. </td></tr><tr> <td> <table class='col380' align='left' border='0' cellpadding='0' cellspacing='0' style='width: 100%; max-width: 380px;'> <tbody> <tr> <td> <table width='100%' border='0' cellspacing='0' cellpadding='0'> <tbody> <tr> <td style='padding: 20px 0 0 0;'> <table class='buttonwrapper' bgcolor='#66bb6a' border='0' cellspacing='0' cellpadding='0'> </table> </td></tr></tbody> </table> </td></tr></tbody> </table></body></html>";
		emailUtils.sendEstimationSummaryEmail(msg, "rakesh.sahu@davidjones.com.au", null, "DJ210120-1", null);
	}
}
