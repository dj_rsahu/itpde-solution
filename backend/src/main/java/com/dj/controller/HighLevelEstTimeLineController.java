package com.dj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dj.dto.HighLevelEstTimeLineDto;
import com.dj.entity.Estimation;
import com.dj.entity.Highlevelesttimeline;
import com.dj.entity.User;
import com.dj.service.HighlevelEstTimelineService;
import com.dj.utils.CommonUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/dj/highleveltimeline")
@CrossOrigin(origins = "*")
@Api(value = "Highleveltimeline", description = "Highlevelesttimeline Controller")
public class HighLevelEstTimeLineController {

	@Autowired
	private HighlevelEstTimelineService highlevelEstTimelineService;

	@ApiOperation(value = "Get Highlevelesttimeline By Estimation Id")
	@GetMapping("/getHighlevelesttimeline/{estId}")
	public ResponseEntity<List<Highlevelesttimeline>> getHighlevelesttimelineByEstId(@PathVariable("estId") int estId) {
		List<Highlevelesttimeline> highlevelesttimeline = highlevelEstTimelineService
				.getHighlevelesttimelineByEstId(estId);
		return new ResponseEntity<>(highlevelesttimeline, HttpStatus.OK);
	}

	@ApiOperation(value = "Create Highlevelesttimeline")
	@PostMapping("/create/highlevelesttimeline")
	public ResponseEntity<HighLevelEstTimeLineDto> createHighlevelesttimeline(
			@RequestBody HighLevelEstTimeLineDto highLevelEstTimeLineDto) {
		highlevelEstTimelineService.createHighlevelesttimeline(highLevelEstTimeLineDto);
		return new ResponseEntity<>(highLevelEstTimeLineDto, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Update Highlevelesttimeline")
	@PutMapping("/update/highlevelesttimeline")
	public ResponseEntity<HighLevelEstTimeLineDto> updateHighlevelesttimeline(
			@RequestBody HighLevelEstTimeLineDto highLevelEstTimeLineDto) {
		highlevelEstTimelineService.updateHighlevelesttimeline(highLevelEstTimeLineDto);
		return new ResponseEntity<>(highLevelEstTimeLineDto, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Delete Highlevelesttimeline")
	@PutMapping("/delete/highlevelesttimeline")
	public ResponseEntity<HighLevelEstTimeLineDto> deleteHighlevelesttimeline(
			@RequestBody HighLevelEstTimeLineDto highLevelEstTimeLineDto) {
		highlevelEstTimelineService.deleteHighlevelesttimeline(highLevelEstTimeLineDto);
		return new ResponseEntity<>(highLevelEstTimeLineDto, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Save Highlevelesttimeline")
	@PostMapping("/save/highlevelesttimeline/{estId}")
	public ResponseEntity<HighLevelEstTimeLineDto> saveHighlevelesttimeline(
			@RequestBody HighLevelEstTimeLineDto highLevelEstTimeLineDto,
			@PathVariable("estId") int estId) {
		highlevelEstTimelineService.saveHighlevelesttimeline(highLevelEstTimeLineDto,estId);
		return new ResponseEntity<>(highLevelEstTimeLineDto, HttpStatus.CREATED);
	}
}
