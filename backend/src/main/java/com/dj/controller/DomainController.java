package com.dj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dj.entity.Domain;
import com.dj.entity.RRDGroup;
import com.dj.service.DomainService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/dj/domain")
@CrossOrigin(origins = "*")
@Api(value="Domain", description="Domain Controller")
public class DomainController {
	@Autowired
	private DomainService domainService;
	
	@ApiOperation(value = "Get Domains")
	@GetMapping("/getDomains")
	public ResponseEntity<List<Domain>> getDomains() {
		List<Domain> domains = domainService.getDomains();
		return new ResponseEntity<>(domains, HttpStatus.OK);
	}

}
