package com.dj.controller;

import javax.xml.ws.soap.AddressingFeature.Responses;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.dj.exception.ErrorResponse;
import com.dj.utils.DJConstants;


@Controller
@CrossOrigin(origins="*")
@RequestMapping("/dj")
public class ScoreBoardMetricsTed {
	
	RestTemplate rt=new RestTemplate();	
	@GetMapping("/metrics/list")
	public ResponseEntity<String> getALlMetrics(){
		String url=DJConstants.BASE_URL+"/list";

		//rt.getForEntity(url, JSONObject.class);
		 ResponseEntity<String> exchange = rt.exchange(url,HttpMethod.GET,null,String.class);
		 if(exchange.getStatusCode() != HttpStatus.OK) {
			 throw new ErrorResponse("FAILED",HttpStatus.METHOD_FAILURE);
		 }
		return new ResponseEntity<String>(exchange.getBody().toString(),HttpStatus.OK);
	}
	@PutMapping("/metrics/{id}")
	public ResponseEntity<String> updateMetric(@PathVariable int id,@RequestBody String revised_actual) throws ParseException {
		String url=DJConstants.BASE_URL+id;
		JSONParser parser = new JSONParser();
		JSONObject jsonObj = (JSONObject) parser.parse(revised_actual);
		// rt.put(url,jsonObj,id);	
		return rt.postForEntity(url, jsonObj, String.class);
	}

}
