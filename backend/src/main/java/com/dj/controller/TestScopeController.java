package com.dj.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dj.dto.TestScopeDto;
import com.dj.entity.TestScope;
import com.dj.service.TestScopeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author ramesh.bandari
 *
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/dj")
@Api(value = "TestScopeDetails", description = "Operations pertaining to insert, update & fetch the test scope details")
public class TestScopeController {

	@Autowired
	private TestScopeService testScopeService;
	
	private static final Logger logger = LoggerFactory.getLogger(TestScopeController.class);

	@ApiOperation(value = "insert test scope details")
	@PostMapping("/saveOrUpdateTestScope/{estId}")
	public  ResponseEntity<TestScopeDto> saveOrUpdateTestScope(@RequestBody TestScopeDto testScopeDto,
			@PathVariable("estId") int estId){
		logger.info("In saveOrUpdate Test Scope");
		if(testScopeDto.getTestScopeListObj() !=null && !testScopeDto.getTestScopeListObj().isEmpty()) {
			testScopeService.saveOrUpdateScopeDetails(testScopeDto, estId);
		}
		return new ResponseEntity<TestScopeDto>(testScopeDto, HttpStatus.OK); 
	}
	
	@ApiOperation(value = "fetch test scope details")
	@GetMapping("/fetchTestScope/{estid}")
	public  ResponseEntity<List<TestScope>> fetchTestScope(@PathVariable("estid") Integer estid){
		List<TestScope> testScopeList = testScopeService.fetchTestScopeByEstId(estid);
		return new ResponseEntity<List<TestScope>>(testScopeList, HttpStatus.OK); 
	}
	
}
