package com.dj.controller;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.dj.entity.Estimation;
import com.dj.entity.Projectstatus;
import com.dj.entity.User;
import com.dj.exception.ErrorResponse;
import com.dj.repository.UserRepository;
import com.dj.service.EstimationService;
import com.dj.service.ProjectStatusService;
import com.dj.utils.CommonUtils;

/**
 * @author thirumalarao.r
 *
 */
@Controller
@CrossOrigin(origins="*")
@RequestMapping("/dj")
public class EstimationController {
	@Autowired
	EstimationService estimationService;
	@Autowired
	private UserRepository userRepository; 
	@Autowired
	private ProjectStatusService projectStatusService;


	private static final Logger logger = LoggerFactory.getLogger(EstimationController.class);


	/*
	 * Method Name: getEstimationById
	 * It will get the user based on the user id provided by api
	 * It will return the estimation entity
	 * API method type is Get
	 * 
	 */
	@GetMapping("/getEstimationById/{estimationId}")
	public ResponseEntity<Estimation> getEstimationById(@PathVariable("estimationId") int estimationId) {
		Estimation estimation = new Estimation();
		estimation= estimationService.findById(estimationId);
		User estimationUpdatedByUser = userRepository.findById(estimation.getUpdatedby());
		estimation.setUpdatedByName(estimationUpdatedByUser.getFirstName()+" "+estimationUpdatedByUser.getLastName());
		Integer userid = null;
		if(estimation.getProject_lead()!=null) {
			try {
				userid = estimation.getProject_lead();
				if(userid!=null && userRepository.findById(userid)==null) {
					throw new ErrorResponse("User not found with id "+userid, HttpStatus.NOT_FOUND);
				}
				User userObj = userRepository.findById(userid);
				estimation.setProjectLeadUser(userObj);
				estimation.setProjectLeadName(userObj.getFirstName()+" "+userObj.getLastName());
				if(estimation!=null) {
					Projectstatus Projectstatus = projectStatusService.getProjectStatusById(estimation.getProjectphaseid());
					if(Projectstatus!=null) {
						estimation.setProjectPhaseName(Projectstatus.getName());
						estimation.setProjectStatusObj(Projectstatus);
					}
				}
			}catch(Exception e) {
				logger.error("Exception while fetching project lead info");
			}
		}
		return new ResponseEntity<Estimation> (estimation,HttpStatus.OK);
	}
	
	@GetMapping("/getEstimationsByUserId")
	public ResponseEntity<List<Estimation>> getEstimationByNewId() {
		User userObj = CommonUtils.getLoggedInUserDetails();
		List<Estimation> estimations = estimationService.getEstimationsByUserId(userObj.getId());
		List<User> users = userRepository.findAll();
		for (Estimation estimation : estimations) {
			for (User user : users) {
				if(estimation.getProject_lead().intValue() == user.getId()) {
					estimation.setProjectLeadName(user.getFirstName() +" "+user.getLastName());
				}
				if(estimation.getUpdatedby() == user.getId()) {
					estimation.setUpdatedByName(user.getFirstName() +" "+user.getLastName());
				}
			}
		}
		return new ResponseEntity<List<Estimation>> (estimations, HttpStatus.OK);
	}




	/*
	 * Method Name: getEstimation
	 * It will get all the estimations 
	 * It will return the list of estimation entity
	 * API method type is Get
	 * 
	 */
	@GetMapping("/getEstimations")
	public ResponseEntity<List<Estimation>> getEstimation() {
		List<Estimation> estimations = estimationService.getEstimations();
		List<User> users = userRepository.findAll();
		for (Estimation estimation : estimations) {
			for (User user : users) {
				if(estimation.getProject_lead().intValue() == user.getId()) {
					estimation.setProjectLeadName(user.getFirstName() +" "+user.getLastName());
				}
				if(estimation.getUpdatedby() == user.getId()) {
					estimation.setUpdatedByName(user.getFirstName() +" "+user.getLastName());
				}
			}
		}
		return new ResponseEntity<List<Estimation>> (estimations, HttpStatus.OK);
	}

	/*
	 * Method Name: saveEstimation
	 * It will save the estimation into database 
	 * It will return the inserted estimation
	 * API method type is POST
	 * 
	 */
	@PostMapping("/saveEstimation")
	public ResponseEntity<Estimation> saveEstimation(@RequestBody Estimation estimation)  {
		estimation=estimationService.saveEstimation(estimation);
		return new ResponseEntity<Estimation> (estimation,HttpStatus.OK);
	}

	/*
	 * Method Name: deleteEstimationById
	 * It will delete or remove the estimation from database based on requested id
	 * It will return success status
	 * API method type is DELETE
	 * 
	 */
	@DeleteMapping("/deleteEstimationById/{estimationId}")
	public ResponseEntity<Estimation> deleteEstimationById(@PathVariable("estimationId") int estimationId) {
		estimationService.deleteEstimationById(estimationId);
		return new ResponseEntity<Estimation> (HttpStatus.OK);
	}



	/*
	 * Method Name: updateEstimation
	 * It will update or modify the estimation date into database based on request
	 * It will return success status and user entity
	 * API method type is PUT
	 * 
	 */
	@PutMapping("/updateEstimation")
	public ResponseEntity<Estimation> updateEstimation(@RequestBody Estimation estimation) {
		Estimation est=estimationService.updateEstimation(estimation);
		return new ResponseEntity<Estimation> (est,HttpStatus.OK);
	}
	
	
	/**
	 * This API is used to fetch the EstimationStatus of respective user role 
	 * which could be Admin,Test Approver,Overall Approver,User 
	 */
	@GetMapping("/getEstimationStatusByRole")
	public ResponseEntity<List<Estimation>> getEstimationStatus() {
		User userObj = CommonUtils.getLoggedInUserDetails();
		List<Estimation> estimations = estimationService.getEstimationStatusByRole(userObj);
		return new ResponseEntity<List<Estimation>> (estimations, HttpStatus.OK);
	}
	
	@DeleteMapping("/deleteEstimationDataById/{estimationId}")
	public ResponseEntity<Estimation> deleteEstimationDataById(@PathVariable("estimationId") int estimationId) {
		estimationService.deleteEstimationDataById(estimationId);
		return new ResponseEntity<Estimation> (HttpStatus.OK);
	}
	
	@PostMapping("/cloneEstimation/{estimationId}")
	public ResponseEntity<Estimation> cloneEstimation(@PathVariable("estimationId") int estimationId)  {
		Estimation estimation=estimationService.cloneEstimation(estimationId);
		return new ResponseEntity<Estimation> (estimation,HttpStatus.OK);
	}
	
	@PostMapping("/estimationEditable/{estimationId}")
	public ResponseEntity<Estimation> makeEstimationEditableOrNonEditable(@PathVariable("estimationId") int estimationId,
			@RequestParam(name = "isEditable", required = true) byte isEditable)  {
		Estimation estimation = estimationService.makeEstimationEditableOrNonEditable(estimationId,isEditable);
		return new ResponseEntity<Estimation>(estimation,HttpStatus.OK);
	}
	
	@PostMapping("/increaseEstimationEditableDuration/{estimationId}")
	public ResponseEntity<Estimation> increaseEstimationEditableDuration(@PathVariable("estimationId") int estimationId)  {
		Estimation estimation = estimationService.increaseEstimationEditableDuration(estimationId);
		return new ResponseEntity<Estimation>(estimation,HttpStatus.OK);
	}
	/*
	 * @GetMapping("/test/{id}") public String test(@PathVariable("id") int id) {
	 * Timer timer = new Timer(); timer.schedule( new TimerTask() { public void
	 * run() { m1(id); } }, 60000 ); return "Success"; }
	 * 
	 * public static void m1(int id) { System.out.println(id+" date: "+new Date());
	 * }
	 */
}
