package com.dj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dj.dto.AssumptionDto;
import com.dj.entity.Assumption;
import com.dj.entity.RRDTransaction;
import com.dj.exception.ErrorResponse;
import com.dj.service.AssumptionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/dj/assumption")
@CrossOrigin(origins = "*")
@Api(value = "Assumption", description = "Assumption Controller")
public class AssumptionController {

	@Autowired
	private AssumptionService assumptionService;
	
	@ApiOperation(value = "Get Assumptions")
	@GetMapping("/getAssumptions")
	public ResponseEntity<List<Assumption>> getAssumptions(
			@RequestParam(name = "search", required = false) String search) {
		List<Assumption> assumptions = assumptionService.getAssumptions(search);
		return new ResponseEntity<>(assumptions, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Get Assumptions By Estimation Id")
	@GetMapping("/getAssumptions/{estId}")
	public ResponseEntity<List<Assumption>> getAssumptionsByEstId(
			@PathVariable("estId") int estId,
			@RequestParam(name = "search", required = false) String search) {
		List<Assumption> assumptions = assumptionService.getAssumptionsByEstId(estId,search);
		return new ResponseEntity<>(assumptions, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Create Assumption")
	@PostMapping("/create/assumption")
	public ResponseEntity<Assumption> createAssumption(@RequestBody Assumption assumption) {
		assumptionService.createAssumption(assumption);
		return new ResponseEntity<>(assumption, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Update Assumption")
	@PutMapping("/update/assumption")
	public ResponseEntity<Assumption> updateAssumption(@RequestBody Assumption assumption) {
		Assumption assumptionTemp = assumptionService.updateAssumption(assumption);
		return new ResponseEntity<>(assumptionTemp, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Delete Assumption")
	@DeleteMapping("/delete/assumption/{assumptionId}")
	public ResponseEntity<Assumption> deleteAssumption(@PathVariable("assumptionId") int assumptionId) {
		Assumption assumption = assumptionService.deleteAssumption(assumptionId);
		if(assumption == null) {
			throw new ErrorResponse("Assumption Id " + assumptionId + "doesnot exists to delete", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(assumption, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Save Assumption")
	@PostMapping("/save/assumption")
	public ResponseEntity<AssumptionDto> saveAssumption(@RequestBody AssumptionDto assumptionDto) {
		assumptionService.saveAssumption(assumptionDto);
		return new ResponseEntity<>(assumptionDto, HttpStatus.CREATED);
	}
	
	/**
	 * This API is used to fetch count of Assumption records by estimationId
	 * @param estimationId
	 * @return Number of Assumpiton being return
	 */
	@ApiOperation(value = "Get Estimations Count")
	@GetMapping("/getEstimationsCountByEstimationId/{estimationId}")
	public Integer getEstimationsCountByEstimationId(@PathVariable("estimationId") int estimationId) {
		return assumptionService.getEstimationsCountByEstimationId(estimationId);
	}
}
