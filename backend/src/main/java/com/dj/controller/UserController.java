package com.dj.controller;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.dj.config.CustomDetailService;
import com.dj.config.JwtTokenUtil;
import com.dj.entity.User;
import com.dj.entity.UserGroup;
import com.dj.exception.ErrorResponse;
import com.dj.service.UserGroupService;
import com.dj.service.UserRepositoryService;
import com.dj.service.UserService;
import com.dj.utils.EmailUtils;

/*
 * This class is a user controller, Act as a interface through request Methods.
 * Listens to the user request through API calls based on the type of methods.
 * User related CRUD and other required methods.
 * 
 * Author: Qentelli Solutions Pvt Ltd
 * Created on August, 2019.
 * 
 */

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/dj")
public class UserController {

	@Autowired
	UserService userService;
	
	@Autowired
	UserRepositoryService userRepositoryService;
	@Autowired
	UserGroupService userGroupService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private CustomDetailService userDetailsService;
	EmailUtils mail = new EmailUtils();
	
	JSONParser parser = new JSONParser();
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	/*
	 * Method Name: getUserById It will get the user based on the user id provided
	 * by api It will return the user entity API method type is Get
	 * 
	 */
	@GetMapping("/getUserById")
	public ResponseEntity<User> getUserById(@RequestParam int userId) {
		User user = new User();
		user = userService.findById(userId);
		user.setGroupName((userGroupService.getUserGroupByUserId(userId)).getGroupName());	
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/*
	 * Method Name: getUserByEmail It will get the user based on the user email
	 * provided by api It will return the user entity API method type is Get
	 * 
	 */
	@GetMapping("/getUserByEmail")
	public ResponseEntity<User> getUserByEmail(@RequestParam String email) {
		User user = new User();
		user = userService.findByEmail(email);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/*
	 * Method Name: getUsers It will get all the users It will return the list of
	 * user entity API method type is Get
	 * 
	 */
	@GetMapping("/getUsers")
	public ResponseEntity<List<User>> getUsers() {
		//List<User> users = userService.getUsers();
		List<User> users = userService.fetchAllNonDeletedUsers();
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getUserGroup() != null) {
				users.get(i).setGroupName(users.get(i).getUserGroup().getGroupName());
			} else {
				users.get(i).setGroupName("notAssigned");
			}
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	@GetMapping("/getUsersByRole/{role}")
	public ResponseEntity<List<User>> getUsersByRole(@PathVariable("role") String role){
		
		List<User> users=userService.fetchUsersByRole(role);
		if(users.isEmpty() || users.size()==0) {
			throw new ErrorResponse("No Users found for this Role", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	@GetMapping("/getAllUsers")
	public ResponseEntity<List<User>> getAllUsers(@RequestParam(name = "search", required = false) String search) {
		List<User> users = userRepositoryService.getAllUsers(search);
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getUserGroup() != null) {
				users.get(i).setGroupName(users.get(i).getUserGroup().getGroupName());
			} else {
				users.get(i).setGroupName("notAssigned");
			}
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}

	/*
	 * Method Name: saveUser It will save or register the user and saveed into
	 * database It will return the registered user API method type is POST
	 * 
	 */
	@PostMapping("/saveUser")
	public ResponseEntity<User> saveUser(@RequestBody User user) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		/*
		 * if(userService.findByEmail(user.getEmail()) != null) { return new
		 * ResponseEntity<User>(user,HttpStatus.ALREADY_REPORTED); }
		 * logger.info("Enterd into the saveUser method"); user.setIsActive((byte) 0);
		 */
				
		userService.saveUser(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	
	@PostMapping("/createUser")
	public ResponseEntity<User> createUser(@RequestBody User user) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		
		  if(userService.findByEmail(user.getEmail()) != null) { 
//			  return new ResponseEntity<User>(user,HttpStatus.IM_USED); 
			  throw new ErrorResponse("Email is already in use.", HttpStatus.IM_USED);
			  
			  }
		  logger.info("Enterd into the saveUser method");
		 
		UserGroup userGroup = userGroupService.getUserGroupBYName(user.getGroupName());
		user.setUserGroup(userGroup);
		user.setPassword("Test@123");
		user.setIsActive((byte)1);
		user.setCreatedDate(LocalDateTime.now());
		user.setUpdatedDate(LocalDateTime.now());
		user.setIsdelete((byte)0);
		
		userService.saveUser(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	/*
	 * Method Name: deleteUserById It will delete or remove the user from database
	 * based on requested id It will return success status API method type is DELETE
	 * 
	 */
	@DeleteMapping("/deleteUserById")
	public ResponseEntity<User> deleteUserById(@RequestParam int userId) {
		//User user = new User();
		//User loggedInUser = CommonUtils.getLoggedInUserDetails();
		User user = userService.findById(userId);
		if(user!=null) {
			user.setUpdatedDate(LocalDateTime.now());
			user.setIsdelete((byte) 1);
			userService.saveUser(user);
		}
		else {
			throw new ErrorResponse("User does not exist with id"+userId, HttpStatus.NOT_FOUND);
		}
		
		//userService.deleteUserById(userId);
		return new ResponseEntity<User>(HttpStatus.OK);
	}

	/*
	 * Method Name: deleteUserByEmail It will delete or remove the user from
	 * database based on requested email It will return success status API method
	 * type is DELETE
	 * 
	 */
	@DeleteMapping("/deleteUserByEmail")
	public ResponseEntity<User> deleteUserByEmail(@RequestParam String email) {
		userService.deleteUserByemail(email);
		return new ResponseEntity<User>(HttpStatus.OK);
	}

	/*
	 * Method Name: updateUser It will update or modify the user date into database
	 * based on request It will return success status and user entity API method
	 * type is PUT
	 * 
	 */
	@PutMapping("/updateUser")
	public ResponseEntity<User> updateUser(@RequestBody User user) {
		user.setIsActive((byte) 1);
		if(user.getGroupName()!=null) {
		UserGroup userGroup = userGroupService.getUserGroupBYName(user.getGroupName());
		user.setUserGroup(userGroup);
		}
		
		userService.updateUser(user);

		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	

	/*
	 * Method Name: mobileLogin It will check the user is valid user or not It will
	 * return the status,user and token generated for security API method type is
	 * POST
	 * 
	 */
	@PostMapping("/mobileLogin")
	public ResponseEntity<User> mobileLogin(@RequestParam String email, @RequestParam String password)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException {
		logger.info("Enterd into the mobile login method");
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
		final UserDetails userDetails = userDetailsService.loadUserByUsername(email);
		User user = userService.findByEmail(email);
		user.setToken("Bearer " + jwtTokenUtil.generateToken(userDetails));
//		user.setToken("Basic "+Base64.getEncoder().encodeToString((email+":"+password).getBytes()));
		if (user.getEmail().equals(email) && user.getPassword().equals(password) && user.getIsActive() == 0) {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
		return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}

	@PostMapping("/login")
	public ResponseEntity<User> login(@RequestBody User requser) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		logger.info("Enterd into the login method");
		String email = requser.getEmail();
		String password = requser.getPassword();
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
		final UserDetails userDetails = userDetailsService.loadUserByUsername(email);
		//User user = userService.findByEmail(email);
		User user = userService.findByEmailAnddDelFlag(email, (byte) 0 );
		if(user == null) {
			throw new ErrorResponse("User is deleted from the records "+"'"+email+"'", HttpStatus.NOT_FOUND);
		}
		user.setToken("Bearer " + jwtTokenUtil.generateToken(userDetails));
		user.setGroupName((userGroupService.getUserGroupByUserId(user.getId()).getGroupName()));
		
		if(user.getRole().equals("No Access")) {
			throw new ErrorResponse("Invalid User!! Please contact Administrator", HttpStatus.UNAUTHORIZED);
	}
//		user.setToken("Basic "+Base64.getEncoder().encodeToString((email+":"+password).getBytes()));
		if (user.getEmail().equals(email) && user.getPassword().equals(password) && user.getIsActive() == 1) {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
		return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}

	/*
	 * Method Name: webLogin It will check the user is valid user(admin) or not It
	 * will return the status,user and token generated for security API method type
	 * is POST
	 * 
	 */
	@PostMapping("/webLogin")
	public ResponseEntity<User> webLogin(@RequestParam String email, @RequestParam String password)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException {
		logger.info("Enterd into the web login method");
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
		final UserDetails userDetails = userDetailsService.loadUserByUsername(email);
		User user = userService.findByEmail(email);
		user.setToken("Bearer " + jwtTokenUtil.generateToken(userDetails));
//		user.setToken("Basic "+Base64.getEncoder().encodeToString((email+":"+password).getBytes()));
		if (user.getEmail().equals(email) && user.getPassword().equals(password)
				&& user.getRole().equalsIgnoreCase("admin") && user.getIsActive() == 1) {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
		return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}

	/*
	 * Method Name: forgotPassword It will check the conditions like valid user,
	 * active user and send the email with link It will return the status API method
	 * type is POST
	 * 
	 */
	@PostMapping("/forgotPassword")
	public ResponseEntity<Boolean> forgotPassword(@RequestParam String email, @RequestParam String path) {
		User user = userService.findByEmail(email);
		path = "http://bes.qentelli.com:8085/#/" + path;
		if (user.getEmail().equals(email) && (user.getIsActive() == 1)) {
			mail.sendEmail(email, path, "forgot");
			return new ResponseEntity<Boolean>(true, HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
	}
	

	/*
	 * Method Name: resetPassword It will get the user based on the requested email
	 * and update the user into database It will return the status API method type
	 * is POST
	 * 
	 */
	@PostMapping("/resetPassword")
	public ResponseEntity<Boolean> resetPassword(@RequestBody String userData) throws ParseException {
//		User user = userService.findByEmail(email);
//		if (user.getPassword().equals(oldPassword)) {
//			user.setPassword(newPassword);
//			userService.updateUser(user);
//			return new ResponseEntity<Boolean>(true, HttpStatus.OK);
//		}
//		return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
		
		JSONObject jsonObject = (JSONObject) parser.parse(userData);
		String email = (String) jsonObject.get("email");
		
		User userByEmail = userService.findByEmail(email);
		
		if(userByEmail != null) {
			String newPwd = (String) jsonObject.get("newPassword");
			String oldPwd = (String) jsonObject.get("oldPassword");
			
			String dbPwd = userByEmail.getPassword();
			if(dbPwd != null) {
				if (dbPwd.equals(oldPwd)) {
					userByEmail.setPassword(newPwd);
					userByEmail.setUpdatedDate(LocalDateTime.now());
					userService.updateUser(userByEmail);
					return new ResponseEntity<Boolean>(true, HttpStatus.OK);
				}
			}
		}
		throw new ErrorResponse("Entered Password is Incorrect", HttpStatus.NOT_FOUND);

	
	}

	/*
	 * Method Name: sendemail It will send the email based on provided email It will
	 * return the status and the user entity API method type is POST
	 * 
	 */
	@PostMapping("/sendEmail")
	public ResponseEntity<User> sendemail(@RequestParam String email) {
		User user = userService.findByEmail(email);
		String path = "http://besconnect.qentelli.com:8085/bes/activeUser?email=" + email;
		mail.sendEmail(email, path, "signup");
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	/*
	 * Method Name: activeUser It will set the user status as activated in email
	 * link or we can use as per the requirement It will return the status and the
	 * user name API method type is GET
	 * 
	 */
	@GetMapping("/activeUser")
	public ResponseEntity<String> activeUser(@RequestParam String email) {
		User user = userService.findByEmail(email);
		if (user.getIsActive() == 1) {
			return new ResponseEntity<String>("Already Active User " + user.getFirstName(), HttpStatus.OK);
		}
		user.setIsActive((byte) 1);
		userService.updateUser(user);
		return new ResponseEntity<>("Activated the user " + user.getFirstName(), HttpStatus.OK);
	}

	/*
	 * Method Name: activateUserFromAdmin It will get the user based on the
	 * requested email It will set the user status as activated by admin in as per
	 * the requirement It will return the status and the user name API method type
	 * is GET
	 * 
	 */
	@GetMapping("/activateUserFromAdmin")
	public ResponseEntity<String> activateUserFromAdmin(@RequestParam String email) {
		User user = userService.findByEmail(email);
		/*
		 * if(user.getIsActive()==1) { return new
		 * ResponseEntity<String>("Already Active User "+user.getFirstName(),
		 * HttpStatus.OK); }else if(user.getIsActive()==0) { return new
		 * ResponseEntity<String>("Already Not Activated "+user.getFirstName(),
		 * HttpStatus.OK); }
		 */
		user.setIsActive((byte) -1);
		userService.updateUser(user);
		return new ResponseEntity<String>("Activated the user: " + user.getFirstName() + " from Admin Side",
				HttpStatus.OK);
	}

	/*
	 * Method Name: uploadImage It will get the user based on the requested user id
	 * It will upload the user pic into images server and update the user It will
	 * return the status and the user entity API method type is POST
	 * 
	 */
	@PostMapping("/uploadImage")
	public ResponseEntity<User> uploadImage(@RequestParam("imageFile") MultipartFile imageFile, @RequestParam int id)
			throws Exception {
		logger.info(imageFile.getOriginalFilename());
		User user = userService.findById(id);
		userService.saveImage(imageFile, id);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	/*
	 * Method Name: downloadImage It will get the user pic based on the requested
	 * user id API method type is GET
	 * 
	 */
	@GetMapping("/getImage")
	public ResponseEntity<String> downloadImage(@RequestParam int id) throws Exception {
		return new ResponseEntity<String>(userService.getImage(id), HttpStatus.OK);
	}

	/*
	 * Method Name: getToken It will get the security token based on the requested
	 * username and password API method type is GET
	 * 
	 */
	@GetMapping("/getToken")
	public ResponseEntity<String> getToken(@RequestParam String username, @RequestParam String password)
			throws Exception {
		String token = "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
		return new ResponseEntity<String>(token, HttpStatus.OK);
	}
}