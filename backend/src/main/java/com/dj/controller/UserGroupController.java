package com.dj.controller;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.dj.entity.UserGroup;
import com.dj.service.UserGroupService;

@Controller
@CrossOrigin(origins="*")
@RequestMapping("/dj")
public class UserGroupController {
	@Autowired
	UserGroupService userGroupService;


	private static final Logger logger = LoggerFactory.getLogger(UserGroupController.class);


	/*
	 * Method Name: getEstimationById
	 * It will get the user based on the user id provided by api
	 * It will return the estimation entity
	 * API method type is Get
	 * 
	 */
	@GetMapping("/getUserGroupById")
	public ResponseEntity<UserGroup> getEstimationById(@RequestParam int userGroupId) {
		UserGroup userGroup= userGroupService.findById(userGroupId);
		return new ResponseEntity<UserGroup> (userGroup,HttpStatus.OK);
	}
	
	
	

	@GetMapping("/getUserGroupByUserId/{userId}")
	public ResponseEntity<UserGroup> getUserGroupByUserId(@PathVariable("userId") int userId){
		
		return new ResponseEntity<UserGroup>(userGroupService.getUserGroupByUserId(userId),HttpStatus.OK);
	}


	/*
	 * Method Name: getEstimation
	 * It will get all the estimations 
	 * It will return the list of estimation entity
	 * API method type is Get
	 * 
	 */
	@GetMapping("/getUserGroups")
	public ResponseEntity<List<UserGroup>> getUserGroup() {
		List<UserGroup> userGroup = userGroupService.getUserGroups();
		return new ResponseEntity<List<UserGroup>> (userGroup, HttpStatus.OK);
	}

	/*
	 * Method Name: saveEstimation
	 * It will save the estimation into database 
	 * It will return the inserted estimation
	 * API method type is POST
	 * 
	 */
	@PostMapping("/saveUserGroup")
	public ResponseEntity<UserGroup> saveUserGroup(@RequestBody UserGroup userGroup) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		userGroup=userGroupService.saveUserGroup(userGroup);
		return new ResponseEntity<UserGroup> (userGroup,HttpStatus.OK);
	}

	/*
	 * Method Name: deleteEstimationById
	 * It will delete or remove the estimation from database based on requested id
	 * It will return success status
	 * API method type is DELETE
	 * 
	 */
	@DeleteMapping("/deletUserGroupById")
	public ResponseEntity<String> deleteEstimationById(@RequestParam int userGroupId) {
		userGroupService.deleteUserGroupById(userGroupId);
		return new ResponseEntity<String> (HttpStatus.OK);
	}



	/*
	 * Method Name: updateEstimation
	 * It will update or modify the estimation date into database based on request
	 * It will return success status and user entity
	 * API method type is PUT
	 * 
	 */
	@PutMapping("/updateUserGroup")
	public ResponseEntity<UserGroup> updateEstimation(@RequestBody UserGroup userGroup) {
		 userGroup=userGroupService.updateUserGroup(userGroup);
		return new ResponseEntity<UserGroup> (userGroup,HttpStatus.OK);
	}

}
