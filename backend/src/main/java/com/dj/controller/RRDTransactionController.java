package com.dj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dj.entity.RRDTransaction;
import com.dj.exception.ErrorResponse;
import com.dj.service.RRDTransactionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/dj/rrdTransaction")
@CrossOrigin(origins = "*")
@Api(value = "RRD Transaction", description = "RRD Transaction Controller")
public class RRDTransactionController {
	@Autowired
	private RRDTransactionService rrdTransactionService;

	@ApiOperation(value = "Get RRD Transactions")
	@GetMapping("/getRRDTransactions")
	public ResponseEntity<List<RRDTransaction>> getAllRRDTransactions(
			@RequestParam(name = "search", required = false) String search) {
		List<RRDTransaction> rrdTransactions = rrdTransactionService.getAllRRDTransactions(search);
		return new ResponseEntity<>(rrdTransactions, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Get RRD Transactions By Group Id")
	@GetMapping("/getRRDTransactionsByGroupId/{groupId}")
	public ResponseEntity<List<RRDTransaction>> getRRDTransactionsByGroupId(
			@PathVariable("groupId") int groupId,
			@RequestParam(name = "search", required = false) String search) {
		List<RRDTransaction> rrdTransactions = rrdTransactionService.getRRDTransactionsByGroupId(search,groupId);
		return new ResponseEntity<>(rrdTransactions, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Get RRD Transaction By Id")
	@GetMapping("/getRRDTransactionById/{rrdTransId}")
	public ResponseEntity<RRDTransaction> getRRDTransactionById(
			@PathVariable("rrdTransId") int rrdTransId) {
		RRDTransaction rrdTransaction = rrdTransactionService.getRRDTransactionById(rrdTransId);
		return new ResponseEntity<>(rrdTransaction, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Create rrd transaction")
	@PostMapping("/create/rrdTransaction")
	public ResponseEntity<RRDTransaction> createRRDTransaction(@RequestBody RRDTransaction rrdTransaction) {
		rrdTransactionService.createRRDTransaction(rrdTransaction);
		return new ResponseEntity<>(rrdTransaction, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Update rrd transaction")
	@PutMapping("/update/rrdTransaction")
	public ResponseEntity<RRDTransaction> updateRRDTransaction(@RequestBody RRDTransaction rrdTransaction) {
		rrdTransactionService.updateRRDTransaction(rrdTransaction);
		return new ResponseEntity<>(rrdTransaction, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Delete rrd transaction")
	@DeleteMapping("/delete/rrdTransaction/{rrdTransactionId}")
	public ResponseEntity<RRDTransaction> deleteRRDTransaction(@PathVariable("rrdTransactionId") int rrdTransactionId) {
		RRDTransaction rrdTransaction = rrdTransactionService.deleteRRDTransaction(rrdTransactionId);
		if(rrdTransaction == null) {
			throw new ErrorResponse("RRD Transaction Id " + rrdTransactionId + "doesnot exists to delete", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(rrdTransaction, HttpStatus.CREATED);
	}
}
