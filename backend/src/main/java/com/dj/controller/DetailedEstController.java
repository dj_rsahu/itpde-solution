package com.dj.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dj.dto.DetailedEstDto;
import com.dj.entity.DetailedEstimates;
import com.dj.service.DetailedEstService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/dj")
@Api(value = "DetailedEstController", description = "Detailed Estimates Controller")
public class DetailedEstController {

	private static final Logger logger = LoggerFactory.getLogger(DetailedEstController.class);

	@Autowired
	DetailedEstService detailedEstService;

	/**
	 * @param detailedEst
	 * @return
	 */
	@ApiOperation(value = "Save Detailed Estimates")
	@PostMapping("/saveDetailedEst/{estId}")
	public ResponseEntity<DetailedEstDto> saveDetailedEst(@RequestBody DetailedEstDto detailedEstDto,
			@PathVariable("estId") int estId) {
		logger.info("DetailedEstController saveDetailedEst ");
		detailedEstService.saveDetailedEst(detailedEstDto, estId);
		return new ResponseEntity<>(detailedEstDto, HttpStatus.CREATED);
	}
	
	/**
	 * @param estId
	 * @return
	 */
	@ApiOperation(value = "Get Detailed Estimates By Estimation Id")
	@GetMapping("/getDetailedEst/{estId}")
	public ResponseEntity<List<DetailedEstimates>> getDetailedEstByEstId(@PathVariable("estId") int estId) {
		logger.info("DetailedEstController getDetailedEstByEstId ");
		List<DetailedEstimates> detailedEstList = detailedEstService
				.getDetailedEstByEstId(estId);
		
		return new ResponseEntity<>(detailedEstList, HttpStatus.OK);
	}
	
	/**
	 * @param estId
	 * @return
	 */
	@ApiOperation(value = "Get Module Type")
	@GetMapping("/getDetEstModuleType")
	public ResponseEntity<List<String>> getDetEstModuleType(){
		logger.info("DetailedEstController getDetEstModuleType ");
		List<String> detEstModuleTypeList = detailedEstService.getDetEstModuleType();
		return new ResponseEntity<>(detEstModuleTypeList, HttpStatus.OK);
	}

}
