package com.dj.controller;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.dj.entity.EstimatedDocuments;
import com.dj.entity.Estimation;
import com.dj.service.EstimationDocumentService;
import com.dj.service.EstimationService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

@Controller
@CrossOrigin(origins="*")
@RequestMapping("/dj")
public class EstimationDocumentController {
	@Autowired
	EstimationDocumentService estimationDocumentService;
	@Autowired
	EstimationService estimationService;


	private static final Logger logger = LoggerFactory.getLogger(EstimationDocumentController.class);


	/*
	 * Method Name: getEstimationById
	 * It will get the user based on the user id provided by api
	 * It will return the estimation entity
	 * API method type is Get
	 * 
	 */
	@GetMapping("/getEstimatedDocumentById")
	public ResponseEntity<EstimatedDocuments> getEstimationById(@RequestParam int estimationId) {
		EstimatedDocuments estimation = new EstimatedDocuments();
		estimation= estimationDocumentService.findById(estimationId);
		return new ResponseEntity<EstimatedDocuments> (estimation,HttpStatus.OK);
	}




	/*
	 * Method Name: getEstimation
	 * It will get all the estimations 
	 * It will return the list of estimation entity
	 * API method type is Get
	 * 
	 */
	@GetMapping("/getEstimatedDocuments")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = "Authorization", value = "Authorization token", 
	                      required = true, dataType = "string", paramType = "header") })
	public ResponseEntity<List<EstimatedDocuments>> getEstimation() {
		List<EstimatedDocuments> estimationDocument = estimationDocumentService.getEstimatedDocuments();
		return new ResponseEntity<List<EstimatedDocuments>> (estimationDocument, HttpStatus.OK);
	}

	/*
	 * Method Name: saveEstimation
	 * It will save the estimation into database 
	 * It will return the inserted estimation
	 * API method type is POST
	 * 
	 */
	@PostMapping("/saveEstimatedDocument")
	public ResponseEntity<EstimatedDocuments> saveEstimation(@RequestBody EstimatedDocuments estimatedDocuments) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		int mappingId=estimatedDocuments.getEstimationID();
		try {
		Estimation estimation=estimationService.findById(mappingId);
		estimatedDocuments.setEstimation(estimation);
		}
		catch(Exception e) {
			return new ResponseEntity<EstimatedDocuments> (HttpStatus.BAD_REQUEST);
		}
		estimatedDocuments=estimationDocumentService.saveEstimatedDocument(estimatedDocuments);
		return new ResponseEntity<EstimatedDocuments> (estimatedDocuments,HttpStatus.OK);
	}

	/*
	 * Method Name: deleteEstimationById
	 * It will delete or remove the estimation from database based on requested id
	 * It will return success status
	 * API method type is DELETE
	 * 
	 */
	@DeleteMapping("/deleteEstimatedDocumentById")
	public ResponseEntity<EstimatedDocuments> deleteEstimationById(@RequestParam int estimationId) {
		estimationDocumentService.deleteEstimatedDocumentById(estimationId);
		return new ResponseEntity<EstimatedDocuments> (HttpStatus.OK);
	}


	@DeleteMapping("/deleteEstimatedDocument")
	public ResponseEntity<EstimatedDocuments> deleteEstimationDocById(@RequestParam int id) {
		estimationDocumentService.deleteEstimatedDocumentById(id);
		return new ResponseEntity<EstimatedDocuments> (HttpStatus.OK);
	}

	/*
	 * Method Name: updateEstimation
	 * It will update or modify the estimation date into database based on request
	 * It will return success status and user entity
	 * API method type is PUT
	 * 
	 */
	@PutMapping("/updateEstimatedDocumentList")
	public ResponseEntity<List<EstimatedDocuments>> updateEstimation(@RequestBody List<EstimatedDocuments> estimatedDocuments) {
		List<EstimatedDocuments> updateEstimationList=estimationDocumentService.updateEstimatedDocuments(estimatedDocuments);
		return new ResponseEntity<List<EstimatedDocuments>> (updateEstimationList,HttpStatus.OK);
	}

}
