package com.dj.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author prasad.digajarla
 * 
 * The persistent class for the TED Pseudo Columns database table.
 */
@MappedSuperclass
public abstract class PseudoColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "createdby")
	private int createdby;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createddate")
	private Date createddate;

	@Column(name = "updatedby")
	private int updatedby;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updateddate")
	private Date updateddate;

	public PseudoColumns() {
	}

	public int getCreatedby() {
		return createdby;
	}

	public void setCreatedby(int createdby) {
		this.createdby = createdby;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public int getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(int updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Date updateddate) {
		this.updateddate = updateddate;
	}
	
	

}
