
package com.dj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "highlevelestweeks")
@NamedQuery(name = "Highlevelestweek.findAll", query = "SELECT hlw FROM Highlevelestweek hlw")
public class Highlevelestweek extends PseudoColumns implements Serializable {
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "highlevelestweekid")
	private Integer highlevelestweekid;
	@Column(name = "estid")
	private Integer estid;
	@Column(name = "highlevelesttimelineid")
	private Integer highlevelesttimelineid;
	@Column(name = "weekname")
	private String weekname;
	@Column(name = "weekvalue")
	private Float weekvalue;
	@Column(name = "isdelete")
	private byte isdelete;
	
	@JsonBackReference(value="highlevelesttimeline")
	@ManyToOne
	@JoinColumn(name = "highlevelesttimelineid",insertable = false,updatable=false)
	private Highlevelesttimeline highlevelesttimeline;
	
	public Integer getHighlevelestweekid() {
		return highlevelestweekid;
	}
	public void setHighlevelestweekid(Integer highlevelestweekid) {
		this.highlevelestweekid = highlevelestweekid;
	}
	public Integer getEstid() {
		return estid;
	}
	public void setEstid(Integer estid) {
		this.estid = estid;
	}
	public Integer getHighlevelesttimelineid() {
		return highlevelesttimelineid;
	}
	public void setHighlevelesttimelineid(Integer highlevelesttimelineid) {
		this.highlevelesttimelineid = highlevelesttimelineid;
	}
	public String getWeekname() {
		return weekname;
	}
	public void setWeekname(String weekname) {
		this.weekname = weekname;
	}
	public Float getWeekvalue() {
		return weekvalue;
	}
	public void setWeekvalue(Float weekvalue) {
		this.weekvalue = weekvalue;
	}
	public byte getIsdelete() {
		return isdelete;
	}
	public void setIsdelete(byte isdelete) {
		this.isdelete = isdelete;
	}
	public Highlevelesttimeline getHighlevelesttimeline() {
		return highlevelesttimeline;
	}
	public void setHighlevelesttimeline(Highlevelesttimeline highlevelesttimeline) {
		this.highlevelesttimeline = highlevelesttimeline;
	}
	
	
}
