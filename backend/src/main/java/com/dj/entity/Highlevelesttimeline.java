package com.dj.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "highlevelesttimelines")
@NamedQuery(name = "Highlevelesttimeline.findAll", query = "SELECT hlt FROM Highlevelesttimeline hlt")
public class Highlevelesttimeline extends PseudoColumns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	@Column(name = "estid")
	private Integer estid;
	@Column(name = "groupid")
	private Integer groupid;
	@Column(name = "resourceid")
	private Integer resourceid;
	@Column(name = "systems")
	private String systems;
	@Column(name= "week")
	private Integer week;
	@Column(name= "weekhours")
	private Integer weekhours;
	@Column(name = "actualspent")
	private Float actualspent;
	@Column(name = "contingency")
	private Float contingency;
	@Column(name = "isdelete")
	private byte isdelete;
	@Column(name="costperday")
	private Double costperday;
	
	@Transient
	private String groupName;
	
	@OneToMany(mappedBy = "highlevelesttimeline")
	private List<Highlevelestweek> highlevelestweeks;
	
	@JsonBackReference(value="rrdgroup")
	@ManyToOne
	@JoinColumn(name = "groupid",insertable = false,updatable=false)
	private RRDGroup rrdgroup;
	
	@ManyToOne
	@JoinColumn(name = "resourceid",insertable = false,updatable=false)
	private RRDTransaction rrdTransaction;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEstid() {
		return estid;
	}
	public void setEstid(Integer estid) {
		this.estid = estid;
	}
	public Integer getGroupid() {
		return groupid;
	}
	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}
	public Integer getResourceid() {
		return resourceid;
	}
	public void setResourceid(Integer resourceid) {
		this.resourceid = resourceid;
	}
	
	public String getSystems() {
		return systems;
	}
	public void setSystems(String systems) {
		this.systems = systems;
	}
	public Integer getWeek() {
		return week;
	}
	public void setWeek(Integer week) {
		this.week = week;
	}
	public Integer getWeekhours() {
		return weekhours;
	}
	public void setWeekhours(Integer weekhours) {
		this.weekhours = weekhours;
	}
	public Float getActualspent() {
		return actualspent;
	}
	public void setActualspent(Float actualspent) {
		this.actualspent = actualspent;
	}
	public Float getContingency() {
		return contingency;
	}
	public void setContingency(Float contingency) {
		this.contingency = contingency;
	}
	public byte getIsdelete() {
		return isdelete;
	}
	public void setIsdelete(byte isdelete) {
		this.isdelete = isdelete;
	}
	public List<Highlevelestweek> getHighlevelestweeks() {
		return highlevelestweeks;
	}
	public void setHighlevelestweeks(List<Highlevelestweek> highlevelestweeks) {
		this.highlevelestweeks = highlevelestweeks;
	}
	public Double getCostperday() {
		return costperday;
	}
	public void setCostperday(Double costperday) {
		this.costperday = costperday;
	}
	public RRDGroup getRrdgroup() {
		return rrdgroup;
	}
	public void setRrdgroup(RRDGroup rrdgroup) {
		this.rrdgroup = rrdgroup;
	}
	
	public RRDGroup getGroup() {
		if(this.rrdgroup != null) {
			return this.rrdgroup;
		}
		return null;
	}
	
	public String getRoleNameId() {
		if(this.rrdgroup != null) {
			return this.rrdgroup.getRole()+"_"+this.rrdgroup.getRrdid();
		}
		return "";
	}
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getResourceNameId() {
		if(this.rrdTransaction != null && this.rrdTransaction.getResourcename() != null && !this.rrdTransaction.getResourcename().isEmpty() && this.rrdTransaction.getRrdtransid() != null && this.rrdTransaction.getRrdtransid() != 0) {
			return this.rrdTransaction.getResourcename()+"_"+this.rrdTransaction.getRrdtransid();
		}
		return "";
	}
}
