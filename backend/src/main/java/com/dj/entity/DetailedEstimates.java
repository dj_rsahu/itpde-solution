package com.dj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "detailedest")
@NamedQuery(name = "DetailedEstimates.findAll", query = "SELECT t FROM DetailedEstimates t")
public class DetailedEstimates extends PseudoColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;

	@Column(name = "estid")
	private Integer estid;
	
	@Column(name = "detestmoduletypeid")
	private Integer detestmoduletypeid;

	@Column(name = "required")
	private String required;

	@Column(name = "requiredcategory")
	private String requiredcategory;

	@Column(name = "releaseno")
	private String releaseno;

	@Column(name = "mainfunction")
	private String mainfunction;

	@Column(name = "workstream")
	private String workstream;

	@Column(name = "subfunction")
	private String subfunction;

	@Column(name = "descr")
	private String descr;

	@Column(name = "sourcename")
	private String sourcename;

	@Column(name = "target")
	private String target;

	@Column(name = "method")
	private String method;

	@Column(name = "difficulty")
	private String difficulty;

	@Column(name = "neworexist")
	private String neworexist;

	@Column(name = "analyse")
	private Integer analyse;

	@Column(name = "design")
	private Integer design;

	@Column(name = "buildunittest")
	private Integer buildunittest;

	@Column(name = "effort")
	private String effort;

	@Column(name = "implementation")
	private Integer implementation;

	@Column(name = "effortimpl")
	private String effortimpl;
	
	@Transient
	private String moduletype;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRequired() {
		return required;
	}

	public void setRequired(String required) {
		this.required = required;
	}

	public String getRequiredcategory() {
		return requiredcategory;
	}

	public void setRequiredcategory(String requiredcategory) {
		this.requiredcategory = requiredcategory;
	}

	public String getMainfunction() {
		return mainfunction;
	}

	public void setMainfunction(String mainfunction) {
		this.mainfunction = mainfunction;
	}

	public String getWorkstream() {
		return workstream;
	}

	public void setWorkstream(String workstream) {
		this.workstream = workstream;
	}

	public String getSubfunction() {
		return subfunction;
	}

	public void setSubfunction(String subfunction) {
		this.subfunction = subfunction;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public String getNeworexist() {
		return neworexist;
	}

	public void setNeworexist(String neworexist) {
		this.neworexist = neworexist;
	}

	public Integer getAnalyse() {
		return analyse;
	}

	public void setAnalyse(Integer analyse) {
		this.analyse = analyse;
	}

	public Integer getDesign() {
		return design;
	}

	public void setDesign(Integer design) {
		this.design = design;
	}

	public Integer getBuildunittest() {
		return buildunittest;
	}

	public void setBuildunittest(Integer buildunittest) {
		this.buildunittest = buildunittest;
	}

	public String getEffort() {
		return effort;
	}

	public void setEffort(String effort) {
		this.effort = effort;
	}

	public Integer getImplementation() {
		return implementation;
	}

	public void setImplementation(Integer implementation) {
		this.implementation = implementation;
	}

	public String getEffortimpl() {
		return effortimpl;
	}

	public void setEffortimpl(String effortimpl) {
		this.effortimpl = effortimpl;
	}

	public Integer getEstid() {
		return estid;
	}

	public void setEstid(Integer estid) {
		this.estid = estid;
	}

	public String getReleaseno() {
		return releaseno;
	}

	public String getSourcename() {
		return sourcename;
	}

	public void setReleaseno(String releaseno) {
		this.releaseno = releaseno;
	}

	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}

	public Integer getDetestmoduletypeid() {
		return detestmoduletypeid;
	}

	public void setDetestmoduletypeid(Integer detestmoduletypeid) {
		this.detestmoduletypeid = detestmoduletypeid;
	}

	public String getModuletype() {
		return moduletype;
	}

	public void setModuletype(String moduletype) {
		this.moduletype = moduletype;
	}

}
