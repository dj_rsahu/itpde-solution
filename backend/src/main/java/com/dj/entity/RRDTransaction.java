package com.dj.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.dj.utils.DJConstants;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "rrdtrans")
@NamedQuery(name = "RRDTransaction.findAll", query = "SELECT t FROM RRDTransaction t")
public class RRDTransaction extends PseudoColumns implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "rrdtransid")
	private Integer rrdtransid;
	@Column(name = "rrdgroupid")
	private Integer rrdgroupid;
	@Column(name = "resourceid")
	private Integer resourceid;
	@Column(name = "resourcename")
	private String resourcename;
	@Column(name = "resourcetype")
	private String resourcetype;
	@Column(name = "costperday")
	private Float costperday;
	@Column(name = "capacity")
	private Integer capacity;
	@Column(name = "isdelete")
	private byte isdelete;

	@JsonBackReference(value = "user")
	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL, CascadeType.REFRESH })
	@JoinColumn(name = "resourceid", insertable = false, updatable = false)
	private User user;

	@Transient
	private RRDGroup rrdGroup;
	@Transient
	private String groupOthers;
	@Transient
	private String roleOthers;
	@Transient
	private String resourcetypeOthers;
	@Transient
	private String resourcenameOthers;
	@Transient
	private String resourceTypeRole;
	@Transient
	private String rscRole;
	@Transient
	private String groupName;
	
	
	/*
	 * @Transient private String roleNameId;
	 * 
	 * @Transient private String resourceNameId;
	 */
	
	/*
	 * @Transient private User user;
	 */

	public Integer getRrdtransid() {
		return rrdtransid;
	}

	public void setRrdtransid(Integer rrdtransid) {
		this.rrdtransid = rrdtransid;
	}

	public Integer getRrdgroupid() {
		return rrdgroupid;
	}

	public void setRrdgroupid(Integer rrdgroupid) {
		this.rrdgroupid = rrdgroupid;
	}

	public String getResourcetype() {
		return resourcetype;
	}

	public void setResourcetype(String resourcetype) {
		this.resourcetype = resourcetype;
	}

	public Float getCostperday() {
		return costperday;
	}

	public void setCostperday(Float costperday) {
		this.costperday = costperday;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public byte getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(byte isdelete) {
		this.isdelete = isdelete;
	}

	public String getGroupOthers() {
		return groupOthers;
	}

	public void setGroupOthers(String groupOthers) {
		this.groupOthers = groupOthers;
	}

	public String getRoleOthers() {
		return roleOthers;
	}

	public void setRoleOthers(String roleOthers) {
		this.roleOthers = roleOthers;
	}

	public RRDGroup getRrdGroup() {
		return rrdGroup;
	}

	public void setRrdGroup(RRDGroup rrdGroup) {
		this.rrdGroup = rrdGroup;
	}

	public String getResourcetypeOthers() {
		return resourcetypeOthers;
	}

	public void setResourcetypeOthers(String resourcetypeOthers) {
		this.resourcetypeOthers = resourcetypeOthers;
	}

	public String getResourcenameOthers() {
		return resourcenameOthers;
	}

	public void setResourcenameOthers(String resourcenameOthers) {
		this.resourcenameOthers = resourcenameOthers;
	}

	public String getResourceTypeRole() {
		return resourceTypeRole;
	}

	public void setResourceTypeRole(String resourceTypeRole) {
		this.resourceTypeRole = resourceTypeRole;
	}

	public String getRscRole() {
		return rscRole;
	}

	public void setRscRole(String rscRole) {
		this.rscRole = rscRole;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getResourceid() {
		if(this.resourceid == null) {
			this.resourceid = 0;
		}
		return resourceid;
	}

	public void setResourceid(Integer resourceid) {
		this.resourceid = resourceid;
		
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUserDetails() {
		return user;
	}
	
	public String getRsrcName() {
		String rsrcName = "";
		if(this.resourceid == null || this.resourceid == 0) {
			rsrcName = DJConstants.RESOURCE_TYPE_UNKNOWN;
		} else {
			if(user != null) {
				rsrcName = user.getFirstName()+ " " + user.getLastName();
			}
		}
		return rsrcName;
	}

	public String getResourcename() {
		return resourcename;
	}

	public void setResourcename(String resourcename) {
		this.resourcename = resourcename;
	}

}
