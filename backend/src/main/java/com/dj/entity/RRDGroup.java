package com.dj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "rrdgroups")
@NamedQuery(name = "RRDGroup.findAll", query = "SELECT r FROM RRDGroup r")
public class RRDGroup extends PseudoColumns implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "rrdid")
	private Integer rrdid;
	@Column(name = "groupname")
	private String groupname;
	@Column(name = "role")
	private String role;
	@Column(name = "isdelete")
	private byte isdelete;

	public Integer getRrdid() {
		return rrdid;
	}

	public void setRrdid(Integer rrdid) {
		this.rrdid = rrdid;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public byte getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(byte isdelete) {
		this.isdelete = isdelete;
	}

}
