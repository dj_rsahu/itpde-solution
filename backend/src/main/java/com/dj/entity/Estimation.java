package com.dj.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "estimation")
@NamedQuery(name = "Estimation.findAll", query = "SELECT t FROM Estimation t")
public class Estimation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "est_project_name")
	private String est_project_name;

	@Column(name = "domainid")
	private Integer domainid;

	@Column(name = "estimation_cost")
	private String estimation_cost;

	@Column(name = "project_lead")
	private Integer project_lead;

	@Column(name = "project_phase")
	private String project_phase;

	@Column(name = "estmation_status")
	private String estmation_status;

	@Column(name = "duration_in_week")
	private Integer durationInWeek;
	@Column(name = "iseditable")
	private Byte iseditable;
	@Column(name = "iseditabledate")
	private Date iseditabledate;
	@Column(name = "iseditableby")
	private Integer iseditableby;

	@CreatedBy
	@Column(name = "createdby", updatable = false)
	private int createdby;
	@Column(name = "createddate", updatable = false)
	@CreationTimestamp
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss", iso = ISO.DATE_TIME)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createdDate;

	@Column(name = "updateddate")
	@UpdateTimestamp
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss", iso = ISO.DATE_TIME)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updatedDate;

	@LastModifiedBy
	@Column(name = "updatedby")
	private int updatedby;
	
	@Column(name = "isdelete")
	private byte isdelete;
	
	@Column(name="issubmit")
	private String isSubmit;
	/*
	 * @JsonBackReference(value = "updatedByUser")
	 * 
	 * @ManyToOne
	 * 
	 * @JoinColumn(name = "updatedby", insertable = false, updatable = false)
	 * private User updatedByUser;
	 */

	

	@JsonManagedReference(value = "estimation")
	@OneToMany(mappedBy = "estimation", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<EstimatedDocuments> estimationDocuments;

	@JsonBackReference(value = "domain")
	@ManyToOne
	@JoinColumn(name = "domainid", insertable = false, updatable = false)
	private Domain domain;

	@Transient
	private String nameOthers;

	
	@Column(name = "projectphaseid")
	private Integer projectphaseid;
	
	@Transient
	private String projectStatusOthers;
	
	@Transient
	private User projectLeadUser;
	
	@Transient
	private String projectLeadName;
	
	@Transient
	private String updatedByName;
	
	@Transient
	private String createdByName;
	
	@Transient
	private String projectPhaseName;
	
	@Transient
	private String domainName;
	
	@Transient
	private Projectstatus projectStatusObj;

	public List<EstimatedDocuments> getEstimationDocuments() {
		return estimationDocuments;
	}

	public void setEstimationDocuments(List<EstimatedDocuments> estimationDocuments) {
		this.estimationDocuments = estimationDocuments;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstimation_cost() {
		return estimation_cost;
	}

	public void setEstimation_cost(String estimation_cost) {
		this.estimation_cost = estimation_cost;
	}

	public Integer getProject_lead() {
		return project_lead;
	}

	public void setProject_lead(Integer project_lead) {
		this.project_lead = project_lead;
	}

	public User getProjectLeadUser() {
		return projectLeadUser;
	}

	public void setProjectLeadUser(User projectLeadUser) {
		this.projectLeadUser = projectLeadUser;
	}

	public String getProject_phase() {
		return project_phase;
	}

	public void setProject_phase(String project_phase) {
		this.project_phase = project_phase;
	}

	public String getEstmation_status() {
		return estmation_status;
	}

	public void setEstmation_status(String estmation_status) {
		this.estmation_status = estmation_status;
	}

	public int getCreatedby() {
		return createdby;
	}

	public void setCreatedby(int createdby) {
		this.createdby = createdby;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(int updatedby) {
		this.updatedby = updatedby;
	}

	public String getEst_project_name() {
		return est_project_name;
	}

	public void setEst_project_name(String est_project_name) {
		this.est_project_name = est_project_name;
	}

	public Integer getDurationInWeek() {
		return durationInWeek;
	}

	public void setDurationInWeek(Integer durationInWeek) {
		this.durationInWeek = durationInWeek;
	}

	public EstimatedDocuments addEstimatedDocuments(EstimatedDocuments estimationDocument) {
		getEstimationDocuments().add(estimationDocument);
		estimationDocument.setEstimation(this);
		return estimationDocument;
	}

	public EstimatedDocuments removeEstimatedDocument(EstimatedDocuments estimationDocument) {
		getEstimationDocuments().remove(estimationDocument);
		estimationDocument.setEstimation(null);
		return estimationDocument;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public void setDomainid(Integer domainid) {
		this.domainid = domainid;
	}
	public String getNameOthers() {
		return nameOthers;
	}
	public void setNameOthers(String nameOthers) {
		this.nameOthers = nameOthers;
	}
	public String getProjectStatusOthers() {
		return projectStatusOthers;
	}
	public void setProjectStatusOthers(String projectStatusOthers) {
		this.projectStatusOthers = projectStatusOthers;
	}
	public Integer getProjectphaseid() {
		return projectphaseid;
	}
	public void setProjectphaseid(Integer projectphaseid) {
		this.projectphaseid = projectphaseid;
	}
	
	public String getDomainName() {
		if (this.domain != null) {
			return this.domain.getName();
		}else {
			return domainName;
		}
			
	}
	
	/*
	 * public String getUpdatedByName() { if (this.updatedByUser != null) { return
	 * this.updatedByUser.getFirstName() + " " + this.updatedByUser.getLastName(); }
	 * return ""; }
	 */

	public String getProjectLeadName() {
		return projectLeadName;
	}

	public void setProjectLeadName(String projectLeadName) {
		this.projectLeadName = projectLeadName;
	}

	public String getUpdatedByName() {
		return updatedByName;
	}

	public void setUpdatedByName(String updatedByName) {
		this.updatedByName = updatedByName;
	}

	public String getProjectPhaseName() {
		return projectPhaseName;
	}
	public void setProjectPhaseName(String projectPhaseName) {
		this.projectPhaseName = projectPhaseName;
	}

	public Projectstatus getProjectStatusObj() {
		return projectStatusObj;
	}

	public void setProjectStatusObj(Projectstatus projectStatusObj) {
		this.projectStatusObj = projectStatusObj;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estimation other = (Estimation) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getCreatedByName() {
		return createdByName;
	}

	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	public byte getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(byte isdelete) {
		this.isdelete = isdelete;
	}

	public Byte getIseditable() {
		return iseditable;
	}

	public void setIseditable(Byte iseditable) {
		this.iseditable = iseditable;
	}

	public Date getIseditabledate() {
		return iseditabledate;
	}

	public void setIseditabledate(Date iseditabledate) {
		this.iseditabledate = iseditabledate;
	}

	public Integer getIseditableby() {
		return iseditableby;
	}

	public void setIseditableby(Integer iseditableby) {
		this.iseditableby = iseditableby;
	}
	public String getIsSubmit() {
		return isSubmit;
	}

	public void setIsSubmit(String isSubmit) {
		this.isSubmit = isSubmit;
	}

	
}
