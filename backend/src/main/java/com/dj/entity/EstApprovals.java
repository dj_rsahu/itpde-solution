package com.dj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "estapprovals")
@NamedQuery(name = "EstApprovals.findAll", query = "SELECT a FROM EstApprovals a")
public class EstApprovals extends PseudoColumns implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "estapprovalid")
	private Integer estapprovalid;
	
	@Column(name = "estid")
	private Integer estId;
	
	@Column(name = "approvaltype")
	private String approvaltype;
	
	@Column(name = "approverid")
	private Integer approverid;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "isdelete")
	private byte isdelete;
	
	@Transient
	private String approverName;
	
	@Transient
	private String estSummaryContent;

	public Integer getEstId() {
		return estId;
	}

	public void setEstId(Integer estId) {
		this.estId = estId;
	}

	public Integer getEstapprovalid() {
		return estapprovalid;
	}

	public void setEstapprovalid(Integer estapprovalid) {
		this.estapprovalid = estapprovalid;
	}

	public String getApprovaltype() {
		return approvaltype;
	}

	public void setApprovaltype(String approvaltype) {
		this.approvaltype = approvaltype;
	}

	public Integer getApproverid() {
		return approverid;
	}

	public void setApproverid(Integer approverid) {
		this.approverid = approverid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public byte getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(byte isdelete) {
		this.isdelete = isdelete;
	}

	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public String getEstSummaryContent() {
		return estSummaryContent;
	}

	public void setEstSummaryContent(String estSummaryContent) {
		this.estSummaryContent = estSummaryContent;
	}

}
