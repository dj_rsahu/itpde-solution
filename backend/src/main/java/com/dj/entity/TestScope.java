package com.dj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "testscope")
@NamedQuery(name = "TestScope.findAll", query = "SELECT t FROM TestScope t")
public class TestScope extends PseudoColumns implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name="estid")
	private Integer estid;
	@Column(name="tsname")
	private String tsname;
	@Column(name="tsexeplan")
	private String tsexeplan;
	
	@Column(name="tceffort")
	private String tceffort;
	
	@Column(name="tdsetup")
	private String tdsetup;
	
	@Column(name="testexe")
	private String testexe;
	
	@Column(name="total")
	private String total;
	
	@Column(name="tcnewreuse")
	private String tcnewreuse;
	
	@Column(name="nooftc")
	private String nooftc;
	
	@Column(name="exeprod")
	private String exeprod;
	
	@Column(name="tcrevised")
	private String tcrevised;
	
	@Column(name="comments")
	private String comments;
	
	@Column(name="interfacerep")
	private String interfacerep;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstid() {
		return estid;
	}

	public void setEstid(Integer estid) {
		this.estid = estid;
	}

	public String getTsname() {
		return tsname;
	}

	public void setTsname(String tsname) {
		this.tsname = tsname;
	}

	public String getTsexeplan() {
		return tsexeplan;
	}

	public void setTsexeplan(String tsexeplan) {
		this.tsexeplan = tsexeplan;
	}

	public String getTceffort() {
		return tceffort;
	}

	public void setTceffort(String tceffort) {
		this.tceffort = tceffort;
	}

	public String getTdsetup() {
		return tdsetup;
	}

	public void setTdsetup(String tdsetup) {
		this.tdsetup = tdsetup;
	}

	public String getTestexe() {
		return testexe;
	}

	public void setTestexe(String testexe) {
		this.testexe = testexe;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getTcnewreuse() {
		return tcnewreuse;
	}

	public void setTcnewreuse(String tcnewreuse) {
		this.tcnewreuse = tcnewreuse;
	}

	public String getNooftc() {
		return nooftc;
	}

	public void setNooftc(String nooftc) {
		this.nooftc = nooftc;
	}

	public String getExeprod() {
		return exeprod;
	}

	public void setExeprod(String exeprod) {
		this.exeprod = exeprod;
	}

	public String getTcrevised() {
		return tcrevised;
	}

	public void setTcrevised(String tcrevised) {
		this.tcrevised = tcrevised;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getInterfacerep() {
		return interfacerep;
	}

	public void setInterfacerep(String interfacerep) {
		this.interfacerep = interfacerep;
	}

}
