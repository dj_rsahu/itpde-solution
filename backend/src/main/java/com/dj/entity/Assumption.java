package com.dj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "assumption")
@NamedQuery(name = "Assumption.findAll", query = "SELECT a FROM Assumption a")
public class Assumption extends PseudoColumns implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	@Column(name = "estid")
	private Integer estid;
	@Column(name = "assumptionref")
	private String assumptionref;
	@Column(name = "area")
	private String area;
	@Lob
	@Column(name = "assumption")
	private String assumption;
	@Lob
	@Column(name = "impact")
	private String impact;
	@Column(name = "isdelete")
	private byte isdelete;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEstid() {
		return estid;
	}
	public void setEstid(Integer estid) {
		this.estid = estid;
	}
	public String getAssumptionref() {
		return assumptionref;
	}
	public void setAssumptionref(String assumptionref) {
		this.assumptionref = assumptionref;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getAssumption() {
		return assumption;
	}
	public void setAssumption(String assumption) {
		this.assumption = assumption;
	}
	public String getImpact() {
		return impact;
	}
	public void setImpact(String impact) {
		this.impact = impact;
	}
	public byte getIsdelete() {
		return isdelete;
	}
	public void setIsdelete(byte isdelete) {
		this.isdelete = isdelete;
	}
	
}
