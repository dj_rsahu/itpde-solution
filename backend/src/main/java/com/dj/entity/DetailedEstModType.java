package com.dj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "detestmoduletype")
@NamedQuery(name = "DetailedEstModType.findAll", query = "SELECT t FROM DetailedEstModType t")
public class DetailedEstModType extends PseudoColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "moduletype")
	private String moduletype;

	public Integer getId() {
		return id;
	}

	public String getModuletype() {
		return moduletype;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setModuletype(String moduletype) {
		this.moduletype = moduletype;
	}
}
