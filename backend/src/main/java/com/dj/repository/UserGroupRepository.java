package com.dj.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dj.entity.User;
import com.dj.entity.UserGroup;

public interface UserGroupRepository extends JpaRepository<UserGroup, Long>{

	UserGroup findById(int userGroupId);

	void deleteById(int userGroupId);
	
	@Query("SELECT e FROM UserGroup e where e.groupName = :name")
	UserGroup findBYName(@Param("name") String name);

	@Query("select g from  UserGroup g inner join fetch g.user u where u.id=:id")
	UserGroup getUserGroupByUserId(@Param("id") int id);

}
