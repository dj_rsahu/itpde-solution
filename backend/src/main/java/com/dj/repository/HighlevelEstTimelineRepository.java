package com.dj.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dj.entity.Highlevelesttimeline;

@Repository
public interface HighlevelEstTimelineRepository extends JpaRepository<Highlevelesttimeline, Integer>, HighlevelEstTimelineRepositoryCustom{

	/*
	 * @Query("select h.groupid as goupid ,SUM(costperday) as estimates, h.actualspent as actualspent, h.contingency as contingency from Highlevelesttimeline h where h.estid=:estid and h.isdelete=:isdelete group by h.groupid"
	 * ) public List<Highlevelesttimeline>
	 * fetchHighlevelEstTimelines(@Param("estid") int estid, @Param("isdelete") byte
	 * isdelete);
	 */
	@Query("select h from Highlevelesttimeline h where h.estid=:estId")
	public List<Highlevelesttimeline> getHighlevelesttimelineByEstId(@Param("estId") int estId);

	@Query("select h from Highlevelesttimeline h where h.id=:id")
	public Highlevelesttimeline getHighlevelesttimelineById(@Param("id") Integer id);

	@Modifying
	@Query(value="DELETE from Highlevelesttimeline ht where ht.id IN(:highlevelesttimelineIds)")
	public void deleteHighlevelesttimelineByIds(@Param("highlevelesttimelineIds") List<Integer> highlevelesttimelineIds);

	public void save(Optional<Highlevelesttimeline> hlt);

	@Query(value="select ht from Highlevelesttimeline ht where ht.id NOT IN(:hltimelineIds) AND ht.estid = :estId")
	public List<Highlevelesttimeline> getHighLevelTimelinesNotInIds(@Param("hltimelineIds") List<Integer> hltimelineIds, @Param("estId") int estId);

	@Query(value="select ht from Highlevelesttimeline ht where ht.groupid IN(:rrdGroupIds) AND ht.estid = :estId")
	public List<Highlevelesttimeline> getHighlevelesttimelinesByGroupIds(@Param("rrdGroupIds") List<Integer> rrdGroupIds, @Param("estId") Integer estId);
}
