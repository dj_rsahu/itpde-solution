package com.dj.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.dj.entity.User;
import com.dj.utils.StringFormatUtils;

public class UserRepositoryImpl implements UserRepositoryCustom {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUsers(String search) {
		String query = "SELECT u FROM User u where u.isdelete = 0";
		if(!StringFormatUtils.isEmpty(search)) {
			query += " AND ((u.firstName LIKE '"+search+"%') OR (u.lastName LIKE '"+search+"%'))";
		}
		List<User> u = entityManager.createQuery(query).getResultList();
		return u;
	}
}
