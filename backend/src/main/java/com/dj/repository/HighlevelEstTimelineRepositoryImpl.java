package com.dj.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.dj.entity.Highlevelesttimeline;
import com.dj.entity.RRDGroup;

public class HighlevelEstTimelineRepositoryImpl implements HighlevelEstTimelineRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private RRDGroupRepository rrdGroupRepository;

	@SuppressWarnings("unchecked")
	@Override
	public List<Highlevelesttimeline> fetchHighlevelEstTimelines(int estid, byte isdelete,int actualDaysInAWeek) {
		List<Highlevelesttimeline> hltList = new ArrayList<Highlevelesttimeline>();
		String query = "SELECT highlevelesttimelines.groupid,rrdgroups.groupname,sum(((highlevelestweeks.weekvalue)/100)*highlevelesttimelines.costperday)*"+actualDaysInAWeek+" as totestimation,highlevelesttimelines.actualspent,\r\n" + 
				"highlevelesttimelines.contingency,highlevelesttimelines.id FROM itpde.highlevelesttimelines inner join estimation on (highlevelesttimelines.estid = estimation.id)\r\n" + 
				"inner join rrdgroups on (highlevelesttimelines.groupid = rrdgroups.rrdid) inner join highlevelestweeks on (highlevelesttimelines.id = highlevelestweeks.highlevelesttimelineid) where highlevelesttimelines.estid="+estid+" and highlevelesttimelines.isdelete = "+isdelete+" group by rrdgroups.groupname;";
		List<Object[]> results = entityManager.createNativeQuery(query).getResultList();
		results.stream().forEach((record) -> {
			Highlevelesttimeline highlevelesttimeline = new Highlevelesttimeline();
			if(results!=null && !results.isEmpty()) {
				highlevelesttimeline.setGroupid((Integer)record[0]);
				highlevelesttimeline.setGroupName((String)record[1]);
				highlevelesttimeline.setCostperday((Double)record[2]);
				highlevelesttimeline.setActualspent((Float)(record[3]));
				highlevelesttimeline.setContingency((Float)(record[4]));
				highlevelesttimeline.setId((Integer)(record[5]));
				hltList.add(highlevelesttimeline);
			}
		});
		/*
		 * String query =
		 * "select h.groupid as groupid ,SUM(costperday) as estimates, h.actualspent as actualspent, h.contingency as contingency, h.id as id from Highlevelesttimeline h where h.estid="
		 * +estid+" and h.isdelete="+isdelete+" group by h.groupid"; List<Object[]>
		 * results = entityManager.createQuery(query).getResultList();
		 * results.stream().forEach((record) -> { Highlevelesttimeline
		 * highlevelesttimeline = new Highlevelesttimeline(); if(results!=null &&
		 * !results.isEmpty()) { highlevelesttimeline.setGroupid((Integer)record[0]);
		 * if(record[0]!=null) { RRDGroup rrdGroupById =
		 * rrdGroupRepository.getRRDGroupById((Integer)record[0]);
		 * if(rrdGroupById!=null) {
		 * highlevelesttimeline.setGroupName(rrdGroupById.getGroupname()); } }
		 * highlevelesttimeline.setCostperday((Double)record[1]);
		 * highlevelesttimeline.setActualspent((Float)(record[2]));
		 * highlevelesttimeline.setContingency((Float)(record[3]));
		 * highlevelesttimeline.setId((Integer)(record[4]));
		 * hltList.add(highlevelesttimeline); } });
		 */
		
		return hltList;
	}

}
