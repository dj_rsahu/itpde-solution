package com.dj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.dj.entity.RRDGroup;

public interface RRDGroupRepository extends CrudRepository<RRDGroup, Integer>, RRDGroupRepositoryCustom {

	@Query("SELECT rg FROM RRDGroup rg WHERE rg.groupname = :groupName AND rg.isdelete = 0")
	RRDGroup getRRDGroupByName(@Param("groupName") String groupName);

	@Query("SELECT rg FROM RRDGroup rg WHERE rg.groupname = :groupName AND rg.role = :role AND rg.isdelete = 0")
	RRDGroup getRRDGroupByNameAndRole(@Param("groupName") String groupName, @Param("role") String role);

	@Query("SELECT rg FROM RRDGroup rg WHERE rg.rrdid = :rrdgroupid AND rg.isdelete = 0")
	RRDGroup getRRDGroupById(@Param("rrdgroupid") Integer rrdgroupid);

	@Query("SELECT rg FROM RRDGroup rg")
	List<RRDGroup> getRRDGroups();

	@Query("SELECT rg FROM RRDGroup rg WHERE rg.groupname = :groupname AND rg.isdelete = 0")
	List<RRDGroup> getRRDGroupsByGroupName(@Param("groupname") String groupname);


}
