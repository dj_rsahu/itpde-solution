package com.dj.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.dj.entity.RRDTransaction;
import com.dj.utils.StringFormatUtils;

public class RRDTransactionRepositoryImpl implements RRDTransactionRepositoryCustom {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<RRDTransaction> getAllRRDTransactions(String search) {
		String query = "SELECT rt FROM RRDTransaction rt WHERE rt.isdelete = 0 :searchCondition";
		String searchCondition = "";
		if (!StringFormatUtils.isEmpty(search)) {
			searchCondition = " AND rt.resourcename LIKE '%" + search + "%' ";
		}
		query = query.replace(":searchCondition", searchCondition);
		return entityManager.createQuery(query).getResultList();
	}

	@Override
	public RRDTransaction getRRDTransactionByGroupIdAndResourceIdAndResourceType(Integer rrdgroupid, Integer resourceid,
			String resourcetype, Integer rrdTransId) {
		
		String query = "SELECT rt FROM RRDTransaction rt WHERE rt.rrdgroupid = "+rrdgroupid+" :resourceCondition AND rt.resourcetype = '"+resourcetype+"' AND rt.isdelete = 0 :condition";
		String condition = "";
		String resourceCondition = "";
		if(rrdTransId != null) {
			condition = " AND rt.rrdtransid != "+rrdTransId;
		}
		if(resourceid == 0 || resourceid == null) {
			resourceCondition = " AND rt.resourceid IS NULL";
		} else {
			resourceCondition = " AND rt.resourceid = "+resourceid;
		}
		query = query.replace(":condition", condition);
		query = query.replace(":resourceCondition", resourceCondition);
		@SuppressWarnings("unchecked")
		List<RRDTransaction> t= entityManager.createQuery(query).getResultList();
		return (t != null && t.size() > 0) ? t.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RRDTransaction> getRRDTransactionsByGroupId(String search, int groupId) {
		String query = "SELECT rt FROM RRDTransaction rt WHERE rt.isdelete = 0 AND rt.rrdgroupid = "+groupId+" :searchCondition";
		String searchCondition = "";
		if (!StringFormatUtils.isEmpty(search)) {
			searchCondition = " AND rt.resourcename LIKE '%" + search + "%' ";
		}
		query = query.replace(":searchCondition", searchCondition);
		return entityManager.createQuery(query).getResultList();
	}

}
/*
 * List<Object[]> results = entityManager.
 * createNativeQuery("SELECT * FROM rrdtrans WHERE rrdgroupid = 4 AND resourceid = 1 AND resourcetype = 'INTERNAL' AND isdelete = 0"
 * ).getResultList(); return new RRDTransaction();
 */