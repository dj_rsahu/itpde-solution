package com.dj.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.dj.entity.Assumption;
import com.dj.utils.StringFormatUtils;

public class AssumptionRepositoryImpl implements AssumptionRepositoryCustom {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Assumption> getAssumptions(String search) {
		String query = "SELECT a FROM Assumption a WHERE a.isdelete = 0 :searchCondition";
		String searchCondition = "";
		if (!StringFormatUtils.isEmpty(search)) {
			searchCondition = " AND a.area LIKE '%" + search + "%' ";
		}
		query = query.replace(":searchCondition", searchCondition);
		return entityManager.createQuery(query).getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Assumption> getAssumptionsByEstId(int estId, String search) {
		String query = "SELECT a FROM Assumption a WHERE a.isdelete = 0 AND a.estid = "+estId+" :searchCondition";
		String searchCondition = "";
		if (!StringFormatUtils.isEmpty(search)) {
			searchCondition = " AND a.area LIKE '%" + search + "%' ";
		}
		query = query.replace(":searchCondition", searchCondition);
		return entityManager.createQuery(query).getResultList();
	}

}
