package com.dj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.dj.entity.Assumption;

public interface AssumptionRepository extends CrudRepository<Assumption, Integer>, AssumptionRepositoryCustom {
	@Query("SELECT a FROM Assumption a WHERE a.id = :id AND a.isdelete = 0")
	Assumption getAssumptionById(@Param("id") Integer id);

	@Query(value="SELECT * FROM assumption WHERE estid = :estid ORDER BY id DESC LIMIT 1",nativeQuery=true)
	Assumption getLastAssumptionByEstId(@Param("estid") Integer estid);

	@Query(value="SELECT count(*) FROM assumption a WHERE a.estid = :estimationId",nativeQuery=true)
	Integer getEstimationsCountByEstimationId(@Param("estimationId") Integer estimationId);

	@Modifying
	@Query("DELETE FROM Assumption a WHERE a.estid = :estimationId")
	void deleteAssumptionsByEstId(@Param("estimationId") int estimationId);

}
