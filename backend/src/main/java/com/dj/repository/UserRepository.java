package com.dj.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dj.entity.Estimation;
//import org.springframework.web.multipart.MultipartFile;
import com.dj.entity.User;
public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom{
	User findByEmail(String email);
	User findById(int userId);
	void deleteUserById(int userId);
	void deleteUserByEmail(String email);
	List<User> findAll();
	
	@Query("SELECT u FROM User u where u.email = :email and u.isdelete=:isdelete")
	User findByEmailAnddDelFlag(@Param("email") String email, @Param("isdelete") byte isdelete);
	
	@Query("SELECT u FROM User u where u.isdelete=:isdelete")
	List<User> fetchAllNonDeletedUsers(@Param("isdelete") byte isdelete);
//	public void saveImage(MultipartFile imageFile);

	@Query("SELECT u FROM User u where u.role=:role and isdelete=0")
	List<User> fetchUsersByRole(@Param("role") String role);
	
}
