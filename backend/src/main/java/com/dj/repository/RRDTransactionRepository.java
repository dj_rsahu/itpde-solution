package com.dj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.dj.entity.RRDTransaction;

public interface RRDTransactionRepository extends CrudRepository<RRDTransaction, Integer>, RRDTransactionRepositoryCustom {
	
	@Query("SELECT rt FROM RRDTransaction rt WHERE rt.rrdtransid = :rrdTransactionId")
	RRDTransaction getRRDTransactionById(@Param("rrdTransactionId") int rrdTransactionId);


	/*
	 * @Query(
	 * value="SELECT * FROM rrdtrans WHERE resourcetype LIKE :resourceType AND rrdgroupid = :rrdgroupId AND resourceid = :resourceId AND isdelete = 0"
	 * , nativeQuery=true) RRDTransaction
	 * getRRDTransactionByGroupIdAndResourceIdAndResourceType(@Param("rrdgroupId")
	 * Integer rrdgroupId, @Param("resourceId") Integer resourceId,
	 * 
	 * @Param("resourceType") String resourceType);
	 */

}
