package com.dj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dj.entity.EstimatedDocuments;


public interface EstimationDocumentRepository extends JpaRepository<EstimatedDocuments, Long>  {

	void deleteById(int id);

	@Query("SELECT e FROM EstimatedDocuments e where e.id = :id")
	EstimatedDocuments findById(@Param("id") int id);

	@Query("SELECT e FROM EstimatedDocuments e where e.estimation.id = :estimationId")
	List<EstimatedDocuments> findEstDocsListByEstId(@Param("estimationId") Integer estimationId);

}
