package com.dj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dj.entity.Highlevelestweek;

@Repository
public interface HighlevelestweekRepository extends JpaRepository<Highlevelestweek, Integer> {

	@Modifying
	@Query(value="DELETE from Highlevelestweek hw where hw.highlevelesttimelineid IN(:highlevelesttimelineIds)")
	public void deleteHighlevelestweeksByHighleveleesttimelineIds(@Param("highlevelesttimelineIds") List<Integer> highlevelesttimelineIds);

}
