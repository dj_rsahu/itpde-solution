package com.dj.repository;

import java.util.List;

import com.dj.entity.RRDTransaction;

public interface RRDTransactionRepositoryCustom {
	List<RRDTransaction> getAllRRDTransactions(String search);
	RRDTransaction getRRDTransactionByGroupIdAndResourceIdAndResourceType(Integer rrdgroupid, Integer resourceid,
			String resourcetype, Integer rrdTransId);
	List<RRDTransaction> getRRDTransactionsByGroupId(String search, int groupId);
}
