package com.dj.repository;

import java.util.List;

import com.dj.entity.Assumption;

public interface AssumptionRepositoryCustom {
	List<Assumption> getAssumptions(String search);
	List<Assumption> getAssumptionsByEstId(int estId, String search);
}
