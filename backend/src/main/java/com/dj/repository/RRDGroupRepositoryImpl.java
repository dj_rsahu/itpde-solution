package com.dj.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.dj.entity.RRDGroup;
import com.dj.utils.StringFormatUtils;

public class RRDGroupRepositoryImpl implements RRDGroupRepositoryCustom {
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<RRDGroup> getAllRRDGroups(String search) {
		String query = "SELECT rg FROM RRDGroup rg WHERE rg.isdelete = 0 :searchCondition GROUP BY rg.groupname";
		String searchCondition = "";
		if (!StringFormatUtils.isEmpty(search)) {
			searchCondition = " AND rg.groupname LIKE '%" + search + "%' ";
		}
		query = query.replace(":searchCondition", searchCondition);
		return entityManager.createQuery(query).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RRDGroup> getAllRRDGroupRoles(String groupName) {
		String query = "SELECT rg FROM RRDGroup rg WHERE rg.isdelete = 0 AND rg.groupname = '"+groupName+"' GROUP BY rg.role";
		return entityManager.createQuery(query).getResultList();
	}
}
