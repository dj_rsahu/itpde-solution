package com.dj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dj.entity.TestScope;

public interface TestScopeRepository extends JpaRepository<TestScope, Integer> {

	@Query("select t from TestScope t where t.estid=:estid")
	public List<TestScope> fetchTestScopeDetails(@Param("estid") Integer estid);

	@Modifying
	@Query("delete from TestScope t where t.estid=:estimationId")
	public void deleteTestscopesByEstId(@Param("estimationId") int estimationId);
}
