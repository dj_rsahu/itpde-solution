package com.dj.repository;

import java.util.List;

import com.dj.entity.User;

public interface UserRepositoryCustom {
	List<User> getAllUsers(String search);
}
