package com.dj.repository;

import java.util.List;

import com.dj.entity.Highlevelesttimeline;

public interface HighlevelEstTimelineRepositoryCustom {
	
	List<Highlevelesttimeline> fetchHighlevelEstTimelines(int estid, byte isdelete, int actualDaysInAWeek);
}
