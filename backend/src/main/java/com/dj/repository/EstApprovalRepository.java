package com.dj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dj.entity.EstApprovals;

@Repository
public interface EstApprovalRepository extends JpaRepository<EstApprovals, Integer> {

	@Query("select e from EstApprovals e where e.estId=:estId and e.isdelete=:isdelete")
	public List<EstApprovals> fetchEstApprovalsByEstId(@Param("estId") Integer estid,@Param("isdelete") byte isdelete);

	@Query("select e from EstApprovals e where e.estId=:estId")
	public List<EstApprovals> fetchEstApprovalsByEstIdwithoutflag(@Param("estId") Integer estid);
	
	@Query("select e from EstApprovals e where e.estId=:estId and e.isdelete=0 and e.approvaltype=:overallApprover")
	public EstApprovals getOverallEstApprovalsByEstId(Integer estId, String overallApprover);
	
	@Query("select e from EstApprovals e where e.estId=:estId and e.isdelete=0 and e.approvaltype=:testApprover")
	public EstApprovals getTestEstApprovalsByEstId(Integer estId, String testApprover);
	
	
	@Query("SELECT count(e) from EstApprovals e where e.estId=:estId and e.isdelete=0")
	public int findEstApprovalById(@Param("estId") Integer estId);
	@Query("SELECT e from EstApprovals e where e.estId=:estId and e.approvaltype=:approver and isdelete=0")
	public EstApprovals fetchEstApprovalByRole(@Param("estId") Integer estId,@Param("approver") String approver);
}
