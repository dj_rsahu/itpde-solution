package com.dj.repository;

import java.util.List;

import com.dj.entity.RRDGroup;

public interface RRDGroupRepositoryCustom {
	List<RRDGroup> getAllRRDGroups(String search);
	List<RRDGroup> getAllRRDGroupRoles(String groupName);

}
