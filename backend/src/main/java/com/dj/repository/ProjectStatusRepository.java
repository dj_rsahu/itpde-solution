package com.dj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dj.entity.Projectstatus;

@Repository
public interface ProjectStatusRepository extends JpaRepository<Projectstatus, Integer> {

	@Query("select p from Projectstatus p where p.name=:name")
	public Projectstatus findProjectStatus(@Param("name") String name);

	@Query("select p from Projectstatus p where p.id=:projectPhaseId")
	public Projectstatus getProjectStatusById(@Param("projectPhaseId") Integer projectPhaseId);
}
