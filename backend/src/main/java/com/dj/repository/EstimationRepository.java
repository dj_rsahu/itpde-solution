package com.dj.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dj.entity.Estimation;


public interface EstimationRepository extends JpaRepository<Estimation, Long> {

	Estimation findById(int estimationId);
	void deleteById(int estimationId);
	
	@Query("SELECT e FROM Estimation e where e.createdby = :createdby AND e.isdelete=0 ORDER BY e.createdDate DESC")
	List<Estimation> getEstimationByUserId(@Param("createdby") int createdby);
	
	@Query("SELECT e FROM Estimation e where e.estmation_status IN (:statuses) AND e.isdelete=0 ORDER BY e.createdDate DESC")
	List<Estimation> getEstimationStatusByRole(@Param("statuses") List<String> statuses);
	
	@Modifying
	@Query("UPDATE Estimation e SET isdelete = 1 where e.id=:estimationId")
	void deleteEstimationById(@Param("estimationId") int estimationId);
	
	@Query("SELECT e FROM Estimation e where e.isdelete=0 ORDER BY e.createdDate DESC")
	List<Estimation> getEstimations();
	
	@Query("SELECT e FROM Estimation e where e.isdelete=:isdelete and e.iseditable = :isEditable")
	List<Estimation> getEstimationsByIsEditable(@Param("isEditable") byte isEditable, @Param("isdelete") byte isdelete);
	
	@Modifying
	@Query("UPDATE Estimation e SET e.iseditable = :iseditable,  e.iseditabledate = :iseditableDate, e.iseditableby = :isEditableBy where e.id=:estId")
	void updateIseditableFlagAndIseditableDate(@Param("estId") int estId, @Param("iseditable") byte iseditable, @Param("iseditableDate") Date iseditableDate, @Param("isEditableBy") Integer isEditableBy);
	
}
