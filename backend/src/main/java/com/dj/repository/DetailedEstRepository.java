package com.dj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dj.entity.DetailedEstimates;

@Repository 
public interface DetailedEstRepository extends JpaRepository<DetailedEstimates, Integer>{

	@Query("SELECT e FROM DetailedEstimates e where e.id = :id")
	public DetailedEstimates getDetailedEstimatesById(@Param("id") int detailedEstId);

	@Query(value="SELECT ht FROM DetailedEstimates ht where ht.id NOT IN(:detailedEstIds) AND ht.estid = :estId")
	public List<DetailedEstimates> getDetailedEstNotInIds(@Param("detailedEstIds") List<Integer> detailedEstIds, @Param("estId") int estId);

	@Modifying
	@Query(value="DELETE FROM DetailedEstimates de where de.id IN(:detailedEstIdsNotExistedInDB)")
	public void deleteDetailedEstByIds(@Param("detailedEstIdsNotExistedInDB") List<Integer> detailedEstIdsNotExistedInDB);

	@Query(value = "SELECT * FROM detailedest where estid= :estId", nativeQuery = true)
	public List<DetailedEstimates> getDetailedEstByEstId(@Param("estId") int estId);

	@Modifying
	@Query(value="DELETE FROM DetailedEstimates de where de.estid = :estimationId")
	public void deleteDetailedEstimationsByEstId(@Param("estimationId") int estimationId);

}
