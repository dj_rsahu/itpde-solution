package com.dj.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dj.entity.DetailedEstModType;

@Repository
public interface DetailedEstModTypeRepository extends JpaRepository<DetailedEstModType, Integer> {

	@Query(value = "select * from detestmoduletype where moduletype = :moduletype", nativeQuery = true)
	public DetailedEstModType getModuleTypeIdByName(@Param("moduletype") String moduletype);

	@Query(value = "select * from detestmoduletype", nativeQuery = true)
	public List<DetailedEstModType> getDetEstModuleTypes();

	@Query(value = "select * from detestmoduletype where id= :detestmoduletypeid", nativeQuery = true)
	public DetailedEstModType getModuleTypeById(@Param("detestmoduletypeid") Integer detestmoduletypeid);

}
