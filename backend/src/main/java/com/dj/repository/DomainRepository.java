package com.dj.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dj.entity.Domain;

@Repository
public interface DomainRepository extends CrudRepository<Domain, Integer> {

}
