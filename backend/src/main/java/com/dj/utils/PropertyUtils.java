package com.dj.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyUtils {
	private static final Logger logger = LoggerFactory.getLogger(PropertyUtils.class);
	public String getPropertyValues(String filePath,String propertyKey) {
		Properties prop = new Properties();
		try (InputStream input = new FileInputStream(filePath)) {
			prop.load(input);
			logger.info(prop.getProperty(propertyKey));
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return prop.getProperty(propertyKey);
	}

	public static void main(String[] args) {
		String path = "src"+File.separator+"main"+File.separator+"resources"+File.separator+"application.properties";
		logger.info(path);
		PropertyUtils pu = new PropertyUtils();
		pu.getPropertyValues(path, "email");
		pu.getPropertyValues(path, "password");
	}
}
