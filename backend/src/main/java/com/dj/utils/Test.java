package com.dj.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Test {

	public static void main(String[] args) throws IOException {
		Properties props = new Properties();
		InputStream inputStream = new FileInputStream(new File("src/main/resources/application.properties"));
		props.load(inputStream);
		
		System.out.println(props.get("tomcatRoot"));
		}

}
