package com.dj.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class StringFormatUtils {
	
	/**
	 * method check where the string is empty or not
	 * 
	 * @param string
	 * @return boolean
	 */

	public static boolean isEmpty(String string) {
		if (string != null && string.trim().length() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public static String toCSV(List<?> list) {
		if (list != null) {
			String csv = "";
			for (Object obj : list) {
				if (csv.length() > 0) {
					csv += ", ";
				}

				csv += obj.toString();
			}

			return csv;
		} else {
			return null;
		}
	}

	public static String toCSV(List<?> list, String property) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		if (list != null) {
			String csv = "";
			for (Object obj : list) {
				if (csv.length() > 0) {
					csv += ", ";
				}

				csv += Reflection.getProperty(obj, property);
			}

			return csv;
		} else {
			return null;
		}
	}

}
