package com.dj.utils;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dj.entity.User;

public class EmailUtils {
	private static final Logger logger = LoggerFactory.getLogger(PropertyUtils.class);

	final String username = DJConstants.FROM_EMAIL;
	final String password = DJConstants.FROM_EMAIL_PASSWORD;

	public Properties getProperties() {
		Properties props = new Properties();

		/*
		 * props.put("mail.smtp.auth", "true"); props.put("mail.smtp.starttls.enable",
		 * "true"); props.put("mail.smtp.host", "smtp.office365.com");
		 * props.put("mail.smtp.port", "587");
		 */

		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.starttls.enable", "false");
		props.put("mail.smtp.host", "relay.davidjones.com.au");
		props.put("mail.smtp.port", "25");

		return props;
	}

	/**
	 * @param email
	 * @param link
	 * @param validate
	 */
	public void sendEmail(String email, String link, String validate) {

		String toEmail = email;
		// Get the Session object.
		Session session = Session.getInstance(getProperties(), new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);
			logger.info("User Email {}", username);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(username));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));

			// Set Subject: header field
			message.setSubject("ITPDE Connect Email Service");

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message
			if (validate.equals("signup")) {
				messageBodyPart.setContent("<font size='3' color='red'><b>BES Connect Sign Up services</b></font> <br>"
						+ "Thanks for sining up with the bes services" + "<br>" + "<a href=" + link
						+ "><button>Login</button></a>" + " <br> ", "text/html");
			} else if (validate.equals("forgot")) {
				messageBodyPart.setContent("<font size='3' color='red'><b>BES Connect FORGOT PASSWORD</b></font> <br>"
						+ "We Heard you need a password reset, click on the below link you will be directed to secure site and also please find the Link mentioned below .If not please kindly ignore this email"
						+ "<br>" + "<a href=" + link + "><button>FORGOT PASSWORD</button></a>" + " <br> ", "text/html");
			} else {
				logger.warn("Not valid");
			}

			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			// Send message
			Transport.send(message);

			logger.info("Sent email successfully....");

		} catch (MessagingException e) {
			logger.info("Email not sent: " + e);
		}
	}

	/**
	 * Sending email when estimation status is in Pending Approval
	 * 
	 * @param msg
	 * @param toEmail
	 * @param ccEmail
	 * @param projectName
	 * @param approverType
	 */
	public void sendEstimationSummaryEmail(String msg, String toEmail, String ccEmail, String projectName,
			String approverType) {
		// Get the Session object.
		Session session = Session.getInstance(getProperties(), new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			if (ccEmail != null && !StringFormatUtils.isEmpty(ccEmail)) {
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail));
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(DJConstants.FROM_EMAIL));

			}
			message.setSubject("ITPD Estimates - Pending Approval | " + approverType + " | " + projectName + "");
			BodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart.setContent(msg, "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
			logger.info("Sent email successfully....");
		} catch (MessagingException e) {
			logger.info("Email not sent: " + e);
		}
	}

	/**
	 * Sending email when estimation status is in Approved
	 * 
	 * @param msg
	 * @param toEmail
	 * @param ccEmail
	 * @param projectName
	 * @param approverType
	 */
	public void sendEstimationSummaryEmailTAApproved(String msg, String toEmail, String ccEmail, String createdUserMail,
			String projectName, String approverType) {
		// Get the Session object.
		Session session = Session.getInstance(getProperties(), new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			if (ccEmail != null && !StringFormatUtils.isEmpty(ccEmail)) {
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail));
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(createdUserMail));
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(DJConstants.FROM_EMAIL));

			}
			message.setSubject("ITPD Estimates - Pending Approval| " + approverType + " | " + projectName + "");
			BodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart.setContent(msg, "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
			logger.info("Sent email successfully....");
		} catch (MessagingException e) {
			logger.info("Email not sent: " + e);
		}
	}

	public void sendEstimationSummaryEmailOAApproved(String msg, String toEmail, String ccEmail, String ccEmail2,
			String projectName, String approverType) {
		// Get the Session object.
		Session session = Session.getInstance(getProperties(), new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			if (ccEmail != null && !StringFormatUtils.isEmpty(ccEmail)) {
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail));
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail2));
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(DJConstants.FROM_EMAIL));

			}
			message.setSubject("ITPD Estimates - Approved | " + approverType + " | " + projectName + "");
			BodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart.setContent(msg, "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
			logger.info("Sent email successfully....");
		} catch (MessagingException e) {
			logger.info("Email not sent: " + e);
		}
	}

	/**
	 * Sending email when estimation status is in Rework
	 * 
	 * @param msg
	 * @param toEmail
	 * @param ccEmail
	 * @param projectName
	 * @param approverType
	 */
	public void sendEstimationSummaryEmailRework(String msg, String toEmail, String ccEmail, String projectName,
			String approverType) {
		// Get the Session object.
		Session session = Session.getInstance(getProperties(), new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			if (ccEmail != null && !StringFormatUtils.isEmpty(ccEmail)) {
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail));
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(DJConstants.FROM_EMAIL));

			}
			message.setSubject("ITPD Estimates - Rework | " + approverType + " | " + projectName + "");
			BodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart.setContent(msg, "text/html");

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
			logger.info("Sent email successfully....");
		} catch (MessagingException e) {
			logger.info("Email not sent: " + e);
		}
	}

}