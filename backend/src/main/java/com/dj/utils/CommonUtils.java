package com.dj.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.dj.entity.User;
import com.dj.repository.UserRepository;
import com.dj.service.BeanUtilsService;

public class CommonUtils {

	
	/**
	 * Gets the logged in user details.
	 * 
	 * @return
	 */
	public static User getLoggedInUserDetails() {
		User user = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = "";
		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		UserRepository userRepsitory = BeanUtilsService.getBean(UserRepository.class);
		if (username != null && !username.isEmpty()) {
			user = userRepsitory.findByEmail(username);
		}
		return user;
	}
}
