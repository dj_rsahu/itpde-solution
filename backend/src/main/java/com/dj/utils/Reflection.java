package com.dj.utils;


import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
public class Reflection {

	public static void setProperty(Object object, String property, Object value) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		PropertyUtils.setProperty(object, property, value);
	}

	public static void setPropertyIfExists(Object object, String property, Object value) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		try {
			PropertyUtils.setProperty(object, property, value);
		} catch (Exception e) {
		}
	}

	public static Object getProperty(Object object, String property) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return PropertyUtils.getProperty(object, property);
	}

	public static Class<?> getPropertyType(Object object, String property) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return PropertyUtils.getPropertyType(object, property);
	}

}
