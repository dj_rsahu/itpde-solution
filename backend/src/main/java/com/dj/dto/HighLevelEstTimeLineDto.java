package com.dj.dto;

import java.io.Serializable;
import java.util.List;

import com.dj.entity.Highlevelesttimeline;

public class HighLevelEstTimeLineDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Highlevelesttimeline> highlevelesttimelines;

	public List<Highlevelesttimeline> getHighlevelesttimelines() {
		return highlevelesttimelines;
	}

	public void setHighlevelesttimelines(List<Highlevelesttimeline> highlevelesttimelines) {
		this.highlevelesttimelines = highlevelesttimelines;
	}
	
	
}
