package com.dj.dto;

import java.io.Serializable;
import java.util.List;

import com.dj.entity.TestScope;

public class TestScopeDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<TestScope> testScopeListObj;
	public List<TestScope> getTestScopeListObj() {
		return testScopeListObj;
	}
	public void setTestScopeListObj(List<TestScope> testScopeListObj) {
		this.testScopeListObj = testScopeListObj;
	}
	
	
 
}
