package com.dj.dto;

import java.io.Serializable;
import java.util.List;

import com.dj.entity.DetailedEstimates;

public class DetailedEstDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<DetailedEstimates> detailedEstList;

	public List<DetailedEstimates> getDetailedEstList() {
		return detailedEstList;
	}

	public void setDetailedEstList(List<DetailedEstimates> detailedEstList) {
		this.detailedEstList = detailedEstList;
	}

}
