package com.dj.dto;

import java.io.Serializable;
import java.util.List;

import com.dj.entity.EstApprovals;
import com.dj.entity.Highlevelesttimeline;

public class EstimateSummeryDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Highlevelesttimeline> highlevelEstTimelineList;
	
	private List<EstApprovals> estApprovalList;
	private String isSubmit;
	private String estCreatedName;
	private String estSummaryContent;

	public List<Highlevelesttimeline> getHighlevelEstTimelineList() {
		return highlevelEstTimelineList;
	}

	public void setHighlevelEstTimelineList(List<Highlevelesttimeline> highlevelEstTimelineList) {
		this.highlevelEstTimelineList = highlevelEstTimelineList;
	}


	public List<EstApprovals> getEstApprovalList() {
		return estApprovalList;
	}

	public void setEstApprovalList(List<EstApprovals> estApprovalList) {
		this.estApprovalList = estApprovalList;
	}

	public String getIsSubmit() {
		return isSubmit;
	}

	public void setIsSubmit(String isSubmit) {
		this.isSubmit = isSubmit;
	}

	public String getEstCreatedName() {
		return estCreatedName;
	}

	public void setEstCreatedName(String estCreatedName) {
		this.estCreatedName = estCreatedName;
	}

	public String getEstSummaryContent() {
		return estSummaryContent;
	}

	public void setEstSummaryContent(String estSummaryContent) {
		this.estSummaryContent = estSummaryContent;
	}

}
