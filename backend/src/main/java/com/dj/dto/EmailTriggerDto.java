package com.dj.dto;

import java.io.Serializable;

public class EmailTriggerDto implements Serializable{


	private static final long serialVersionUID = 1L;
	
	private Integer estId;
	private Integer approverId;
	private String statusType;
	private String approverType;
	private String emailContent;
	private String hyperLink;
	
	public Integer getEstId() {
		return estId;
	}
	public void setEstId(Integer estId) {
		this.estId = estId;
	}
	
	public Integer getApproverId() {
		return approverId;
	}
	public void setApproverId(Integer approverId) {
		this.approverId = approverId;
	}
	public String getStatusType() {
		return statusType;
	}
	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}
	
	public String getApproverType() {
		return approverType;
	}
	public void setApproverType(String approverType) {
		this.approverType = approverType;
	}
	public String getEmailContent() {
		return emailContent;
	}
	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}
	public String getHyperLink() {
		return hyperLink;
	}
	public void setHyperLink(String hyperLink) {
		this.hyperLink = hyperLink;
	}

}
