package com.dj.dto;

import java.io.Serializable;
import java.util.List;

import com.dj.entity.Assumption;

public class AssumptionDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Assumption> assumptions;

	public List<Assumption> getAssumptions() {
		return assumptions;
	}

	public void setAssumptions(List<Assumption> assumptions) {
		this.assumptions = assumptions;
	}
	
}
