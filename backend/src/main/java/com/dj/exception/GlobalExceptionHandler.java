package com.dj.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * 
 * @author rameshbandari 
 * Purpose of this class is to handle centralized
 * exceptions which occured at any controller class level and also to
 * handle any customized exceptions
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleAnyException(Exception ex, HttpServletRequest request) {
		String errorMsgDescription = ex.getLocalizedMessage();
		if (errorMsgDescription == null)
			errorMsgDescription = ex.toString();
		ErrorResponse response = new ErrorResponse(ex.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(NullPointerException.class)
	public ResponseEntity<Object> handleNullPointerException(HttpServletResponse res, NullPointerException ex) {
		String errorMsgDescription = ex.getLocalizedMessage();
		if (errorMsgDescription == null)
			errorMsgDescription = ex.toString();
		ErrorResponse response = new ErrorResponse("Null pointer exception", HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ErrorResponse.class)
	//@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public ResponseEntity<Object> handleRecordNotFoundException(HttpServletResponse res, ErrorResponse ex)
			throws IOException {
		res.sendError(ex.getHttpStatus().value(), ex.getMessage());
		/*
		 * String errorMsgDescription = ex.getLocalizedMessage(); if(errorMsgDescription
		 * == null) errorMsgDescription = ex.toString(); ErrorResponse response = new
		 * ErrorResponse(errorMsgDescription, HttpStatus.NOT_FOUND);
		 */
		return new ResponseEntity<>(res, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(FetchApiResponseFailureException.class)
	  public ErrorResponse handleNotFoundException(FetchApiResponseFailureException ex, WebRequest request) {
		ErrorResponse exceptionResponse = new ErrorResponse(ex.getMessage(),"1004"
	        );
	    return exceptionResponse;
	  }
	/*
	 * @ExceptionHandler(ErrorResponse.class) public void
	 * handleCustomException(HttpServletResponse res, ErrorResponse ex) throws
	 * IOException { res.sendError(ex.getHttpStatus().value(), ex.getMessage()); }
	 */

	/*
	 * @ExceptionHandler(AccessDeniedException.class) public void
	 * handleAccessDeniedException(HttpServletResponse res) throws IOException {
	 * res.sendError(HttpStatus.FORBIDDEN.value(), "Access denied"); }
	 */

}
