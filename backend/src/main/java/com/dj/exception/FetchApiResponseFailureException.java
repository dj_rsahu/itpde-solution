package com.dj.exception;

public class FetchApiResponseFailureException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FetchApiResponseFailureException(String message) {
		super(message);
	}

}
