package com.dj.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
public class SecurityConfiguration  extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	AuthenticationEntry jwtAuthenticationEntry;
	
	@Autowired
    private RequestFilter jwtRequestFilter;
	
	

	@Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

	@Bean
	public AuthenticationProvider  configure() throws Exception {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(userDetailsService);
		provider.setPasswordEncoder(getPasswordEncoder());
		return provider;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
	
		http
		.authorizeRequests().antMatchers("/apple-app-site-association").permitAll().and()
		.authorizeRequests().antMatchers("/dj/getToken").permitAll().and()
		.authorizeRequests().antMatchers("/dj/login").permitAll().and()
		.authorizeRequests().antMatchers("/dj/metrics/*").permitAll().and()
		.authorizeRequests().antMatchers("/dj/webLogin").permitAll().and()
		.authorizeRequests().antMatchers("/dj/saveUser").permitAll().and()
		.authorizeRequests().antMatchers("/dj/getUsers").permitAll().and()
		.authorizeRequests().antMatchers("/dj/forgotPassword").permitAll().and()
		.authorizeRequests().antMatchers("/dj/activeUser").permitAll().and()
		.authorizeRequests().antMatchers("/dj/uploadImage").permitAll().and()
		.authorizeRequests().antMatchers("/dj/sendEmail").permitAll().and()
		.authorizeRequests().antMatchers("/dj/mail").permitAll().and()
		.authorizeRequests().antMatchers("/dj/updateUserByEmail").permitAll().and()
		.authorizeRequests().anyRequest().authenticated().and()/*.httpBasic().and()*/
		.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntry).and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
		.and().csrf().disable().cors();
		
		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

	}

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		//config.setAllowCredentials(true); // you USUALLY want this
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("PATCH");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
	 @Override
	  public void configure(WebSecurity web) throws Exception {
	    // Allow swagger to be accessed without authentication
	    web.ignoring()
	    	.antMatchers("/*")
	    	.antMatchers("/v2/api-docs")//
	        .antMatchers("/swagger-resources/**")//
	        .antMatchers("/swagger-ui.html")//
	        .antMatchers("/configuration/**")//
	        .antMatchers("/webjars/**")//
	        .antMatchers("/public")
	        .antMatchers("/assets/*")
			.antMatchers("/assets/img/*")
			.antMatchers("/assets/img/tool_icons/*")
			.antMatchers("/assets/img/charts/*")
			.antMatchers("/assets/img/chart_types/*")
			.antMatchers("/assets/fonts/*")
			.antMatchers("/assets/json/*")
			.antMatchers("/user/login")
			.antMatchers("/user/forgotPassword")
			.antMatchers("/user/resetPassword")
			.antMatchers("/user/verifyToken")
			.antMatchers("/notification/sendEmailNotification");;
	 
	  }

	private PasswordEncoder getPasswordEncoder() {
		return new PasswordEncoder() {
			@Override
			public String encode(CharSequence charSequence) {
				return charSequence.toString();
			}

			@Override
			public boolean matches(CharSequence charSequence, String s) {
				if(charSequence.equals(s)) {
					return true;
				}
				return false;
			}
		};
	}

}